/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Type_Repository.hxx>

#include <iostream>

int main()
{

  // printf("%s\n",INTIFADA_TOP_SRCDIR);
  std::string asterix_description=INTIFADA_TOP_SRCDIR;
  asterix_description +="/asterix.xml";
  intifada::Configuration_XML_Reader p(asterix_description);
  p.open();


  intifada::Record_Repository* rep=intifada::Record_Repository::instance();
  p.parse(rep);

  intifada::Type_Repository *types=intifada::Type_Repository::instance();

  types->foreach();



}
