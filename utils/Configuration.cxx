/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Configuration.hxx>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>

#include <intifada/Path_Name.hxx>
#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <intifada/Condition.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Configuration_Internal_Reader.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Logger.hxx>

static Options options;

Options::Options():asterixDescription_(),ot_(NONE){
  asterixDescription_=INTIFADA_TOP_SRCDIR;
  asterixDescription_ +="/asterix.xml";
  makefile_am_=INTIFADA_TOP_SRCDIR;
  makefile_am_+="gintifada/Makefile.gintifada";
}
void Options::set_asterix_description(const char* f){asterixDescription_=f;}
std::string Options::get_asterix_description()const{return asterixDescription_;}

void Options::set_output(const Generation& g){ot_=g;}

bool Options::is_mode(const Generation& g)const{return (g & ot_) == g;}

void help()
  {
    std::cout << "usage: -h -f <asterix description file> -x -c -t -n <class name> -l <where to generate> -m\n";
    std::cout << "-x : generate a xml description principaly for testing purpose" << std::endl;
    std::cout << "-t : generate experimental text description" << std::endl;
  }
int handle_options(int argc, char**argv)
  {
    int opt;
    while ((opt = getopt(argc, argv, "hf:xt")) != -1) {
      switch (opt) {
      case 'h':
        help();
        exit(EXIT_SUCCESS);
        break;
      case 'f':
        options.set_asterix_description(optarg);
        break;
      case 'x':
        options.set_output(Options::XML);
        break;
      case 't':
        options.set_output(Options::TXT);
        break;
      default:
        help();
        exit(EXIT_FAILURE);
      }
    }
    return 0;
  }


Dump_Configuration::Dump_Configuration():families_(),tabs_(){}

int Dump_Configuration::insert_ssv(ssv_t& l,const std::string& f,const std::string& id)
  {
    ssv_iterator_t it = l.find(f);
    sv_t *v;

    if(it==l.end()){
      ssv_pair_t p=l.insert(std::make_pair(f,sv_t()));
      ssv_iterator_t sit=p.first;
      v=&((*p.first).second);

    }else{
      v=&((*it).second);
    }
    v->push_back(id);
    return 0;
  }

XML_Generator::XML_Generator():
  uaps_(),
  current_field_(),
  extended_repetitive_(false)
        {}

int XML_Generator::begin_uap(const intifada::Condition*c)
  {
    ++tabs_;
    uaps_ << tabs_() << "<uap";
    if(c!=NULL){
      uaps_
      << " if=\"" << c->get_lvalue()
      << "\" value=\"" << c->get_rvalue() << "\"";
    }
    uaps_ << ">" <<std::endl;
    return 0;
  }

int XML_Generator::end_uap()
  {
    uaps_ << tabs_() << "</uap>" << std::endl;
    --tabs_;
    return 0;
  }

int XML_Generator::begin_item(const std::string& longname,const intifada::Path_Name& reference,const Properties& p)
  {
    Property_Bool force;
    p.find_property("force_presence",force);
    std::string f="";
    if(force==true){
      f=" forcepresence=\"true\"";
    }

    ++tabs_;
    uaps_ << tabs_() << "<item name=\"" << longname << "\" reference=\"" << reference << "\""<< f <<">" << std::endl;
    return 0;
  }
int XML_Generator::end_item()
  {
    uaps_ << tabs_() << "</item>" << std::endl;
    --tabs_;
    return 0;
  }
int XML_Generator::frn(int f,const intifada::Path_Name& s)
  {
    ++tabs_;
    uaps_
    << tabs_()
    << "<frn num=\"" << f << "\" reference=\"" << s << "\"></frn>"
    << std::endl;
    --tabs_;
    return 0;
  }

int XML_Generator::family_name_id(const std::string& f,const std::string& id)
  {

    insert_ssv(families_,f,id);
    return 0;
  }

int XML_Generator::begin_record_identity(const std::string& desc,uint8_t c, const std::string& id)
  {
    ++tabs_;
    uaps_
    << tabs_() << "<record name=\"" << desc << "\" cat=\""
    << static_cast<uint16_t>(c) << "\" id=\"" << id << "\">" << std::endl;
    return 0;
  }
int XML_Generator::end_record_identity()
  {
    uaps_ << tabs_() << "</record>" << std::endl;
    --tabs_;
    return 0;
  }

int XML_Generator::begin_fixed_field(const Properties& f)
  {
    extended_repetitive_=false;
    push_field("fixed");
    ++tabs_;
    uaps_ << tabs_() << "<fixed>" << std::endl;
    XML_Properties props(tabs_,uaps_);
    f.for_each_property(props);
    return 0;
  }

int XML_Generator::begin_extended_field(const Properties& f)
  {
    extended_repetitive_=false;
    push_field("extended");
    ++tabs_;
    uaps_ << tabs_() << "<extended>" << std::endl;
    // XML_Extended_Properties props(tabs_,uaps_);

    XML_Properties props(tabs_,uaps_);
    if(f.get_property("repetitive")=="true"){
      extended_repetitive_=true;
      props.hide("secondaries_size");
    }
    f.for_each_property(props);
    return 0;
  }

int XML_Generator::begin_repetitive_field(const Properties& f)
  {
    extended_repetitive_=false;
    push_field("repetitive");
    ++tabs_;
    uaps_ << tabs_() << "<repetitive>" << std::endl;
    XML_Properties props(tabs_,uaps_);
    f.for_each_property(props);
    return 0;
  }

int XML_Generator::begin_compound_field(uint8_t size)
  {
    extended_repetitive_=false;
    push_field("compound");
    ++tabs_;
    uaps_ << tabs_() << "<compound length=\"" << (int)size << "\">" << std::endl;
    return 0;
  }

int XML_Generator::end_field()
  {
    uaps_ << tabs_() << "</" << pop_field() << ">" << std::endl;
    --tabs_;
    return 0;
  }

int XML_Generator::part(const intifada::Type& type,const intifada::Path_Name& name,uint8_t start,uint8_t size,uint8_t p)
  {
    std::ostringstream shift;
    shift << static_cast<uint16_t>(start);
    std::ostringstream sz;
    sz << static_cast<uint16_t>(size);
    std::ostringstream part;
    part << static_cast<uint16_t>(p);
    ++tabs_;
    uaps_ << tabs_() << "<part type=\"" << type.get_name() << "\""
    << " name=\"" << name.get_full_name() << "\""
    << " start=\"" << shift.str() << "\""
    << " size=\"" << sz.str() << "\"";
    if( p !=intifada::Data_Field_Characteristics_List::npos)
      {
        uaps_ << " byte=\"" << part.str() << "\"";
      }
    uaps_ << ">" << std::endl;
    {
      XML_Properties props(tabs_,uaps_);
      type.for_each_property(props);
    }
    uaps_ << tabs_() << "</part>"<<std::endl;
    --tabs_;
    return 0;
  }

int XML_Generator::dump()
  {
    std::ostream &os=std::cout;

    os << "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" << std::endl << std::endl;
    os << "<!DOCTYPE intifada SYSTEM \"asterix.dtd\">" << std::endl;
    os << "<!-- @version: -->" << std::endl;

    os << "<intifada>" << std::endl;
    ++tabs_;
    os << tabs_() << "<families>" << std::endl;
    for(ssv_iterator_t it=families_.begin();it!=families_.end();++it){
      const std::string& family=(*it).first;
      sv_t &ids=(*it).second;

      ++tabs_;
      os << tabs_() << "<family name=\"" << family << "\">" << std::endl;

      ++tabs_;
      for(sv_iterator_t j=ids.begin();j!=ids.end();++j){
        std::string& id=(*j);
        os << tabs_() <<"<id name=\"" << id << "\"></id>" << std::endl;
      }
      --tabs_;
      os << tabs_() << "</family>" << std::endl;
    }
    --tabs_;
    os << tabs_() << "</families>" << std::endl;
    os << uaps_.str();
    os << "</intifada>" << std::endl;
    return 0;
  }


void XML_Generator::push_field(const std::string& f)
  {
    current_field_.push(f);
  }
std::string XML_Generator::pop_field()
  {
    std::string f=current_field_.top();
    current_field_.pop();
    return f;
  }

    Text_Generator::Text_Generator()
      {}

        int Text_Generator::begin_uap(const intifada::Condition*c)
          {
            std::ostringstream &os=*records_.back();
            if(c!=NULL){
              os << "create_uap_id="<<c->get_lvalue()<<std::endl;
              os << "create_uap_frn="<<c->get_rvalue()<<std::endl;
            }else{
              os << "create_uap_id="<<std::endl;
              os << "create_uap_frn="<<std::endl;
            }
            return 0;
          }

        int Text_Generator::end_uap()
          {
            return 0;
          }

        int Text_Generator::begin_item(const std::string& longname,const intifada::Path_Name& reference,const Properties& p)
          {
            Property_Bool force;
            p.find_property("force_presence",force);
            std::string f="false";
            if(force==true){
              f="true";
            }
            std::ostringstream &os=*records_.back();
            os << "create_item_longname="<<longname<<std::endl;
            os << "create_item_reference="<<reference<<std::endl;
            os << "create_item_force="<<f<<std::endl;
            return 0;
          }
        int Text_Generator::end_item()
          {
            return 0;
          }
        int Text_Generator::frn(int f,const intifada::Path_Name& s)
          {
            std::ostringstream &os=*records_.back();
            os << "create_frn=" << f << std::endl;
            os << "create_frn_id="<<s<<std::endl;
            return 0;
          }

        int Text_Generator::family_name_id(const std::string& f,const std::string& id)
          {

            insert_ssv(families_,f,id);
            return 0;
          }

        int Text_Generator::begin_record_identity(const std::string& desc,uint8_t c, const std::string& id)
          {
            records_.push_back(new std::ostringstream);
            std::ostringstream &os=*records_.back();

            os << "create_record_desc=" << desc << std::endl;
            os << "create_record_id=" << id << std::endl;
            os << "create_record_cat=" << static_cast<uint16_t>(c) << std::endl;
            os << "push_record="<<std::endl;
            return 0;
          }
        int Text_Generator::end_record_identity()
          {
            std::ostringstream &os=*records_.back();
            os << "pop_record="<< std::endl;
            return 0;
          }

        int Text_Generator::begin_fixed_field(const Properties& f)
          {
            std::ostringstream &os=*records_.back();
            os << "create_fixed=" << std::endl;
            Text_Properties props(os,"set_item_property");
            f.for_each_property(props);
            return 0;
          }

        int Text_Generator::begin_extended_field(const Properties& f)
          {
            std::ostringstream &os=*records_.back();
            os << "create_extended=" << std::endl;
            Text_Properties props(os,"set_item_property");
            if(f.get_property("repetitive")=="true"){
              props.hide("secondaries_size");
            }
            f.for_each_property(props);
            return 0;
          }

        int Text_Generator::begin_repetitive_field(const Properties& f)
          {
            std::ostringstream &os=*records_.back();
            os << "create_repetitive="<<std::endl;
            Text_Properties props(os,"set_item_property");
            f.for_each_property(props);
            return 0;
          }

        int Text_Generator::begin_compound_field(uint8_t size)
          {
            std::ostringstream &os=*records_.back();
            os << "create_compound_size="<<static_cast<uint16_t>(size)<<std::endl;
            os << "push_record="<<std::endl;
            return 0;
          }

        void Text_Generator::end_compound_field()
          {
            std::ostringstream &os=*records_.back();
            os << "pop_record="<< std::endl;
            os << "end_compound=" << std::endl;
            return;
          }

        int Text_Generator::part(const intifada::Type& type,const intifada::Path_Name& name,uint8_t start,uint8_t size,uint8_t part)
          {
            std::ostringstream &os=*records_.back();
            os << "handle_parts_full_name="<<name.get_full_name()<<std::endl;
            os << "handle_parts_type="<<type.get_name()<<std::endl;
            if(part!=intifada::Data_Field_Characteristics_List::npos)
              {
                os << "handle_parts_byte="<<static_cast<int16_t>(part) <<std::endl;
              }
            else
              {
                os << "handle_parts_byte:-1" <<std::endl;
              }
            os << "handle_parts_start="<< static_cast<uint16_t>(start) <<std::endl;
            os << "handle_parts_size="<< static_cast<uint16_t>(size) <<std::endl;

            Text_Properties props(os,"set_type_property");
            type.for_each_property(props);
            return 0;
          }
        int Text_Generator::dump()
          {
            for(ssv_iterator_t it=families_.begin();it!=families_.end();++it){
              const std::string& family=(*it).first;
              ccv_ << "create_family="<< family << std::endl;
              sv_t &ids=(*it).second;
              for(sv_iterator_t j=ids.begin();j!=ids.end();++j){
                std::string& id=(*j);
                ccv_ << "create_family_id="<< id << std::endl;
              }
            }
            std::cout << ccv_.str();
            // create categories files
            for(ostringstream_tab_iterator_type it=records_.begin();it!=records_.end();++it){
              std::ostringstream &os=*(*it);
              std::ostringstream cfs;
              cfs << os.str() << std::endl;
              std::cout << cfs.str() << std::endl;
            }

            std::ostringstream hfs;
            hfs << hcv_.str();
            std::ostringstream cfs;

            return 0;
          }

    int main(int argc, char **argv)
      {
        clog.insert_mask("Condition");

        handle_options(argc,argv);

        intifada::Configuration_XML_Reader p(options.get_asterix_description());
        //intifada::Configuration_Internal_Reader p("/home/spion/compils/intifada/test.kv");
        p.open();


        intifada::Record_Repository* rep=intifada::Record_Repository::instance();
        p.parse(rep);

        Dump_Configuration *sf=NULL;
        if(options.is_mode(Options::XML)==true){
          sf=new XML_Generator;
        }else if(options.is_mode(Options::TXT)==true){
          sf=new Text_Generator();
        }
        if(sf!=NULL){
          rep->foreach(sf);
          sf->dump();
        }else{
          std::cerr << "No defined mode" << std::endl;
        }
      }
