/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_CONFIGURATION_HXX
# define UTILS_CONFIGURATION_HXX

#include <intifada/Logger.hxx>

#include <string>
#include <vector>
#include <intifada/Map.hxx>
#include <stack>
#include <sstream>

#include <intifada/Structure_Functor.hxx>

class Options
  {
public:
  typedef enum{
    NONE=0x00,
    XML=0x01,
    TXT=0x02
  }Generation;
public:
  Options();
public:
  void set_asterix_description(const char* f);
  std::string get_asterix_description()const;

  void set_output(const Generation& g);

  bool is_mode(const Generation& g)const;
private:
  std::string asterixDescription_;

  std::string makefile_am_;

  Generation ot_;
  };

void help();

int handle_options(int argc, char**argv);

class Tabs
  {
public:
    Tabs():tabs_(0){}
    std::string operator()()
  {
    std::string tab;
    for(int i=0;i<tabs_;++i){
      tab+="    ";
    }
    return tab;
  }

    void operator()(std::ostream& os)
  {
    std::string tab="";
    for(int i=0;i<tabs_;++i){
      os << "    ";
    }
  }

    Tabs& operator++()
      {
        ++tabs_;
        return *this;
      }
    Tabs& operator--()
  {
    if(tabs_>0){
          --tabs_;
        }
    return *this;
  }
    void reset()
      {
        tabs_=0;
      }
private:
  int tabs_;
  };

class Dump_Configuration : public intifada::Structure_Functor
  {
  protected:
    typedef std::vector<std::string> sv_t;
    typedef sv_t::iterator sv_iterator_t;
    typedef intifada::Map<std::string,sv_t> ssv_t;
    typedef ssv_t::iterator ssv_iterator_t;
    typedef ssv_t::pair ssv_pair_t;

  public:
    Dump_Configuration();

    virtual int dump()=0;

  protected:
    int insert_ssv(ssv_t& l,const std::string& f,const std::string& id);
    void tabs(std::ostream& os);

  protected:
    ssv_t families_;
    Tabs tabs_;
  };

class XML_Properties : public Properties::Property_Functor
  {
  private:
    typedef std::set<std::string> string_set_type;
    typedef string_set_type::iterator string_set_iterator_type;
  public:
    XML_Properties(Tabs& t,std::ostringstream& os,bool forcedisplay=true):
      tabs_(t),os_(os),forcedisplay_(forcedisplay),hide_(),begin_node_(false)
      {}
    virtual ~XML_Properties()
      {
        if(begin_node_==true){
        --tabs_;
        os_<< tabs_() << "</properties>" << std::endl;
        --tabs_;
        }
      }
    void hide(const std::string& h)
      {
        hide_.insert(h);
      }
    virtual void operator()(const std::string& ps, const Property_Type& prop)
  {
    if(prop.default_value()==false || forcedisplay_==true){
      string_set_iterator_type it=hide_.find(ps);
      bool hide=false;
      if(it!=hide_.end()){
        hide=true;
      }
      if(hide!=true){
        begin_node();
        os_ << tabs_() << "<property>" << ps << "=" << prop.string_value() << "</property>" << std::endl;
      }
    }
  }
  private:
    void begin_node()
      {
        if(begin_node_==false){
          begin_node_=true;
        ++tabs_;
        os_<< tabs_() << "<properties>" << std::endl;
        ++tabs_;
        }
      }
  protected:
    Tabs& tabs_;
    std::ostringstream& os_;
    bool forcedisplay_;
    string_set_type hide_;
    bool begin_node_;
  };


class Text_Properties : public Properties::Property_Functor
  {
  private:
    typedef std::set<std::string> string_set_type;
    typedef string_set_type::iterator string_set_iterator_type;
  public:
    Text_Properties(std::ostringstream& os,const std::string& method,bool forcedisplay=true):
      os_(os),method_(method),forcedisplay_(forcedisplay),hide_()
      {}
    virtual ~Text_Properties()
      {}
    void hide(const std::string& h)
      {
        hide_.insert(h);
      }
    virtual void operator()(const std::string& ps, const Property_Type& prop)
  {
    if(prop.default_value()==false || forcedisplay_==true){
      string_set_iterator_type it=hide_.find(ps);
      bool hide=false;
      if(it!=hide_.end()){
        hide=true;
      }
      if(hide!=true){
        os_ << "property_name:"<<ps << std::endl;
        os_ << "property_value:"<<prop.string_value()<<std::endl;
        //os_ << tabs_() << "property:" << ps << "=" << prop.string_value() << std::endl;
      }
    }
  }
  private:
    std::ostringstream& os_;
    std::string method_;
    bool forcedisplay_;
    string_set_type hide_;
  };

/// XML
/**
 * display if ps!="secondaries" or display_secondaries_size!=true
 */
class XML_Extended_Properties : public XML_Properties
  {
public:
  XML_Extended_Properties(Tabs& t,std::ostringstream& os,bool forcedisplay=true):XML_Properties(t,os,forcedisplay){}

  virtual void operator()(const std::string& ps, const Property_Type& prop)
  {
    bool display_secondaries_size=true;
    if(ps=="repetitive" && prop.string_value()=="true"){
      display_secondaries_size=false;
    }
    if(prop.default_value()==false || forcedisplay_==true){
      if(ps!="secondaries" || display_secondaries_size!=true){
        os_ << tabs_() << "<property>" << ps << "=" << prop.string_value() << "</property>" << std::endl;
      }
    }
  }

  };

class XML_Generator : public Dump_Configuration
  {
private:
  typedef std::vector<std::string> sv_t;
  typedef sv_t::iterator sv_iterator_t;
  typedef intifada::Map<std::string,sv_t> ssv_t;
  typedef ssv_t::iterator ssv_iterator_t;
  typedef ssv_t::pair ssv_pair_t;

  typedef std::stack<std::string> fifo_string_type;

public:
  XML_Generator();
  int begin_uap(const intifada::Condition*c);
  int end_uap();

  int begin_item(const std::string& longname,const intifada::Path_Name& reference,const Properties& p);
  virtual int end_item();
  int frn(int f,const intifada::Path_Name& s);

  virtual int family_name_id(const std::string& f,const std::string& id);
  virtual int begin_record_identity(const std::string& desc,uint8_t c, const std::string& id);
  virtual int end_record_identity();

  virtual int begin_fixed_field(const Properties& f);
  virtual int begin_extended_field(const Properties& f);
  virtual int begin_repetitive_field(const Properties& f);

  virtual int begin_compound_field(uint8_t size);

  virtual int end_field();

  virtual int part(const intifada::Type& type,const intifada::Path_Name& name,uint8_t start,uint8_t size,uint8_t part);
  int dump();

  void push_field(const std::string& f);
  std::string pop_field();

private:
  std::ostringstream uaps_;
  fifo_string_type current_field_;
  bool extended_repetitive_;
  };

class Text_Generator : public Dump_Configuration
  {
private:
  typedef std::vector<std::ostringstream*> ostringstream_tab_type;
  typedef ostringstream_tab_type::const_iterator ostringstream_tab_iterator_type;

public:
  Text_Generator();
  int begin_uap(const intifada::Condition*c);
  int end_uap();

  int begin_item(const std::string& longname,const intifada::Path_Name& reference,const Properties& p);
  virtual int end_item();
  int frn(int f,const intifada::Path_Name& s);

  virtual int family_name_id(const std::string& f,const std::string& id);
  virtual int begin_record_identity(const std::string& desc,uint8_t c, const std::string& id);
  virtual int end_record_identity();
  virtual int begin_fixed_field(const Properties& f);

  virtual int begin_extended_field(const Properties& f);

  virtual int begin_repetitive_field(const Properties& f);
  virtual int begin_compound_field(uint8_t size);

  virtual void end_compound_field();

  virtual int part(const intifada::Type& type,const intifada::Path_Name& name,uint8_t start,uint8_t size,uint8_t byte);

  void header_preamble();

  int dump();
private:
  void record_method_declaration(std::ostringstream& os, int t);
  void begin_record_method_implementation(std::ostringstream& os, int t);
  void end_record_method_implementation(std::ostringstream& os);
private:
  ostringstream_tab_type records_;

  std::ostringstream hcv_;
  std::ostringstream ccv_;

  };

#endif // CONFIGURATION_HXX
