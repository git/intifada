/*
   Copyright (C) 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOURCE_READER_HXX_
# define SOURCE_READER_HXX_

# include <intifada/inttypes.h>
# include <string>

/// Basic class to handle data sources
class Source_Reader
{
public:
  Source_Reader();

  virtual ~Source_Reader();

public:

  /// Open source
  /**
   *  After this call, source could be accessed
   *
   * @return -1 if something goes wrong (errno=ENOTSUP)
   * @return 0 if source is opened
   */
  virtual int open();

  /// Close source
  /**
   * After this call, source is closed and couldn't be accessed anymore
   *
   * @return -1 if something goes wrong  (errno=ENOTSUP)
   * @return 0 if source is opened
   */
  virtual int close();

  /// Read part source
  /**
   * read coherent source part
   * @return -1 if part couldn't be read (errno=ENOTSUP)
   * @return >0 if part is acquired size is returned
   */
  virtual int read();

  /// apply a user operation
  virtual void operator()();

  /// set filter
  /**
   *
   * @param filter filter to apply to the source
   * @return -1 if filter couldn't be applied
   * @return 0 if filter could be applied
   */
  virtual int set_filter(const std::string& filter);
};
#endif // SOURCE_READER_HXX_
