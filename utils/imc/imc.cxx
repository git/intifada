/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <imc.hxx>

#include <intifada/Configuration_Internal_Parser.hxx>
#include <fstream>
#include <Options.hxx>
#include <Check_Operation.hxx>
#include <Dump_Operation.hxx>
#include <cstring>

#include <intifada/Configuration_Internal_Reader.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Record_Repository.hxx>

#include <Pure_Asterix_Source.hxx>
#ifdef INTIFADA_HAVE_PCAP
# include <PCAP_Asterix_Source.hxx>
#endif
#include <Message_Database_Source.hxx>
#include <errno.h>

static Options options;

static std::ifstream *input_file=NULL;

int open_input_stream(std::istream& input_stream,const std::string& source)
{
	input_stream.rdbuf(std::cin.rdbuf());
	if(source!="-")
	{
		input_file=new std::ifstream(source.c_str());
		if(*input_file)
		{
			input_stream.rdbuf(input_file->rdbuf());
		}
		else
		{
			std::cerr << "Error opening " << source << std::endl;
			return -1;
		}
	}
	return 0;
}

int close_input_stream()
{
	int ret=-1;
	if(input_file!=NULL)
	{
		input_file->close();
		delete input_file;
		input_file=NULL;
		ret=0;
	}
	return ret;
}

int
main(int argc, char**argv)
{
	try{
		// clog.insert_mask("Condition");
		// clog.insert_mask("Configuration_XML_Reader");
		// clog.insert_mask("Configuration_Reader");
		// clog.insert_mask("Record");
		// clog.insert_mask("Foreach_Field_Iterator");
		// clog.insert_mask("Configuration_Reader");
		// clog.insert_mask("Field_Specification_Data");
		// clog.insert_mask("Field_Iterator_Size");
		// clog.insert_mask("Check_Item_Iterator");
		// clog.insert_mask("Repetitive_Length_Data_Field");
		// clog.insert_mask("Extended_Length_Data_Field");
		// clog.insert_mask("size debug");
		// clog.insert_mask("stream pos");
		// clog.insert_mask("String_Type");
		// clog.insert_mask("Memory_Allocation");
		// clog.insert_mask("Configuration_Internal_Reader");
		// clog.insert_mask("Data_Field_Characteristics_List");
		// clog.insert_mask("pcap");
		handle_options(options,argc,argv);

		std::string asterix_description=INTIFADA_TOP_SRCDIR;
		std::string asterix_validation=INTIFADA_TOP_SRCDIR;
		if(options.get_internal_description()==true)
		{
			asterix_description+="/test.kv";
		}
		else
		{
			asterix_description +="/asterix.xml";
			asterix_validation +="/asterix.dtd";
		}
		std::string alternate_asterix_description=options.get_asterix_description();
		std::string alternate_asterix_validation=options.get_asterix_validation();
		if(alternate_asterix_description.empty()!=true){
			asterix_description=alternate_asterix_description;
		}
		if(alternate_asterix_validation.empty()!=true){
			asterix_validation=alternate_asterix_validation;
		}


		intifada::Configuration_Reader *p=NULL;
		// Reading asterix description
		if(options.get_internal_description()==true)
		{
			p= new intifada::Configuration_Internal_Reader(asterix_description);
		}
		else
		{
			p= new intifada::Configuration_XML_Reader(asterix_description,asterix_validation);
		}


		int opened=p->open();
		if(opened!=0){
			std::cerr << "Error opening asterix description (" << asterix_description << ")" << std::endl;
			exit(EXIT_FAILURE);
		}
		// Loading repository with asterix description
		intifada::Record_Repository* rep=intifada::Record_Repository::instance();
		p->parse(rep);
		//debut loop
		for(int loop_iteration=0;loop_iteration<options.get_loop();++loop_iteration)
		{
			Operation *op_asterix=NULL;
			if(options.get_dump()==false){
				op_asterix = new Check_Operation(rep);
			}else{
				op_asterix = new Dump_Operation(rep,options.get_dump_type());
			}
			Source_Reader* source_type=NULL;

			std::istream input_stream(std::cin.rdbuf());

			if(options.get_type()=="INTERNAL"){
				std::string dbt=INTIFADA_TOP_SRCDIR;
				dbt+="/utils/imc/";
				dbt+="Message_Database.dbt";
				if(options.get_source().empty()!=true)
				{
					dbt=options.get_source();
				}
				open_input_stream(input_stream,dbt);
				source_type=new Message_Database_Source(input_stream,options,*op_asterix);
				// check_internal_database(*op_asterix);
				// check_text_database(*op_asterix);
				// dump_internal_database();
			}else if(options.get_type()=="PURE"){
				open_input_stream(input_stream,options.get_source());
				source_type=new Pure_Asterix_Source(input_stream,options.get_family(),*op_asterix);
#ifdef INTIFADA_HAVE_PCAP
			}else if(options.get_type()=="PCAP"){
				source_type=new PCAP_Asterix_Source(options.get_source(),options.get_family(),*op_asterix);
#endif
			}else{
				std::cout << "type:"<<options.get_type()<< " not implemented" << std::endl;
			}
			if(source_type!=NULL)
			{
				int opened_state=source_type->open();
				if(opened_state!=0)
				{
					std::cerr << "Error opening source stream" << std::endl;
					exit(EXIT_FAILURE);
				}
				if(options.get_userdata().empty()!=true)
				{
					int filter_state = source_type->set_filter(options.get_userdata());
					if(filter_state!=0)
					{
						std::cerr << "Error setting filter" << std::endl;
						exit(EXIT_FAILURE);
					}
				}
				int size=0;
				do
				{
					size=source_type->read();
					(*source_type)();
				}while(size > 0);
				int closed=source_type->close();
				if(closed==-1 && errno==ENOTSUP)
					close_input_stream();
			}
			delete op_asterix;
			delete source_type;
		}
		// fin de loop
		exit(EXIT_SUCCESS);
	}catch(intifada::Description_Field_Unknow_Exception & ex){
		std::cerr << "Unknow field:"<<ex.get_name()<<std::endl;
		exit(EXIT_FAILURE);
	}catch(intifada::Parsing_Input_Length_Exception & ex){
		std::cerr << "Invalid size:fatal error" <<std::endl;
		exit(EXIT_FAILURE);
	}catch(intifada::Parsing_Unknow_Category & ex){
		std::cerr << "Invalid category:fatal error (got "
				<< static_cast<uint16_t>(ex.get_category())
				<< " for " << ex.get_family() << " family at "
				<< ex.get_error_position() << " bytes from asterix begining)"
				<< std::endl;
		exit(EXIT_FAILURE);

	}
}
