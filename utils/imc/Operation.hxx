/*
   Copyright (C) 2009, 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPERATION_HXX_
# define OPERATION_HXX_

# include <string>
# include <intifada/Exception.hxx>

namespace intifada
{
  class Record_Repository;
}

class Operation
  {
public:
  Operation(intifada::Record_Repository* rep);
  virtual ~Operation();

  virtual int operator()(
      const std::string& family,
      const std::string& midx,
      const uint8_t*m,
      uint16_t msg_size,
      const std::string&status,
      void* userdata=NULL
      )=0;
protected:
  intifada::Record_Repository* rep_;
  };


#endif // OPERATION_HXX_
