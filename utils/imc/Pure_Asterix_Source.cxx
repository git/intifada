/*
   Copyright (C) 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Pure_Asterix_Source.hxx>
#include <Operation.hxx>
#include <cstring>
#include <sstream>

Pure_Asterix_Source::Pure_Asterix_Source(std::istream& source,const std::string& family,Operation &op):
source_(source),
buf_(NULL),
buf_size_(0),
family_(family),
idx_(0),
op_(op)
{}

Pure_Asterix_Source::~Pure_Asterix_Source()
{
  if(buf_!=NULL)
    {
    delete[] buf_;
    }
}

int Pure_Asterix_Source::open()
{
  return 0;
}

int Pure_Asterix_Source::close()
{
  return 0;
}

int Pure_Asterix_Source::read()
{
  if(buf_!=NULL)
    {
    delete[] buf_;
    }
  buf_size_=-1;
  if(source_)
    {
    char asthead[3];
    source_.read(asthead,3);
    if(source_)
      {
      buf_size_=((asthead[1]& 0xff) << 8) | (asthead[2] & 0xff);
      buf_=new uint8_t[buf_size_];
      ::memcpy(static_cast<void*>(buf_),asthead,3);
      uint8_t* ap=buf_;
      ap+=3;
      source_.read(reinterpret_cast<char*>(ap),buf_size_-3);
      ++idx_;
      }
    if(!source_)
      {
      delete[]buf_;
      buf_=NULL;
      buf_size_=-1;
      }
    }
  return buf_size_;
}

void Pure_Asterix_Source::operator()()
{
  std::ostringstream os;
  os << idx_;
  op_(family_,os.str(),buf_,buf_size_,"PASS");
}
