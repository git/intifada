/*
   Copyright (C) 2010, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Message_Database_Source.hxx>
#include <Operation.hxx>
#include <Options.hxx>
#include <cstring>
#include <sstream>

Message_Database_Source::Message_Database_Source(std::istream& source,const Options& options, Operation &op):
source_(source),
options_(options),
parser_(),
message_family_(),
message_id_(),
message_count_(),
message_status_(),
buf_(NULL),
buf_size_(0),
idx_(0),
op_(op)
{}

Message_Database_Source::~Message_Database_Source()
{
	if(buf_!=NULL)
	{
		delete[] buf_;
	}
}

int Message_Database_Source::open()
{
	// Parser initialization
	parser_.add_key("id",intifada::Configuration_Internal_Parser::STRING);
	parser_.add_key("family",intifada::Configuration_Internal_Parser::STRING);
	parser_.add_key("message_count",intifada::Configuration_Internal_Parser::STRING);
	parser_.add_key("message",intifada::Configuration_Internal_Parser::INTEGER_ARRAY);
	parser_.add_key("message_status",intifada::Configuration_Internal_Parser::STRING);
	return 0;
}

int Message_Database_Source::read()
{
	message_status_="PASS";
	bool completed=false;
	while(source_ && completed==false)
	{
		parser_.parse(source_);
		if(source_)
		{
			const std::string &key=parser_.get_key();
			if(key=="family")
			{
				parser_.get_arg(message_family_);
			}
			else if(key=="id")
			{
				parser_.get_arg(message_id_);
			}
			else if(key=="message_count")
			{
				parser_.get_arg(message_count_);
			}
			else if(key=="message_status")
			{
				// expected status (default=PASS)
				// PASS (assertion is true)
				// XFAIL (fail, but it's normal)
				// Parsing_Input_Length_Exception (expected error on input length problem)
				parser_.get_arg(message_status_);
			}
			else if(key=="message" && options_.check_exist(message_id_)==true && (options_.get_idx().empty()!=false||(options_.get_idx()==message_count_)))
			{
				intifada::Configuration_Internal_Parser::int_array_type array;
				parser_.get_arg(array);
				if(buf_!=NULL)
				{
					delete[]buf_;
				}
				buf_=new uint8_t[array.size()];
				buf_size_=array.size();
				for(
						intifada::Configuration_Internal_Parser::int_array_type::size_type i=0;
						i<array.size();
						++i
				){
					buf_[i]=array[i];
				}
				completed=true;
			}
		}
		else
		{
			buf_size_=0;
		}
	};
	return buf_size_;
}

void Message_Database_Source::operator()()
{

	if(buf_!=NULL)
	{
		op_(message_family_,message_count_,buf_,buf_size_,message_status_);
		delete[]buf_;
		buf_=NULL;
	}
}
