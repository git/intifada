/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Check_Iterator.hxx>
#include <intifada/Record.hxx>

Check_Iterator::Check_Iterator(intifada::Message& msg):it_(),msg_(msg),block_(NULL){}

int Check_Iterator::operator()(uint8_t c)
                                                {
    // Get a new block to insert
    block_=msg_.get_new_block(c);
    // Push back it
    msg_.push_back(block_);

    // std::cout << block_ << " cat=" << (uint32_t) c << std::endl;
    return 0;
                                                }

int Check_Iterator::operator()(
    intifada::Record_Iterator::record_type& i,
    const intifada::Message::block_list_size_t& /*b*/,
    const intifada::Block::record_list_size_t& /*r*/)
                                {
    intifada::Record *new_rec=block_->get_new_record();
    it_.set_record(new_rec);
    i.foreach(it_,intifada::Path_Name());
    block_->push_back(new_rec);
    return 0;
                                }
