/*
   Copyright (C) 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESSAGE_DATABASE_SOURCE_HXX_
# define MESSAGE_DATABASE_SOURCE_HXX_

# include <Source_Reader.hxx>
# include <string>
# include <istream>
# include <intifada/Configuration_Internal_Parser.hxx>
class Operation;
class Options;

class Message_Database_Source : public Source_Reader
{
public:
  typedef Source_Reader inherited;

public:
  Message_Database_Source(std::istream& source,const Options& options, Operation &op);

  virtual ~Message_Database_Source();

  /// Open source
    /**
     *  After this call, source could be accessed
     *
     * @return -1 if something goes wrong (errno=ENOTSUP)
     * @return 0 if source is opened
     */
    virtual int open();

    /// Read part source
    /**
     * read coherent source part
     * @return -1 if part couldn't be read
     * @return >0 if part is acquired size is returned
     *
     */
    virtual int read();

    void operator()();

private:
  std::istream& source_;

  const Options& options_;
  intifada::Configuration_Internal_Parser parser_;

  std::string message_family_;
  std::string message_id_;
  std::string message_count_;
  std::string message_status_;

  uint8_t* buf_;
  int buf_size_;

  int idx_;
  Operation& op_;
};

#endif // PURE_ASTERIX_SOURCE_HXX_
