/*
   Copyright (C) 2009, 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECK_ITEM_ITERATOR_HXX_
# define CHECK_ITEM_ITERATOR_HXX_

# include <intifada/Data_Field.hxx>

namespace intifada
{
  class Record;
}

/// Data field functor
/**
 * operator() is Called for each encountered data field
 */
class Check_Item_Iterator : public intifada::Data_Field::Functor
  {
  public:
    Check_Item_Iterator();
    virtual int operator()(const intifada::Stream::size_type&,const intifada::Stream::size_type&,const intifada::Path_Name& full_name,intifada::Type& t);
    void set_record(intifada::Record* r);
  private:
    intifada::Record* record_;
  };


#endif // CHECK_ITEM_ITERATOR_HXX_
