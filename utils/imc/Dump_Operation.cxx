/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Dump_Operation.hxx>
#include <intifada/Message.hxx>
#include <intifada/Dump_Iterator.hxx>
#include <iostream>

Dump_Operation::Dump_Operation(intifada::Record_Repository* rep,const std::string& dump_type):inherited(rep),dump_type_(dump_type){}

int Dump_Operation::operator()(
		const std::string& family,
		const std::string& midx,
		const uint8_t*m,
		uint16_t msg_size,
		const std::string& status,
		void* userdata)
{
	std::ostream& os=std::cout;

	if(userdata!=NULL)
	{
		os << *static_cast<std::string *>(userdata) << std::endl;
	}

	try
	{
		intifada::Message msg(rep_,family);

		intifada::Stream::size_type s;
		s = msg.set_stream(m,0,msg_size);
		intifada::Dump_Iterator cit(os);
		if(dump_type_=="ALL"){
			cit.dump_all();
		}

		intifada::Record_Iterator_List ril;
		ril.register_iterator(cit);
		msg.foreach(ril);
	}
	catch(intifada::Parsing_Input_Length_Exception & ex){
		std::cerr << "Invalid size:expected:"<<ex.get_expected() << " bytes, got:"
				<< ex.get_received() << " bytes error pos:"<< ex.get_error_position()
				<< " for message:" << midx <<std::endl;
		if(status!="Parsing_Input_Length_Exception")
		{
			throw;
		}
	}
	catch(intifada::Parsing_Unknow_Category & ex){
		std::cerr << "Invalid category at offset from begining:" << ex.get_error_position()
  			<< ":" << ex.get_family()<<":"<<static_cast<int>(ex.get_category())
  			<< " for message:" << midx <<std::endl;
		if(status!="Parsing_Unknow_Category")
		{
			throw;
		}
	}
	return 0;
}
