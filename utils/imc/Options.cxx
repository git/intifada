/*
   Copyright (C) 2009, 2010, 2011, 2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Options.hxx>
#include <unistd.h>

std::string Options::internal_source_default_=std::string(INTIFADA_TOP_SRCDIR)+"/utils/imc/Message_Database.dbt";

const std::string& Options::get_internal_source_default()
{
	return Options::internal_source_default_;
}


Options::Options()
:asterixDescription_()
,asterixValidation_()
,checks_()
,idx_()
,source_()
,source_type_("INTERNAL")
,dump_(false)
,dump_type_()
,family_("eurocontrol")
,internal_description_(false)
,userdata_()
,loop_(1)
{}

void Options::set_asterix_description(const char* f){asterixDescription_=f;}
const std::string& Options::get_asterix_description()const{return asterixDescription_;}
void Options::set_asterix_validation(const char* f){asterixValidation_=f;}
const std::string& Options::get_asterix_validation()const{return asterixValidation_;}

void Options::add_check(const char* c)
{

	checks_.insert(c);
}
bool Options::check_exist(const std::string& c)const
{
	bool found=false;
	desc_const_iterator_type it=checks_.find(c);
	if(it!=checks_.end()){
		found=true;
	}
	return found;
}
void Options::set_idx(const char* i)
{
	idx_=i;
}
const std::string& Options::get_idx()const
{
	if(checks_.size()>1){idx_.clear();}
	return idx_;
}
void Options::set_source(const char* s)
{
	source_=s;
}
const std::string& Options::get_source()const
{
	return source_;
}

void Options::set_type(const std::string& t)
{
	source_type_=t;
}

const std::string& Options::get_type()const
{
	return source_type_;
}
void Options::set_dump(const bool&d)
{
	dump_=d;
}
bool Options::get_dump()const
{
	return dump_;
}
void Options::set_dump_type(const std::string& t)
{
	dump_type_=t;
}

const std::string& Options::get_dump_type()const
{
	return dump_type_;
}

void Options::set_family(const std::string& t)
{
	family_=t;
}

const std::string& Options::get_family()const
{
	return family_;
}

bool Options::get_internal_description()const
{
	return internal_description_;
}
void Options::set_internal_description(const bool& t)
{
	internal_description_=t;
}

const std::string& Options::get_userdata()const
{
	return userdata_;
}
void Options::set_userdata(const std::string& ud)
{
	userdata_=ud;
}

void Options::set_loop(int l)
{
	loop_=l;
}

int Options::get_loop()const
{
	return loop_;
}

//static Options options;

void help(Options& options)
{
	std::cout << "usage:imc -h -f <asterix description file> -c asterix type -i idx -s source -t source type\n";
	std::cout << "\t-h  short help" << std::endl;
	std::cout << "\t-f asterix description file" << std::endl;
	std::cout << "\t-c associated asterix type (only for INTERNAL. match 'id=' lines in .dbt files)" << std::endl;
	std::cout << "\t-i index if applicable" << std::endl;
	std::cout << "\t-m memory check (loop around operations default <<" << options.get_loop() << "). if specified, don't put a space between this option and argument" << std::endl;
	std::cout << "\t-s source file ('-' means stdin)" << std::endl;
	std::cout << "\t-t source type (INTERNAL|PURE|PCAP)" << std::endl;
	std::cout << "\t-F family if applicable (for PURE|PCAP)" << std::endl;
	std::cout << "\t-d ALL|NOTNULL dump (if NOTNULL no null values are printed)" << std::endl;
	std::cout << "\t-u user data text" << std::endl;
	std::cout << "\tif -t and -s aren't specified, messages are taken from internal" << std::endl;
	std::cout << "\tmessage database" << std::endl << std::endl;
	std::cout << "source type:INTERNAL. index is applicable"<<std::endl;
	std::cout << "\tPURE|PCAP. index isn't applicable, but family is" << std::endl;
	std::cout << "\tassociated asterix type:001-002|034-048|ARTAS|STRODS|MAESTRO|062-065" << std::endl;
	std::cout << "\tuser data is transmitted to code which handle source type if applicable" << std::endl;
	std::cout << "\n\tif INTERNAL is selected and no source file is selected, source is " << Options::get_internal_source_default() << std::endl;
	std::cout << "\n\tif -d isn't specified, check mecanism is activated" << std::endl;

}
int handle_options(Options& options,int argc, char**argv)
{
	int opt;
	bool selected_source=false;
	while ((opt = getopt(argc, argv, "hf:D:c:i:Is:m::t:d:F:u:")) != -1) {
		switch (opt) {
		case 'h':
			help(options);
			exit(EXIT_SUCCESS);
			break;
		case 'f':
			options.set_asterix_description(optarg);
			break;
		case 'D':
			options.set_asterix_validation(optarg);
			break;
		case 'c':
			options.add_check(optarg);
			break;
		case 'i':
			options.set_idx(optarg);
			break;
		case 'I':
			//options.set_internal_description(true);
			std::cout << "-I option is inactive" << std::endl;
			break;
		case 's':
			options.set_source(optarg);
			selected_source=true;
			break;
		case 't':
			options.set_type(optarg);
			break;
		case 'd':
			options.set_dump(true);
			options.set_dump_type(optarg);
			break;
		case 'F':
			options.set_family(optarg);
			break;
		case 'm':
			options.set_loop(100);
			if(optarg!=NULL)
			{
				std::istringstream is(optarg);
				int loop;
				is >> loop;
				options.set_loop(loop);
			}
			break;
		case 'u':
			options.set_userdata(optarg);
			break;
		default:
			help(options);
			exit(EXIT_FAILURE);
		}
	}
	if(selected_source==false)
	{
		if(options.get_type()=="INTERNAL")
		{
			options.set_source(Options::get_internal_source_default().c_str());
		}else{
			options.set_source("-");
		}
	}
	return 0;
}
