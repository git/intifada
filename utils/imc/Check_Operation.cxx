/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Check_Operation.hxx>
#include <intifada/Message.hxx>
#include <intifada/Record_Iterator_List.hxx>
#include <Check_Iterator.hxx>
#include <iostream>

Check_Operation::Check_Operation(intifada::Record_Repository* rep):inherited(rep){}

int Check_Operation::operator()(
    const std::string& family,
    const std::string& midx,
    const uint8_t*m,
    uint16_t msg_size,
    const std::string&status,
    void*)
{
  try
  {
    intifada::Message msg(rep_,family);
    intifada::Stream::size_type s = msg.set_stream(m,0,msg_size);
    // Expected cloned message
    intifada::Message ecmsg(rep_,family);
    Check_Iterator cit(ecmsg);

    intifada::Record_Iterator_List ril;
    ril.register_iterator(cit);
    msg.foreach(ril);

    intifada::Stream::size_type est=ecmsg.size();
    uint8_t *t=new uint8_t[est];

    intifada::Stream::size_type st=ecmsg.get_stream(t,0,est);
    if(s!=est){
      std::cerr << midx << " Error computing size src->dest (source size=" << s
          << " dest size=" << est << ")" << std::endl;
    }
    if(st!=est){
      std::cerr << midx << " Error computing size calculated dest->real dest (calculated dest size=" << est
          << " real size=" << st << ")" << std::endl;
    }
    unsigned int minsize=std::min(s,st);

    for(unsigned int i=0;i<minsize;++i){
      if(m[i]!=t[i]){
        std::cerr << midx << ":Error ["<< i <<"], 0x" << std::hex
            << static_cast<uint16_t>(m[i]) << "->0x" << static_cast<uint16_t>(t[i])
            << std::endl;
      }
    }
    delete[]t;
  }
  catch(intifada::Parsing_Input_Length_Exception & ex){
    std::cerr << "Invalid size:expected:"<<ex.get_expected() << " bytes, got:"
        << ex.get_received() << " bytes error pos:"<< ex.get_error_position()
        << " for message:" << midx <<std::endl;
    if(status!="Parsing_Input_Length_Exception")
      {
      throw;
      }
  }
  catch(intifada::Parsing_Unknow_Category & ex){
	std::cerr << "Invalid category at offset from begining:" << ex.get_error_position()
	<< ":" << ex.get_family()<<":"<<static_cast<int>(ex.get_category())
	<< " for message:" << midx <<std::endl;
	if(status!="Parsing_Unknow_Category")
      {
      throw;
      } 
  }
  return 0;
    }
