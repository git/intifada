/*
   Copyright (C) 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PURE_ASTERIX_SOURCE_HXX_
# define PURE_ASTERIX_SOURCE_HXX_

# include <Source_Reader.hxx>
# include <string>
# include <istream>

class Operation;

class Pure_Asterix_Source : public Source_Reader
{
public:
  typedef Source_Reader inherited;

public:
  Pure_Asterix_Source(std::istream& source,const std::string& family,Operation &op);

  virtual ~Pure_Asterix_Source();

  /// Open source
    /**
     *  After this call, source could be accessed
     *
     * @return -1 if something goes wrong (errno=ENOTSUP)
     * @return 0 if source is opened
     */
    virtual int open();

    /// Close source
    /**
     * After this call, source is closed and couldn't be accessed anymore
     *
     * @return -1 if something goes wrong
     * @return 0 if source is opened
     */
    virtual int close();

    /// Read part source
    /**
     * read coherent source part
     * @return -1 if part couldn't be read
     * @return >0 if part is acquired size is returned
     *
     */
    virtual int read();

    void operator()();

private:
  std::istream& source_;

  uint8_t* buf_;
  int buf_size_;

  std::string family_;
  int idx_;
  Operation& op_;
};

#endif // PURE_ASTERIX_SOURCE_HXX_
