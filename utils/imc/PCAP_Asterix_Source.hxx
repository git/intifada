/*
   Copyright (C) 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PCAP_ASTERIX_SOURCE_HXX_
# define PCAP_ASTERIX_SOURCE_HXX_

# ifdef INTIFADA_HAVE_PCAP

# include <Source_Reader.hxx>
# include <string>
# include <pcap.h>

class Operation;

/// Pcap frame analysis
/**
 * Guess a frame nature:
 * eth.type=0x8000 (IP packet), size for 802.3 (offset=12)
 * ip.proto=0x11 (UDP packet) (offset=23)
 * ip.flags |=0x02 (unfragmented) (offset=20)
 * udp packet:asterix start at 0x2a
 */
class PCAP_Asterix_Source : public Source_Reader
{
public:
  typedef Source_Reader inherited;

public:
  PCAP_Asterix_Source(const std::string& filename,const std::string& family,Operation &op);

  virtual ~PCAP_Asterix_Source();

  /// Open source
    /**
     *  After this call, source could be accessed
     *
     * @return -1 if something goes wrong (errno=ENOTSUP)
     * @return 0 if source is opened
     */
    virtual int open();

    /// Close source
    /**
     * After this call, source is closed and couldn't be accessed anymore
     *
     * @return -1 if something goes wrong
     * @return 0 if source is opened
     */
    virtual int close();

    /// Read part source
    /**
     * read coherent source part
     * @return -1 if part couldn't be read
     * @return >0 if part is acquired size is returned
     *
     */
    virtual int read();

    void operator()();

    int set_filter(const std::string& filter);
private:
    void binary_to_string(const u_char *buf,std::string& str,int bytes)const;
private:
  std::string filename_;
  pcap_t *handle_;
  struct bpf_program filter_;
  struct pcap_pkthdr pcap_packet_header_;

  uint8_t* buf_;
  int buf_size_;

  std::string family_;
  int idx_;
  std::string userdata_;
  Operation& op_;
};
#endif
#endif // PCAP_ASTERIX_SOURCE_HXX_
