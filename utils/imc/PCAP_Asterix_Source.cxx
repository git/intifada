/*
   Copyright (C) 2010,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <PCAP_Asterix_Source.hxx>

#ifdef INTIFADA_HAVE_PCAP
#include <Operation.hxx>
#include <cstring>
#include <sstream>
#include <intifada/Logger.hxx>

static char conv[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

PCAP_Asterix_Source::PCAP_Asterix_Source(const std::string& filename,const std::string& family,Operation &op):
    		filename_(filename),
    		handle_(NULL),
    		filter_(),
    		buf_(NULL),
    		buf_size_(0),
    		family_(family),
    		idx_(0),
    		userdata_(),
    		op_(op)
{}

PCAP_Asterix_Source::~PCAP_Asterix_Source()
{
	if(buf_!=NULL)
	{
		delete[] buf_;
	}
}

int PCAP_Asterix_Source::open()
{
	idx_=0;
	int ret=-1;
	char *ebuf=NULL;
	handle_=pcap_open_offline(filename_.c_str(),ebuf);
	if(handle_!=NULL)
	{
		ret=0;
	}
	return ret;
}

int PCAP_Asterix_Source::close()
{
	pcap_close(handle_);
	return 0;
}

int PCAP_Asterix_Source::read()
{

	if(handle_!=NULL)
	{
		const u_char *buf=pcap_next(handle_,&pcap_packet_header_);
		bool handled_layer=false;
		buf_size_=0;
		int datalink=pcap_datalink(handle_);
		size_t data_length=0;
		size_t asterix_offset=0;
		if(buf!=NULL)
		{
			if(datalink==1)
			{
				data_length=(buf[12]<<8)|buf[13];
				if(data_length!=0x0800)
				{
					// frame:
					// 111111 222222 33 44 5 XXX...XXXXYYY CCCC
					// 1: source mac
					// 2: target mac
					// 3: data size (44+5+X)
					// 4: SAP
					// 5: type
					// X: data
					// Y:padding
					// C:CRC

					userdata_="==== dst:0x";
					binary_to_string(&buf[0],userdata_,6);
					userdata_+=" src:0x";
					binary_to_string(&buf[6],userdata_,6);
					userdata_+=" sap:0x";
					binary_to_string(&buf[14],userdata_,2);
					userdata_+=" idx:";
					std::ostringstream os;
					os << idx_;
					userdata_+=os.str();
					userdata_+=" ====";
					// skip all non asterix part for ethernet:
					// if France: source addr(6)+target addr(6)+length(2)+sap(2)+service(1)=17 bytes skipped
					// asterix data length is 'length'-3 (ssap+dsap+service)
					//
					asterix_offset=17;
					data_length-=3;
					handled_layer=true;
				}
				// handle udp unfragmented packet
				else if(data_length==0x0800 && buf[23]==0x11 && (buf[20]|0x20)!=0)
				{
					asterix_offset=0x2a;
					// udp.len
					data_length=(buf[0x26]<<8)|buf[0x27];
					data_length-=8;
					handled_layer=true;
				}
			}

			int ret=-1;
			if(handled_layer!=false)
			{
				ret=0;
				buf_size_=data_length;
				if(buf_!=NULL)
				{
					delete[] buf_;
				}
				buf_=new uint8_t[buf_size_];
				::memcpy(static_cast<void*>(buf_),&buf[asterix_offset],buf_size_);
			}
		}
	}
	return buf_size_;
}

void PCAP_Asterix_Source::operator()()
{
	clog("pcap") << "family:" << family_ << " size:"<<buf_size_ << " asterix begining:" << std::hex
			<< "0x" << static_cast<uint16_t>(buf_[0]) << ":"
			<< "0x" << static_cast<uint16_t>(buf_[1]) << ":"
			<< "0x" << static_cast<uint16_t>(buf_[2]) << std::dec << std::endl;
	std::ostringstream os;
	os << idx_;
	op_(family_,os.str(),buf_,buf_size_,"PASS",&userdata_);
	++idx_;
}

int PCAP_Asterix_Source::set_filter(const std::string& filter)
{
	int ret=-1;
	ret=pcap_compile(handle_,&filter_,const_cast<char *>(filter.c_str()),1,0);
	if(ret!=-1)
	{
		ret=pcap_setfilter(handle_,&filter_);
	}
	return ret;
}

void PCAP_Asterix_Source::binary_to_string(const u_char *buf,std::string& str,int bytes)const
{
	std::ostringstream os;
	for(int i=0;i<bytes;++i)
	{
		os << std::hex << conv[(buf[i] & 0xf0) >> 4] << conv[buf[i] & 0x0f] ;
	}
	str+=os.str();
}
#endif
