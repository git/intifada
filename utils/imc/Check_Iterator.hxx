/*
   Copyright (C) 2009, 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECK_ITERATOR_HXX_
# define CHECK_ITERATOR_HXX_

# include <intifada/Record_Iterator.hxx>
# include <Check_Item_Iterator.hxx>

class Check_Iterator:public intifada::Record_Iterator
  {
  public:
    Check_Iterator(intifada::Message& msg);
    /// Called when a block began
    virtual int operator()(uint8_t c);
    /// virtual Call operator
    /**
     * Called when a record is encountered
     *
     * \param i record pointer
     * \param b block id in message (first block in message id is 0)
     * \param r record id in block (first record in block is 0)
     */
    virtual int operator()(
        intifada::Record_Iterator::record_type& i,
        const intifada::Message::block_list_size_t& /*b*/,
        const intifada::Block::record_list_size_t& /*r*/);
  private:
    Check_Item_Iterator it_;
    intifada::Message& msg_;
    intifada::Block* block_;
  };


#endif // CHECK_ITERATOR_HXX_
