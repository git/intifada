/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Check_Item_Iterator.hxx>
#include <intifada/Logger.hxx>
#include <intifada/Record.hxx>

Check_Item_Iterator::Check_Item_Iterator():record_(NULL)
{
}

int Check_Item_Iterator::operator()(const intifada::Stream::size_type&,const intifada::Stream::size_type&,const intifada::Path_Name& full_name,intifada::Type& t)
                                  {
    clog("Check_Item_Iterator") << "-->" << std::endl;
    intifada::Path_Name path_name=full_name.get_path();

    try{
      (*record_)(full_name)=t;
      //record_->set_value(full_name,t);
      record_->set_presence(path_name,true);
    }catch(intifada::Description_Field_Unknow_Exception& ex){
      std::cerr << "FATAL:EDASTERIX: unknow field:full name:<"<<full_name<<">" << std::endl;
      std::cerr << "EDASTERIX: unknow field:path name:<"<<path_name<<">(path size="<< full_name.get_name_size() << ")" << std::endl;
      exit(EXIT_FAILURE);
    }
    clog("Check_Item_Iterator") << full_name << "=" << t.get_to_string() << std::endl;
    clog("Check_Item_Iterator") << path_name << " presence=true" << std::endl;

    clog("Check_Item_Iterator") << "<--" << std::endl;
    return 0;
                                  }

void Check_Item_Iterator::set_record(intifada::Record* r){record_=r;}
