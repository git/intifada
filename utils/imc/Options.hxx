/*
   Copyright (C) 2009, 2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPTIONS_HXX_
# define OPTIONS_HXX_
# include <set>
# include <string>
# include <intifada/Data_Field.hxx>
# include <intifada/Record.hxx>
# include <intifada/Record_Iterator_List.hxx>


class Options
  {
private:
  typedef std::set<std::string> desc_type;
  typedef desc_type::iterator desc_iterator_type;
  typedef desc_type::const_iterator desc_const_iterator_type;
  typedef std::pair<desc_iterator_type,bool> desc_pair_type;

public:
  static const std::string& get_internal_source_default();

public:
  Options();
public:
  void set_asterix_description(const char* f);
  const std::string& get_asterix_description()const;
  void set_asterix_validation(const char* f);
  const std::string& get_asterix_validation()const;

  void add_check(const char* c);
  bool check_exist(const std::string& c)const;
  void set_idx(const char* i);
  const std::string& get_idx()const;
  void set_source(const char* s);
  const std::string& get_source()const;

  void set_type(const std::string& t);

  const std::string& get_type()const;
  void set_dump(const bool&d);
  bool get_dump()const;
  void set_dump_type(const std::string& t);

  const std::string& get_dump_type()const;
  void set_family(const std::string& t);

  const std::string& get_family()const;

  bool get_internal_description()const;
  void set_internal_description(const bool& t);

  const std::string& get_userdata()const;
  void set_userdata(const std::string& ud);

  void set_loop(int l);
  int get_loop()const;
private:
  static std::string internal_source_default_;
private:
  std::string asterixDescription_;
  std::string asterixValidation_;
  desc_type checks_;
  mutable std::string idx_;
  std::string source_;
  std::string source_type_;
  bool dump_;
  std::string dump_type_;
  std::string family_;
  bool internal_description_;
  std::string userdata_;
  int loop_;
  };

void help(Options& options);
int handle_options(Options& options,int argc, char**argv);

#endif // OPTIONS_HXX_
