/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/cintifada.h>
#include <stdlib.h>
#include <stdio.h>

int new_block(uint8_t c,user_data_type ud)
  {
  printf("new block:%u user data:%x\n",c,(unsigned int)ud);
    return 0;
  }

int new_record(i_ptr_Record r,block_size_type bi,record_size_type ri,user_data_type ud)
  {
    printf("new record:block size:%u record size=%u record pointer:%x user data:%x\n",bi,ri,(unsigned int)r,(unsigned int)ud);
    return 0;
  }

int main()
  {
    /* Load a description */
    const char asterix_description[]=INTIFADA_XML_FILE;
    const char asterix_validation[]=INTIFADA_DTD_FILE;
    i_ptr_Record_Repository rep;
    i_ptr_Message msg;
    i_ptr_Record_Iterator_List rl;
    i_ptr_Record_Iterator rit;

    i_ptr_Configuration_XML_Reader dloader=
        i_create_xml_configuration_reader(asterix_validation,asterix_description);

    int opened=i_open_xml_configuration(dloader);
    if(opened!=0){
      printf("Error opening asterix description (%s)\n",asterix_description);
      exit(EXIT_FAILURE);
    }
    rep=i_get_record_repository_instance();

    i_xml_parse(dloader,rep);

    msg=i_create_message(rep,"eurocontrol");

    rl=i_create_record_iterator_list();

    rit=i_register_record_iterator(rl,new_block,new_record,NULL);



    i_release_record_iterator(rit);
    i_release_record_iterator_list(rl);
    i_release_message(msg);
    exit(EXIT_SUCCESS);
  }
