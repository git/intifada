/*
   Copyright (C) 2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Message.hxx>
#include <intifada/Block.hxx>
#include <intifada/Record.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/String_Type.hxx>

void get_stream(const intifada::Message& msg,const bool& dump=false);

void get_stream(const intifada::Message& msg,const bool& dump)
{
	intifada::Stream::size_type size=msg.size();
	uint8_t *t=new uint8_t[size];
	intifada::Stream::size_type st=msg.get_stream(t,0,size);
	if(dump==true)
	{
		for(int i=0;i<size;++i)
		{
			std::cerr << std::setw(2) << std::setfill('0') << std::hex << (uint16_t) t[i] << " ";
		}
		std::cerr << std::dec << std::endl;
	}
	delete[]t;
}

// 3e 00 11 81 10 08 08 c0 00 00 01 20 20 48 45 4c 4f
// 3e 00 06 80 08 08
// 3e 00 11 81 10 08 08 c0 00 00 01 20 20 48 45 4c 4f
// 3e 00 07 81 10 08 08

void update_compound(intifada::Message& msg,intifada::Record& record)
{
	bool dump=true;
	for(int idx=0;idx < 1000;++idx)
	{
		// zone mode S
		::get_stream(msg,dump);
		dump=false;

	}
	int dump_count=1;
	dump=true;
	for(int idx=0;idx < 1000;++idx)
	{

		// primary zone
		if(record.get_presence("380.ADR")==true)
			record.set_presence("380.ADR",false);
		if(record.get_presence("380.ID")==true)
			record.set_presence("380.ID",false);
		//record.set_presence("380",false);
		::get_stream(msg,dump);
		--dump_count;
		if(dump_count==0)
			dump=false;
	}
}

void update_moda(intifada::Message& msg,intifada::Record& record)
{
	bool dump=true;
	for(int idx=0;idx < 1000;++idx)
	{
		// zone mode S
		::get_stream(msg,dump);
		dump=false;

	}
	int dump_count=2;
	dump=true;
	for(int idx=0;idx < 100000;++idx)
	{

		// primary zone
		if(record.get_presence("382")==true)
			record.set_presence("382",false);
		::get_stream(msg,dump);
		if(record.get_presence("384")==false)
			record.set_presence("384",true);
		::get_stream(msg,dump);
		--dump_count;
		if(dump_count==0)
			dump=false;
	}
}

int main()
{

	{
		intifada::Record_Repository* rep=intifada::Record_Repository::instance();
		std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
		std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
		intifada::Configuration_XML_Reader p(xml_file,dtd_file);
		p.open();
		p.parse(rep);

		intifada::Message message(rep,"eurocontrol");
		intifada::Block *block=message.get_new_block(62);
		message.push_back(block);
		intifada::Record& record=*(block->get_new_record());

		try
		{
			block->push_back(&record);

			record("010.SIC")=intifada::uint8_T(0x08);
			record("010.SAC")=intifada::uint8_T(0x08);
			record.set_presence("010",true);
			record("380.ADR.VALUE") = intifada::uint32_T(0x01);
			record.set_presence("380.ADR",true);
			record("380.ID.Ident") = intifada::String_Type("HELO");
			record.set_presence("380.ID",true);
			record.set_presence("380",true);
			::update_compound(message,record);
		}catch(intifada::Description_Field_Unknow_Exception & ex){
			std::cerr << "<" << ex.get_name() << ">" << std::endl;
		}
	}
	{
		intifada::Record_Repository* rep=intifada::Record_Repository::instance();
		std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
		std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
		intifada::Configuration_XML_Reader p(xml_file,dtd_file);
		p.open();
		p.parse(rep);

		intifada::Message message(rep,"maestro");
		intifada::Block *block=message.get_new_block(30);
		message.push_back(block);
		intifada::Record& record=*(block->get_new_record());

		try
		{
			block->push_back(&record);

			record("010.SIC")=intifada::uint8_T(0x08);
			record("010.SAC")=intifada::uint8_T(0x08);
			record.set_presence("010",true);
			record("060.Mode-3/A")=intifada::oct_T(1000);
			record.set_presence("060",true);
			record("382.MODE-S ADDRESS")=intifada::uint32_T(0x123456);
			record("384.IDS")=intifada::String_Type("POUET");
			record.set_presence("382",true);
			record.set_presence("384",true);

			::update_moda(message,record);
		}catch(intifada::Description_Field_Unknow_Exception & ex){
			std::cerr << "<" << ex.get_name() << ">" << std::endl;
		}
	}
}
