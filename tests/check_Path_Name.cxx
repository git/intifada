/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Path_Name.hxx>
#include <iostream>
#include <cstdlib>

int main(int,char**)
  {
    int exit_status=EXIT_SUCCESS;
    std::string path="part1.part2.part3";
    intifada::Path_Name pn(path);
    if(pn.get_full_name()!=path){
      std::cerr << "Error using " << path << std::endl;
      exit_status=EXIT_FAILURE;
    }
    if(pn!=path){
      std::cerr << "operator==:Error using " << path << std::endl;
      exit_status=EXIT_FAILURE;
    }

    pn.pop_back();
    if(pn.get_full_name()!="part1.part2"){
      std::cerr << "Error using " << path << " after pop" << std::endl;
      exit_status=EXIT_FAILURE;
    }
    pn.push_back("part3");
    if(pn.get_full_name()!="part1.part2.part3"){
      std::cerr << "Error using " << path << " after push" << std::endl;
      exit_status=EXIT_FAILURE;
    }
    path="part1";
    pn=path;
    if(pn.get_full_name()!=path){
      std::cerr << "Error using " << path << std::endl;
      exit_status=EXIT_FAILURE;
    }
    path="";
    pn=path;
    if(pn.get_full_name()!=path){
      std::cerr << "Error using <" << path << ">" << std::endl;
      exit_status=EXIT_FAILURE;
    }
    if(pn!=path){
      std::cerr << "operator==:Error using <" << path << ">" << std::endl;
      exit_status=EXIT_FAILURE;
    }
    path="1.2.3.4";
    pn=path;
    std::string first=pn.get_front();
    pn.pop_front();
    if(first != "1"){
      std::cerr << "operator==:Error using pop_front()<" << path << ">" << std::endl;
      exit_status=EXIT_FAILURE;
    }
    // get path name
    pn=path;
    if(pn.get_path()!="1.2.3"){
      std::cerr << "Error getting path name, got " << pn.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }
    pn="";
    if(pn.get_path()!=""){
      std::cerr << "Error getting path name, got " << pn.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }

    pn="1";
    if(pn.get_path()!=""){
      std::cerr << "Error getting path name, got " << pn.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }
    pn="1.2";
    if(pn.get_path()!="1"){
      std::cerr << "Error getting path name, got " << pn.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }
    pn="1.2.3";
    pn.set_name_size(2);
    if(pn.get_path()!="1"){
      std::cerr << "Error getting path name (name size=2), got " << pn.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }
    pn="1";
    pn.set_name_size(1);
    intifada::Path_Name pn1="1";
    intifada::Path_Name pn2="2.3";
    pn2.set_name_size(2);
    intifada::Path_Name pna=pn1+pn2;
    if(pna.get_path()!="1"){
      std::cerr << "Error getting path name (copy name size=2), got " << pna.get_path()<< std::endl;
      exit_status=EXIT_FAILURE;
    }
    exit(exit_status);
  }
