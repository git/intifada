/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_Internal_Parser.hxx>
#include <iostream>
#include <sstream>
#include <cstdlib>

static uint8_t expected_values[]={
    0x01,0x00,0x18,0xf5,0x22,0x08,0x32,0x98,0x01,0x79,0x11,0x7a,0x74,0x70,0x03,
    0x67,0x71,0x30,0xc1,0x01,0xcd,0x01,0xcc,0x02,0x02,0x00,0x0c,0xf4,0x08,0x32,
    0x02,0x78,0x31,0xa6,0xd9,0x22
};

int main()
{
  std::stringstream is;
  is << "id=001-002\n\
      family =eurocontrol\n\
      message_count=0\n\
      message=\n\
      {       // size=36 1st bloc:24 2nd bloc 12\n\
      1\n\
      ,0x00, 0x18\n\
      ,0xf5             // fspec = 11110101:010/020/161/040/200\n\
      ,0x22             // fspec = 00100010:130/210\n\
      ,010, 0x32       // 010 TRAC 2000 de Lyon Satolas\n\
      ,0x98             // 020 TYP=1/SIM=0/DEC=01/ANT=1/SPI=0/TF=0\n\
      ,0x01, 0x79       // 161 TRACK=377\n\
      ,0x11, 0x7a       // 040\n\
      ,0x74, 0x70 // 040\n\
      ,0x03, 0x67       // 200\n\
      ,0x71, 0x30       // 200\n\
      ,0xc1, 0x01       // 130\n\
      ,0xcd, 0x01       // 130\n\
      ,0xcc             // 130\n\
      ,0x02             // 210\n\
      ,0x02\n\
      ,0x00, 0x0c\n\
      ,0xf4                             // fspec 11110100:010/000/020/030/050\n\
      ,0x08, 0x32                       // 010\n\
      ,0x02                             // 000 Fin de secteur\n\
      ,0x78                             // 020\n\
      ,0x31, 0xa6, 0xd9 // 030\n\
      ,0x22                             // 050\n\
      }";

  intifada::Configuration_Internal_Parser p;
  p.add_key("id",intifada::Configuration_Internal_Parser::STRING);
  p.add_key("family",intifada::Configuration_Internal_Parser::STRING);
  p.add_key("message_count",intifada::Configuration_Internal_Parser::INTEGER);
  p.add_key("message",intifada::Configuration_Internal_Parser::INTEGER_ARRAY);
  while(is)
    {
    p.parse(is);
    if(p.get_key()=="id")
      {
      std::string arg;
      p.get_arg(arg);
      if(arg!="001-002")
        {
        std::cerr << "Error getting id" << std::endl;
        exit(EXIT_FAILURE);
        }
      }
    else if(p.get_key()=="family")
      {
      std::string arg;
      p.get_arg(arg);
      if(arg!="eurocontrol")
        {
        std::cerr << "Error getting family" << std::endl;
        exit(EXIT_FAILURE);
        }
      }
    else if(p.get_key()=="message_count")
      {
      int arg;
      p.get_arg(arg);
      if(arg!=0)
        {
        std::cerr << "Error getting message_count" << std::endl;
        exit(EXIT_FAILURE);
        }
      }
    else if(p.get_key()=="message")
          {
          intifada::Configuration_Internal_Parser::int_array_type arg;
          p.get_arg(arg);
          for(unsigned int idx=0;idx < arg.size();++idx)
            {
              if(arg[idx]!=expected_values[idx])
                {
                std::cerr << "Error getting message content" << std::endl;
                exit(EXIT_FAILURE);
                }
            }
          }
    else
      {
        std::cerr << "Error, unknow key:"<<p.get_key()<<std::endl;
        exit(EXIT_FAILURE);
      }
    }
  exit(EXIT_SUCCESS);
}
