/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECK_DATAS_UTILS_HXX
# define CHECK_DATAS_UTILS_HXX

#include <intifada/inttypes.h>

namespace intifada
{
  class Data;
}

void check_parsing_data(const uint8_t *input_stream,uint8_t stream_size,intifada::Data &d);
void check_scratch_data(const uint8_t *input_stream,uint8_t stream_size,intifada::Data &d);

#endif // CHECK_DATAS_UTILS_HXX

