/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Extended_Length_Data_Field.hxx>

#include <intifada/Exception.hxx>

#include <check_datas_utils.hxx>
#include <intifada/Integer.hxx>
#include <iostream>

void check_basic()
{
  uint8_t stream[]=
    {
      0x00,0x01,
      0x06,0x01,0x01,
      0x06,0x02,0x01,
      0x06,0x03,0x00
    };


  intifada::Extended_Length_Data_Field data_good;
  data_good.set_property("primary_size","2");
  data_good.set_property("secondaries_size","3");
  data_good.set_property("repetitive","false");
  try{
    check_parsing_data(stream,11,data_good);
    return;
  }catch(intifada::Parsing_Input_Length_Exception&ex){
    std::cerr << "throw Erreur, invalid read size ("
	      << uint16_t(ex.get_expected()) << " expected, "
	      << uint16_t(ex.get_received()) << " got)"
	      << std::endl;
  }catch(intifada::Parsing_Output_Length_Exception&ex){
    std::cerr << "throw Erreur, invalid write size ("
	      << uint16_t(ex.get_expected()) << " expected, "
	      << uint16_t(ex.get_received()) << " got)"
	      << std::endl;

  }catch(intifada::Parsing_Exception&ex){
    std::cerr << "Re encoding error" << std::endl;
  }
  exit(1);
}

void
check_cat_62_080()
{
  // Simple test is based on following structure:
  // I062/080
  // 10000001 00010001 00000011 01100010
  intifada::Extended_Length_Data_Field trd;
  trd.set_property("primary_size","1");
  trd.set_property("secondaries_size","1");
  trd.set_property("repetitive","false");


  trd.insert("MON","bool_T",8,1,0);
  trd.insert("SPI","bool_T",7,1,0);
  trd.insert("MRH","bool_T",6,1,0);
  trd.insert("SRC","uint8_T",3,3,0);
  trd.insert("CNF","bool_T",2,1,0);

  trd.insert("SIM","bool_T",8,1,1);
  trd.insert("TSE","bool_T",7,1,1);
  trd.insert("TSB","bool_T",6,1,1);
  trd.insert("FPC","bool_T",5,1,1);
  trd.insert("AFF","bool_T",4,1,1);
  trd.insert("STP","bool_T",3,1,1);
  trd.insert("KOS","bool_T",2,1,1);

  trd.insert("AMA","bool_T",8,1,2);
  trd.insert("MD4","uint8_T",6,2,2);
  trd.insert("ME","bool_T",5,1,2);
  trd.insert("MI","bool_T",4,1,2);
  trd.insert("MD5","uint8_T",2,2,2);

  trd.insert("CST","bool_T",8,1,3);
  trd.insert("PSR","bool_T",7,1,3);
  trd.insert("SSR","bool_T",6,1,3);
  trd.insert("MDS","bool_T",5,1,3);
  trd.insert("ADS","bool_T",4,1,3);
  trd.insert("SUC","bool_T",3,1,3);
  trd.insert("AAC","bool_T",2,1,3);

  intifada::bool_T true_value=true;
  intifada::bool_T false_value=false;
  intifada::uint8_T md5v=1;
  trd("MON")=true_value;
  trd("FPC")=true_value;
  trd("MD5")=md5v;
  trd("PSR")=true_value;
  trd("SSR")=true_value;
  trd("AAC")=true_value;
  // inserted values verification
  intifada::bool_T field_value=trd("MON");
  if(field_value != true){
    std::cerr << "Error getting MON value" << std::endl;
    exit(1);
  }
  field_value=trd("CNF");
  if(field_value != false){
    std::cerr << "Error getting CNF value" << std::endl;
    exit(1);
  }
  field_value=trd("SSR");
  if(field_value != true){
    std::cerr << "Error getting SSR value" << std::endl;
    exit(1);
  }
  field_value=trd("SUC");
  if(field_value != false){
    std::cerr << "Error getting SUC value" << std::endl;
    exit(1);
  }

  // Check field size
  if(trd.size()!=4){
    std::cerr << "Error computing trd size" << std::endl;
    exit(1);
  }
  // Setting to false all before last byte (setting MD5 to false)
  md5v=0;
  trd("MD5")=md5v;
  if(trd.size()!=4){
    std::cerr << "Error computing trd size after MD5 reset" << std::endl;
    exit(1);
  }
  // Setting to false all last byte (setting PSR,SSR and AAC to false)
  trd("PSR")=false_value;
  trd("SSR")=false_value;
  trd("AAC")=false_value;
  if(trd.size()!=2){
    std::cerr << "Error computing trd size after PSR/SSR/AAC reset (size="
	      << trd.size()
	      << std::endl;
    exit(1);
  }
  return;
}

void
check_cat_01_010()
{
  intifada::Extended_Length_Data_Field *extend=new intifada::Extended_Length_Data_Field;
  extend->set_property("primary_size","1");
  extend->set_property("secondaries_size","1");
  extend->set_property("repetitive","false");

  extend->insert("TYP","bool_T",8,1,0);
  extend->insert("SIM","bool_T",7,1,0);
  extend->insert("SSR-PSR","uint8_T",5,2,0);
  extend->insert("ANT","bool_T",4,1,0);
  extend->insert("SPI","bool_T",3,1,0);
  extend->insert("RAB","bool_T",2,1,0);

  extend->insert("TST","bool_T",8,1,1);
  extend->insert("DS1-DS2","uint8_T",6,2,1);
  extend->insert("ME","bool_T",5,1,1);
  extend->insert("MI","bool_T",4,1,1);

  uint8_t stream[]={0x91,0x20};

  extend->set_stream(stream,0,2);

  intifada::bool_T typ=(*extend)("TYP");
  if(typ!=true){
    exit(1);
  }
  delete extend;
  return;
}

void check_repetitive()
{
  // Ok, let's try a extended length composed by a repetitive field
  // Sample test is based on folowing structure:
  // three bytes for first part following by 3 bytes extents
  intifada::Extended_Length_Data_Field field;
  field.set_property("primary_size","3");
    field.set_property("repetitive","true");


  field.insert("sui","uint8_T",17,8,0);
  field.insert("stn","uint16_T",2,15,0);

  intifada::uint8_T wsui=10;

  // Create sui main part
  field("sui.9")=wsui;
  intifada::uint8_T rsui=field("sui.9");

  if(rsui.get_name()!="uint8_T"){
    std::cerr << "Error getting type string" << std::endl;
    exit(1);
  }

  if(rsui != wsui)
    {
      std::cerr << "Error computing repetitive field" << std::endl;
      exit(1);
    }
  intifada::uint8_T tn=field("sui.9");
  uint8_t tn2=tn;
  if(wsui != tn2)
    {
      std::cerr << "Error computing repetitive field" << std::endl;
      exit(1);
    }

  intifada::Data_Field::Dump dump;
  // field.foreach(dump);
}

int main()
{
  // register used types
  // register_builtintypes();

  check_basic();
  check_cat_62_080();
  check_repetitive();

  //sample test:WER
//   Extended_Length_Data_Field wer(1,1);
//   field.insert("wer","uint8_T",2,7,0,true);

  // Insertion des valeurs 1,2,3,4,5
//   intifada::uint8_T wer_value;
//   for(int i=0;i<5;++i){
//     wer_value=i+1;
    // field.set_value("sui",wer_value,i);
//   }
//   if(wer.size() != 5)
//     {
//       fprintf(stderr,"wer: 5 waited, got %u\n",wer.size());
//       exit(1);
//     }

  check_cat_01_010();

  for(int i =0;i<10000;++i){
    check_basic();
    check_cat_62_080();
    check_repetitive();
    check_cat_01_010();
  }

  exit(0);
}
