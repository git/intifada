/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Properties.hxx>
#include <intifada/Path_Name.hxx>
#include <iostream>
#include <cstdlib>

class Check_Property_Relink : public Properties
  {
public:
  typedef Properties inherited;
public:
  Check_Property_Relink():inherited(),prop_(),propa_(),propb_(){
    this->register_property("prop",prop_);
    this->register_property("propa",propa_);
    this->register_property("propb",propb_);
  }
  Check_Property_Relink(const Check_Property_Relink& rhs):inherited(rhs),prop_(),propa_(),propb_(){
    this->relink_property("prop",prop_);
    this->relink_property("propa",propa_);
    this->relink_property("propb",propb_);
  }
  Check_Property_Relink& operator=(const Check_Property_Relink& rhs)
    {
      if(this!=&rhs){
        this->Properties::operator=(rhs);
        this->relink_property("prop",prop_);
        this->relink_property("propa",propa_);
        this->relink_property("propb",propb_);
      }
      return *this;
    }
private:
  Property_Bool prop_;
  Property_Int propa_;
  Property_String propb_;

  };

int main(int, char**)
  {
    int exit_status=EXIT_SUCCESS;
    do{
      // Create a properties collection
      Properties props;

      props.register_property<Property_String>("p1");
      props.register_property<Property_String>("p2");
      props.register_property<Property_Int>("p3");
      props.register_property<Property_Int>("p4");
      props.register_property<Property_Bool>("pbool");
      props.set_property("pbool","true");


      props.erase_property("p1");

      Property_Type &p2=props.find_property("p2");
      Property_Type &p3=props.find_property("p3");
      props.find_property("p4");
      p2="p2";
      if(p2!="p2"){
        std::cerr << "error mismatch value for p2" << std::endl;
        exit_status=EXIT_FAILURE;
      }
      p3="3";
      if(p3!="3"){
        std::cerr << "error mismatch value for p3 (string comparison)" << std::endl;
      }

      // Check copy constructor
      Properties props2=props;
      Property_Type &p23=props2.find_property("p3");
      if(p23!="3"){
        std::cerr << "error mismatch value for p23 (string comparison)" << std::endl;
        exit_status=EXIT_FAILURE;
      }

      Properties props3;
      // Check affectation operator
      props3=props;
      Property_Type &p232=props3.find_property("p3");
      if(p232!="3"){
        std::cerr << "error mismatch value for p232 (string comparison)" << std::endl;
        exit_status=EXIT_FAILURE;
      }

      // Create a property linked to a local variable
      Property_Int lprop;
      props3.register_property("lprop",lprop);
      lprop=12345;

      Property_Bool pbool3;
      props3.relink_property("pbool",pbool3);

      Properties props4=props3;
      props4=props3;

      Property_Int lprop2;
      props4.relink_property("lprop",lprop2);
      if(lprop2!=12345){
        std::cerr << "error mismatch value for lprop2 (type comparison)" << std::endl;
        exit_status=EXIT_FAILURE;
      }
      Property_Bool pbool;
      props4.relink_property("pbool",pbool);
      if(pbool!=true){
        std::cerr << "error mismatch value (false) for pbool (bool propagation)" << std::endl;
        exit_status=EXIT_FAILURE;
      }
      pbool=std::string("false");
      if(pbool!=false){
        std::cerr << "error mismatch value (true) for pbool (bool propagation)" << std::endl;
        exit_status=EXIT_FAILURE;
      }
    }while(0);
    int idx=0;
    do{
      Check_Property_Relink original;
      original.set_property("prop","true");
      original.set_property("propb","shit");

      Check_Property_Relink copy;//=original;
      copy=original;
      ++idx;
    }while(idx < 1000000);
    exit(exit_status);
  }
