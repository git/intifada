/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <check_datas_utils.hxx>

#include <intifada/Data.hxx>

#include <cstring>
#include <iostream>

void check_parsing_data(const uint8_t *input_stream,uint8_t stream_size,intifada::Data &d)
{
  uint8_t r=d.set_stream(input_stream,0,stream_size);
  if(r != d.size()){
    throw intifada::Parsing_Input_Length_Exception(d.size(),r);
  }
  uint8_t *result_stream=new uint8_t[r];

  uint8_t w;
  try{
    w=d.get_stream(result_stream,0,r);
  }catch(intifada::Parsing_Output_Length_Exception& ex){
    delete[] result_stream;
    throw;
  }
  if(w != r){
    delete[] result_stream;
    throw intifada::Parsing_Output_Length_Exception(r,w);
  }
  if(memcmp(result_stream,input_stream,w)!=0){
    for(int i=0;i<r; ++i) std::cerr << std::hex << static_cast<uint16_t>(input_stream[i]) << std::endl;
    for(int i=0;i<w; ++i) std::cerr << std::hex << static_cast<uint16_t>(result_stream[i]) << std::endl;
    delete[] result_stream;
    throw intifada::Parsing_Exception();
  }
  delete[] result_stream;
}

void check_scratch_data(const uint8_t *input_stream,uint8_t stream_size,intifada::Data &d)
{
  if(stream_size != d.size()){
    throw intifada::Parsing_Input_Length_Exception(d.size(),stream_size);
  }
  uint8_t *result_stream=new uint8_t[stream_size];

  uint8_t w;
  try{
    w=d.get_stream(result_stream,0,stream_size);
  }catch(intifada::Parsing_Output_Length_Exception& ex){
    delete[] result_stream;
    throw;
  }
  if(w != stream_size){
    delete[]result_stream;
    throw intifada::Parsing_Output_Length_Exception(stream_size,w);
  }
  if(memcmp(result_stream,input_stream,w)!=0){
    delete[]result_stream;
    throw intifada::Parsing_Exception();
  }
  delete[]result_stream;
}
