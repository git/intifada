/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record.hxx>
#include <intifada/Fixed_Length_Data_Field.hxx>
#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <intifada/Extended_Length_Data_Field.hxx>
#include <intifada/Integer.hxx>
#include <intifada/Condition.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <iostream>

void check_coumpound()
{
	// Compound field definition
	intifada::Record compound(1,"FOO");
	compound.set_property("compound","true");

	compound.insert(8,"CAL");
	compound.insert(7,"RDS");

	// Create a fixed length data field
	intifada::Fixed_Length_Data_Field *CAL = new intifada::Fixed_Length_Data_Field;
	CAL->set_property("size","2");

	CAL->insert("D","bool_T",16,1);
	CAL->insert("CAL","uint16_T",1,10);
	compound.insert("CAL","sample",CAL);
	// Create a Repetitive length data field
	intifada::Repetitive_Length_Data_Field *RDS = new intifada::Repetitive_Length_Data_Field;
	RDS->set_property("length","6");

	RDS->insert("DOP","uint16_T",33,16);
	RDS->insert("AMB","uint16_T",17,16);
	RDS->insert("FRQ","uint16_T",1,16);
	compound.insert("RDS","sample",RDS);
}

void create_62(intifada::Record& cat_062)
{
	cat_062.insert(1,"010");
	cat_062.insert(3,"015");
	cat_062.insert(4,"070");
	cat_062.insert(5,"105");
	cat_062.insert(6,"100");
	cat_062.insert(7,"085");

	cat_062.insert(8,"210");
	cat_062.insert(9,"060");
	cat_062.insert(10,"245");
	cat_062.insert(11,"380");
	cat_062.insert(12,"040");
	cat_062.insert(13,"080");
	cat_062.insert(14,"290");

	cat_062.insert(15,"200");
	cat_062.insert(16,"295");
	cat_062.insert(17,"136");
	cat_062.insert(18,"130");
	cat_062.insert(19,"135");
	cat_062.insert(20,"220");
	cat_062.insert(21,"390");

	cat_062.insert(22,"270");
	cat_062.insert(23,"300");
	cat_062.insert(24,"110");
	cat_062.insert(25,"120");
	cat_062.insert(26,"510");
	cat_062.insert(27,"500");
	cat_062.insert(28,"340");

	cat_062.insert(34,"RE");
	cat_062.insert(35,"SP");
}

void create_001_eurocontrol(bool xml=false)
{
	intifada::Record_Repository* rep=intifada::Record_Repository::instance();

	// Compound field definition
	if(xml==false){
		intifada::Record *r=new intifada::Record;

		rep->insert("eurocontrol","eurocontrol_V1_001");
		rep->insert(r,"plots bruts/pistés","eurocontrol_V1_001",1);
		r->set_identity("plots bruts/pistés","eurocontrol_V1_001",1);

		r->insert(1,"010");
		r->insert(2,"020");

		intifada::Condition* C1 = new intifada::Condition("020.TYP","0");
		intifada::Condition* C2 = new intifada::Condition("020.TYP","1");


		r->insert(3,"040",C1);
		r->insert(4,"070",C1);
		r->insert(5,"090",C1);
		r->insert(6,"130",C1);
		r->insert(7,"141",C1);

		r->insert(8,"050",C1);
		r->insert(9,"120",C1);
		r->insert(10,"131",C1);
		r->insert(11,"080",C1);
		r->insert(12,"100",C1);
		r->insert(13,"060",C1);
		r->insert(14,"030",C1);

		r->insert(15,"150",C1);
		r->insert(20,"SP",C1);
		r->insert(21,"RFS",C1);

		r->insert(3,"161",C2);
		r->insert(4,"040",C2);
		r->insert(5,"042",C2);
		r->insert(6,"200",C2);
		r->insert(7,"070",C2);

		r->insert(8,"090",C2);
		r->insert(9,"141",C2);
		r->insert(10,"130",C2);
		r->insert(11,"131",C2);
		r->insert(12,"120",C2);
		r->insert(13,"170",C2);
		r->insert(14,"210",C2);

		r->insert(15,"050",C2);
		r->insert(16,"080",C2);
		r->insert(17,"100",C2);
		r->insert(18,"060",C2);
		r->insert(19,"030",C2);

		r->insert(22,"150",C2);
		intifada::Fixed_Length_Data_Field *fixed=new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");

		fixed->insert("SAC","uint8_T",9,8);
		fixed->insert("SIC","uint8_T",1,8);
		r->insert("010","sample",fixed);

		intifada::Extended_Length_Data_Field *extend=new intifada::Extended_Length_Data_Field;
		extend->set_property("primary_size","1");
		extend->set_property("secondaries_size","1");
		extend->set_property("repetitive","false");

		extend->insert("TYP","bool_T",8,1,0);
		extend->insert("SIM","bool_T",7,1,0);
		extend->insert("SSR/PSR","uint8_T",5,2,0);
		extend->insert("ANT","bool_T",4,1,0);
		extend->insert("SPI","bool_T",3,1,0);
		extend->insert("RAB","bool_T",2,1,0);

		extend->insert("TST","bool_T",8,1,1);
		extend->insert("DS1/DS2","uint8_T",6,2,1);
		extend->insert("ME","bool_T",5,1,1);
		extend->insert("MI","bool_T",4,1,1);
		r->insert("020","sample",extend);

		extend=new intifada::Extended_Length_Data_Field;
		extend->set_property("primary_size","1");
		extend->set_property("repetitive","true");

		extend->insert("W/E","uint8_T",2,7,0);//repetitive
		r->insert("030","sample",extend);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","4");
		fixed->insert("RHO","uint16_T",17,16);
		fixed->insert("THETA","uint16_T",1,16);
		r->insert("040","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","4");
		fixed->insert("X-Component","int16_T",17,16);
		fixed->insert("Y-Component","int16_T",1,16);
		r->insert("042","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("V","bool_T",16,1);
		fixed->insert("G","bool_T",15,1);
		fixed->insert("L","bool_T",14,1);
		fixed->insert("Mode/2","uint16_T",1,12);
		r->insert("050","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("QA4","bool_T",12,1);
		fixed->insert("QA2","bool_T",11,1);
		fixed->insert("QA1","bool_T",10,1);
		fixed->insert("QB4","bool_T",9,1);
		fixed->insert("QB2","bool_T",8,1);
		fixed->insert("QB1","bool_T",7,1);
		fixed->insert("QC4","bool_T",6,1);
		fixed->insert("QC2","bool_T",5,1);
		fixed->insert("QC1","bool_T",4,1);
		fixed->insert("QD4","bool_T",3,1);
		fixed->insert("QD2","bool_T",2,1);
		fixed->insert("QD1","bool_T",1,1);
		r->insert("060","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("V","bool_T",16,1);
		fixed->insert("G","bool_T",15,1);
		fixed->insert("L","bool_T",14,1);
		fixed->insert("Mode/3A","uint16_T",1,12);
		r->insert("070","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("QA4","bool_T",12,1);
		fixed->insert("QA2","bool_T",11,1);
		fixed->insert("QA1","bool_T",10,1);
		fixed->insert("QB4","bool_T",9,1);
		fixed->insert("QB2","bool_T",8,1);
		fixed->insert("QB1","bool_T",7,1);
		fixed->insert("QC4","bool_T",6,1);
		fixed->insert("QC2","bool_T",5,1);
		fixed->insert("QC1","bool_T",4,1);
		fixed->insert("QD4","bool_T",3,1);
		fixed->insert("QD2","bool_T",2,1);
		fixed->insert("QD1","bool_T",1,1);
		r->insert("080","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("V","bool_T",16,1);
		fixed->insert("G","bool_T",15,1);
		fixed->insert("Mode-C HEIGHT","uint16_T",1,14);
		r->insert("090","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","4");
		fixed->insert("V","bool_T",32,1);
		fixed->insert("G","bool_T",31,1);
		fixed->insert("Mode/C GRAY","uint16_T",17,12);
		fixed->insert("QC1","bool_T",12,1);
		fixed->insert("QA1","bool_T",11,1);
		fixed->insert("QC2","bool_T",10,1);
		fixed->insert("QA2","bool_T",9,1);
		fixed->insert("QC4","bool_T",8,1);
		fixed->insert("QA4","bool_T",7,1);
		fixed->insert("QB1","bool_T",6,1);
		fixed->insert("QD1","bool_T",5,1);
		fixed->insert("QB2","bool_T",4,1);
		fixed->insert("QD2","bool_T",3,1);
		fixed->insert("QB4","bool_T",2,1);
		fixed->insert("QD4","bool_T",1,1);
		r->insert("100","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","1");
		fixed->insert("DOPPLER SPEED","uint8_T",1,8);
		r->insert("120","sample",fixed);

		extend=new intifada::Extended_Length_Data_Field;
		extend->set_property("primary_size","1");
		extend->set_property("repetitive","true");

		extend->insert("RPC","uint8_T",2,7,0);//repetitive
		r->insert("130","sample",extend);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","1");
		fixed->insert("POWER","uint8_T",1,8);
		r->insert("131","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("TTOD","uint16_T",1,16);
		r->insert("141","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","1");
		fixed->insert("XA","bool_T",8,1);
		fixed->insert("XC","bool_T",6,1);
		fixed->insert("X2","bool_T",3,1);
		r->insert("150","sample",fixed);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","2");
		fixed->insert("TRACK PLOT NUMBER","uint16_T",1,16);
		r->insert("161","sample",fixed);

		extend=new intifada::Extended_Length_Data_Field;
		extend->set_property("primary_size","1");
		extend->set_property("secondaries_size","1");
		extend->set_property("repetitive","false");

		extend->insert("CON","bool_T",8,1,0);
		extend->insert("RAD","bool_T",7,1,0);
		extend->insert("MAN","bool_T",6,1,0);
		extend->insert("DOU","bool_T",5,1,0);
		extend->insert("RDPC","bool_T",4,1,0);
		extend->insert("GHO","bool_T",2,1,0);

		extend->insert("TRE","bool_T",8,1,1);
		r->insert("170","sample",extend);

		fixed = new intifada::Fixed_Length_Data_Field;
		fixed->set_property("size","4");
		fixed->insert("CALCULATED GROUNDSPEED","uint16_T",17,16);
		fixed->insert("CALCULATED HEADING","uint16_T",1,16);
		r->insert("200","sample",fixed);

		extend=new intifada::Extended_Length_Data_Field;
		extend->set_property("primary_size","1");
		extend->set_property("repetitive","true");

		extend->insert("QUALITY","uint8_T",2,7,0);//repetitive
		r->insert("210","sample",extend);
	}else{

		std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
		std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
		intifada::Configuration_XML_Reader p(xml_file,dtd_file);
		int status = p.open();
		if (status){
			std::cerr << "Error opening xml parser" << std::endl;
			exit(1);
		}


		/* oto */
		p.parse(intifada::Record_Repository::instance());
		p.release();
	}
}

void check_from_scratch(intifada::Record* cat_001)
{
	// Track sample
	// 11110001 10000000 (0xf1,0x80)
	// 010 020 161 040 090
	// 0x08,0x08 (0x08,0x08)
	// 10010001 00100000 (0x91,0x20)
	// 00000001 161 (0x01,0xa1)
	// 0 40 0 40 (0x00,0x28,0x00,0x28)
	// 0 90 (0x00,0x5a)
	uint8_t track_sample[]={
			0xf1,0x80,
			0x08,0x80,			// O1O 1 SIC/SAC
			0x91,0x20,			// 020 2
			0x01,0xa1,			// 161 3
			0x00,0x28,0x00,0x28,	// 040 4
			0x00,0x5a			// 090 8
	}; // 14
	uint8_t track_size=14;
	// Plot sample
	// 11100001 10000000 (0xe1,0x80)
	// 010 020 040 050
	// 08 08 (0x08,0x08)
	// 00010001 00100000 (0x11,0x20)
	// 0 40 0 40 (0x00,0x28,0x00,0x28)
	// 0 50 (0x00,0x32)

	//   uint8_t plot_sample[]={
	//     0xe1,0x80,
	//     0x08,0x08,			// 010 1
	//     0x11,0x20,			// 020 2
	//     0x00,0x28,0x00,0x28,	// 040 3
	//     0x00,0x32			// 050 8
	//   }; // 12
	//   uint8_t plot_size=12;

	// cat_001->set_stream(plot_sample,plot_size);
	cat_001->set_stream(track_sample,0,track_size);
	if(cat_001->size()!=track_size){
		std::cerr << "Error Record::size() get " << cat_001->size()
    		  << " wanted " << static_cast<uint16_t>(track_size)
    		  << std::endl;
	}
	intifada::uint8_T sic=(*cat_001)("010.SIC");
	if(sic != (uint8_t)0x80){
		std::cerr << "Error Record::get_value()" << std::endl;
		exit(1);
	}
	// Check operator() access
	sic=0;
	sic=(*cat_001)("010.SIC");
	if(sic!=(uint8_t)0x80)
	{
		std::cerr << "Error Record::operator()()" << std::endl;
		exit(EXIT_FAILURE);
	}

	bool p;
	p=cat_001->get_presence(intifada::Path_Name("010"));

	if(p!=true){
		std::cerr << "Error Record::get_presence(010)" << std::endl;
		exit(1);
	}


}

void load_repository(bool xml)
{
	intifada::Record_Repository* rep=intifada::Record_Repository::instance();
	rep->release();
	create_001_eurocontrol(xml);
}

void check(bool half)
{
	intifada::Record_Repository* rep=intifada::Record_Repository::instance();

	intifada::Record *r=NULL;
	r=rep->get_clone("eurocontrol",1);
	if(r==NULL){
		std::cerr << "Error NULL record" << std::endl;
		exit(1);
	}
	check_from_scratch(r);
	if(half==true)
	{
		intifada::Record *rc=new intifada::Record;
		rc->clone(r);
		check_from_scratch(rc);

		// Check if pointers are different pointer sense
		intifada::Type &rsic=r->get_value("010.SIC");
		intifada::Type &rcsic=rc->get_value("010.SIC");
		if(&rsic==&rcsic)
		{
			std::cerr << "Error Rewrite code, data pointers are same between records" << std::endl;
			exit(EXIT_FAILURE);
		}
		delete rc;
	}
	delete r;
}

int main()
{
	// register used types
	// register_builtintypes();
	bool xml=true;
	//clog.insert_mask("Configuration_XML_Reader");
	//clog.insert_mask("Configuration_Reader");
	//clog.insert_mask("Memory_Allocation");
	load_repository(xml);

	for(int i=0;i<1000;++i)
	{
		bool half=true;
		check(half);

		//load_repository(true);
		//check();
	}
	intifada::Record_Repository::instance()->release();
}
