/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record.hxx>
#include <intifada/Fixed_Length_Data_Field.hxx>
#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <intifada/Extended_Length_Data_Field.hxx>
#include <intifada/Integer.hxx>
#include <intifada/Condition.hxx>

void create_record(intifada::Record *record)
  {
    // Compound field definition
    intifada::Record *compound = new intifada::Record(1,intifada::Path_Name("130"));
    compound->set_property("compound","true");

    compound->insert(8,"SLR");
    compound->insert(7,"SRR");
    compound->insert(6,"SAM");
    compound->insert(5,"PRL");
    compound->insert(4,"PAM");
    compound->insert(3,"RPD");
    compound->insert(2,"APD");

    intifada::Fixed_Length_Data_Field *fixed;
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("SLR","uint8_T",1,8);
    compound->insert("SLR","slr",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("SRR","uint8_T",1,8);
    compound->insert("SRR","srr",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("SAM","int8_T",1,8);
    compound->insert("SAM","sam",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("PRL","uint8_T",1,8);
    compound->insert("PRL","prl",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("PAM","uint8_T",1,8);
    compound->insert("PAM","pam",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("RPD","uint8_T",1,8);
    compound->insert("RPD","rpd",fixed);
    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("APD","uint8_T",1,8);
    compound->insert("APD","slr",fixed);

    fixed=new intifada::Fixed_Length_Data_Field;
    fixed->set_property("size","1");
    fixed->insert("YOU","uint8_T",1,8);
    // Insert in a record
    record->insert(8,"130");
    record->insert("130","compound sample",compound);

    record->insert(9,"DO");
    record->insert("DO","foo",fixed);
  }

/// Compound definition
/**
 * Compound FOO
 * FOO: CAL|RDS|0|0|0|0|0|0
 * FOO.CAL(fixed length):D00000CC CCCCCCCC D:D C:CAL
 * FOO.RDS(repetitive):DDDDDDDD DDDDDDDD AAAAAAAA AAAAAAAA FFFFFFFF FFFFFFFF
 *
 * Record FSPEC=00000000 000C0000
 * COMP=FOO
 */
void check_compound_from_values()
  {
    intifada::Record *originalrecord=new intifada::Record;
    create_record(originalrecord);

    intifada::Record *record=new intifada::Record;
    record->clone(originalrecord);

    // Setting SRR value
    intifada::uint8_T d=0x1;
    (*record)("130.SRR.SRR")=d;

    bool presence=record->get_presence("130.SRR");
    if(presence!=false){
      std::cerr << "130.SRR" << !presence << " expected" << std::endl;
    }
    record->set_presence("130.SRR",true);
    presence=record->get_presence("130.SRR");
    if(presence!=true){
      std::cerr << "130.SRR " << !presence << " expected" << std::endl;
    }
    presence=record->get_presence("130");
    if(presence!=true){
      std::cerr << "130 " << !presence << " expected" << std::endl;
    }

    // Setting SAM value
    intifada::int8_T sd=-80;
    (*record)("130.SAM.SAM")=sd;
    record->set_presence("130.SAM",true);
    uint8_t expectedStream[]={0x1,0x80,0x60, 0x01,0xB0};
    intifada::Stream::size_type expectedStreamSize=5;

    intifada::Stream::size_type est=record->size();
    if(expectedStreamSize!=est){
      std::cout << "check_compound_from_value:size error" << std::endl;
      exit(EXIT_FAILURE);
    }
    uint8_t *t=new uint8_t[est];
    intifada::Stream::size_type sst = record->get_stream(t,0,est);
    if(est!=sst){
      std::cout << "check_compound_from_value:size error" << std::endl;
      exit(EXIT_FAILURE);
    }
    for(intifada::Stream::size_type idx=0;idx < est;++idx){
      if(t[idx]!=expectedStream[idx]){
        std::cout << "check_compound_from_values:stream error" << std::endl;
        exit(1);
      }
    }
    delete t;
    delete record;
  }

void check_compound_from_stream()
  {
    intifada::Record *originalrecord=new intifada::Record;
    create_record(originalrecord);

    intifada::Record *record=new intifada::Record;
    record->clone(originalrecord);

    // fspec: 01 C0 ->8:1(130) 9:1(DO)
    // 130: 40 17 -> 7:1(SRR) SRR=17
    // DO: 17
    uint8_t s[]={0x1,0xc0,0x40,0x17,0x17};
    uint8_t s2[]={0x1,0x40,0x17};
    record->set_stream(s,0,5);

    intifada::Stream::size_type est=record->size();
    uint8_t *t=new uint8_t[est];
    intifada::Stream::size_type st=record->get_stream(t,0,est);
    if(est!=5){
      std::cout << "check_compound_from_stream:size error" << std::endl;
      exit(1);
    }
    for(intifada::Stream::size_type idx=0;idx < est;++idx){
      if(t[idx]!=s[idx]){
        std::cout << "check_compound_from_stream:stream error" << std::endl;
        exit(1);
      }
    }
    intifada::uint8_T d=0x16;
    (*record)("130.SRR.SRR")=d;
    // clog.insert_mask("Field_Specification_Data");
    //record->set_presence("130.SRR",false);
    record->set_presence("130",false);
    // expected
    est=record->size();
    st=record->get_stream(t,0,est);
    if(st!=est){
      std::cout << "check_compound_from_stream:size error (2) ("<<est<< " expected, got " << st << ")" << std::endl;
      exit(1);
    }
    for(intifada::Stream::size_type idx=0;idx < st;++idx){
      if(t[idx]!=s2[idx]){
        std::cout << "check_compound_from_stream:stream error (2)" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
    delete t;
    delete record;
  }


int main()
  {
    // clog.insert_mask("Record");
	try{
    check_compound_from_values();
    check_compound_from_stream();
	}catch(intifada::Description_Field_Unknow_Exception& ex)
	{
		std::cerr << "Champ inconnu:"<<ex.get_name()<<std::endl;
	}
    exit(EXIT_SUCCESS);
  }
