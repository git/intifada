/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <intifada/Integer.hxx>
#include <intifada/String_Type.hxx>

template<typename T>
int check_stream2stream(
		intifada::Stream& b
		,int shift
		,int size
		,typename T::value_type expected
		,uint16_t check_number)
{
	T result;
	result.set_from_stream(b.rbegin(),shift,size);
	//  printf("begin write\n");
	result.to_stream(b.rbegin(),shift,size);

	if(result != expected){
		std::cerr << "Acquisition error, check " << check_number
				<< ": 0x" << std::hex << expected << " expected, got 0x" << result << std::endl;
		exit(1);
	}
	return 0;

}

int main()
{
	// check a standard int casts
	uint8_t ref1=97;
	intifada::uint8_T c1=ref1;
	if(c1!=intifada::uint8_T(97))
	{
		std::cerr << "Error on type<T>!=T" << std::endl;
		exit(1);
	}
	if(ref1!=c1)
	{
		std::cerr << "Error on type<T>!=T" << std::endl;
		exit(1);
	}


	// preliminary check on octal
	uint16_t dv=7562;
	intifada::oct_T ov=dv;
	if(ov!=dv)
	{
		std::cerr << "Error on octal type" << std::endl;
		exit(1);
	}
	std::string ovs=ov.get_to_string();
	if(ovs!="7562")
	{
		std::cerr << "Error on octal type (string)" << std::endl;
		exit(1);
	}

	intifada::Stream buf(14);
	// 43333333 33322222 22222111 11111110 00000000
	// 09876543 21098765 43210987 65432109 87654321
	// 00000001 00100000 00100001 00110100 00100011
	buf[0]=0x01;
	buf[1]=0x20;
	buf[2]=0x21;
	buf[3]=0x34;
	buf[4]=0x23;
	buf[5]=0x01;
	buf[6]=0x20;
	buf[7]=0x21;
	buf[8]=0x34;
	buf[9]=0x23;
	buf[10]=0xF0;
	buf[11]=0xFF;
	buf[12]=0x71;
	buf[13]=0xFC;

	// OxO120 2134 23O1 2021 3423

	// Transfert of size=6, start=1, expected=35 (0x23)
	check_stream2stream<intifada::uint16_T>(buf,33,6,35,1);
	//  for(int i =0;i<=4;++i){printf("0x%02x ",buf[i]);}printf("\n");
	//   intifada::uint16_T r;
	//   r=257;
	//   r.set_value(buf.rbegin(),1,10);

	// transfert of size=7, start=2, expected=17 (0x11)
	check_stream2stream<intifada::uint16_T>(buf,34,7,17,2);

	// transfert of size=8,start=9, expected=52 (0x34)
	// 00100001 00110100 00100011
	check_stream2stream<intifada::uint16_T>(buf,41,8,52,3);

	// transfert of size=8,start=6, expected=161 (0xa1)
	// 001 10100  001 00011
	// 0x34       0x23
	// 000 10100  001 00011
	// 0x14       0x23
	check_stream2stream<intifada::uint16_T>(buf,38,8,161,4);

	// transfert of size=9,start=6, expected=417 (0x1a1)
	check_stream2stream<intifada::uint16_T>(buf,38,9,417,5);

	// transfert of size=16,start=1 expected=13347 (0x3423)
	check_stream2stream<intifada::uint16_T>(buf,33,16,13347,6);

	// transfert of size=25,start=6 expected=16845217 (0x10109a1)
	check_stream2stream<intifada::uint32_T>(buf,38,25,16845217,7);

	// transfert of size=1,start=20 expected=false
	check_stream2stream<intifada::bool_T>(buf,52,1,false,8);

	// transfert of size=1,start=14 expected=true
	check_stream2stream<intifada::bool_T>(buf,18,1,true,9);

	// transfert of size=64,start=9 expected=0x20213423O1202134
	//check_stream2stream<intifada::uint64_T>(buf,9,64,0x2021342301202134,9);
	uint64_t ex=0x01202134;
	check_stream2stream<intifada::uint64_T>(buf,41,32,ex,10);

	// transfert of size=25,start=6 expected=16845217 (0x10109a1)
	check_stream2stream<intifada::int32_T>(buf,1,32,-251694596,11);

	// transfert of size
	buf.resize(2);
	buf[0]=0x0f;
	buf[1]=0x54;
	check_stream2stream<intifada::oct_T>(buf,1,12,3924,12);
	// Check types
	intifada::int32_T num=5;
	num+=1;
	if(num!=6)
	{
		exit(EXIT_FAILURE);
	}
	intifada::int32_T n=num+3;
	if(n!=9)
	{
		exit(EXIT_FAILURE);
	}
	intifada::String_Type s;
	s="foo";
	if(s!="foo")
	{
		std::cerr << "string copy fail" << std::endl;
		exit(EXIT_FAILURE);
	}
	intifada::String_Type ds=s;
	if(ds!=s)
	{
		std::cerr << "string constructor copy fail" << std::endl;
		exit(EXIT_FAILURE);
	}

	// check type affectation from Type
	intifada::uint32_T original=65456;
	const intifada::Type& poriginal=original;
	intifada::uint32_T copy=0;
	//copy=dynamic_cast<intifada::uint32_T&>(*poriginal);
	copy=poriginal;
	if(copy!=original)
	{
		exit(EXIT_FAILURE);
	}

	exit(0);
}
