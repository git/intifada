/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Byte_Manipulation.hxx>
#include <cstdlib>
#include <iostream>
int
main()
{

  // walk throught the entire byte
  uint8_t v=0;
  do{
    for(uint8_t size=1;size <= 8;++size){
      for(uint8_t offset=0;offset<=8-size;++offset){
	uint8_t get_byte=intifada::Byte_Manipulation::get_byte_part(v,size,offset);
	uint8_t set_byte=0;
	intifada::Byte_Manipulation::set_byte_part(set_byte,get_byte,size,offset);
	if((v & intifada::Byte_Manipulation::mask(size,offset))!= set_byte){
	  std::cerr << "error (" << uint16_t(v) << "," << uint16_t(size) << ","
		    << uint16_t(offset) << ") " << uint16_t(get_byte)
		    << " -> " << uint16_t(set_byte) << std::endl;
	  exit(1);
	}
      }
    }
    ++v;
  }while(v!=0);

  // check if target byte is correctly set
  // source0= 11101111 = 0xef
  // source1= 11010111 = 0xd7
  uint8_t set_byte=0xff;
  intifada::Byte_Manipulation::set_byte_part(set_byte,intifada::Byte_Manipulation::get_byte_part(0xef,3,3),3,3);
  intifada::Byte_Manipulation::set_byte_part(set_byte,intifada::Byte_Manipulation::get_byte_part(0xd7,3,3),3,3);
  if(set_byte != 0xd7){
    std::cerr << "Error bit position" << std::endl;
    exit(1);
  }

  // check if computation of entire parts is good
  // size=8 offset=5 -> return=5 size=3
  uint8_t size=8;
  uint8_t p=intifada::Byte_Manipulation::get_access(size,5,8);
  if( (p != 5)||
      (size !=3)){
    std::cerr << "Error calculating entire parts (test 1) ret=" << uint16_t(p)
	      << " size=" << uint16_t(size) << std::endl;
    exit(1);
  }
  // size=8 offset=0 ->return=0 size=8
  size=8;

  p = intifada::Byte_Manipulation::get_access(size,0,8);
  if( (p != 0)||
      (size !=8)){
    std::cerr << "Error calculating entire parts (test 2) ret=" << uint16_t(p)
	      << " size=" << uint16_t(size) << std::endl;
    exit(1);
  }

  // size = 3 shift = 0 max write = 5 -> return=0, size = 3
  size = 3;
  p = intifada::Byte_Manipulation::get_access(size,0,5);
  if( (p != 0)||
      (size !=3)){
    std::cerr << "Error calculating entire parts (test 3) ret=" << uint16_t(p)
	      << " size=" << uint16_t(size) << std::endl;
    exit(1);
  }
  exit(0);
}
