/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

// Checking std::map and unordered_map

#include <intifada/Map.hxx>
#include <intifada/Path_Name.hxx>
#include <string>
#include <sstream>
#include <iostream>
#include <stdlib.h>

typedef intifada::Path_Name key_type;
//typedef std::string key_type;
typedef intifada::Unordered_Map<key_type,std::string> string_map;
//typedef intifada::Map<key_type,std::string> string_map;
typedef string_map::iterator string_iterator_map;

int main()
  {
    std::string key;
    int imax=100000;
    string_map m;
    std::string prefix="a map item .";
    for(int i=0;i<imax;++i)
      {
        std::ostringstream os;
        os << prefix;
        os << i;
        key_type key=os.str();

        m.insert(std::make_pair(key,os.str()));

      }
    for(int i=0;i<imax;++i)
      {
        std::ostringstream os;
        os << prefix;
        os << i;
        string_iterator_map it = m.find(os.str());
        if((*it).second!=os.str())
          {
            std::cout << "assert error" << std::endl;
            exit(1);
          }
        exit(0);
      }
  }
