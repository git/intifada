/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record_Iterator_List.hxx>
#include <intifada/Record_Iterator.hxx>
#include <cstdlib>
#include <iostream>
class My_Iterator : public intifada::Record_Iterator
{
public:
  My_Iterator():intifada::Record_Iterator(){}
  virtual int operator()(
			 record_type& i,
			 const intifada::Message::block_list_size_t& b,
			 const intifada::Block::record_list_size_t& r);
};

class My_Const_Iterator : public intifada::Record_Const_Iterator
{
public:
  My_Const_Iterator():intifada::Record_Const_Iterator(){}
  virtual int operator()(
			 record_type& i,
			 const intifada::Message::block_list_size_t& b,
			 const intifada::Block::record_list_size_t& r);
};

int
My_Iterator::operator()(
			record_type&,
			const intifada::Message::block_list_size_t&,
			const intifada::Block::record_list_size_t&)
{
  return 0;
}

int
My_Const_Iterator::operator()(
			record_type& ,
			const intifada::Message::block_list_size_t&,
			const intifada::Block::record_list_size_t&)
{
  return 0;
}

int
main()
{
  intifada::Record_Iterator_List rci;

  My_Iterator it1_1;
  My_Iterator it1_2;
  My_Iterator it2_1;
  My_Iterator it2_2;
  My_Iterator it3_1;

  int r=rci.register_iterator(it1_1,1);
  if(r!=0){
    std::cerr << "Error inserting iterator" << std::endl;
    exit(1);
  }
  r=rci.register_iterator(it1_2,1);
  if(r!=0){
    std::cerr << "Error inserting iterator" << std::endl;
    exit(1);
  }
  r=rci.register_iterator(it2_1,2);
  if(r!=0){
    std::cerr << "Error inserting iterator" << std::endl;
    exit(1);
  }
  r=rci.register_iterator(it2_2,2);
  if(r!=0){
    std::cerr << "Error inserting iterator" << std::endl;
    exit(1);
  }
  r=rci.register_iterator(it3_1,3);
  if(r!=0){
    std::cerr << "Error inserting iterator" << std::endl;
    exit(1);
  }
//  rci.apply(4,NULL,0,0,false);

}
