/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record_Repository.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Block.hxx>
#include <iostream>

// Track sample
// 11110001 10000000 (0xf1,0x80)
// 010 020 161 040 090
// 0x08,0x08 (0x08,0x08)
// 10010001 00100000 (0x91,0x20)
// 00000001 161 (0x01,0xa1)
// 0 40 0 40 (0x00,0x28,0x00,0x28)
// 0 90 (0x00,0x5a)

// Plot sample
// 11100001 10000000 (0xe1,0x80)
// 010 020 040 050
// 08 08 (0x08,0x08)
// 00010001 00100000 (0x11,0x20)
// 0 40 0 40 (0x00,0x28,0x00,0x28)
// 0 50 (0x00,0x32)


uint8_t block_001[]={
		0x01,0x00,0x1d,
		0xf1,0x80,
		0x08,0x80,			// O1O 1 SIC/SAC
		0x91,0x20,			// 020 2
		0x01,0xa1,			// 161 3
		0x00,0x28,0x00,0x28,		// 040 4
		0x00,0x5a,			// 090 8

		0xe1,0x80,
		0x08,0x08,			// 010 1
		0x11,0x20,			// 020 2
		0x00,0x28,0x00,0x28,		// 040 3
		0x00,0x32			// 050 8
};

uint16_t block_001_size=0x1d;

int
main()
{
	intifada::Record_Repository* rep=intifada::Record_Repository::instance();

	std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
	std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
	intifada::Configuration_XML_Reader p(xml_file,dtd_file);
	p.open();

	/* oto */
	int readed=p.parse(intifada::Record_Repository::instance());

	if(readed==-1){
		std::cerr << "Error Invalid asterix description" << std::endl;
	}

	intifada::Block block(rep,"eurocontrol");
	intifada::Stream::size_type bs=block.set_stream(block_001,0,0x1d);

	if(bs==0){
		std::cerr << "set_stream::can't set stream" << std::endl;
	}

	uint8_t copy_block_001[255];
	intifada::Stream::size_type r=block.get_stream(copy_block_001,0,255);
	if(r!=block_001_size){
		std::cerr << "Error returning size (" << block_001_size
				<< "expected, " << r << "obtained)"
				<< std::endl;
	}

	intifada::Block block_001(rep,"eurocontrol",1);
	intifada::Record *rec;
	rec=block_001.get_new_record();
	exit(0);
}
