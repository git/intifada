/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/inttypes.h>
#include <intifada/Record_Repository.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Message.hxx>
#include <intifada/Block.hxx>
#include <intifada/Record.hxx>
#include <intifada/Integer.hxx>
#include <intifada/Extended_Length_Data_Field.hxx>
#include <iostream>

int check_size(
		intifada::Record &rec
)
{

	rec.set_presence("020",true);

	int size=rec.size();
	if(size!=1)
	{
		std::cerr << "extended calcul size wrong (presence true, all zero) got "<< size << std::endl;
		return -1;
	}

	rec("020.TST")=intifada::bool_T(true);
	rec("020.SIM")=intifada::bool_T(true);

	//rec("080.MON")=intifada::bool_T(true);
	//   rec("080.FPC")=intifada::bool_T(true);
	//   rec("080.PSR")=intifada::bool_T(true);
	//   rec("080.SSR")=intifada::bool_T(true);
	//   rec("080.AAC")=intifada::bool_T(true);

	size=rec.size();
	if(size!=3)
	{
		std::cerr << "extended calcul size wrong (presence true, first and second part set) got "<< size << std::endl;
		return -1;
	}

	rec("020.TST")=intifada::bool_T(false);
	rec("020.SIM")=intifada::bool_T(false);

	size=rec.size();
	if(size!=1)
	{
		std::cerr << "extended calcul size wrong (presence true, first and second part reseted to zero) got "<< size << std::endl;
		return -1;
	}

	return 0;
}

int
main()
{

	intifada::Record_Repository* rep=intifada::Record_Repository::instance();
	std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
	std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
	intifada::Configuration_XML_Reader p(xml_file,dtd_file);
	p.open();
	p.parse(rep);

	try{

		intifada::Message message(rep,"eurocontrol");
		uint8_t cat=1;

		intifada::Record* record=
				intifada::Record_Repository::instance()->get_clone("eurocontrol",cat);
		check_size(*record);

	}catch(intifada::Description_Field_Unknow_Exception & ex){
		std::cerr << "Exception:<" << ex.get_name() << ">" << std::endl;
	}


	exit(0);
}
