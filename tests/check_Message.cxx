/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/inttypes.h>
#include <intifada/Record_Repository.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Message.hxx>
#include <intifada/Block.hxx>
#include <intifada/Record.hxx>
#include <intifada/Integer.hxx>
#include <intifada/Record_Iterator_List.hxx>
#include <iostream>

class Record_Iterator_Check:public intifada::Record_Iterator
{
	/// virtual call operator
	/**
	 *
	 */
	virtual int operator()(uint8_t)
	{
		//std::cout << "new block " << static_cast<int>(c);
		return 0;
	}

	/// virtual Call operator
	/**
	 * Called when a record is encountered
	 *
	 * \param i record pointer
	 * \param b block id in message (first block in message id is 0)
	 * \param r record id in block (first record in block is 0)
	 */
	virtual int operator()(
			record_type&,
			const intifada::Message::block_list_size_t&,
			const intifada::Block::record_list_size_t&)
	{
		//std::cout << "\t(" << static_cast<int>(b) << "," <<  static_cast<int>(r) << ")";
		//i->foreach(dfd_);
		//std::cout << std::endl;

		return 0;
	}

private:
	intifada::Data_Field::Dump dfd_;
};

uint8_t message_001[]={
		0x01,0x00,0x1d,
		0xf1,0x80,
		0x08,0x80,			// 010 1 SIC/SAC
		0x91,0x20,			// 020 2
		0x01,0xa1,			// 161 3
		0x00,0x28,0x00,0x28,		// 040 4
		0x00,0x5a,			// 090 8

		0xe1,0x80,
		0x08,0x08,			// 010 1
		0x11,0x20,			// 020 2
		0x00,0x28,0x00,0x28,		// 040 3
		0x00,0x32,			// 050 8

		0x01,0x00,0x1d,
		0xf1,0x80,
		0x08,0x81,			// 010 1 SIC/SAC
		0x91,0x20,			// 020 2
		0x01,0xa1,			// 161 3
		0x00,0x28,0x00,0x28,		// 040 4
		0x00,0x5a,			// 090 8

		0xe1,0x80,
		0x08,0x82,			// 010 1
		0x11,0x20,			// 020 2
		0x00,0x28,0x00,0x28,		// 040 3
		0x00,0x32			// 050 8
};

uint16_t message_001_size=0x1d*2;

int fill_record(
		intifada::Record *r
		,uint8_t sicp,uint8_t sacp
		,const intifada::bool_T& typ
		,const intifada::uint16_T& rho,const intifada::uint16_T& theta
)
{
	intifada::uint8_T sic=sicp;
	intifada::uint8_T sac=sacp;

	// check direct setting throught operator()
	(*r)("010.SIC")=sic;
	intifada::uint8_T vsic=(*r)("010.SIC");
	if(vsic!=sicp)
	{
		std::cerr << "Error using Record::operator()() expected:"<<(int)sicp << " got:"<< (int)vsic << std::endl;
		exit(EXIT_FAILURE);
	}

	(*r)("010.SIC")=sic;
	(*r)("010.SAC")=sac;
	r->set_presence(intifada::Path_Name("010"),true);

	(*r)("020.TYP")=typ;
	intifada::bool_T tst=true;
	(*r)("020.TST")=tst;
	r->set_presence(intifada::Path_Name("020"),true);


	intifada::uint8_T wer=7;
	(*r)("030.W/E.0")=wer;
	wer=6;
	(*r)("030.W/E.1")=wer;
	r->set_presence(intifada::Path_Name("030"),true);

	(*r)("040.RHO")=rho;
	(*r)("040.THETA")=theta;
	r->set_presence(intifada::Path_Name("040"),true);

	if(typ==true)
	{
		intifada::oct_T o=7541;
		intifada::bool_T l=true;
		(*r)("070.Mode-3/A")=o;
		(*r)("070.L")=l;
		r->set_presence(intifada::Path_Name("070"),true);
		intifada::int16_T f;
		(*r)("090.Flight Level")=f;
		(*r)("090.G")=l;
		r->set_presence(intifada::Path_Name("090"),true);
	}
	return 0;
}

int
main()
{

	intifada::Record_Repository* rep=intifada::Record_Repository::instance();
	std::string xml_file=std::string(INTIFADA_TOP_SRCDIR)+"/etc/intifada/asterix.xml";
	std::string dtd_file=std::string(INTIFADA_TOP_SRCDIR)+"/share/intifada/dtd/asterix.dtd";
	intifada::Configuration_XML_Reader p(xml_file,dtd_file);
	p.open();
	p.parse(rep);
#if 0
	intifada::Message msg(rep,"eurocontrol");
	intifada::Stream::size_type s = msg.set_stream(message_001,message_001_size);

	uint8_t *t=new uint8_t[s];
	intifada::Stream::size_type st=msg.get_stream(t,s);
	if(s!=st){
		std::cerr << "Error computing size" << std::endl;
		exit(1);
	}
	for(unsigned int i=0;i<s;++i){
		if(message_001[i]!=t[i]){
			std::cerr << "Error ["<< i <<"], 0x"
					<< static_cast<uint16_t>(message_001[i]) << "->0x" << static_cast<uint16_t>(t[i])
					<< std::endl;
			exit(1);
		}
	}
	intifada::bool_T typ;
	intifada::Block*b=msg.get(0);
	intifada::Record*r=b->get(0);
	r->get_value("020.TYP",typ);
#endif

	try{

		intifada::Message message(rep,"eurocontrol");
		intifada::Record_Iterator_List it_list;
		Record_Iterator_Check it;
		it_list.register_iterator(it);
		for(int i=0;i<10;++i){
			intifada::Block *block=new intifada::Block(rep,"eurocontrol",1);
			message.push_back(block);
			for(int j=0;j<255;++j){
				intifada::Record* record=
						intifada::Record_Repository::instance()->get_clone("eurocontrol",1);
				fill_record(record,i,j,true,i*2,i);
				block->push_back(record);

			}
			message.foreach(it_list);
		}
	}catch(intifada::Description_Field_Unknow_Exception & ex){
		std::cerr << "<" << ex.get_name() << ">" << std::endl;
	}
	exit(0);
}
