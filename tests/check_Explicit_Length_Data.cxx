/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Explicit_Stream.hxx>

#include <intifada/Parsing_Input_Length_Error.hxx>
#include <intifada/Parsing_Output_Length_Error.hxx>

#include <intifada/check_datas_utils.hxx>
#include <iostream>

int main()
{
  uint8_t stream[]=
    {
      0x05,
      0x06,0x01,0x01,0x06
    };


  Explicit_Stream data_good;
  try{
    check_parsing_data(stream,5,data_good);
  }catch(Parsing_Input_Length_Error&ex){
    std::cerr << "Error, Invalid reading size (" << ex.get_waited()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;
    exit(1);
  }catch(Parsing_Output_Length_Error&ex){
    std::cerr << "Error, Invalid writing size (" << ex.get_waited()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;
    exit(1);
  }catch(Parsing_Error&ex){
    std::cerr << "Reencoding error" << std::endl;
    exit(1);
  }
  exit(0);
  Explicit_Stream data_bad;
  try{
    check_parsing_data(stream,4,data_bad);
  }catch(Parsing_Input_Length_Error&ex){
    std::cerr << "Error, Invalid reading size (" << ex.get_waited()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;
   exit(1);
  }catch(Parsing_Output_Length_Error&ex){
    std::cerr << "Error, Invalid writing size (" << ex.get_waited()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;
    exit(1);
  }catch(Parsing_Error&ex){
    std::cerr << "Reencoding error" << std::endl;
    exit(1);
  }

  exit(0);
}
