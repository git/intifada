/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Fixed_Length_Data_Field.hxx>

#include <intifada/Exception.hxx>
#include <check_datas_utils.hxx>
#include <intifada/Integer.hxx>
#include <iostream>

void check_basic()
{
  uint8_t stream[]={0x01,0x02,0x03,0x04};


  intifada::Fixed_Length_Data_Field fixed_data_good;
  fixed_data_good.set_property("size","4");
  try{
    check_parsing_data(stream,4,fixed_data_good);
  }catch(intifada::Parsing_Input_Length_Exception&ex){
    std::cerr << "throw Erreur, invalid read size ("
	      << uint16_t(ex.get_expected()) << " expected, "
	      << uint16_t(ex.get_received()) << " got)"
	      << std::endl;
    exit(1);
  }catch(intifada::Parsing_Output_Length_Exception&ex){
    std::cerr << "throw Erreur, invalid writed size ("
	      << uint16_t(ex.get_expected()) << " expected, "
	      << uint16_t(ex.get_received()) << " got)"
	      << std::endl;
    exit(1);
  }catch(intifada::Parsing_Exception&ex){
    std::cerr << "Reparsing error 01" << std::endl;
    exit(1);
  }
  intifada::Fixed_Length_Data_Field fixed_data_bad;
  fixed_data_bad.set_property("size","5");
  try{
    check_parsing_data(stream,4,fixed_data_bad);
  }catch(intifada::Parsing_Input_Length_Exception&ex){
  }catch(intifada::Parsing_Output_Length_Exception&ex){
    std::cerr << "throw Erreur, invalid writed size ("
	      << uint16_t(ex.get_expected()) << " expected, "
	      << uint16_t(ex.get_received()) << " got)"
	      << std::endl;
    exit(1);
  }catch(intifada::Parsing_Exception&ex){
    std::cerr << "Reparsing error 02" << std::endl;
    exit(1);
  }
}

void
check_advanced()
{
  uint8_t modea[]={0xc5,0xb9};
  // 16: v
  // 15: g
  // 14: l
  // 13
  // 12-1:0x5b9
  // 11000101 10111001
  intifada::Fixed_Length_Data_Field modea_field;
  modea_field.set_property("size","2");
  modea_field.insert("v","bool_T",16,1);
  modea_field.insert("g","bool_T",15,1);
  modea_field.insert("l","bool_T",14,1);
  modea_field.insert("val","oct_T",1,12);

  modea_field.set_stream(modea,0,2);

  intifada::bool_T v=modea_field("v");

  if(v!=true){
    std::cerr << "error 0" << std::endl;
  }
  v=modea_field("g");
  if(v!=true){
    std::cerr << "error 1" << std::endl;
  }
  v=modea_field("l");
  if(v!=false){
    std::cerr << "error 2" << std::endl;
  }

  intifada::oct_T val=modea_field("val");
  if(1465!=val){
    std::cerr << "error 3 (" << val << ")" << std::endl;
  }
  val=0x5c9;
  modea_field("val")=val;
  val=modea_field("val");
  if(0x5c9!=val){
    std::cerr << "error 4" << std::endl;
  }
  intifada::Data_Field::Dump dump;
  modea_field.foreach(dump,intifada::Path_Name());
}

int main()
{
  // register used types
  // register_builtintypes();

  // Check basic functions
  check_basic();
  check_advanced();
  exit(0);
  // Check memory for basic functions
  for(int i=0;i<100000;++i){
    check_basic();
  }
  for(int i=0;i<100000;++i){
    check_advanced();
  }
  exit(0);
}
