/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>

#include <intifada/boolean_interpreter/Evaluation_Driver.hxx>
#include <intifada/boolean_interpreter/Asterix_Context.hxx>
#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Type_Repository.hxx>
#include <intifada/Record.hxx>
#include <intifada/Type.hxx>

int main(int argc, char *argv[])
{
	// Asterix initialisation
	intifada::Configuration_XML_Reader p;
	p.open();
	p.parse(intifada::Record_Repository::instance());
	// ressources could be released now
	p.release();

	Asterix_Context context("eurocontrol",intifada::Record_Repository::instance(),intifada::Type_Repository::instance());

	Evaluation calc;
	Evaluation_Driver driver(calc);


	bool readfile = false;

	for(int ai = 1; ai < argc; ++ai)
	{
		if (argv[ai] == std::string ("-p")) {
			driver.trace_parsing(true);
		}
		else if (argv[ai] == std::string ("-s")) {
			driver.trace_scanning(true);
		}
		else
		{
			// read a file with expressions

			std::fstream infile(argv[ai]);
			if (!infile.good())
			{
				std::cerr << "Could not open file: " << argv[ai] << std::endl;
				return 0;
			}

			calc.clear();
			bool result = driver.parse_stream(context,infile, argv[ai]);
			if (result)
			{
				std::cout << "Expressions:" << std::endl;
				for (unsigned int ei = 0; ei < calc.size(); ++ei)
				{
					std::cout << "[" << ei << "]:" << std::endl;
					std::cout << "tree:" << std::endl;
					calc.dump(ei,std::cout);
					std::cout << "evaluated: "
							<< calc.evaluate(ei)
							<< std::endl;
				}
			}

			readfile = true;
		}
	}

	if (readfile) return 0;

	std::cout << "Reading expressions from stdin" << std::endl;

	std::string line;
	while( std::cout << "input: " &&
			std::getline(std::cin, line) &&
			!line.empty() )
	{
		calc.clear();
		bool result = driver.parse_string(context,line, "input");

		if (result)
		{
			for (unsigned int ei = 0; ei < calc.size(); ++ei)
			{
				std::cout << "tree:" << std::endl;
				calc.dump(ei,std::cout);
				std::cout << "evaluated: "
						<< calc.evaluate(ei)
						<< std::endl;
			}
		}
	}
}
