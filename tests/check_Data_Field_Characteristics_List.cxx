#include <intifada/Data_Field_Characteristics_List.hxx>
#include <intifada/Data_Field_Characteristics.hxx>

#include <intifada/Integer.hxx>
#include <iostream>


// Check if only the first 16 datas created by create_fixed are read
class Repetitive_Structure_Functor : public intifada::Data_Field_Characteristics_List::Functor
  {
  public:
    Repetitive_Structure_Functor():current_(0){}

    virtual int operator()(const intifada::Path_Name &n,const intifada::Data_Field_Characteristics &t,const intifada::Data_Field_Characteristics_List::part_size_type& part);
  private:
    int current_;
  };

class Extended_Structure_Functor : public intifada::Data_Field_Characteristics_List::Functor
  {
  public:
    Extended_Structure_Functor():part_(0),current_(0){}

    virtual int operator()(const intifada::Path_Name &n,const intifada::Data_Field_Characteristics &t,const intifada::Data_Field_Characteristics_List::part_size_type& part);
  private:
    int part_;
    int current_;
  };


int Repetitive_Structure_Functor::operator()(const intifada::Path_Name &n,const intifada::Data_Field_Characteristics &,const intifada::Data_Field_Characteristics_List::part_size_type& part)
  {
    if(part!=intifada::Data_Field_Characteristics_List::npos)
      {
        std::cerr << "Error, part isn't -1 (structure is repetitive) got " << (int)part << std::endl;
        exit(EXIT_FAILURE);
      }
    std::ostringstream os;
    os << "T";
    os << current_;
    if(n!=os.str())
      {
        std::cerr << "Error repetitive structure read" << std::endl;
        exit(EXIT_FAILURE);
      }
    ++this->current_;
    return 0;
  }

int Extended_Structure_Functor::operator()(const intifada::Path_Name &n,const intifada::Data_Field_Characteristics &,const intifada::Data_Field_Characteristics_List::part_size_type& part)
  {
    if(part!=part_)
      {
        std::cerr << "Error, part isn't right: got " << (int)part << " expected " << (int) part_ << std::endl;
        exit(EXIT_FAILURE);
      }
    std::ostringstream os;
    os << "T";
    os << current_;
    if(n!=os.str())
      {
        std::cerr << "Error extended structure read (expected " << os.str()<< ", got " << n << ")" << std::endl;
        exit(EXIT_FAILURE);
      }
    ++this->current_;

    if(this->current_%16==0)
      {
        this->current_=0;
        ++this->part_;
      }
    return 0;
  }



void insert(
    intifada::Data_Field_Characteristics_List& r
    ,const std::string& n,const std::string& t
    ,uint8_t size,uint8_t lsb
    ,uint8_t part=0)
  {
    intifada::Data_Field_Characteristics* f = r.get_new_description();

    f->set(t,lsb,size);
    r.insert(n,f,part);
  }

void create_fixed(intifada::Data_Field_Characteristics_List& r)
  {
    for(int i=0;i<16;++i)
      {
        std::ostringstream os;
        os << "T";
        os << i;
        insert(r,os.str(),"bool_T",1,i);
      }
  }

void create_extended(intifada::Data_Field_Characteristics_List& r)
  {
    for(int p=0;p<16;++p)
      {
        for(int i=0;i<16;++i)
          {
            std::ostringstream os;
            os << "T";
            os << i;
            insert(r,os.str(),"bool_T",1,i,p);
          }
      }
  }
void create_repetitive(intifada::Data_Field_Characteristics_List& r)
  {
    for(int i=0;i<16;++i)
      {
        std::ostringstream os;
        os << "T";
        os << i;
        insert(r,os.str(),"bool_T",1,i);
      }
  }


void initial_check()
  {
    intifada::Data_Field_Characteristics_List record;

    insert(record,"T1","bool_T",1,1);
    insert(record,"T2","uint8_T",2,2);
    insert(record,"T3","uint8_T",2,4);

    const intifada::Data_Field_Characteristics &c=record.get_characteristics("T1");
    // set bool
    intifada::bool_T *t1=dynamic_cast<intifada::bool_T*>(c.get_type());
    *t1=true;

    intifada::Data_Field_Characteristics_List cloned_record=record;

    const intifada::Data_Field_Characteristics &cloned_c=cloned_record.get_characteristics("T1");
    // set bool
    t1=dynamic_cast<intifada::bool_T*>(cloned_c.get_type());
    if(*t1!=true)
      {
        std::cerr << "Error cloning Data_Field_Characteristics" << std::endl;
        exit(EXIT_FAILURE);
      }
    cloned_record.set_repetitive();
    const intifada::Data_Field_Characteristics &repetitive_c=cloned_record.get_characteristics("T1",1);
    t1=dynamic_cast<intifada::bool_T*>(repetitive_c.get_type());
    if(*t1!=false)
      {
        std::cerr << "Error Data_Field_Characteristics:repetitive duplication error" << std::endl;
        exit(EXIT_FAILURE);
      }
  }

int main()
  {
    initial_check();
    // Check repetitive structure (fixed)
    intifada::Data_Field_Characteristics_List record_rep_fixed;
    create_fixed(record_rep_fixed);
    record_rep_fixed.set_repetitive();
    Repetitive_Structure_Functor f;
    record_rep_fixed.foreach_structure(f);

    intifada::Data_Field_Characteristics_List record_extended;
    create_extended(record_extended);
    record_extended.set_extended();
    Extended_Structure_Functor f2;
    record_extended.foreach_structure(f2);

    intifada::Data_Field_Characteristics_List record_extended_repetitive;
    record_extended_repetitive.set_extended();
    record_extended_repetitive.set_repetitive();
    Repetitive_Structure_Functor f3;
    record_extended_repetitive.foreach_structure(f3);

    exit(EXIT_SUCCESS);
  }
