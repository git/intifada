/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Repetitive_Length_Data_Field.hxx>

#include <intifada/Exception.hxx>

#include <check_datas_utils.hxx>
#include <intifada/Integer.hxx>
#include <iostream>
void
check_basic()
{
  uint8_t stream[]=
    {
      0x05,
      0x06,0x01,0x01,0x09,
      0x07,0x02,0x03,0x0b,
      0x08,0x03,0x05,0x0d,
      0x09,0x04,0x07,0x0f,
      0x0a,0x05,0x09,0x11
    };


  intifada::Repetitive_Length_Data_Field data_good;
  data_good.set_property("length","4");
  try{
    check_parsing_data(stream,21,data_good);
  }catch(intifada::Parsing_Input_Length_Exception&ex){
    std::cerr << "Error, Invalid reading size (" << ex.get_expected()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;
      exit(1);
  }catch(intifada::Parsing_Output_Length_Exception&ex){
    std::cerr << "Error, Invalid writing size (" << ex.get_expected()
	      << " waited, " << ex.get_received() << " got)"
	      << std::endl;


    exit(1);
  }catch(intifada::Parsing_Exception&ex){
    std::cerr << "Error reencoding sequence" << std::endl;
    exit(1);
  }
}

void
check_advanced()
{
  // Ok, let's try a repetitive length composed by a two fields
  // Sample test is based on following structure:
  // three bytes for first part following by 3 bytes extents
  intifada::Repetitive_Length_Data_Field field;
  field.set_property("length","3");
  field.insert("sui","uint8_T",17,8);
  field.insert("stn","uint16_T",2,15);

  intifada::uint8_T wsui=10;

  // Create sui part 9
  field("sui.9")=wsui;
  intifada::uint8_T rsui=field("sui.9");

  if(rsui != wsui){
    std::cerr << "Error computing repetitive field" << std::endl;
    exit(1);
  }
}

int main()
{
  // register used types
  // register_builtintypes();

  check_basic();

  check_advanced();

  for(int i =0;i<100000;++i){
    check_basic();
    check_advanced();
  }

  exit(0);
}
