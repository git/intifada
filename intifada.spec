Name:			intifada
License:		GNU/GPL
Group:			Devel
version:		1.2.0
Autoreqprov:		off
Release:		1
Source:			%{name}-%{version}.tar.gz
Packager: 		Stephane Pion <stephane.pion at free.fr>
Summary:		intifada library
BuildRoot:		%{_tmppath}/%{name}-root
Requires:		libxml2 >= 2.6.16,libpcap
%description
intifada is a asterix interpreter written in C++. It can encode and decode any
asterix message which is described


%package devel
Group:          Development/Libraries
Summary:        Header files for intifada
Autoreqprov:	off
Requires:	intifada

%description devel
intifada is a asterix interpreter written in C++. It can encode and decode any
asterix message which is described

%prep
## -D -T avoid building erasing and untar new sources (use in developpement phase)
##%setup -q -D -T -n intifada-%{version}
%setup -q -n intifada-%{version}

%build
[ -d objdir ] || mkdir objdir
cd objdir
unset PKG_CONFIG_PATH PATH
PATH=/bin:/usr/bin
export PATH
../configure --prefix=%{_usr} --exec-prefix=%{_usr} --bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} --datadir=%{_datadir} --includedir=%{_includedir} --libdir=%{_libdir} --libexecdir=/usr/libexec --localstatedir=/var --sharedstatedir=/usr/com --mandir=/usr/share/man --infodir=/usr/share/info
make

%install
[ %{buildroot} != "/" ] && [ -d %{buildroot} ] && rm -rf %{buildroot}
cd objdir
make DESTDIR=%{buildroot} install

%clean
[ %{buildroot} != "/" ] && [ -d %{buildroot} ] && rm -rf %{buildroot}
cd objdir
make clean

%files
%defattr(755,root,root)
#%{_bindir}/asterix
#%{_bindir}/configuration
%{_bindir}/imc
%{_libdir}/libintifada.so
%{_libdir}/libintifada.so.0
%{_libdir}/libintifada.so.0.0.0
%{_libdir}/libxmlintifada.so
%{_libdir}/libxmlintifada.so.0
%{_libdir}/libxmlintifada.so.0.0.0
%{_libdir}/libcintifada.so
%{_libdir}/libcintifada.so.0
%{_libdir}/libcintifada.so.0.0.0

%defattr(644,root,root)
%{_sysconfdir}/intifada/asterix.xml
%{_datadir}/intifada/dtd/asterix.dtd

%files devel
%defattr(644,root,root)
%{_includedir}/intifada/*.hxx
%{_includedir}/intifada/*.txx
%{_includedir}/intifada/*.ixx
%{_includedir}/intifada/*_T.cxx
%{_includedir}/intifada/cintifada.h
%{_includedir}/intifada/inttypes.h
%{_includedir}/intifada/build_configuration.h
%{_libdir}/include/intifada/os_wrappers/build_configuration.h
%{_libdir}/include/intifada/os_wrappers/inttypes.h
%{_libdir}/include/intifada/os_wrappers/map.hxx
%{_libdir}/libintifada.a
%{_libdir}/libintifada.la
%{_libdir}/libxmlintifada.a
%{_libdir}/libxmlintifada.la
%{_libdir}/libcintifada.a
%{_libdir}/libcintifada.la
%{_libdir}/pkgconfig/intifada.pc
%{_libdir}/pkgconfig/xmlintifada.pc




