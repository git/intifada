/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FIELD_SPECIFICATION_MANIPULATION
# define FIELD_SPECIFICATION_MANIPULATION

# include <intifada/inttypes.h>

/// Fspec byte manipulation
class Field_Specification_Manipulation
{
public:
  /// frn conversion. Get concerned byte
  /**
   * This method return affected byte by frn
   * \param f field reference number
   * \return affected byte (starting with 1)
   */
  static uint8_t frn_to_byte(uint8_t f);

  /// frn  conversion. Get offset in concerned byte
  /**
   * \return offset in concerned byte (starting with 1)
   * \param f field reference number
   */
  static uint8_t frn_to_offset(uint8_t f);

  /// subfield conversion Get concerned byte
  /**
   * This method return affected byte by compound subfield indicator
   * \param f subfield indicator
   * \param s field size
   * \return affected byte (starting with 1)
   */
  static uint8_t sf_to_byte(uint8_t f,uint8_t s);

  /// subfield conversion Get offset in concerned byte
  /**
   * \return offset in concerned byte (starting with 1)
   * \param f subfield indicator
   */
  static uint8_t sf_to_offset(uint8_t f);

  /// offset and bit offset to uap representation
  static uint8_t offset_bit_to_bit(uint8_t byte,uint8_t offset);

  static uint8_t bit_to_frn(uint8_t bit);

  static uint8_t bit_to_frn(uint8_t bit,uint8_t size);
};

#endif // FIELD_SPECIFICATION_MANIPULATION
