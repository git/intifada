/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_INTEGER_T_HXX
# define INTIFADA_INTEGER_T_HXX

#include <intifada/Type.hxx>

namespace intifada
{

template <typename T_>
class Integer_T : public intifada::Type
{
public:
	typedef intifada::Type inherited;
	typedef T_ value_type;

	/// constructor
	/**
	 * @param id type identification
	 */
	Integer_T(const std::string& id);


	/// constructor
	/**
	 * @param id type identification
	 */
	Integer_T(const std::string& id,const T_& i);

	/// Copy constructor
	Integer_T(const Integer_T&rhs);

	/// Copy operator
	Integer_T& operator=(const Integer_T&rhs);

	/// Set this to null value
	/**
	 * set a value's instance to null (eg 0 for a numeric type, "" for a string type ...)
	 */
	virtual void set_null();

	/// handle changed state
	/**
	 * changed states could be detected. This method should be called to
	 * indicate which value to remember
	 */
	virtual void reset_changed_state();

	/// handle changed state
	/**
	 * \return true if current value differs from value when reset_changed_state were called
	 * \return false if current value doesn't differ from value when reset_changed_state were called
	 */
	virtual bool is_changed_state()const;


	/// Get size
	/**
	 * \return size object (unit is the bit)
	 */
	virtual ssize_t get_size()const;

	/// Compare with a string
	virtual bool operator==(const std::string& rhs)const;

private:

	/// adapt stream to internal data
	/**
	 * before setting internal data from stream, this method
	 * is called with raw stream extracted. This could be used by sub classes
	 * to do anything particular
	 *
	 * \param v data coming from stream
	 * \param size size in bits extracted from stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual T_ stream_to_internal(const T_&v,int size)const;

	/// adapt internal data to stream
	/**
	 * above symetric
	 * \param v data coming from internal
	 * \param size size in bits that will be inserted in stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual T_ internal_to_stream(const T_&v,int size)const;

public:

#if 0
	// Equality operator
	Integer_T& operator=(const T_&rhs);
#endif

	/// Get internal value to string
	/**
	 * \return internal value
	 */
	virtual std::string get_to_string()const;

	/// Set internal value from string
	/**
	 * \param v value
	 * \return 0 if no error
	 */
	virtual int set_from_string(const std::string& v);

	/// Transfert a stream into a real type
	/**
	 * \param b stream to transfert
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 */
	virtual void set_from_stream(Stream::const_reverse_iterator b,int start,int size);

	/// Transfert a real type into a stream
	/**
	 * \param b target stream
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 */
	virtual void to_stream(Stream::reverse_iterator b,int start,int size)const;

	bool operator==(const Integer_T& rhs)const;
	bool operator==(const T_& rhs)const;

	bool operator!=(const Integer_T& rhs)const;
	bool operator!=(const T_& rhs)const;

	operator const T_&()const;


	Integer_T& operator+=(const T_& rhs);

	Integer_T& operator-=(const T_& rhs);

	Integer_T& operator*=(const T_& rhs);

	Integer_T& operator/=(const T_& rhs);

private:
	T_ value_;
	T_ base_value_;
};
}

//template<typename T>
//bool operator!=(const intifada::Integer_T<T>& lhs,const T& rhs)
//{
//
//}
//template<typename T>
//bool operator!=(const intifada::Integer_T<T>& lhs,const intifada::Integer_T<T>& rhs)
//{
//	return lhs.operator!=(rhs);
//}

#define CONCRETE_TYPE(NAMESPACE,CLASS,TYPE,BASE) \
		namespace NAMESPACE \
		{ \
	class CLASS : public BASE<TYPE> \
	{ \
	public: \
	typedef BASE<TYPE> inherited; \
	public: \
	CLASS() \
	:inherited(""#CLASS"") \
	 { \
		::clog("Memory_Allocation") << "0x" << this << ":ctor #CLASS" << std::endl; \
	 } \
	 CLASS(const TYPE& i) \
	 :inherited(""#CLASS"",i) \
	  { \
		 clog("Memory_Allocation") << "0x" << this << ":ctor #CLASS" << std::endl; \
	  } \
	  CLASS(const CLASS& rhs) \
	  :inherited(rhs) {} \
	   CLASS(const intifada::Type& rhs) \
	   :inherited(dynamic_cast<const CLASS&>(rhs)) \
	    { \
		  clog("Memory_Allocation") << "0x" << this << ":ctor #CLASS" << std::endl; \
	    } \
	    ~CLASS() \
	    { \
	    	clog("Memory_Allocation") << "0x" << this << ":dtor #CLASS" << std::endl; \
	    } \
	    \
	    CLASS& operator=(const Type&rhs){return this->operator=(dynamic_cast<const CLASS&>(rhs));} \
	    CLASS& operator=(const CLASS& rhs){if(this != &rhs){inherited::operator=(rhs);}return *this;} \
	    \
	    intifada::Type* clone()const{Type *ret=new CLASS;*ret=*this;return ret;} \
	    \
	    \
	}; \
		} \
		inline std::ostream& operator<<(std::ostream& os, const NAMESPACE::CLASS& rhs) \
		{ \
			os << rhs.get_to_string(); \
			return os; \
		}



#include <intifada/Integer_T.cxx>


#endif // INTIFADA_INTEGER_T_HXX
