/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Structure_Functor.hxx>

intifada::Structure_Functor::Structure_Functor()
{}

intifada::Structure_Functor::~Structure_Functor()
{}

int intifada::Structure_Functor::frn(int,const Path_Name&)
{
  return -1;
}

int intifada::Structure_Functor::family_name_id(const std::string&,const std::string&)
{
  return -1;
}

int intifada::Structure_Functor::begin_record_identity(const std::string&,uint8_t, const std::string&)
{
  return -1;
}

int intifada::Structure_Functor::end_record_identity()
{
  return -1;
}

int intifada::Structure_Functor::begin_item(const std::string&,const Path_Name&,const Properties&)
{
  return -1;
}

int intifada::Structure_Functor::end_item()
{
  return -1;
}

int intifada::Structure_Functor::begin_uap(const Condition *)
{
  return -1;
}

int intifada::Structure_Functor::end_uap()
{
  return -1;
}

int intifada::Structure_Functor::begin_fixed_field(const Properties&)
{
  return -1;
}

int intifada::Structure_Functor::begin_extended_field(const Properties&)
{
  return -1;
}

int intifada::Structure_Functor::begin_repetitive_field(const Properties&)
{
  return -1;
}

int intifada::Structure_Functor::begin_compound_field(uint8_t)
{
  return -1;
}

void intifada::Structure_Functor::end_compound_field()
  {
    return;
  }

int intifada::Structure_Functor::end_field()
{
  return -1;
}

int intifada::Structure_Functor::part(const Type&,const Path_Name&,uint8_t,uint8_t,uint8_t)
{
  return -1;
}
