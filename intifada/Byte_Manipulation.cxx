/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Byte_Manipulation.hxx>

uint8_t intifada::Byte_Manipulation::mask_[]={0x1,0x3,0x7,0xf,0x1f,0x3f,0x7f,0xff};

#ifndef INTIFADA_USE_INLINE
# include<Byte_Manipulation.ixx>
#endif

uint8_t intifada::Byte_Manipulation::get_access(uint8_t &size,uint8_t offset,uint8_t max)
{
  // This function returns
  // - size size to read the next time
  // - size to read currently

  if(offset >= max)
    {
      throw std::out_of_range("");
    }

  uint8_t ret=0;

  uint8_t to_entire_part = max - offset;
  if(size >= to_entire_part)
    {
      ret = size - to_entire_part;
      size = to_entire_part;
    }

  return ret;
}
