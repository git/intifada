/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNSIGNED_INTEGER_T_HXX
# define UNSIGNED_INTEGER_T_HXX

#include <intifada/Integer_T.hxx>

namespace intifada
{
  /// Convenience class
/**
 * Convenience class just to reflect Signed_Integer_T. For symetry
 */
  template <typename T_>
  class Unsigned_Integer_T : public Integer_T<T_>
  {
  private:
    typedef Integer_T<T_> inherited;

  public:

    Unsigned_Integer_T(const std::string& id);
    Unsigned_Integer_T(const std::string& id,const T_& i);

  };
}
#include <intifada/Unsigned_Integer_T.cxx>

#endif // UNSIGNED_INTEGER_T_HXX
