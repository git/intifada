/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_INTEGER_HXX
# define INTIFADA_INTEGER_HXX

# include <intifada/Unsigned_Integer_T.hxx>
# include <intifada/Signed_Integer_T.hxx>
# include <iostream>





CONCRETE_TYPE(intifada,uint8_T,uint8_t,Unsigned_Integer_T)
CONCRETE_TYPE(intifada,uint16_T,uint16_t,Unsigned_Integer_T)
CONCRETE_TYPE(intifada,uint32_T,uint32_t,Unsigned_Integer_T)
CONCRETE_TYPE(intifada,uint64_T,uint64_t,Unsigned_Integer_T)
CONCRETE_TYPE(intifada,int8_T,int8_t,Signed_Integer_T)
CONCRETE_TYPE(intifada,int16_T,int16_t,Signed_Integer_T)
CONCRETE_TYPE(intifada,int32_T,int32_t,Signed_Integer_T)
CONCRETE_TYPE(intifada,int64_T,int64_t,Signed_Integer_T)

CONCRETE_TYPE(intifada,bool_T,bool,Unsigned_Integer_T)

namespace intifada
{


#if 0
class uint8_T : public Unsigned_Integer_T<uint8_t>
{
public:
	typedef Unsigned_Integer_T<uint8_t> inherited;
public:
	uint8_T();
	uint8_T(uint8_t i);

	uint8_T(const uint8_T& rhs);

	uint8_T(const Type&rhs);

	virtual ~uint8_T();

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual uint8_T& operator=(const Type&rhs);

	uint8_T& operator=(const uint8_T& rhs);

	virtual Type* clone()const;
};

class uint16_T : public Unsigned_Integer_T<uint16_t>
{
public:
	typedef Unsigned_Integer_T<uint16_t> inherited;
public:
	uint16_T();
	uint16_T(uint16_t i);

	uint16_T(const uint16_T& rhs);

	uint16_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual uint16_T& operator=(const Type&rhs);

	uint16_T& operator=(const uint16_T& rhs);

	virtual Type* clone()const;
};

class uint32_T : public Unsigned_Integer_T<uint32_t>
{
public:
	typedef Unsigned_Integer_T<uint32_t> inherited;
public:
	uint32_T();
	uint32_T(uint32_t i);

	uint32_T(const uint32_T& rhs);

	uint32_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual uint32_T& operator=(const Type&rhs);

	uint32_T& operator=(const uint32_T& rhs);
	virtual Type* clone()const;
};

class uint64_T : public Unsigned_Integer_T<uint64_t>
{
public:
	typedef Unsigned_Integer_T<uint64_t> inherited;
public:
	uint64_T();
	uint64_T(uint64_t i);

	uint64_T(const uint64_T& rhs);

	uint64_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual uint64_T& operator=(const Type&rhs);

	uint64_T& operator=(const uint64_T& rhs);
	virtual Type* clone()const;
};

class bool_T : public Unsigned_Integer_T<bool>
{
public:
	typedef Unsigned_Integer_T<bool> inherited;
public:
	bool_T();
	bool_T(bool i);

	bool_T(const bool_T& rhs);

	bool_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual bool_T& operator=(const Type&rhs);

	bool_T& operator=(const bool_T& rhs);
	virtual Type* clone()const;

	/// Get size
	/**
	 * \return size object (unit is the bit)
	 */
	virtual ssize_t get_size()const;

};

class int8_T : public Signed_Integer_T<int8_t>
{
public:
	typedef Signed_Integer_T<int8_t> inherited;
public:
	int8_T();
	int8_T(int8_t i);

	int8_T(const int8_T& rhs);

	int8_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual int8_T& operator=(const Type&rhs);

	int8_T& operator=(const int8_T& rhs);
	virtual Type* clone()const;
};

class int16_T : public Signed_Integer_T<int16_t>
{
public:
	typedef Signed_Integer_T<int16_t> inherited;
public:
	int16_T();
	int16_T(int16_t i);

	int16_T(const int16_T& rhs);

	int16_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual int16_T& operator=(const Type&rhs);

	int16_T& operator=(const int16_T& rhs);
	virtual Type* clone()const;
};

class int32_T : public Signed_Integer_T<int32_t>
{
public:
	typedef Signed_Integer_T<int32_t> inherited;
public:
	int32_T();
	int32_T(int32_t i);

	int32_T(const int32_T& rhs);

	int32_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual int32_T& operator=(const Type&rhs);

	int32_T& operator=(const int32_T& rhs);
	virtual Type* clone()const;
};

class int64_T : public Signed_Integer_T<int64_t>
{
public:
	typedef Signed_Integer_T<int64_t> inherited;
public:
	int64_T();
	int64_T(int64_t i);

	int64_T(const int64_T& rhs);

	int64_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual int64_T& operator=(const Type&rhs);

	int64_T& operator=(const int64_T& rhs);
	virtual Type* clone()const;
};
#endif

class oct_T : public Unsigned_Integer_T<uint16_t>
{
public:
	typedef Unsigned_Integer_T<uint16_t> inherited;
public:
	oct_T();
	oct_T(const uint16_t& i);

	oct_T(const oct_T& rhs);

	oct_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual oct_T& operator=(const Type&rhs);

	oct_T& operator=(const oct_T& rhs);

	virtual Type* clone()const;

private:

	/// adapt stream to internal data
	/**
	 * before setting internal data from stream, this method
	 * is called with raw stream extracted. This could be used by sub classes
	 * to do anything particular
	 *
	 * \param v data coming from stream
	 * \param size size in bits extracted from stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual value_type stream_to_internal(const value_type&v,int size)const;

	/// adapt internal data to stream
	/**
	 * above symetric
	 * \param v data coming from internal
	 * \param size size in bits that will be inserted in stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual value_type internal_to_stream(const value_type&v,int size)const;

public:
	/// Get internal value to string
	/**
	 * \return internal value
	 */
	//virtual std::string get_to_string()const;
};

class gray_T : public Signed_Integer_T<int16_t>
{
public:
	typedef Signed_Integer_T<int16_t> inherited;
public:
	gray_T();
	gray_T(int16_t i);

	gray_T(const gray_T& rhs);

	gray_T(const Type&rhs);

	/// Copy operator
	/**
	 * Used to operate affectations from Type
	 * \param rhs Type to copy from
	 */
	virtual gray_T& operator=(const Type&rhs);

	gray_T& operator=(const gray_T& rhs);

	virtual Type* clone()const;
private:

	/// adapt stream to internal data
	/**
	 * before setting internal data from stream, this method
	 * is called with raw stream extracted. This could be used by sub classes
	 * to do anything particular
	 *
	 * \param v data coming from stream
	 * \param size size in bits extracted from stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual value_type stream_to_internal(const value_type&v,int size)const;

	/// adapt internal data to stream
	/**
	 * above symetric
	 * \param v data coming from internal
	 * \param size size in bits that will be inserted in stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	virtual value_type internal_to_stream(const value_type&v,int size)const;
};
}

extern "C"{
/// Function used to register types
/**
 * here is a special internal part and this is called by type repository
 * when it is created.
 * In read types libraries, this function will have the following form:
 * register_<library name>().
 * This function must be C encoded
 */
void register_builtintypes();
}


#endif // INTIFADA_INTEGER_HXX
