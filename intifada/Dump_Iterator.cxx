/*
   Copyright (C) 2009, 2010, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Dump_Iterator.hxx>
#include <iostream>
#include <intifada/Record.hxx>

intifada::Dump_Iterator::Dump_Iterator(std::ostream& os)
:os_(os)
,it_(os)
,_current_category(0)
,_filter()
,_value()
{}

void intifada::Dump_Iterator::dump_all()
{
	it_.dump_all();
}

void intifada::Dump_Iterator::set_filter(const std::string& filter,const std::string& value)
{
	_filter=filter;
	_value=value;
}

void intifada::Dump_Iterator::add_display(const intifada::Path_Name& d)
{
	it_.display(d);
}

/// Called when a block began
int intifada::Dump_Iterator::operator()(uint8_t c)
{
	_current_category=c;
	return 0;
}

int intifada::Dump_Iterator::operator()(
		intifada::Record_Iterator::record_type& i,
		const intifada::Message::block_list_size_t& b,
		const intifada::Block::record_list_size_t& r)
{
	bool iterate=true;
	if(_filter.empty()!=true)
	{
		try
		{
			//os_ << "debug:i("<< _filter << ")=" << i(_filter).get_to_string() << std::endl;
			iterate=false;
			if(i(_filter)==_value)
			{
				iterate=true;
			}
		}
		catch(intifada::Description_Field_Unknow_Exception& ex)
		{
			// filter doesn't exist
			iterate=false;
		}
	}
	if(iterate==true)
	{
		if(_current_category!=0)
		{
			os_ << "cat=" << (uint32_t) _current_category << std::endl;
			_current_category=0;
		}
		os_ << "\tblock=" << b << " record=" << r << std::endl;
		i.foreach(it_,intifada::Path_Name());
	}
	return 0;
}
