/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Integer.hxx>
#include <intifada/Type_Repository.hxx>
#include <intifada/Octal_Converter.hxx>
#include <intifada/Logger.hxx>

#if 0
intifada::uint8_T::uint8_T()
:inherited()
{
	clog("Memory_Allocation") << "0x" << this << ":ctor uint8_T" << std::endl;
	this->set_type_id("uint8_T");
}

intifada::uint8_T::uint8_T(uint8_t i)
:inherited(i)
{
	clog("Memory_Allocation") << "0x" << this << ":ctor uint8_T" << std::endl;
	this->set_type_id("uint8_T");
}

intifada::uint8_T::uint8_T(const uint8_T& rhs)
:inherited(rhs)
{}

intifada::uint8_T::uint8_T(const Type&rhs)
:inherited(dynamic_cast<const uint8_T&>(rhs))
{
	clog("Memory_Allocation") << "0x" << this << ":ctor uint8_T" << std::endl;
}

intifada::uint8_T::~uint8_T()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor uint8_T" << std::endl;
}

intifada::uint8_T& intifada::uint8_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const uint8_T&>(rhs));
  }

intifada::uint8_T&
intifada::uint8_T::operator=(const uint8_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::uint8_T::clone()const
{
  Type *ret=new uint8_T;
  *ret=*this;
  return ret;
}


intifada::uint16_T::uint16_T()
:inherited()
{
	this->set_type_id("uint16_T");
}


intifada::uint16_T::uint16_T(uint16_t i):
  inherited(i)
{
	this->set_type_id("uint16_T");
}


intifada::uint16_T::uint16_T(const uint16_T& rhs):
  inherited(rhs)
{}

intifada::uint16_T::uint16_T(const Type&rhs):
  inherited(dynamic_cast<const uint16_T&>(rhs))
{}

intifada::uint16_T& intifada::uint16_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const uint16_T&>(rhs));
  }

intifada::uint16_T&
intifada::uint16_T::operator=(const uint16_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::uint16_T::clone()const
{
  Type *ret=new uint16_T;
  *ret=*this;
  return ret;
}

intifada::uint32_T::uint32_T():
  inherited()
{
	this->set_type_id("uint32_T");
}

intifada::uint32_T::uint32_T(uint32_t i):
  inherited(i)
{
	this->set_type_id("uint32_T");
}

intifada::uint32_T::uint32_T(const uint32_T& rhs):
  inherited(rhs)
{}

intifada::uint32_T::uint32_T(const Type&rhs):
  inherited(dynamic_cast<const uint32_T&>(rhs))
{}

intifada::uint32_T& intifada::uint32_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const uint32_T&>(rhs));
  }

intifada::uint32_T&
intifada::uint32_T::operator=(const uint32_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::uint32_T::clone()const
{
  Type *ret=new uint32_T;
  *ret=*this;
  return ret;
}

intifada::uint64_T::uint64_T():
  inherited()
{
	this->set_type_id("uint64_T");
}


intifada::uint64_T::uint64_T(uint64_t i):
  inherited(i)
{
	this->set_type_id("uint64_T");
}

intifada::uint64_T::uint64_T(const uint64_T& rhs):
  inherited(rhs)
{}

intifada::uint64_T::uint64_T(const Type&rhs):
  inherited(dynamic_cast<const uint64_T&>(rhs))
{}

intifada::uint64_T& intifada::uint64_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const uint64_T&>(rhs));
  }

intifada::uint64_T&
intifada::uint64_T::operator=(const uint64_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::uint64_T::clone()const
{
  Type *ret=new uint64_T;
  *ret=*this;
  return ret;
}

intifada::bool_T::bool_T():
  inherited()
{
	this->set_type_id("bool_T");
}


intifada::bool_T::bool_T(bool i):
  inherited(i)
{
	this->set_type_id("bool_T");
}


intifada::bool_T::bool_T(const bool_T& rhs):
  inherited(rhs)
{}

intifada::bool_T::bool_T(const Type&rhs):
  inherited(dynamic_cast<const bool_T&>(rhs))
{}

intifada::bool_T& intifada::bool_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const bool_T&>(rhs));
  }

intifada::bool_T&
intifada::bool_T::operator=(const bool_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::bool_T::clone()const
{
  Type *ret=new bool_T;
  *ret=*this;
  return ret;
}

ssize_t intifada::bool_T::get_size()const
{
  return 1;
}

intifada::int8_T::int8_T():
  inherited()
{
	this->set_type_id("int8_T");
}


intifada::int8_T::int8_T(int8_t i):
  inherited(i)
{
	this->set_type_id("int8_T");
}


intifada::int8_T::int8_T(const int8_T& rhs):
  inherited(rhs)
{}

intifada::int8_T::int8_T(const Type&rhs):
  inherited(dynamic_cast<const int8_T&>(rhs))
{}

intifada::int8_T& intifada::int8_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const int8_T&>(rhs));
  }

intifada::int8_T&
intifada::int8_T::operator=(const int8_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::int8_T::clone()const
{
  Type *ret=new int8_T;
  *ret=*this;
  return ret;
}

intifada::int16_T::int16_T():
  inherited()
{
	this->set_type_id("int16_T");
}


intifada::int16_T::int16_T(int16_t i):
  inherited(i)
{
	this->set_type_id("int16_T");
}


intifada::int16_T::int16_T(const int16_T& rhs):
  inherited(rhs)
{}

intifada::int16_T::int16_T(const Type&rhs):
  inherited(dynamic_cast<const int16_T&>(rhs))
{}

intifada::int16_T& intifada::int16_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const int16_T&>(rhs));
  }

intifada::int16_T&
intifada::int16_T::operator=(const int16_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::int16_T::clone()const
{
  Type *ret=new int16_T;
  *ret=*this;
  return ret;
}

intifada::int32_T::int32_T():
  inherited()
{
	this->set_type_id("int32_T");
}


intifada::int32_T::int32_T(int32_t i):
  inherited(i)
{
	this->set_type_id("int32_T");
}

intifada::int32_T::int32_T(const int32_T& rhs):
  inherited(rhs)
{
}


intifada::int32_T::int32_T(const Type&rhs):
  inherited(dynamic_cast<const int32_T&>(rhs))
{}

intifada::int32_T& intifada::int32_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const int32_T&>(rhs));
  }

intifada::int32_T&
intifada::int32_T::operator=(const int32_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::int32_T::clone()const
{
  Type *ret=new int32_T;
  *ret=*this;
  return ret;
}

intifada::int64_T::int64_T():
  inherited()
{
	this->set_type_id("int64_T");
}


intifada::int64_T::int64_T(int64_t i):
  inherited(i)
{
	this->set_type_id("int64_T");
}

intifada::int64_T::int64_T(const int64_T& rhs):
  inherited(rhs)
{}


intifada::int64_T::int64_T(const Type&rhs):
  inherited(dynamic_cast<const int64_T&>(rhs))
{}

intifada::int64_T& intifada::int64_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const int64_T&>(rhs));
  }

intifada::int64_T&
intifada::int64_T::operator=(const int64_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::int64_T::clone()const
{
  Type *ret=new int64_T;
  *ret=*this;
  return ret;
}
#endif
intifada::oct_T::oct_T()
:inherited("oct_T")
{}


intifada::oct_T::oct_T(const uint16_t& i)
:inherited("oct_T",i)
{}

intifada::oct_T::oct_T(const oct_T& rhs):
  inherited(rhs)
{}


intifada::oct_T::oct_T(const Type&rhs):
  inherited(dynamic_cast<const oct_T&>(rhs))
{}

intifada::oct_T& intifada::oct_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const oct_T&>(rhs));
  }

intifada::oct_T&
intifada::oct_T::operator=(const oct_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::oct_T::clone()const
{
  Type *ret=new oct_T;
  *ret=*this;
  return ret;
}

intifada::oct_T::value_type
intifada::oct_T::stream_to_internal(const value_type&v,int)const
{
	return v;
}

intifada::oct_T::value_type
intifada::oct_T::internal_to_stream(const value_type&v,int)const
{
	value_type decnum=v;
	uint16_t external=0;
	int shift=0;
	while(decnum > 0)
	{
		external |= (decnum%8) << shift;
		decnum/=8;
		shift+=3;
	}
  return external;
}

//std::string intifada::oct_T::get_to_string()const
//{
//	value_type v=*this;
//	std::ostringstream os;
//	os << std::oct << v;
//	return os.str();
//}

intifada::gray_T::gray_T()
:inherited("gray_T")
{}


intifada::gray_T::gray_T(int16_t i)
:inherited("gray_T",i)
{}


intifada::gray_T::gray_T(const gray_T& rhs):
  inherited(rhs)
{}

intifada::gray_T::gray_T(const Type&rhs):
  inherited(dynamic_cast<const gray_T&>(rhs))
{}

intifada::gray_T& intifada::gray_T::operator=(const Type&rhs)
  {
    return this->operator=(dynamic_cast<const gray_T&>(rhs));
  }
intifada::gray_T&
intifada::gray_T::operator=(const gray_T& rhs)
{
  if(this != &rhs){
    inherited::operator=(rhs);
  }
  return *this;
}

intifada::Type*
intifada::gray_T::clone()const
{
  Type *ret=new gray_T;
  *ret=*this;
  return ret;
}

intifada::gray_T::value_type
intifada::gray_T::stream_to_internal(const value_type&v,int)const
{
  value_type gray_read = 0;

  // B4 -> 00000001
  if(v & 0x02)
    gray_read |= 0x01;
  // B2 -> 00000010
  if(v & 0x08)
    gray_read |= 0x02;
  // B1 -> 00000100
  if(v & 0x20)
    gray_read |= 0x04;
  // A4 -> 00001000
  if(v & 0x40)
    gray_read |= 0x08;
  // A2 -> 00010000
  if(v & 0x0100)
    gray_read |= 0x10;
  // A1 -> 00100000
  if(v & 0x0400)
    gray_read |= 0x20;
  // D4 -> 01000000
  if (v & 0x01)
    gray_read |= 0x40;
  // D2 -> 10000000
  if(v & 0x04)
    gray_read |= 0x80;

  char vernier = 0;
  if (v & 0x0800)
    vernier |= 0x04;
  if (v & 0x0200)
    vernier |= 0x02;
  if (v & 0x80)
    vernier |= 0x01;
  switch (vernier)
    {
    case 0x04:
      vernier = -2;
      break;
    case 0x06:
      vernier = -1;
      break;
    case 0x02:
      vernier = 0;
      break;
    case 0x03:
      vernier = 1;
      break;
    case 0x01:
      vernier = 2;
      break;
    }
  // return (((gray_value.binary () - 2) * 5) + vernier) * 4;

  return v;
}

intifada::gray_T::value_type
intifada::gray_T::internal_to_stream(const value_type&v,int)const
{
  value_type ret=0;
  // Le mode C est en 1/4 de FL
   short z = v / 4;
   char vernier = z % 5;
   // Calcul du vernier
   // Le vernier est extrait de la manière suivante:
   // 0 -> 0 FL    (C2)
   // 1 -> 1 FL    (C2 C4)
   // 2 -> 2 FL    (C4)
   // 3 -> -2 FL    (C1)
   // 4 -> -1 FL    (C1 C2)
   switch(vernier)
   {
       case -4:
           ret |= 0x0200;
           ret |= 0x80;
           vernier = 1;
           break;
       case -3:
           ret |= 0x80;
           vernier = 2;
           break;
       case -2:
           ret |= 0x0800;
           break;
       case -1:
           ret |= 0x0a00;
           ret |= 0x0200;
           break;
       case 0:
           ret |= 0x0200;
           break;
       case 1:
           ret |= 0x0200;
           ret |= 0x80;
           break;
       case 2:
           ret |= 0x80;
           break;
       case 3:
           ret |= 0x0800;
           vernier = -2;
           break;
       case 4:
           ret |= 0x0a00;
           ret |= 0x0200;
           vernier = -1;
           break;
   }
   //ivs::gray_short alt = ((z - vernier) / 5) + 2;
   // On est en mesure d'extraire l'altitude décimale
   // ivs::gray_short::gray_type alt_gray = alt.gray();
   value_type alt_gray=0;
   if (alt_gray & 0x01)
       ret |= 0x02;    // B4
   if (alt_gray & 0x02)
       ret |= 0x08;    // B2
   if (alt_gray & 0x04)
       ret |= 0x20;    // B1
   if (alt_gray & 0x08)
       ret |= 0x40;    // A4
   if (alt_gray & 0x10)
       ret |= 0x0100;    // A2
   if (alt_gray & 0x20)
       ret |= 0x0400;    // A1
   if (alt_gray & 0x40)
       ret |= 0x01;    // D4
   if (alt_gray & 0x80)
       ret |= 0x04;    // D2
      //    printf("qc_mcd(%d):0x%02x%02x,gray_alt=%d vernier=%d\n",mcd,C_QC_1_,C_QC_2_,((z - vernier) / 5) + 2,vernier);

  return v;
}


void register_builtintypes()
{

  intifada::Type_Repository *rep=
    intifada::Type_Repository::instance();

  rep->register_type<intifada::uint8_T>();
  rep->register_type<intifada::uint16_T>();
  rep->register_type<intifada::uint32_T>();
  rep->register_type<intifada::uint64_T>();
  rep->register_type<intifada::bool_T>();

  rep->register_type<intifada::int8_T>();
  rep->register_type<intifada::int16_T>();
  rep->register_type<intifada::int32_T>();
  rep->register_type<intifada::int64_T>();

  rep->register_type<intifada::oct_T>();
  rep->register_type<intifada::gray_T>();

}
