/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record.hxx>
#include <intifada/Fixed_Length_Data_Field.hxx>
#include <intifada/Extended_Length_Data_Field.hxx>
#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <intifada/Structure_Functor.hxx>
#include <intifada/Logger.hxx>
#include <cassert>

class Dump_Item : public intifada::Field_Specification_Functor
{
public:
	Dump_Item(intifada::Record* r):record_(r){}
	int operator()(const intifada::Path_Name& name)
	{
		record_->set_presence(name,true);
		return 0;
	}
private:
	intifada::Record *record_;
};

intifada::Record::Record_Field_Iterator::Record_Field_Iterator(data_field_map_t& dl)
:dl_(dl)
{}

intifada::Data_Field* intifada::Record::Record_Field_Iterator::find_data_field(const Path_Name& name)
{
	data_field_map_iterator_t it = dl_.find(name);
	Data_Field *df=NULL;
	if(it!=dl_.end()){
		df=(*it).second;
	}

	return df;
}

intifada::Record::Record_Field_Const_Iterator::Record_Field_Const_Iterator(const data_field_map_t& dl)
:dl_(dl)
{}

const intifada::Data_Field* intifada::Record::Record_Field_Const_Iterator::find_data_field(const Path_Name& name)const
{
	data_field_map_const_iterator_t it = dl_.find(name);
	const Data_Field *df=NULL;
	if(it!=dl_.end()){
		df=(*it).second;
	}

	return df;
}

// Walk throught the message
intifada::Record::Field_Iterator_Writer::Field_Iterator_Writer(data_field_map_t& dl,const uint8_t *s,const Stream::size_type& offset,const Stream::size_type& total_size)
:inherited(dl)
,s_(s)
,offset_(offset)
,total_size_(total_size)
{}

int intifada::Record::Field_Iterator_Writer::operator()(const Path_Name& name)
{
	clog("Record") << "Field_Iterator_Writer::operator()(" << name << ")" << std::endl;
	// Get associated pointer

	Path_Name lastpart;
	if(name.empty()!=true){
		lastpart=name.get_back();
	}

	Data_Field *df=find_data_field(lastpart);
	Stream::size_type writed_size=0;
	if(df!=NULL){
		clog("size debug") << "Field_Iterator_Writer::set_stream(0x"<<s_<<","<<offset_<<","<< total_size_ << ")" << std::endl;
		writed_size = df->set_stream(s_,offset_,total_size_);
		clog ("stream pos") << name << ":pos=" << std::dec << offset_ << " size=" << writed_size << std::endl;
		if(writed_size==0){
			// throw an exception as a record couldn't have a null size
			throw Parsing_Input_Record_Length_Exception(name,0);
		}
		clog("size debug") << "Field_Iterator_Writer::wrote:" << writed_size << std::endl;
		offset_+=writed_size;
	}else{
		throw Description_Field_Unknow_Exception(name);
	}

	return writed_size;
}

// Walk throught the message
intifada::Record::Field_Iterator_Reader::Field_Iterator_Reader(const data_field_map_t& dl,uint8_t *s,const Stream::size_type& offset,const Stream::size_type& total_size)
:inherited(dl),
 s_(s),
 offset_(offset),
 total_size_(total_size)
{}

int intifada::Record::Field_Iterator_Reader::operator()(const Path_Name& name)
{
	clog("Record") << "Field_Iterator_Reader::operator()(" << name << ")" << std::endl;
	// Get associated pointer
	Path_Name lastpart=name.get_back();
	const Data_Field *df=find_data_field(lastpart);

	Stream::size_type read_size=0;
	if(df!=NULL){
		read_size = df->get_stream(s_,offset_,total_size_);
#warning correction bug DTI
		//assert(read_size!=0);
		offset_+=read_size;
	}else{
		std::cerr << "31-" << name.get_full_name() << std::endl;
		throw Description_Field_Unknow_Exception(name);
	}
	return read_size;
}

// Walk throught the message
intifada::Record::Field_Iterator_Size::Field_Iterator_Size(const data_field_map_t& dl)
:inherited(dl)
{}

int intifada::Record::Field_Iterator_Size::operator()(const Path_Name& name)
{
	// Get associated pointer
	Path_Name lastpart=name.get_back();
	const Data_Field *df=find_data_field(lastpart);
	Stream::size_type read_size=0;
	if(df!=NULL){
		read_size = df->size();
	}else{
		std::cerr << "32-" << name.get_full_name() << std::endl;
		throw Description_Field_Unknow_Exception(name);
	}
	clog("size debug") << "\t\t\t" << name << " size=" << read_size << std::endl;
	return read_size;
}

intifada::Record::Foreach_Field_Iterator::Foreach_Field_Iterator(data_field_map_t& dl,Data_Field::Functor& it)
:inherited(dl)
,it_(it)
{}

int intifada::Record::Foreach_Field_Iterator::operator()(const Path_Name& name)
{
	Path_Name back=name.get_back();
	Data_Field *df=find_data_field(back);

	clog("Foreach_Field_Iterator") << "operator()(" << name << "=" << df << ")" << std::endl;
	if(df!=NULL){
		df->foreach(it_,name);
	}else{
		std::cerr << "33-" << name.get_full_name() << std::endl;
		throw Description_Field_Unknow_Exception(name);
	}
	return 0;
}

intifada::Record::Foreach_Field_Const_Iterator::Foreach_Field_Const_Iterator(const data_field_map_t& dl,Data_Field::Const_Functor& it)
:inherited(dl)
,it_(it)
{}

int intifada::Record::Foreach_Field_Const_Iterator::operator()(const Path_Name& name)
{
	const Data_Field *df=find_data_field(name);
	if(df!=NULL){
		df->foreach(it_,name);
	}else{
		std::cerr << "34-" << name.get_full_name() << std::endl;
		throw Description_Field_Unknow_Exception(name);
	}
	return 0;
}

intifada::Record::Record()
:inherited()
,description_()
,identity_()
,category_()
,fspec_()
,dl_()
,longname_list_()
,presence_()
,compound_(false)
,basicexplicit_(false)
,indicatorlength_(false){
	clog("Memory_Allocation") << "0x" << this << ":ctor Record" << std::endl;
	this->register_property("compound",compound_);
	this->register_property("basicexplicit",basicexplicit_);
	this->register_property("indicatorlength",indicatorlength_);
}

intifada::Record::Record(const Path_Name& compound_name)
:inherited()
,description_()
,identity_()
,category_()
,fspec_(compound_name)
,dl_()
,longname_list_()
,presence_()
,compound_(false)
,basicexplicit_(false)
,indicatorlength_(false){
	clog("Memory_Allocation") << "0x" << this << ":ctor Record(" << compound_name << ")" << std::endl;
	this->register_property("compound",compound_);
	this->register_property("basicexplicit",basicexplicit_);
	this->register_property("indicatorlength",indicatorlength_);
}

intifada::Record::Record(uint8_t max_size,const Path_Name& compound_name)
:inherited()
,description_()
,identity_()
,category_()
,fspec_(max_size,compound_name)
,dl_()
,longname_list_()
,presence_()
,compound_(false)
,basicexplicit_(false)
,indicatorlength_(false){
	clog("Memory_Allocation") << "0x" << this << ":ctor Record(" << (int)max_size << "," << compound_name << ")" << std::endl;
	this->register_property("compound",compound_);
	this->register_property("basicexplicit",basicexplicit_);
	this->register_property("indicatorlength",indicatorlength_);
}


intifada::Record::Record(const Record&rhs)
:inherited(rhs)
,description_(rhs.description_)
,identity_(rhs.identity_)
,category_(rhs.category_)
,dl_()
,longname_list_()
,presence_(rhs.presence_)
,compound_(false)
,basicexplicit_(false)
,indicatorlength_(false){
	// relink properties
	this->relink_property("compound",compound_);
	this->relink_property("basicexplicit",basicexplicit_);
	this->relink_property("indicatorlength",indicatorlength_);

	// Copy data fields instantiation just structure
	for(data_field_map_const_iterator_t it = rhs.dl_.begin();it != rhs.dl_.end();++it){
		const Path_Name &rhs_name=(*it).first;
		string_string_list_const_iterator_t it_longname=rhs.longname_list_.find(rhs_name);
		Data_Field *d = (*it).second->clone();
		this->insert(rhs_name,(*it_longname).second,d);
	}
}

intifada::Record::~Record()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor Record" << std::endl;
	this->clear();
}

void intifada::Record::set_property(const std::string& prop,const std::string& val){
	Properties::set_property(prop,val);
	if(compound_==true){
		// compound type reverse frn order
		fspec_.set_property("reversefrnorder","true");
	}
	if(basicexplicit_==true){
		// don't read fspec but the field size on one byte
		// don't write fspec but the field size on one byte

		// force item presence
		fspec_.set_property("forceitempresence","true");
	}
}

void intifada::Record::clear()
{
	// Delete all data fields
	for(data_field_map_const_iterator_t it = dl_.begin();it != dl_.end();++it){
		delete (*it).second;
	}
}

void
intifada::Record::set_identity(const std::string& desc,const std::string& ident,uint8_t cat)
{
	description_=desc;
	identity_=ident;
	category_=cat;
}

/// get field size
intifada::Stream::size_type intifada::Record::size()const
{

	Stream::size_type size = 0;
	/*if(basicexplicit_==true){
    size=1;
  }else{
    size = fspec_.size();
  }*/
	size = fspec_.size();
	if(indicatorlength_==true){
		// Reserve 1 byte for field size
		size+=1;
	}

	clog("size debug") << "\t\t\tfspec size=" << size << std::endl;
	Field_Iterator_Size d(dl_);
	Stream::size_type partial_size = fspec_.foreach(d,this);
	return size+partial_size;
}

uint8_t intifada::Record::get_max_size()const
{
	return fspec_.get_max_size();
}

/// Register a condition
// void
// intifada::Record:: register_condition(const std::string& name,Data_Field_Condition* cond)
// {
//   fspec_.register_condition(name,cond);
//   return;
// }

int intifada::Record::insert(
		uint8_t pos
		,const Path_Name& name
		,const Condition* cond)
{
	return fspec_.insert(pos,name,cond);
}

int intifada::Record::insert(
		uint8_t pos
		,const Path_Name& name)
{
	return fspec_.insert(pos,name);
}


/// association between a name, a field type and a position in a stream
int
intifada::Record::insert(const Path_Name& name,const std::string& longname,Data_Field*data)
{
	int ret=0;
	data_field_map_pair_t p = dl_.insert(std::make_pair(name,data));
	if(p.second==false){
		ret= -1;
	}else{
		string_string_pair_t lni=longname_list_.insert(std::make_pair(name,longname));
		if(lni.second==false){
			ret=-1;
		}
	}

	return ret;
}

/// Byte stream acquisition
intifada::Stream::size_type intifada::Record::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	clog("Record") << "Record::set_stream(0x"<<s<<","<<o<<","<< m << ")" << std::endl;
	// Setting fspec part of record field if isn't an explicit stream
	Stream::size_type size=0;
	Stream::size_type offset=o;
	if(indicatorlength_==true)
	{
		clog("Record")<<"indicator length true"<< std::endl;
		// Ignoring indicator length. It isn't used because we know structure field
		offset+=1;
	}
	/*
    if(basicexplicit_==true){
      size = 1;
      Dump_Item di(this);
      fspec_.foreach_structure(di);
    }else{

    }*/
	size = fspec_.set_stream(s,offset,m);
	clog("size debug") << "Record::set_stream:fspec size="<<size<<std::endl;
	offset+=size;
	Field_Iterator_Writer d(dl_,s,offset,m);

	Stream::size_type partial_size= fspec_.foreach(d,this);

	clog("size debug") << "Record::set_stream:record size="<<partial_size<<std::endl;
	clog("Record")<<"<--set_stream"<< std::endl;
	return partial_size+size;
}

/// Byte stream restitution
intifada::Stream::size_type intifada::Record::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{
	// Getting fspec part of record field
	Stream::size_type offset=o;
	Stream::size_type size = fspec_.get_stream(s,offset,m);
#warning correction bug DTI
	//assert(size!=0);
	intifada::Stream::size_type total_size=0;
	if(size!=0)
	{
		offset+=size;
		if(indicatorlength_==true)
		{
			// Reserve 1 byte for field size
			offset+=1;
		}
		Field_Iterator_Reader d(dl_,s,offset,m);
		Stream::size_type partial_size = fspec_.foreach(d,this);
		total_size=partial_size+size;
		if(basicexplicit_==true){
			s[o]=total_size & 0xff;
		}
	}
	return total_size;
}

bool intifada::Record::get_presence(const Path_Name& name)const
{
	bool presence=false;

	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	if(subpart.empty()!=false){
		presence=fspec_.get_presence(part,this);
	}else{
		Data_Field *df=get_subfield(part);
		clog("Record") << "get_presence("<<name<<"):"<<part<< "=" << df << " (" << subpart<< ")"<<std::endl;
		presence=df->get_presence(subpart);
	}
	return presence;
}

void intifada::Record::set_presence(const Path_Name& name,bool set)
{
	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	if( (set==true)||((set==false)&&(subpart.empty()!=false))){
		clog("Record") << "fspec_.set_presence(" << part << "," << set << ")" << std::endl;
		fspec_.set_presence(part,set,this);
	}
	if(subpart.empty()!=true){
		Data_Field *df=get_subfield(part);
		clog("Record") << "set_presence("<<name<<","<<set<<"):"<<part<< "=" << df << " (" << subpart<< ")"<<std::endl;
		df->set_presence(subpart,set);
	}
}

const intifada::Type& intifada::Record::get_value(const Path_Name& name)const
{
	clog("Record") << "get_value(" << name << ")-->" << std::endl;
	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	Data_Field *df=get_subfield(part);
	clog("Record") << "\tpart="<<part<<",datafield=0x"<<df<<std::endl;
	const Type*ret=NULL;

	if(df != NULL){
		if(subpart.empty()!=true){
			ret=&df->get_value(subpart);
		}else{

			bool f = this->get_presence(name);
			//#warning "Potential problem with concurrent runnings"
			presence_=f;
			ret=&presence_;
		}
	}else{}
	clog("Record") << "<--get_value(" << name << ")" << std::endl;
	return *ret;
}

intifada::Type& intifada::Record::get_value(const Path_Name& name)
{
	clog("Record") << "get_value(" << name << ")-->" << std::endl;
	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	Data_Field *df=this->get_subfield(part);
	clog("Record") << "\tpart="<<part<<",datafield=0x"<<df<<std::endl;
	Type*ret=NULL;

	if(df != NULL){
		if(subpart.empty()!=true){
			ret=&df->get_value(subpart);
		}else{
			bool f = this->get_presence(name);
			presence_=f;
			ret=&presence_;
		}
	}else{
		throw Description_Field_Unknow_Exception(name);
	}
	clog("Record") << "<--get_value(" << name << ")" << std::endl;
	return *ret;
}

intifada::Data_Field* intifada::Record::clone()const
{
	// create a record
	Record * ret=new Record;
	ret->clone(this);

	return ret;
}

intifada::Record* intifada::Record::clone(const Record*rhs)
{
	if(this!=rhs){
		// copy flags
		this->Properties::operator=(*rhs);

		description_=rhs->description_;
		identity_=rhs->identity_;
		category_=rhs->category_;
		presence_=rhs->presence_;

		// relink properties
		this->relink_property("compound",compound_);
		this->relink_property("basicexplicit",basicexplicit_);

		// 2/11/2011
		this->relink_property("indicatorlength",indicatorlength_);

		// Cloning field specification
		fspec_.clone(rhs->fspec_);

		// Cloning sub fields
		for(data_field_map_const_iterator_t it=rhs->dl_.begin();it != rhs->dl_.end();++it){
			const Path_Name &name=(*it).first;
			string_string_list_const_iterator_t it_longname=rhs->longname_list_.find(name);
			Data_Field *data=(*it).second;
			this->insert(name,(*it_longname).second,data->clone());
		}
	}
	return this;
}

int
intifada::Record::foreach(Functor &it,const Path_Name& sel)
{
	Foreach_Field_Iterator fspec_iterator(dl_,it);
	int ret=fspec_.foreach(fspec_iterator,this,sel);
	return ret;
}

int
intifada::Record::foreach(Const_Functor &it,const Path_Name& sel)const
{
	Foreach_Field_Const_Iterator fspec_iterator(dl_,it);
	int ret=fspec_.foreach(fspec_iterator,this,sel);
	return ret;
}

int
intifada::Record::foreach_structure(Structure_Functor *f)const
{
	int status=fspec_.foreach_structure(f);
	for(data_field_map_const_iterator_t it = dl_.begin();it!=dl_.end();++it){
		Data_Field *field=(*it).second;
		const Path_Name& name=(*it).first;
		string_string_list_const_iterator_t it_longname=longname_list_.find(name);
		f->begin_item((*it_longname).second,name,*field);


		const Fixed_Length_Data_Field *fldf=dynamic_cast<const Fixed_Length_Data_Field *>(field) ;
		const Extended_Length_Data_Field *eldf=dynamic_cast<const Extended_Length_Data_Field *>(field);
		const Repetitive_Length_Data_Field *rldf=dynamic_cast<const Repetitive_Length_Data_Field *>(field);
		const Record *cdf=dynamic_cast<const Record *>(field);

		if(fldf!=NULL){
			// f->begin_fixed_field(fldf->size());
			f->begin_fixed_field(*fldf);
		}else if(eldf!=NULL){
			//f->begin_extended_field(eldf->is_repetitive(),eldf->primary_size(),eldf->secondaries_size());
			f->begin_extended_field(*eldf);
		}else if(rldf!=NULL){
			// f->begin_repetitive_field(rldf->get_part_size(),rldf->getf());
			f->begin_repetitive_field(*rldf);
		}else if(cdf!=NULL){
			f->begin_compound_field(cdf->get_max_size());
			cdf->foreach_structure(f);
			f->end_compound_field();
		}

		field->foreach_structure(f);
		f->end_field();
		f->end_item();
	}
	return status;
}

const intifada::Type& intifada::Record::get_type(const Path_Name& name)const
{
	clog("Record") << "get_type(" << name << ")-->" << std::endl;
	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	Data_Field *df=this->get_subfield(part);
	clog("Record") << "\tpart="<<part<<",datafield=0x"<<df<<std::endl;
	const Type*ret=NULL;

	if(df != NULL){
		ret=&df->get_type(subpart);
	}
	clog("Record") << "<--get_type(" << name << ")" << std::endl;
	return *ret;
}

bool intifada::Record::exist_item(const Path_Name& name)const
{
	clog("Record") << "exist_item(" << name << ")-->" << std::endl;
	Path_Name subpart=name;
	std::string part=subpart.get_front();
	subpart.pop_front();

	Data_Field *df=this->get_subfield(part);
	if(df!=NULL)
		return true;
	clog("Record") << "<--exist_item(" << name << ")" << std::endl;
	return false;
}

intifada::Data_Field *
intifada::Record::get_subfield(const std::string& name)const
{
	// Get associated pointer
	data_field_map_const_iterator_t it = dl_.find(name);
	Data_Field *df=NULL;
	if(it!=dl_.end()){
		df=(*it).second;
	}
	return df;
}
