/*
   Copyright (C) 2009-2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_HXX
# define LOGGER_HXX

# include <iostream>
# include <iomanip>
# include <string>
# include <set>

# define UNUSED_ARG(arg) do{}while(&arg<0)

class Logger
  {
public:
  typedef std::set<std::string> strings_mask_type;
private:

  typedef strings_mask_type::const_iterator strings_mask_const_iterator_type;
public:
  Logger(std::ostream &os);
static Logger& instance(std::ostream &os);

/// insert a mask
/**
 * insert a mask in the log system. corresponding Logger("mask") will be displayed.
 * More than one mask could be set at one time using '|' as separator
 * @param m mask(s) to display
 * @return number of set masks
 */
  int insert_mask(const std::string& m);

  /// remove a mask
  /**
   * remove a mask in the log system. corresponding Logger("mask") will be not displayed.
   * More than one mask could be unset at one time using '|' as separator
   * @param m mask(s) to undisplay
   * @return number of unset masks
   */
  int remove_mask(const std::string& m);

  /// Get a constant reference on active masks
  /**
   *
   * @return constant reference on active masks
   */
  const strings_mask_type& get_masks()const;

  /// Clear all masks
  void clear_mask();

  /// Check if a mask is active
  /**
   *
   * @param m mask to check presence
   * @return true if m is present, false otherwise
   */
  bool is_present(const std::string& m)const;

  Logger& operator()(const std::string& n);

  template <typename T>
  Logger & operator<< (const T& n);

  Logger & operator<< (const std::string& n);

  Logger & operator<< (const char* n);

  Logger & operator<< (int n);

  Logger& operator<<(std::ostream& (*pf)(std::ostream&));

  Logger& operator<<(std::ios_base& (*pf) (std::ios_base&));


  Logger& operator<<(Logger& (*pf)(Logger&));

  void lock(const std::string& cond);

  void unlock();

private:
  void split(strings_mask_type& s,const std::string& list_filter);
private:
  static Logger* instance_;
  bool display_;

  std::ostream *pos_;
  strings_mask_type mask_;

  };

template <typename T>
Logger & Logger::operator<< (const T& n)
  {
#ifndef NDEBUG
    if(pos_!=NULL && display_!=false) (*pos_)<<n;
#else
    UNUSED_ARG(n);
#endif
    return *this;
  }

//extern Logger clog;
#define clog Logger::instance(std::cout)
//static nifty_counter nifty;


#endif // LOGGER_HXX
