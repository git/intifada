/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_DATA_HXX
# define INTIFADA_DATA_HXX

# include <intifada/Properties.hxx>
# include <intifada/inttypes.h>

# include <intifada/Exception.hxx>
# include <intifada/Stream.hxx>

namespace intifada
  {
    class Data : public Properties
      {
      public:
        typedef Properties inherited;
      protected:
        Data();
        Data(const Data&rhs);
      public:
        virtual ~Data();

        /// get field size
        /**
         * \return size of field
         */
        virtual Stream::size_type size()const=0;

        /// Byte stream acquisition
        /**
         * Handle acquisition of asterix binary stream
         * \param s stream pointer
         * \param o offset in stream pointer
         * \param m maximum stream size
         * \return readed total bytes
         * \throw Parsing_Input_Length_Exception if bytes to read exceed m
         * \throw Description_Field_Unknow_Exception if an unknow field is encountered
         */
        virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m) = 0;

        /// Byte stream restitution
        /**
         * Handle byte stream restitution.
         * \param s stream pointer
         * \param o offset in stream pointer
         * \param m maximum stream size
         * \return writed total bytes
         * \throw Parsing_Output_Length_Exception if bytes to read exceed m
         */
        virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const=0;
      };
  }

#endif // INTIFADA_DATA_HXX
