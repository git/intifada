/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Data_Field_Characteristics.hxx>

#include <intifada/Type_Repository.hxx>
#include <intifada/Unit_Converter_Repository.hxx>
#include <intifada/Type.hxx>
#include <intifada/Logger.hxx>
#ifndef INTIFADA_USE_INLINE
# include<intifada/Data_Field_Characteristics.ixx>
#endif

intifada::Data_Field_Characteristics::Data_Field_Characteristics(/*const type& t*/):
ti_(NULL)
,start_position_(0)
,size_(0)
,cached_(false)
{
	clog("Memory_Allocation") << "0x" << this << ":ctor Data_Field_Characteristics" << std::endl;
}

intifada::Data_Field_Characteristics::Data_Field_Characteristics(const Data_Field_Characteristics&rhs):
				  ti_()
,start_position_(rhs.start_position_)
,size_(rhs.size_)
//,part_(rhs.part_)
,cached_(false)
{
	if(rhs.ti_!=NULL)
	{
		ti_=rhs.ti_->clone();
	}
}

intifada::Data_Field_Characteristics&
intifada::Data_Field_Characteristics::operator=(const Data_Field_Characteristics&rhs)
{
	if(this != &rhs)
	{
		if(rhs.ti_!=NULL)
		{
			ti_=rhs.ti_->clone();
		}
		start_position_=rhs.start_position_;
		size_=rhs.size_;
		cached_=false;
	}
	return *this;
}

intifada::Data_Field_Characteristics::~Data_Field_Characteristics()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor Data_Field_Characteristics" << std::endl;
	if(ti_!=NULL)
	{
		delete ti_;
	}
}

int intifada::Data_Field_Characteristics::set(const std::string& t,uint8_t s, uint8_t sz)
{
	const Type* ti=Type_Repository::instance()->get(t);

	int ret=-1;

	if(ti!=NULL)
	{
		// size verification. return -1 and doesn't allocate anything if anything
		// goes wrong
		// -1 >= C
		// 0  0  0
		// 0  1  1
		// 1  0  1
		// 1  1  1
		if((ti->get_size()>=sz) || (ti->get_size()==-1))
		{
			ti_=ti->clone();
			start_position_=s;
			size_=sz;
			ret=0;
		}
	}
	return ret;
}

void intifada::Data_Field_Characteristics::set_property(const std::string& prop,const std::string& val)
{
	if(ti_!=NULL)
	{
		ti_->set_property(prop,val);
	}
	return;
}

void intifada::Data_Field_Characteristics::set_converter(const std::string& converter)
{
	if(ti_!=NULL)
	{
		ti_->set_unit_converter(Unit_Converter_Repository::instance()->clone(converter));
	}
}
