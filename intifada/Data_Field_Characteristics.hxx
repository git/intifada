/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_FIELD_CHARACTERISTICS_HXX
# define DATA_FIELD_CHARACTERISTICS_HXX

# include <intifada/inttypes.h>
# include <intifada/build_configuration.h>

# include <string>
# include <stdexcept>

namespace intifada
{

  class Type;

  /// field description
/**
 * This class describe field characteristics:
 * - type
 * - start position in byte
 * - size
 */
  class Data_Field_Characteristics
  {
  public:
    Data_Field_Characteristics();

    Data_Field_Characteristics(const Data_Field_Characteristics&rhs);

    Data_Field_Characteristics& operator=(const Data_Field_Characteristics&rhs);

    virtual ~Data_Field_Characteristics();

    /// Get cached status
    /**
     * \return true if cached
     * \return false if not cached
     *
     * mean of cached is that ti_ reflect stream. when a record is set from a
     * stream, only items are stored (fixed, extended, repetitive or compound).
     * When a data item is accessed, the associated type is updated with the
     * stream content. we could saying that the stream part is cached in associated
     * type.
     * This information is important because when the stream is rebuild, only
     * cached ti_ are used to update, as other are the same.
     */
    bool is_cached()const;

    /// Set cached status
    /**
     *
     */
    void set_cached()const;

    /// Initialize type
    /**
     * \param t type name
     * \param s lsb type position
     * \param sz type size
     *
     * \return -1 on error (certainly, t couldn't be associated with concret type)
     * \return 0 if successfull
     */
    int set(const std::string& t,uint8_t s, uint8_t sz);

    //int set(const std::string& t,uint8_t s, uint8_t sz, uint8_t part);

    void set_property(const std::string& prop,const std::string& val);

    void set_converter(const std::string& converter);

    uint8_t size()const;
    uint8_t shift()const;

    Type* get_type()const;

    void set_null();

  private:
    // type t_;
    Type *ti_;
    uint8_t start_position_;
    uint8_t size_;

    mutable bool cached_;
  };

}
# ifdef INTIFADA_USE_INLINE
#  include<intifada/Data_Field_Characteristics.ixx>
# endif

#endif // DATA_FIELD_CHARACTERISTICS_HXX
