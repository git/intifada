/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_STREAM_HXX
# define INTIFADA_STREAM_HXX

# include <intifada/inttypes.h>
# include <intifada/build_configuration.h>
# include <vector>
# include <intifada/Exception.hxx>
namespace intifada
  {
    class Data_Field_Characteristics;

    /// Stream byte
    class Stream
      {
    private:
      typedef std::vector<uint8_t> tabint_t;

      typedef tabint_t::allocator_type::reference reference;
      typedef tabint_t::allocator_type::const_reference const_reference;

      typedef tabint_t::iterator iterator;
      typedef tabint_t::const_iterator const_iterator;

    public:
      typedef tabint_t::value_type value_type;
      typedef tabint_t::size_type size_type;

      typedef tabint_t::const_reverse_iterator const_reverse_iterator;
      typedef tabint_t::reverse_iterator reverse_iterator;

    public:
      /// Build an extensible stream
      Stream();

      /// Build an fixed size stream
      Stream(size_type n);

      virtual ~Stream();

      const_reverse_iterator rbegin()const;
      const_reverse_iterator rend()const;

      reverse_iterator rbegin();
      reverse_iterator rend();

      reference operator[] (size_type n);
      const_reference operator[] (size_type n) const;

      void push_back (const value_type &x);

      void pop_back(size_type s=1);
      reference back();

      /// Check if stream size is null
      /**
       * \return true if *this is empty
       * \return false if *this isn't empty
       *
       * \remarks prefer using empty instead of size if you want only check
       * empty stream state
       */
      bool empty()const;

      size_type size()const;

      /// Resize internal vector
      /**
       * \param new_size extend (or shrink) internal vector
       */
      void resize(size_type new_size);
    public:
      /// Extend internal vector as necessary
      /**
       *
       * @param new_size size to extend vector if necessary
       */
      void extend(size_type new_size);

      void clear();

      /// bit modificator by byte and offset
      /**
       * this method set a bit.
       * Internal data handler isn't resized
       * \param b byte (begins at 1)
       * \param o offset in byte b (begin at 1)
       */
      void set_bit(uint8_t b,uint8_t o,bool s);

      /// bit modificator by total offset
      /**
       * this method set a bit.
       * Internal data handler isn't resized
       * \param b total byte
       */
      void set_bit(uint8_t b,bool s);

      /// bit reader by total offset
      /**
       * This method read a bit.
       *
       * Access is by total offset: Starting at the last byte
       * and go back to the first (begining to 1)
       */
      bool get_bit(uint8_t b)const;

      /// bit reader by byte and offset
      /**
       * read a bit
       * \param byte byte to read (starting at 1)
       * \param o offset to read (starting at 1)
       */
      bool get_bit(uint8_t b,uint8_t o)const;

      ///
      /**
       */
      //   template<typename T>
      //   void get_value(const std::string& name,T& ret)const;

      ///
      /**
       */
      //   template<typename T>
      //   void set_value(const std::string& name,T& ret);

      /// Byte stream acquisition
      /**
       * Handle acquisition of asterix binary stream
       * \param s stream pointer
       * \param m maximum stream size
       * \return readed total bytes
       * \throw Parsing_Input_Length_Exception if bytes to read exceed m
       */
      virtual Stream::size_type set_stream(const uint8_t *s, Stream::size_type m);

      /// Byte stream restitution
      /**
       * Handle byte stream restitution.
       * \param s stream pointer
       * \param m maximum stream size
       * \return writed total bytes
       * \throw Parsing_Output_Length_Exception if bytes to read exceed m
       */
      virtual Stream::size_type get_stream(uint8_t *s, Stream::size_type m)const;

      // const Data_Field_Characteristics& get_structure(const std::string& name)const;

      /// Set position and size, related to asterix stream byte used in set_stream
      /**
       * \param pos stream position
       * \param size stream size
       * \remaks could be const as it doesn't change state of stream
       */
      void set_stream_position(const size_type& pos,const size_type& size)const;

      /// Get position and size, related to asterix stream byte used in set_stream
      /**
       * \param pos stream position
       * \param size stream size
       */
      void get_stream_position(size_type& pos,size_type& size)const;

    protected:
      virtual const_reverse_iterator get_stream_position(const Data_Field_Characteristics& c)const;
      virtual reverse_iterator get_stream_position(const Data_Field_Characteristics& c);

    private:
      // Internal stream byte
      tabint_t t_;

      // Position in original stream used in set_stream
      mutable size_type stream_pos_;
      // Size in original stream used in set_stream
      mutable size_type stream_size_;

      };
  }
#ifdef INTIFADA_USE_INLINE
# include<intifada/Stream.ixx>
#endif

#include <intifada/Stream.txx>

#endif // STREAM_HXX
