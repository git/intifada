/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_CONDITION_LIST_HXX
# define INTIFADA_CONDITION_LIST_HXX

# include <vector>
# include <intifada/Condition.hxx>
namespace intifada
{
  class Condition;

  class Condition_List
  {
  public:
    typedef const Condition* value_type;

  private:
    typedef std::vector<value_type> conditionals_t;

  public:
    typedef conditionals_t::size_type size_type;

  public:
    Condition_List();
    virtual ~Condition_List();

    value_type operator[](size_type)const;

    void push_back(value_type&);

    size_type size()const;

  private:
    conditionals_t conds_;
  };
}

#endif // INTIFADA_CONDITION_LIST_HXX
