/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */


template <typename _Elem>
intifada::Record_Iterator_List::multimap<_Elem>::multimap()
{}

template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::const_iterator
intifada::Record_Iterator_List::multimap<_Elem>::end()const
{
  return list_.end();
}

template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::iterator
intifada::Record_Iterator_List::multimap<_Elem>::lower_bound(key k)
{
  return list_.lower_bound(k);
}

template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::const_iterator
intifada::Record_Iterator_List::multimap<_Elem>::lower_bound(key k)const
{
  return list_.lower_bound(k);
}

template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::iterator
intifada::Record_Iterator_List::multimap<_Elem>::upper_bound(key k)
{
  return list_.upper_bound(k);
}

template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::const_iterator
intifada::Record_Iterator_List::multimap<_Elem>::upper_bound(key k)const
{
  return list_.upper_bound(k);
}


template <typename _Elem>
typename intifada::Record_Iterator_List::multimap<_Elem>::iterator
intifada::Record_Iterator_List::multimap<_Elem>::insert(_Elem e,key c)
{
  return list_.insert(std::make_pair(c,e));
}

template <typename _Elem>
bool
intifada::Record_Iterator_List::multimap<_Elem>::exist(key k)const
{
  const_iterator it=list_.find(k);
  bool ret=false;
  if(it!=list_.end()){
    ret=true;
  }
  return ret;
}
