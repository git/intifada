/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_RECORD_HXX
# define INTIFADA_RECORD_HXX

# include <intifada/Data_Field.hxx>
# include <intifada/Field_Specification_Data.hxx>
# include <intifada/Field_Specification_Functor.hxx>
# include <intifada/Map.hxx>
# include <intifada/Integer.hxx>
namespace intifada
{
class Structure_Iterator;

/// Record Class
/**
 * Record class is used to handle Record part of an asterix message.
 * It could implement Compound and Explicit Data fields
 * Compound fields (COMPOUND):
 * # user application profile is reversed
 * Explicit fields:
 * - BASICEXPLICIT : UNUSED
 * # record size is readed/writed from/to stream instead of field specification
 * # sub items are always presents regardless of field specification setup
 * # sub items must be described in UAP.
 *
 */
class Record : public Data_Field
{
public:
	typedef Data_Field inherited;

private:
	typedef Unordered_Map<Path_Name,Data_Field *> data_field_map_t;
	typedef data_field_map_t::iterator data_field_map_iterator_t;
	typedef data_field_map_t::const_iterator data_field_map_const_iterator_t;
	typedef data_field_map_t::pair data_field_map_pair_t;

private:
	class Record_Field_Iterator : public
	virtual Field_Specification_Functor
	{
	public:
		Record_Field_Iterator(data_field_map_t& dl);

	protected:
		Data_Field * find_data_field(const Path_Name& name);

	private:
		data_field_map_t &dl_;
	};

	class Record_Field_Const_Iterator : public
	virtual Field_Specification_Functor
	{
	public:
		Record_Field_Const_Iterator(const data_field_map_t& dl);

	protected:
		const Data_Field * find_data_field(const Path_Name& name)const;

	private:
		const data_field_map_t &dl_;
	};

	class Field_Iterator_Writer : public Record_Field_Iterator
	{
	public:
		typedef Record_Field_Iterator inherited;
	public:
		Field_Iterator_Writer(data_field_map_t& dl,const uint8_t *s,const Stream::size_type& offset,const Stream::size_type& total_size);
		virtual int operator()(const Path_Name& name);


	private:
		const uint8_t *s_;
		Stream::size_type offset_;

		Stream::size_type total_size_;
	};
	class Field_Iterator_Reader : public Record_Field_Const_Iterator
	{
	public:
		typedef Record_Field_Const_Iterator inherited;
	public:
		Field_Iterator_Reader(const data_field_map_t& dl,uint8_t *s,const Stream::size_type& offset,const Stream::size_type& total_size);
		virtual int operator()(const Path_Name& name);
	private:
		uint8_t *s_;
		Stream::size_type offset_;
		Stream::size_type total_size_;
	};
	class Field_Iterator_Size : public Record_Field_Const_Iterator
	{
	public:
		typedef Record_Field_Const_Iterator inherited;
	public:
		Field_Iterator_Size(const data_field_map_t& dl);
		virtual int operator()(const Path_Name& name);
	};

	class Foreach_Field_Iterator : public Record_Field_Iterator
	{
	public:
		typedef Record_Field_Iterator inherited;
	public:
		Foreach_Field_Iterator(data_field_map_t& dl,Data_Field::Functor& it);
		virtual int operator()(const Path_Name& name);

	private:
		Data_Field::Functor& it_;
	};

	class Foreach_Field_Const_Iterator : public Record_Field_Const_Iterator
	{
	public:
		typedef Record_Field_Const_Iterator inherited;
	public:
		Foreach_Field_Const_Iterator(const data_field_map_t& dl,Data_Field::Const_Functor& it);
		virtual int operator()(const Path_Name& name);

	private:
		Data_Field::Const_Functor& it_;
	};



	private:
	typedef Map<Path_Name,std::string> string_string_list_t;
	typedef string_string_list_t::iterator string_string_list_iterator_t;
	typedef string_string_list_t::const_iterator string_string_list_const_iterator_t;
	typedef string_string_list_t::pair string_string_pair_t;

	public:

	/// Record constructor without any control on fspec size
	Record();

	/// Record constructor with control on fspec size
	/**
	 * explicit type oriented constructor
	 * \param name explicit type name
	 */
	 Record(const Path_Name& name);

	/// Record constructor with control on fspec size
	/**
	 * compound oriented constructor
	 * \param max_size field specification max size
	 * \param name record name
	 */
	 Record(uint8_t max_size,const Path_Name& name);

	/// Copy operator
	Record(const Record&rhs);

	virtual ~Record();

	/// add flags
	/**
	 * Virtual to handling automatic set flags
	 */
	virtual void set_property(const std::string& prop,const std::string& val);

	/// clear the message content
	void clear();

	/// initialize identiy
	/**
	 * \param description (long) record name
	 * \param id identity unique identification of record. Must be unique
	 * \param cat record category
	 */
	void set_identity(const std::string& desc,const std::string& ident,uint8_t cat);

	/// get field size
	/**
	 * \return size of field
	 */
	virtual Stream::size_type size()const;

	/// get maximum record size
	/**
	 * \return maximum record size (compound only)
	 */
	uint8_t get_max_size()const;

	/// Register condition
	/**
	 * \param name condition name
	 * \param cond associated name cond
	 */
	// void register_condition(const std::string& name,Data_Field_Condition* cond);

	/// create an UAP with a condition
	/**
	 * \param pos position in field specification (msb of last field specification byte is 1)
	 * \param name subfield name
	 * \param cond existance condition
	 *
	 * \return 0 if insertion is successfull
	 * \return -1 if insertion fail
	 */
	int insert(
			uint8_t pos
			,const Path_Name& name
			,const Condition* cond);

	/// create an UAP without any condition
	/**
	 * \param pos position in field specification (msb of last field specification byte is 1)
	 * \param name subfield name
	 *
	 * \return 0 if insertion is successfull
	 * \return -1 if insertion fail
	 */
	int insert(uint8_t pos, const Path_Name& name);

	/// association between a name, a field type and a position in a stream
	/**
	 * \param name subfield name
	 * \param longname subfield long name
	 * \param data data field to insert
	 * \return 0 if insertion is successfull
	 * \return -1 if insertion fail
	 */
	int insert(const Path_Name& name,const std::string& longname,Data_Field*data);

	/// Byte stream acquisition
	/**
	 * Handle acquisition of asterix binary stream
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return readed total bytes
	 * \throw Parsing_Input_Length_Exception if bytes to read exceed m
	 * \throw Description_Field_Unknow_Exception if an unknow field is encountered
	 */
	virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

	/// Byte stream restitution
	/**
	 * Handle byte stream restitution.
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return writed total bytes
	 * \throw Parsing_Output_Length_Exception if bytes to read exceed m
	 */
	virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

	/// get presence of a field
	/**
	 * \param name target item
	 * \return true if item is present in the record
	 * \return false if item isn't present in the record
	 * \throw Description_Field_Unknow_Exception if name isn't an item
	 */
	virtual bool get_presence(const Path_Name& name)const;

	/// set presence of a field
	/**
	 * \param name target item
	 * \param set presence/absence (true/false) of name
	 * \throw Description_Field_Unknow_Exception if name isn't an item
	 *
	 * it can handle nested compound item set=true: 020 will set 020 presence
	 * at true, 120.CAL will set 120 and CAL presence at true.
	 * If set=false:020 will set presence at false, 120.CAL will set presence of
	 * CAL to false.
	 */
	virtual void set_presence(const Path_Name& name,bool set);

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual const Type& get_value(const Path_Name& name)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual Type& get_value(const Path_Name& name);

	/// Clone the data field
	/**
	 * Cloning is meaning only structure copy
	 * \return a freshly allocated Data_Field
	 */
	virtual Data_Field *clone()const;

	/// Cloning the data field
	/**
	 * \param rhs already structured record
	 * \return this based on rhs structure
	 */
	Record* clone(const Record*rhs);

	/// walk throught the record
	/**
	 * Call Data_Field_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Functor &it,const Path_Name& path);

	/// walk through the record
	/**
	 * Call Data_Field_Const_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a su item will be met
	 * \param path path acces to item
	 * \return 0
	 */
	virtual int foreach(Const_Functor &,const Path_Name& path)const;

	/// read entire user application profile
	/**
	 *
	 * @param f object functor to apply on
	 * @return 0 on success
	 * @return -1 if field specification read problem
	 */
	int foreach_structure(Structure_Functor *f)const;

	/// get type from an identified item
	/**
	 * @param name item to extract type
	 * @return the associated type
	 * @throw
	 */
	const Type& get_type(const Path_Name& name)const;

	/// check if item exist
	/**
	 * @param name item to extract type
	 * @return true if item exist
	 */
	bool exist_item(const Path_Name& name)const;

	private:
	Data_Field *get_subfield(const std::string& name)const;

	private:
	std::string description_;
	std::string identity_;
	uint8_t category_;

	Field_Specification_Data fspec_;

	data_field_map_t dl_;
	string_string_list_t longname_list_;
	// used to get fspec
	mutable bool_T presence_;

	// properties:setup Record to handle Compound type (reverse frn order)
	Property_Bool compound_;
	// setup Record to handle Explicit type (hide field specification and add size)
	Property_Bool basicexplicit_;

	// setup record to handle indicator length before fspec
	Property_Bool indicatorlength_;
};
}
#endif // INTIFADA_RECORD_HXX
