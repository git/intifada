/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Explicit_Stream.hxx>

intifada::Explicit_Stream::Explicit_Stream()
{}

intifada::Stream::size_type
intifada::Explicit_Stream::set_stream(const uint8_t *s, Stream::size_type m)
{
   Stream::size_type read_size=0;

  // Acquisition du nombre d'octets � lire
  if(m==0){
    throw Parsing_Input_Length_Exception(0,0);
  }

  ++read_size;

  // total_size represents field_ size and length indication field

  read_size +=inherited::set_stream(&s[read_size],m-read_size);

  return read_size;
}

/// Ecriture du flux binaire
intifada::Stream::size_type
intifada::Explicit_Stream::get_stream(uint8_t *s, Stream::size_type m)const
{
  Stream::size_type write_size=0;

  if(m==0){
    throw Parsing_Input_Length_Exception(0,0);
  }
  s[0]=size()+1;
  ++write_size;

  write_size +=inherited::get_stream(&s[write_size],m - write_size);

  return write_size;
}
