/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_STRUCTURE_FUNCTOR_HXX
# define INTIFADA_STRUCTURE_FUNCTOR_HXX

# include <string>
# include <intifada/Properties.hxx>
namespace intifada
{
  class Condition;
  class Path_Name;
  class Type;

  class Structure_Functor
  {
    public:
    Structure_Functor();
    virtual ~Structure_Functor();

    virtual int frn(int f,const Path_Name& d);

    virtual int family_name_id(const std::string& f,const std::string& id);

    virtual int begin_record_identity(const std::string& desc,uint8_t c, const std::string& id);

    virtual int end_record_identity();

    virtual int begin_item(const std::string& longname,const Path_Name& reference,const Properties& p)=0;

    virtual int end_item();

    virtual int begin_uap(const Condition *c);

    virtual int end_uap();

    virtual int begin_fixed_field(const Properties& p)=0;

    // virtual int begin_extended_field(bool repetitive,uint8_t psize,uint8_t ssize);
    virtual int begin_extended_field(const Properties& p)=0;

    // virtual int begin_repetitive_field(uint8_t size,const Flags::flags& f)=0;
    virtual int begin_repetitive_field(const Properties& p)=0;

    virtual int begin_compound_field(uint8_t size);

    virtual void end_compound_field();

    virtual int end_field();

    virtual int part(const Type& type,const Path_Name& name,uint8_t start,uint8_t size,uint8_t byte)=0;
  };
}

#endif // INTIFADA_STRUCTURE_FUNCTOR_HXX
