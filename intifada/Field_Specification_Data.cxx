/*
   Copyright (C) 2009,2011,2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Field_Specification_Data.hxx>
#include <intifada/Condition.hxx>
#include <intifada/Structure_Functor.hxx>
#include <intifada/Field_Specification_Functor.hxx>

intifada::Field_Specification_Data::Field_Specification_Data()
:inherited(),
 byte_stream_(),
 size_(1),
 max_size_(0),
 compound_name_(),
 uap_(new User_Application_Profile),
 release_uap_(true),
 reversefrnorder_(false),
 forceitempresence_(false),
 hidefx_(false),
 maxsize_(false)
{
	this->register_property("reversefrnorder",reversefrnorder_);
	this->register_property("forceitempresence",forceitempresence_);
	this->register_property("hidefx",hidefx_);
	this->register_property("maxsize",maxsize_);
}

intifada::Field_Specification_Data::Field_Specification_Data(const Path_Name& name)
:inherited(),
 byte_stream_(),
 size_(1),
 max_size_(0),
 compound_name_(name),
 uap_(new User_Application_Profile),
 release_uap_(true),
 reversefrnorder_(false),
 forceitempresence_(false),
 hidefx_(false),
 maxsize_(false)
{
	this->register_property("reversefrnorder",reversefrnorder_);
	this->register_property("forceitempresence",forceitempresence_);
	this->register_property("hidefx",hidefx_);
	this->register_property("maxsize",maxsize_);
}

intifada::Field_Specification_Data::Field_Specification_Data(uint8_t max_size,const Path_Name& compound_name)
:inherited(),
 byte_stream_(),
 size_(1),
 max_size_(max_size),
 compound_name_(compound_name),
 //conds_(),
 //uap_(&conds_,max_size),
 uap_(new User_Application_Profile(max_size)),
 release_uap_(true),
 reversefrnorder_(false),
 forceitempresence_(false),
 hidefx_(false),
 maxsize_(false)
{
	this->register_property("reversefrnorder",reversefrnorder_);
	this->register_property("forceitempresence",forceitempresence_);
	this->register_property("hidefx",hidefx_);
	this->register_property("maxsize",maxsize_);
}

intifada::Field_Specification_Data::~Field_Specification_Data()
{
	if(release_uap_==true){
		delete uap_;
	}
}

void intifada::Field_Specification_Data::set_property(const std::string& prop,const std::string& val)
												{
	Properties::set_property(prop,val);
	if(reversefrnorder_==true){
		// compound type reverse frn order
		uap_->set_property("reversefrnorder","true");
	}
												}


intifada::Stream::size_type
intifada::Field_Specification_Data::size()const
{
	update();
	return byte_stream_.size();
}

uint8_t
intifada::Field_Specification_Data::get_max_size()const
{
	return max_size_;
}

int
intifada::Field_Specification_Data::insert(
		uint8_t pos
		,const Path_Name& name
		,const Condition* cond)
{
	Condition_List::size_type idx=uap_->insert_cond(cond);
	return uap_->insert(pos,name,idx);
}

int
intifada::Field_Specification_Data::insert(
		uint8_t pos
		,const Path_Name& name)
{
	return uap_->insert(pos,name);
}

intifada::Stream::size_type intifada::Field_Specification_Data::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	Stream::size_type read_size=0;

	// Last byte memorization of first part because it's last lsb byte
	// which indicate next byte presence
	uint8_t last_byte;
	bool loop;
	do{
		Stream::size_type stream_pos=read_size+o;
		if(stream_pos > m)
		{
			// throw an exception as stream size is overload
			throw Parsing_Input_Field_Specification_Exception(max_size_,stream_pos);
			//throw Parsing_Input_Length_Exception(max_size_,stream_pos);
		}
		last_byte = s[stream_pos];

		byte_stream_.push_back(last_byte);
		++read_size;

		// Choice end condition:
		// If HIDEFX is not set then loop if (last_byte & 0x01)==1
		// If HIDEFX is set then thrust size, loop if read_size < max_size_
		if(hidefx_!=true){
			loop=(last_byte & 0x01)==1;
		}else{
			loop=read_size < max_size_;
		}

	}while(loop);
	byte_stream_.set_stream_position(o,read_size);
	return read_size;
}

intifada::Stream::size_type intifada::Field_Specification_Data::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{
	Stream::size_type write_size;

	// update FX information
	this->update();
	for(write_size=0;write_size<byte_stream_.size();++write_size){
		if((m-o)<=write_size)
			throw Parsing_Output_Length_Exception(write_size,m);
		s[o+write_size]=byte_stream_[write_size];
	}
	byte_stream_.set_stream_position(o,write_size);
	return write_size;
}

/// frn modificator
void intifada::Field_Specification_Data::set_frn(uint8_t f,bool s)
{

	uint8_t byte = Field_Specification_Manipulation::frn_to_byte(f);
	uint8_t offset = Field_Specification_Manipulation::frn_to_offset(f);
	clog("Field_Specification_Data") << "set_frn: byte=" << (int)byte << " offset=" << (int) offset << std::endl;
	byte_stream_.extend(byte);
	byte_stream_.set_bit(byte,offset,s);
}

/// frn accessor
bool
intifada::Field_Specification_Data::get_frn(uint8_t f) const
{
	return byte_stream_.get_bit(
			Field_Specification_Manipulation::frn_to_byte(f),
			Field_Specification_Manipulation::frn_to_offset(f)
	);

}

int
intifada::Field_Specification_Data::get_frn(const std::string& item,const intifada::Data_Field* msg) const
{
	int uap_representation = uap_->get(item,msg);
	return Field_Specification_Manipulation::bit_to_frn(uap_representation);
}

bool
intifada::Field_Specification_Data::get_presence(const std::string& item,const intifada::Data_Field* msg)const
{
	int status=this->get_frn(item,msg);
	if(status==-1)
	{
		std::cerr << "20-" << item << std::endl;
		throw Description_Field_Unknow_Exception(item);
	}
	uint8_t frn=status;
	return this->get_frn(frn);
}

int
intifada::Field_Specification_Data::set_presence(const std::string& item,bool p,const intifada::Data_Field* msg)
{
	int status=this->get_frn(item,msg);
	clog("Field_Specification_Data") << "set_presence(" << item << "," << p << ")--> get frn = " << status << std::endl;
	if(status!=-1){
		uint8_t frn=status;
		this->set_frn(frn,p);
	}
	return status;
}

intifada::Field_Specification_Data&
intifada::Field_Specification_Data::clone(
		const Field_Specification_Data& rhs)
{

	if(this!=&rhs){
		Properties::operator=(rhs);
		this->relink_property("reversefrnorder",reversefrnorder_);
		this->relink_property("forceitempresence",forceitempresence_);
		this->relink_property("hidefx",hidefx_);
		this->relink_property("maxsize",maxsize_);

		size_=rhs.size_;
		max_size_=rhs.max_size_;
		compound_name_=rhs.compound_name_;
		if(release_uap_!=false){
			delete uap_;
		}
		uap_=rhs.uap_;
		release_uap_=false;
	}
	return *this;
}

uint8_t
intifada::Field_Specification_Data::get_part(uint8_t o) const
{
	uint8_t part=0;
	if(o>=size_){
		o -=size_;
		part=o;
		++part;
	}

	return part;
}

void intifada::Field_Specification_Data::update()const
{

	// Start with the last byte and back to the first secondary part
	// not yet the last byte
	uint8_t last_byte = byte_stream_.size();
	clog("Field_Specification_Data") << "(0x"<< this <<")fspec size=" << (int) last_byte << std::endl;
	if(last_byte > 0){
		--last_byte;
		// now, last byte is really the last byte
	}else{
		// fspec size null ?
		// TODO:Check this problem !
		return;
	}

	// Remove null parts if MAXSIZE is disabled
	uint8_t last_part = this->get_part(last_byte);
	clog("Field_Specification_Data") << "fspec last part=" << (int) last_part << std::endl;
	if(maxsize_!=true){
		++last_part;
		do{
			--last_part;
			// Removing all null parts
			if(is_part_null(last_part)==true){
				clog("Field_Specification_Data") << "fspec last part null remove " << size_ << " bytes" << std::endl;
				byte_stream_.pop_back(size_);
			}else{
				break;
			}
		}while(last_part !=0);
	}

	// last_part is pointing on the last non null part.
	// lsb must be set to 0
	// set_bit interface: byte(begin at one) bit(in byte) value
	bool fx=false;
	byte_stream_.set_bit(last_part+1,1,fx);
	if(hidefx_!=true){
		fx=true;
	}
	while(last_part>0){
		--last_part;
		byte_stream_.set_bit(last_part+1,1,fx);
	}
}

bool
intifada::Field_Specification_Data::is_part_null(uint8_t part)const
{
	bool is_null=true;
	// Handle lsb of the concerned part only if isn't already
	// not null
	if((byte_stream_[part] & 0xfe)!=0){
		is_null=false;
	}
	return is_null;
}

int intifada::Field_Specification_Data::foreach(Field_Specification_Functor& it,const intifada::Data_Field* msg,const Path_Name&sel)const
{
	clog("Field_Specification_Data") << "foreach byte_stream_.size()=" << byte_stream_.size() << " sel=" << sel << std::endl;
	int total_size=0;
	int partial_size=0;
	// update FX information
	update();
	// First loop walking
	for(Stream::size_type size=0;size<byte_stream_.size();++size)
	{
		clog("Field_Specification_Data") << this << ":" << size << std::endl;
		uint8_t b = byte_stream_[size];
		for(uint8_t o = 7;o>0;--o)
		{
			if((b & (1<<o)) != 0)
			{
				clog("Field_Specification_Data") << this << ":bit " << (int)o << " set" << std::endl;
				uint8_t uap_bit=Field_Specification_Manipulation::offset_bit_to_bit(size+1,o+1);
				Path_Name uap_id = uap_->get(uap_bit,msg);
				clog("Field_Specification_Data") << "uap_id="<<uap_id<<std::endl;
				//if(sel.empty()==true || sel==uap_id){
				clog("Field_Specification_Data") << this << ":reverse order:" << reversefrnorder_.string_value() << " force presence:" << forceitempresence_.string_value() << std::endl;
				if((reversefrnorder_==true)||(forceitempresence_==true)){
					//if(compound_==true){
					uap_id=Path_Name(compound_name_)+uap_id;
				}
				clog("Field_Specification_Data") << "foreach():it(" << uap_id << ")" << std::endl;
				partial_size=it(uap_id);
				//}
				clog("Field_Specification_Data") << "foreach():partial size="<<partial_size<< std::endl;
				if(partial_size>0){
					total_size+=partial_size;
				}else if(partial_size==-1){
					return total_size;
				}else {
					byte_stream_[size]=(uint8_t)(b & ~(1<<o)) ;
				}
			}
		}
	}
	clog("Field_Specification_Data") << "<--foreach" << std::endl;
	return total_size;
}


int intifada::Field_Specification_Data::foreach_structure(Structure_Functor *f)const
{
	return uap_->foreach(f);
}

int intifada::Field_Specification_Data::foreach_structure(Field_Specification_Functor& it)const
{
	return uap_->foreach(it);
}
