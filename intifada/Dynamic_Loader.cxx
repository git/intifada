/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Dynamic_Loader.hxx>

#if 0
#include <errno.h>

intifada::Dynamic_Loader::Dynamic_Loader():
  dl_(),
  handle_(NULL),
  symbols_(NULL)
{
  lt_dlinit();
}

intifada::Dynamic_Loader::~Dynamic_Loader()
{
  lt_dlexit();
}

int
intifada::Dynamic_Loader::open(const std::string& n)
{
  dl_=n;
  handle_=lt_dlopenext(n.c_str());
  if(handle_==NULL){
    errno=ENOENT;
    return -1;
  }

  std::string s="register_";
  s+=n;
  lt_ptr sym=lt_dlsym(handle_, s.c_str());
  if(sym==NULL){
    errno=EADDRNOTAVAIL;
    return -1;
  }

  record_func_t f=record_func_t(sym);
  f();

  return 0;
}
#endif
