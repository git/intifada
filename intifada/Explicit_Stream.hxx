/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPLICIT_STREAM_HXX
# define EXPLICIT_STREAM_HXX

# include <intifada/Stream.hxx>
# include <string>
namespace intifada
{
  ///
  class Explicit_Stream : public Stream
  {
  public:
    typedef Stream inherited;

  public:

    Explicit_Stream();

    /// Lecture du flux binaire
    /**
     * cette méthode a en charge la récupération du
     * flux binaire représentant la partie du message
     * asterix à décoder
     * \param s pointeur sur la partie du message à décoder
     * \param m taille maximum lisible
     * \return le nombre total d'octets lus
     * \throw Parsing_Input_Length_Exception si le nombre d'octets
     * à lire dépasse m
     *
     * Elle redéfinit la méthode Data_Base::get_stream car cette dernière
     * ne sait travailler que sur une taille de blocs fixe
     */
    virtual Stream::size_type set_stream(const uint8_t *s, Stream::size_type m);

    /// Ecriture du flux binaire
    /**
     * cette méthode a en charge l'écriture de la partie
     * Asterix stockée dans this vers le flux binaire
     *
     * \param s pointeur sur le flux destination
     * \param m taille maximum écrivable
     * \return le nombre total d'octets écrits
     * \throw Parsing_Output_Length_Exception si le nombre d'octets
     * à écrire dépasse m
     */
    virtual Stream::size_type get_stream(uint8_t *s, Stream::size_type m)const;

    /// modificator of subfield
    /**
     * repetitive subfield modificator
     * \param part associated part with name
     */
    template<typename T>
    void set_value(const std::string& name,T& ret,uint8_t part)
    {
      set_value(name,ret);
    }

  };
}
#endif // EXPLICIT_STREAM_HXX
