/*
   Copyright (C) 2009 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECORD_CAST_HXX_
# define RECORD_CAST_HXX_

# include <intifada/Path_Name.hxx>
# include <intifada/Data_Field.hxx>
# include <map>

namespace intifada
{
  class Record;

  /// Handling asterix record casts
  /**
   * This class purpose is to cast an asterix category into another.
   * Several handlings are possible (and concurrently usable).
   *
   * path_name_map_type is used:
   * # to copy a data item to another without any
   * conversion. It's called data items shifts.
   * # to copy items to another without any conversion
   */
  class Record_Cast
  {
  private:
    class Copy_Data_Field_Functor : public intifada::Data_Field::Functor
      {
      public:
      Copy_Data_Field_Functor(intifada::Record* r=NULL);
        virtual int operator()(const intifada::Stream::size_type&,const intifada::Stream::size_type&,const intifada::Path_Name& full_name,intifada::Type& t);
        void set_record(intifada::Record* r);
      private:
        intifada::Record* record_;
      };

  private:
    typedef std::multimap<intifada::Path_Name,intifada::Path_Name> path_name_map_type;
    typedef path_name_map_type::iterator path_name_map_iterator_type;
    typedef std::pair<path_name_map_iterator_type,bool> path_name_map_pair_type;
  public:
    Record_Cast();

    virtual ~Record_Cast();

    /// Insert a couple from/to data item to copy
    /**
     * @param from data item to copy
     * @param to target data item
     * @return 0
     */
    int insert_data_item_shift(const intifada::Path_Name& from,const intifada::Path_Name& to);

    /// Insert a couple from/to data fields to copy
    /**
     *
     * @param from data field to copy
     * @param to target data field
     * @return 0
     */
    int insert_data_field_shift(const intifada::Path_Name& from,const intifada::Path_Name& to);

    void cast(const Record& source,Record& target);

  private:
    path_name_map_type data_items_shifts_;
    path_name_map_type data_fields_shifts_;
  };
}

#endif // RECORD_CAST_HXX_
