/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <intifada/Byte_Manipulation.hxx>

template <typename T>
const intifada::Unit_Converter* intifada::Type::set_unit_converter()
{
	this->release_unit_converter();
	_unit_converter=new T;
	return _unit_converter;
}

/// Put asterix stream in a numeric type
template <typename T_>
void
intifada::Type::streamcpy(T_& t,Stream::const_reverse_iterator b,int start,unsigned int size) const
{
	if(sizeof(T_) < (size / 8)){
		std::ostringstream os;
		os<<"Type<T_>::streamcpy():sizeof(";
		os<<this->get_name();
		os<<") < (";
		os<<size;
		os<<"/8)";
		throw std::out_of_range(os.str());
	}
	// Compute byte number and initial offset
	// lsb begin at stream end, so start / 8 give number of byte
	// to shift to get the concerned part. The modulo give shift
	// position in the concerned byte
	Stream::const_reverse_iterator input_it = b + (start / 8);
	uint8_t input_shift = start % 8;
	uint8_t input_size = size;

	// Acqusition strategy:
	// Reading is drived by output write possibility

	uint8_t next_input_size;

	// output write capability is 8 like we don't write anything in
	uint8_t output_write_capability = 8;
	uint8_t output_shift = 0;
	uint8_t output_index = 0;
	uint8_t *output_buffer= reinterpret_cast<uint8_t *>(&t);
	//uint8_t output_buffer=t & 0xff;
	do{ // Reading loop

		next_input_size= Byte_Manipulation::get_access(input_size,input_shift,output_write_capability);
		// next_input_size hold size remain to read
		// input_size hold size to currently read

		uint8_t input_read=Byte_Manipulation::get_byte_part(*input_it,input_size,input_shift);

		Byte_Manipulation::set_byte_part(output_buffer[output_index],input_read,input_size,output_shift);
		//Byte_Manipulation::set_byte_part(output_buffer,input_read,input_size,output_shift);

		output_write_capability-=input_size;
		output_shift+=input_size;
		input_shift+=input_size;
		if(next_input_size!=0){
			// we must do another loop
			input_size=next_input_size;

			if(input_shift==8){
				++input_it;
				input_shift=0;
			}
			if(output_write_capability==0){
				output_write_capability=8;
				output_shift=0;
				++output_index;
				// output_buffer= (t & (0xff << (8*output_index))) >> (8*output_index);
			}
		}

	}while(next_input_size!=0); // reading loop
}


/// Put asterix stream in a numeric type
template <typename T_>
void
intifada::Type::streamcpy(Stream::reverse_iterator t,const T_ &b,int start,unsigned int size) const
{
	// Compute de octet number and initial offset
	// lsb begin at stream end, so start / 8 give number of byte
	// to shift to get the concerned part. The modulo give shift
	// position in the concerned byte
	Stream::reverse_iterator output_it = t + (start / 8);
	uint8_t input_byte = b & T_(0xff);
	uint8_t input_byte_number = 0;
	uint8_t input_shift = 0;
	uint8_t input_size = size;

	// Acqusition strategy:
	// Reading is drived by output write possibility

	uint8_t next_input_size;

	// output write capability is 8 like we don't write anything in
	uint8_t output_shift = start % 8;
	uint8_t output_write_capability = 8 - output_shift;

	do{ // Reading loop

		next_input_size= Byte_Manipulation::get_access(input_size,input_shift,output_write_capability);
		// next_input_size hold size remain to read
		// input_size hold size to currently read

		uint8_t input_read=Byte_Manipulation::get_byte_part(input_byte,input_size,input_shift);
		Byte_Manipulation::set_byte_part(*output_it,input_read,input_size,output_shift);

		output_write_capability-=input_size;
		output_shift+=input_size;
		input_shift+=input_size;
		if(next_input_size!=0){
			// we must do another loop
			input_size=next_input_size;

			if(input_shift==8){
				++input_byte_number;
				input_byte = (b & ( T_(0xff) << (input_byte_number * 8))) >> (input_byte_number * 8);

				input_shift=0;
			}
			if(output_write_capability==0){
				output_write_capability=8;
				output_shift=0;
				++output_it;
			}
		}

	}while(next_input_size!=0); // reading loop
}
