/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_FIXED_LENGTH_DATA_FIELD_HXX
# define INTIFADA_FIXED_LENGTH_DATA_FIELD_HXX

# include <intifada/Data_Field.hxx>

# include <intifada/Data_Field_Characteristics.hxx>
namespace intifada
{


/// Fixed length data field
/**
 * Fixed length shall comprise a fixed number of bytes
 */
class Fixed_Length_Data_Field : public Data_Field
{
public:
	template <typename F>
	class Data_Functor_T : public Data_Field_Characteristics_List::Functor
	{
	public:
		Data_Functor_T(const Path_Name& n,F &it,Stream& byte_stream);
		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		const Path_Name& n_;
		F &it_;
		intifada::Stream::size_type stream_pos_;
		intifada::Stream::size_type stream_size_;
		Stream& byte_stream_;

	};

	class Update_Stream_Functor : public Data_Field_Characteristics_List::Functor
	{
	public:
		Update_Stream_Functor(Stream& byte_stream);

		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		intifada::Stream::reverse_iterator it_;

	};

	public:
	typedef Data_Field inherited;
	typedef Data_Functor_T<Data_Field::Functor> Data_Functor;
	typedef Data_Functor_T<Data_Field::Const_Functor> Data_Const_Functor;

	public:
	// Fixed_Length_Data_Field(Stream::size_type n);
	Fixed_Length_Data_Field();

	private:

	Fixed_Length_Data_Field(const Fixed_Length_Data_Field&rhs);
	public:

	/// Set stream property
	virtual void set_property(const std::string& prop,const std::string& val);

	/// get field size
	/**
	 * \return size of field
	 */
	virtual Stream::size_type size()const;

	/// association between a name, a field type and a position in a stream
	/**
	 * \param name data name
	 * \param type data type
	 * \param start in the record, the begining of the data (begin at one at the lsb)
	 * \param size data size
	 */
	Data_Field_Characteristics* insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size);

	/// Byte stream acquisition
	/**
	 * Handle acquisition of asterix binary stream
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return readed total bytes
	 * \throw Parsing_Input_Length_Exception if bytes to read exceed m
	 * \throw Description_Field_Unknow_Exception if an unknow field is encountered
	 */
	virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

	/// Byte stream restitution
	/**
	 * Handle byte stream restitution.
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return writed total bytes
	 * \throw Parsing_Output_Length_Exception if bytes to read exceed m
	 */
	virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual const Type& get_value(const Path_Name& name)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual Type& get_value(const Path_Name& name);
#if 0
	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \param ret concrete target variable
	 * \remarks use it when you know the target type
	 */
	virtual void get_value(const Path_Name& name,intifada::Type& ret)const;

	/// set value to asterix message
	/**
	 * \param name name of data item to get
	 * \param ret source variable
	 */
	virtual void set_value(const Path_Name& name,const intifada::Type& ret);
#endif
	/// Clone the data field
	/**
	 * Cloning is meaning only structure copy
	 * \return a freshly allocated Fixed_Length_Data_Field
	 */
	virtual Data_Field *clone()const;


	/// walk throught the record
	/**
	 * Call Data_Field_Iterator for each sub item met in the record.
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Functor &it,const Path_Name& path);

	/// walk throught the record
	/**
	 * Call Data_Field_Const_Iterator for each sub item met in the record.
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Const_Functor &,const Path_Name& path)const;

	/// get type from asterix message
	/**
	 * \param name name of data item to get type
	 * \return a reference on type
	 * See Type for details
	 *
	 */
	virtual const Type& get_type(const Path_Name& name)const;

	private:

	/// Update stream with cached values
	/**
	 * Report all cached values to the stream
	 */
	void update_stream()const;

	private:
	Property_Uint8 initial_size_;
	mutable Stream byte_stream_;

};

template <typename F>
Fixed_Length_Data_Field::Data_Functor_T<F>::Data_Functor_T(const Path_Name& n,F &it,Stream& byte_stream):
n_(n)
,it_(it)
,byte_stream_(byte_stream)
{
	byte_stream.get_stream_position(stream_pos_,stream_size_);
}
template <typename F>
int Fixed_Length_Data_Field::Data_Functor_T<F>::operator()(
		const Path_Name &n
		,const Data_Field_Characteristics &t
		,const Data_Field_Characteristics_List::part_size_type&)
{
	Type *pt=t.get_type();
	if(t.is_cached()==false)
	{
		intifada::Stream::const_reverse_iterator it=byte_stream_.rbegin();
		// Data_Field::get_value mean we want take data from stream to type
		pt->set_from_stream(it,t.shift(),t.size());
		t.set_cached();
		pt->reset_changed_state();
	}
	Path_Name full_name=n_+n;
	it_(stream_pos_,stream_size_,full_name,*pt);
	return 0;
}

}
#endif // INTIFADA_FIXED_LENGTH_DATA_FIELD_HXX
