/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record_Repository.hxx>
#include <intifada/Record.hxx>
#include <intifada/Structure_Functor.hxx>

intifada::Record_Repository* intifada::Record_Repository::instance_=NULL;

intifada::Record_Repository::Record_Node::Record_Node():
  r_(NULL),
  name_(),
  id_(),
  cat_(0)
{}

intifada::Record_Repository::Record_Node::Record_Node(Record* r,const std::string&name,const std::string& id,uint8_t cat):
  r_(r),
  name_(name),
  id_(id),
  cat_(cat)
{}

intifada::Record_Repository::Record_Node::Record_Node(const Record_Node&rhs)
{
  this->copy(rhs);
}

void
intifada::Record_Repository::Record_Node::release()
{
  if(r_!=NULL) delete r_;
}

intifada::Record_Repository::Record_Node&
intifada::Record_Repository::Record_Node::operator=(const Record_Node&rhs)
{
  return this->copy(rhs);
}

void
intifada::Record_Repository::Record_Node::set(Record* r,const std::string&name,const std::string& id,uint8_t cat)
{
  r_=r;
  name_=name;
  id_=id;
  cat_=cat;
}

const intifada::Record*
intifada::Record_Repository::Record_Node::get()const
{
  return r_;
}

const std::string&
intifada::Record_Repository::Record_Node::get_description()const
{
  return name_;
}

uint8_t
intifada::Record_Repository::Record_Node::get_category()const
{
  return cat_;
}

const std::string&
intifada::Record_Repository::Record_Node::get_id()const
{
  return id_;
}


intifada::Record_Repository::Record_Node&
intifada::Record_Repository::Record_Node::copy(const Record_Node&rhs)
{
  if(this!=&rhs){
    r_=rhs.r_;
    name_=rhs.name_;
    id_=rhs.id_;
    cat_=rhs.cat_;
  }
  return *this;
}

intifada::Record_Repository::Record_Repository():
  rep_(),
  temp_families_()
{}

intifada::Record_Repository::~Record_Repository()
{
  this->release();
}

intifada::Record_Repository*
intifada::Record_Repository::instance()
{
  if(instance_==NULL){
    instance_=new Record_Repository;
  }
  return instance_;
}

int
intifada::Record_Repository::insert(Record* r,const std::string&name,const std::string& id,uint8_t cat)
{
  Record_Node node(r,name,id,cat);

  record_repository_pair_t p=rep_.insert(std::make_pair(id,node));
  int ret=0;
  if(p.second==false){
    ret=-1;
  }else{
    move_family(id,cat);
  }
  return ret;
}

int
intifada::Record_Repository::insert(const std::string&name,const std::string& id)
{
  id_family_repository_iterator_t it=temp_families_.find(id);

  if(it==temp_families_.end()){
    id_family_repository_pair_t families_p=temp_families_.insert(std::make_pair(id,family_list_t()));
    it=families_p.first;
  }

  family_list_t &l = (*it).second;
  family_list_pair_t p = l.insert(name);
  int ret=0;
  if(p.second==false){
    ret=-1;
  }
  return ret;
}

intifada::Record*
intifada::Record_Repository::get_clone(const std::string&family,uint8_t cat)
{
  const Record *ret=this->get_node_pointer(family,cat);

  // Return a freshly new allocated record
  intifada::Record* cloned_ret=NULL;

  if(ret!=NULL){
    cloned_ret=new Record;
    cloned_ret->clone(ret);
  }
  return cloned_ret;
}

const intifada::Record*
intifada::Record_Repository::get_pointer(const std::string&family,uint8_t cat)const
{
  return this->get_node_pointer(family,cat);
}

void
intifada::Record_Repository::release()
{
  for(record_repository_iterator_t it=rep_.begin();it!=rep_.end();++it){
    Record_Node&r=(*it).second;
    r.release();
  }
  rep_.clear();
  temp_families_.clear();
  families_.clear();
}

int intifada::Record_Repository::foreach(Structure_Functor *f)const
{
  for(family_id_repository_const_iterator_t fit=families_.begin();fit!=families_.end();++fit){

    const cat_id_repository_t &cir=(*fit).second;
    const std::string& family=(*fit).first;

    for(cat_id_repository_const_iterator_t cit=cir.begin();cit!=cir.end();++cit){
      const std::string& id=(*cit).second;
      record_repository_const_iterator_t rit=rep_.find(id);
      if(rit!=rep_.end()){
	const Record *r= ((*rit).second).get();
	f->family_name_id(family,id);
	std::string name=((*rit).second).get_description();
	std::string id_cat=((*rit).second).get_id();
	uint8_t cat=((*rit).second).get_category();
	f->begin_record_identity(name,cat,id_cat);
	r->foreach_structure(f);
	f->end_record_identity();
      }
    }
  }
  return 0;
}

void intifada::Record_Repository::get_families(family_list_t& f)const
{
  for(family_id_repository_const_iterator_t fit=families_.begin();fit!=families_.end();++fit){
    const std::string& family=(*fit).first;
    f.insert(family);
  }
}

const intifada::Record*
intifada::Record_Repository::get_node_pointer(const std::string&family,uint8_t cat)const
{
  const Record *ret=NULL;
  family_id_repository_const_iterator_t it = families_.find(family);
  if(it!=families_.end()){
    const cat_id_repository_t &cir=(*it).second;
    cat_id_repository_const_iterator_t cit = cir.find(cat);
    if(cit!=cir.end()){
      const std::string& id=(*cit).second;
      record_repository_const_iterator_t rit=rep_.find(id);
      if(rit!=rep_.end()){
	ret= ((*rit).second).get();
      }
    }
  }
  return ret;
}

void
intifada::Record_Repository::move_family(const std::string&id,uint8_t cat)
{
  // Locale preceding stored map<id,list<family>>
  id_family_repository_iterator_t it=temp_families_.find(id);

  if(it!=temp_families_.end()){
    // for each associated family founded create map<family,map<cat,id>>
    family_list_t &f=(*it).second;
    for(family_list_iterator_t fit=f.begin();fit!=f.end();++fit){
      const std::string& family=(*fit);
      create_family(family,id,cat);
    }
    temp_families_.erase(it);
  }
}

int
intifada::Record_Repository::create_family(const std::string&family,const std::string&id,uint8_t cat)
{
  family_id_repository_iterator_t it = families_.find(family);
  if(it==families_.end()){
    // create a cat_id_repository_t
    family_id_repository_pair_t p = families_.insert(std::make_pair(family,cat_id_repository_t()));
    if(p.second!=false){
      it=p.first;
    }else{
      return -1;
    }
  }

  cat_id_repository_t &cir=(*it).second;

  cat_id_repository_pair_t cirp=cir.insert(std::make_pair(cat,id));
  if(cirp.second!=true){
    return -1;
  }
  return 0;
}
