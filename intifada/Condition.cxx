/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Condition.hxx>
#include <intifada/Integer.hxx>
#include <intifada/Data_Field.hxx>
#include <intifada/Logger.hxx>

intifada::Condition::Condition(
		const std::string& data_item,
		const std::string& value):
		data_item_(data_item)
,svalue_(value)
{
	clog("Memory_Allocation") << "0x" << this << ":ctor Condition" << std::endl;
}

intifada::Condition::Condition(const Condition&rhs)
{
	this->copy(rhs);
}

intifada::Condition::~Condition()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor Condition" << std::endl;
}

intifada::Condition&
intifada::Condition::operator=(const Condition&rhs)
{
	return copy(rhs);
}

bool intifada::Condition::operator()(const intifada::Data_Field* msg)const
{
	clog("Condition") << "operator()():" << std::endl;
	const intifada::Type &pvalue=(*msg)(data_item_);
	clog("Condition") << "operator()():" << data_item_<<"=" << pvalue.get_to_string() << std::endl;
	bool ret=false;

	ret=pvalue.get_to_string()==svalue_;
	clog("Condition") << "operator()():"<< pvalue.get_to_string() << "==" << svalue_ << "=" << ret << std::endl;

	return ret;
}

intifada::Condition* intifada::Condition::clone()const
{
	clog("Condition") << "clone" << std::endl;
	return new Condition(data_item_,svalue_);
}

void
intifada::Condition::close()
{
	delete this;
}

std::string
intifada::Condition::get()const
{
	std::string ret=data_item_;
	ret+="=";
	ret+=svalue_;
	return ret;
}

const std::string&
intifada::Condition::get_lvalue()const
{
	return data_item_;
}

const std::string&
intifada::Condition::get_rvalue()const
{
	return svalue_;
}

intifada::Condition&
intifada::Condition::copy(const Condition& rhs)
{
	if(this!=&rhs){
		data_item_=rhs.data_item_;
		svalue_=rhs.svalue_;
		//value_=rhs.value_;
	}
	return *this;
}
