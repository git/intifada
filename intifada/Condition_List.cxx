/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Condition_List.hxx>

intifada::Condition_List::Condition_List():
  conds_()
{}

intifada::Condition_List::~Condition_List(){}

intifada::Condition_List::value_type
intifada::Condition_List::operator[](size_type idx)const
{
  return conds_[idx];
}

void
intifada::Condition_List::push_back(value_type & v)
{
  conds_.push_back(v);
}

intifada::Condition_List::size_type
intifada::Condition_List::size()const
{
  return conds_.size();
}
