/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_FIELD_LIST_HXX
# define DATA_FIELD_LIST_HXX

# include <intifada/Map.hxx>
# include <vector>
# include <string>
# include <stdexcept>

# include <intifada/Exception.hxx>

# include <intifada/Data_Field_Characteristics.hxx>
# include <intifada/Path_Name.hxx>

namespace intifada
  {
    /// List of data field characteristics
    /**
     *
     */
    class Data_Field_Characteristics_List
      {
      public:
        typedef uint8_t part_size_type;

      public:
        static part_size_type npos;

      public:
        class Functor
          {
          public:
            virtual ~Functor();

            virtual int operator()(const Path_Name &n,const Data_Field_Characteristics &t,const part_size_type& part)=0;
          };

      private:
        typedef Unordered_Map<Path_Name,Data_Field_Characteristics *> map;
        typedef map::iterator map_iterator;
        typedef map::const_iterator map_const_iterator;
        typedef map::pair map_pair;

        typedef std::vector<map> vector_map_type;
        typedef vector_map_type::size_type vector_map_size_type;
        typedef vector_map_type::iterator vector_map_iterator_type;
        typedef vector_map_type::const_iterator vector_map_const_iterator_type;

        typedef Unordered_Map<Path_Name,part_size_type> name_part_map_type;
        typedef name_part_map_type::const_iterator name_part_map_const_iterator_type;

        typedef std::vector<std::pair<Path_Name,part_size_type> > vector_name_part_type;
        typedef vector_name_part_type::const_iterator vector_name_part_const_iterator_type;

      public:
        Data_Field_Characteristics_List();

        Data_Field_Characteristics_List(const Data_Field_Characteristics_List&rhs);

        Data_Field_Characteristics_List& operator=(const Data_Field_Characteristics_List&rhs);

        virtual ~Data_Field_Characteristics_List();

        /// Obtain new item description object
        /**
         * if return value isn't passed to insert, it must be passed to the
         * release_description method to release memory
         */
        Data_Field_Characteristics* get_new_description(/*const Data_Field_Characteristics::type &t*/)const;

        void release_description(Data_Field_Characteristics* d)const;

        /// Item description insertion
        /**
         * \param item_desc item description must be obtained by get_new_description method
         * \throw Description_Field_Duplication_Exception if name is already registered
         */
        void insert(const Path_Name& name,Data_Field_Characteristics* item_desc, const part_size_type& part);

        /// Get a node characteristics
        /**
         * \param type node name
         * \part part where to search
         * \return node characteristics
         * \throw Description_Field_Unknow_Exception if node doesn't exist
         */
        Data_Field_Characteristics& get_characteristics(const Path_Name& name,const part_size_type& part)const;

        /// Get a node characteristics without referencing part
        /**
         * \param name node name
         * \return node characteristics
         * \throw Description_Field_Unknow_Exception if node doesn't exist
         */
        Data_Field_Characteristics& get_characteristics(const Path_Name& name)const;

        /// Get a node part
        /**
         * \param name node name
         * \return node part
         * \throw Description_Field_Unknow_Exception if node doesn't exist
         */
        const part_size_type& get_part(const Path_Name& name)const;

        void set_repetitive();

        void set_extended();

        /// pass on each data, following insertion order
        /**
         * if Data_Field_Characteristics_List is a repetitive type, only part 0 is
         * got.
         * part is transmitted only if extended and non repetitive declared
         */
        int foreach_structure(Data_Field_Characteristics_List::Functor&dflit)const;

        /// pass on each data
        /**
         * walk throught internal data is easier via foreach than classical index
         * as path name are adapted if repetitive fields are detected (index is added
         * to path name)
         */
        int foreach(Data_Field_Characteristics_List::Functor&dflit)const;

        /// Clone first part
        /**
         * Used to handle repetitive fields (add structure as stream is readed)
         */
        int push_back();
      private:
        uint8_t copy(map&target,const map&source)const;

        /// Item part insertion
        /**
         * \param item_desc item description must be obtained by get_new_description method
         * \throw Description_Field_Duplication_Exception if name is already registered
         *
         * not update order_ vector
         */
        map_iterator insert_parts(const Path_Name& name,Data_Field_Characteristics* item_desc, const part_size_type& part=0);

      private:
        bool repetitive_;
        bool extended_;

        mutable vector_map_type parts_;

        name_part_map_type fast_access_;

        vector_name_part_type insert_order_;
      };
  }

#endif // DATA_FIELD_LIST_HXX
