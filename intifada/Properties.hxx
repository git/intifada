/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROPERTIES_HXX
# define PROPERTIES_HXX

# include <string>
# include <intifada/Map.hxx>
# include <sstream>
# include <stdexcept>
# include <intifada/inttypes.h>
class Property_Type
{
public:
	Property_Type();

	virtual ~Property_Type();

	virtual Property_Type& operator=(const std::string& rhs)=0;

	virtual std::string string_value()const=0;

	virtual Property_Type* duplicate()const=0;

	virtual bool operator==(const std::string& rhs)const=0;

	virtual bool operator<(const std::string& rhs)const=0;

	virtual bool operator>(const std::string& rhs)const=0;

	virtual bool operator!=(const std::string& rhs)const;

	virtual bool operator>=(const std::string& rhs)const;

	virtual bool operator<=(const std::string& rhs)const;

	virtual bool default_value()const=0;
private:
	Property_Type& operator=(const Property_Type& rhs);
};

class Property_String : public Property_Type
{
public:
	/// Default constructor
	/**
	 * intitializing Property_String to empty string
	 */
	Property_String();

	/// String initialized constructor
	/**
	 * \param s intial string value
	 * \param d default value
	 */
	Property_String(const std::string& s, const std::string& d="");

	/// Copy constructor
	Property_String(const Property_String& rhs);

	Property_String& operator=(const Property_String& rhs);

	virtual Property_String& operator=(const std::string& rhs);

	virtual std::string string_value()const;

	virtual Property_String* duplicate()const;

	virtual bool operator==(const std::string& rhs)const;

	virtual bool operator<(const std::string& rhs)const;

	virtual bool operator>(const std::string& rhs)const;

	virtual bool default_value()const;

private:
	std::string str_;
	std::string default_value_;
};

template<typename T>
class Property_Integer : public Property_Type
{
public:
	typedef T type;
public:
	/// Default constructor
	/**
	 * intitializing Property_String to 0
	 */
	Property_Integer();

	/// String initialized constructor
	/**
	 * \param v initial integer value
	 * \param d default value
	 */
	Property_Integer(const T& v,const T& d=0);

	/// Copy constructor
	Property_Integer(const Property_Integer& rhs);

	Property_Integer& operator=(const Property_Integer& rhs);

	virtual Property_Integer& operator=(const std::string& rhs);

	virtual std::string string_value()const;

	virtual Property_Integer<T>* duplicate()const;

	operator T()const {return int_;}

	virtual bool operator==(const std::string& rhs)const;

	virtual bool operator<(const std::string& rhs)const;

	virtual bool operator>(const std::string& rhs)const;

	bool operator==(const T& rhs)const;

	bool operator!=(const T& rhs)const;

	virtual bool default_value()const;
private:
	T to_numeric(const std::string& sval)const;
private:
	T int_;
	T default_value_;
};

typedef Property_Integer<int> Property_Int;
typedef Property_Integer<uint16_t> Property_Uint8;

class Property_Bool : public Property_Type
{
public:
	/// Default constructor
	/**
	 * intitializing Property_String to false
	 */
	Property_Bool();

	/// String initialized constructor
	/**
	 * \param v initial integer value
	 * \param d default value
	 */
	Property_Bool(const bool& v,const bool& b=false);

	/// Copy constructor
	Property_Bool(const Property_Bool& rhs);

	Property_Bool& operator=(const Property_Bool& rhs);

	virtual Property_Bool& operator=(const std::string& rhs);

	virtual std::string string_value()const;

	virtual Property_Bool* duplicate()const;

	virtual bool operator==(const std::string& rhs)const;

	virtual bool operator<(const std::string& rhs)const;

	virtual bool operator>(const std::string& rhs)const;

	bool operator==(const bool& rhs)const;

	bool operator!=(const bool& rhs)const;

	bool operator==(const Property_Bool& rhs)const;

	bool operator!=(const Property_Bool& rhs)const;

	virtual bool default_value()const;

private:
	bool to_bool(const std::string& sval)const;
private:
	bool v_;
	bool default_value_;
};

/// Handling properties
/**
 * Properties are used to handle running time variables.
 * A property could be defined, undefined, got or setted.
 * Properties could be attached to a derived class.
 *
 */
class Properties
{
private:


	/// handle a property and decide if it must be freed
	typedef std::pair<Property_Type*,bool> prop_pair;

	typedef intifada::Unordered_Map<std::string, prop_pair> prop_list_type;
	typedef prop_list_type::iterator prop_list_iterator_type;
	typedef prop_list_type::const_iterator prop_list_const_iterator_type;

	typedef prop_list_type::pair prop_list_pair_type;
	typedef prop_list_type::value_type prop_list_value_type;
	typedef prop_list_type::mapped_type prop_list_mapped_type;

	class Destroy
	{
	public:
		virtual ~Destroy (){}
		virtual void operator () (prop_list_value_type p)
		{

			prop_list_mapped_type &n=p.second;
			if(n.second==true){
				delete n.first;
			}
		}
	};
	class Dump
	{
	public:
		Dump(std::ostream& os):os_(os){}
		virtual ~Dump (){}
		virtual void operator () (prop_list_value_type p)
		{
			prop_list_mapped_type &n=p.second;
			os_ << p.first << ":" << n.first->string_value() << " (delete=" << n.second << ")" << std::endl;
		}
	private:
		std::ostream &os_;
	};
	class Duplicate
	{
	public:
		Duplicate(Properties& target):target_(target){}
		virtual ~Duplicate (){}
		virtual void operator () (prop_list_value_type p)
		{
			prop_list_mapped_type &n=p.second;
			Property_Type *target_prop=n.first->duplicate();
			target_.register_property(p.first,target_prop,true);
		}
	private:
		Properties& target_;
	};
public:
	class Property_Functor
	{
	public:
		virtual ~Property_Functor(){}
		virtual void begin(){}
		virtual void operator()(const std::string& ps, const Property_Type& prop)=0;
		virtual void end(){}
	};
private:
	/// External functor connector
	/**
	 * prop_list_value_type::first: property name
	 * prop_list_value_type::second: prop_list_mapped_type
	 * prop_list_mapped_type::first: property
	 *
	 */
	class Property_Functor_Connector
	{
	public:
		Property_Functor_Connector(Property_Functor& target):target_(target){}
		virtual ~Property_Functor_Connector (){}
		virtual void operator () (prop_list_value_type p)
		{
			target_(p.first,*p.second.first);
		}
	private:
		Property_Functor& target_;
	};
public:
	Properties();

	Properties(const Properties& rhs);

	Properties& operator=(const Properties& rhs);

	virtual ~Properties();

	/// Registering a new type T property
	/**
	 * \return 0 if property has been sucessfully inserted
	 * \param n property name
	 *
	 * Internally, a dynamic allocation of type T is performed and will be freed
	 * when property is removed.
	 */
	template <typename T> int register_property(const std::string& n);

	/// Registering a new external property
	/**
	 * A property registered will not be internaly cleared
	 * Note that if Properties is copied, relink_property must be used. If
	 * isn't the case, concerned property will be internaly cleared
	 */
	template <typename T> int register_property(const std::string& n,T&prop);

	/// Link a Registred property to an external property
	/**
	 * A relinked registred will not be internally cleared
	 * The initial property is cleared.
	 * prop is updated with the internal value which will be cleared if needed
	 */
	template <typename T> int relink_property(const std::string& n,T&prop);

protected:
	int register_property(const std::string& n, Property_Type*p,bool del);
public:
	int erase_property(const std::string& n);

	Property_Type& find_property(const std::string& n);

	const Property_Type& find_property(const std::string& n)const;

	template <typename T> int find_property(const std::string& n, T*&prop);

	template <typename T> int find_property(const std::string& n, T&prop)const;

	virtual void set_property(const std::string& prop,const std::string& val);

	virtual void set_property(const std::string& prop,const Property_Type& val);
	virtual std::string get_property(const std::string& prop)const;

	void for_each_property(Property_Functor& f)const;

	void dump(std::ostream& os)const;
private:
	prop_list_type props_;
};

template<typename T>
Property_Integer<T>::Property_Integer():int_(0),default_value_(0){}

template<typename T>
Property_Integer<T>::Property_Integer(const T& v,const T& d):int_(v),default_value_(d){}

template<typename T>
Property_Integer<T>::Property_Integer(const Property_Integer& rhs):
int_(rhs.int_)
,default_value_(rhs.default_value_)
{}

template<typename T>
Property_Integer<T>& Property_Integer<T>::operator=(const Property_Integer<T>& rhs)
{
	if(this!=&rhs){
		int_=rhs.int_;
		default_value_=rhs.default_value_;
	}
	return *this;
}

template<typename T>
Property_Integer<T>& Property_Integer<T>::operator=(const std::string& rhs)
{
	int_ = to_numeric(rhs);
	return *this;
}

template<typename T>
std::string Property_Integer<T>::string_value()const
{
	std::ostringstream os;
	os << int_;
	return os.str();
}

template<typename T>
Property_Integer<T>* Property_Integer<T>::duplicate()const
{
	return new Property_Integer<T>(int_);
}

template<typename T>
bool Property_Integer<T>::operator==(const std::string& rhs)const
{
	T nval=to_numeric(rhs);
	return nval==int_;
}

template<typename T>
bool Property_Integer<T>::operator<(const std::string& rhs)const
{
	T nval=to_numeric(rhs);
	return int_<nval;
}

template<typename T>
bool Property_Integer<T>::operator>(const std::string& rhs)const
{
	T nval=to_numeric(rhs);
	return int_>nval;
}

template<typename T>
bool Property_Integer<T>::operator==(const T& rhs)const
{
	return int_==rhs;
}

template<typename T>
bool Property_Integer<T>::operator!=(const T& rhs)const
{
	return int_!=rhs;
}

template<typename T>
bool Property_Integer<T>::default_value()const
{
	return int_==default_value_;
}

template<typename T>
T Property_Integer<T>::to_numeric(const std::string& sval)const
{
	T ret;
	std::istringstream is(sval);
	is >> ret;
	return ret;
}

template <typename T>
int Properties::register_property(const std::string& n)
{
	T *p=new T;
	int i=register_property(n,p,true);
	if(i==-1){
		delete p;
	}
	return i;
}

template <typename T>
int Properties::register_property(const std::string& n,T&prop)
{
	return register_property(n,&prop,false);

}

template <typename T>
int Properties::relink_property(const std::string& n,T&prop)
{
	T *original;
	int f=this->find_property(n,original);
	if(f==0){
		prop=*original;
		this->erase_property(n);
		this->register_property(n,prop);
	}
	return f;
}

template <typename T>
int Properties::find_property(const std::string& n, T*&prop)
{
	int ret=0;
	try{
		Property_Type& f=this->find_property(n);
		prop=dynamic_cast<T*>(&f);
	}catch(std::domain_error& ex){
		prop=NULL;
		ret=-1;
	}
	return ret;
}

template <typename T>
int Properties::find_property(const std::string& n, T& prop)const
{
	int ret=0;
	try{
		const Property_Type& f=this->find_property(n);
		const T& p=dynamic_cast<const T&>(f);
		prop=p;
	}catch(std::domain_error& ex){
		ret=-1;
	}
	return ret;
}

#endif // PROPERTIES_HXX
