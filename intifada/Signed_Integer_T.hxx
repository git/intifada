/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNED_INTEGER_T_HXX
# define SIGNED_INTEGER_T_HXX

#include <intifada/Integer_T.hxx>

namespace intifada
{

  template <typename T_>
  class Signed_Integer_T : public Integer_T<T_>
  {
  private:
    typedef Integer_T<T_> inherited;

  public:

    Signed_Integer_T(const std::string& id);
    Signed_Integer_T(const std::string& id,T_ i);

  private:

    /// adapt stream to internal data
    /**
     * before setting internal data from stream, this method
     * is called with raw stream extracted. This could be used by sub classes
     * to do anything particular
     *
     * \param v data coming from stream
     * \param size size in bits extracted from stream
     * \return v by default
     *
     * inherited new method must adapt the returned value
     */
    virtual T_ stream_to_internal(const T_&v,int size)const;

    /// adapt internal data to stream
    /**
     * above symetric
     * \param v data coming from internal
     * \param size size in bits that will be inserted in stream
     * \return v by default
     *
     * inherited new method must adapt the returned value
     */
    virtual T_ internal_to_stream(const T_&v,int size)const;

  private:
    template <typename T>
    T mask(int size)const;

    template<typename S,typename T>
    void move_sign(int initial_size, const S& a,int target_size,T& target)const;

  };
}
#include <intifada/Signed_Integer_T.cxx>

#endif // SIGNED_INTEGER_T_HXX
