/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Path_Configuration.hxx>

intifada::Path_Configuration* intifada::Path_Configuration::instance_=NULL;


intifada::Path_Configuration::Path_Configuration():
  datadir_(DATADIR),
  sysconfdir_(SYSCONFDIR),
  description_(),
  validation_()
{
  this->make_file_access(description_,sysconfdir_,"asterix.xml");
  this->make_file_access(validation_,datadir_,"dtd/asterix.dtd");
}

intifada::Path_Configuration::~Path_Configuration(){}

intifada::Path_Configuration* intifada::Path_Configuration::instance()
  {
    if(instance_==NULL){
      instance_=new Path_Configuration;
    }
    return instance_;
  }

void intifada::Path_Configuration::release()
  {
    delete this;
  }

const std::string& intifada::Path_Configuration::get_factory_datadir_path()const
{
  return datadir_;
}

const std::string& intifada::Path_Configuration::get_factory_sysconfdir_path()const
{
  return sysconfdir_;
}

const std::string& intifada::Path_Configuration::get_factory_description()const
{
  return description_;
}

const std::string& intifada::Path_Configuration::get_factory_validation()const
{
 return validation_;
}

void intifada::Path_Configuration::make_file_access(std::string& file,const std::string& p, const std::string& f)const
{
  file=p;
  if(file.empty()!=true)
      {
        file+='/';
      }
  file+=f;
  return;
}

