/*
   Copyright (C) 2009 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGURATION_INTERNAL_PARSER_HXX_
# define CONFIGURATION_INTERNAL_PARSER_HXX_

# include <string>
# include <vector>
# include <intifada/Map.hxx>
# include <intifada/inttypes.h>
namespace intifada
  {
  /// Internal parser to read some internal files
  /**
   * The key form depending on type
   */
    class Configuration_Internal_Parser
      {
      private:
      typedef std::ios_base& (*base_func_type)(std::ios_base&);
      public:
      typedef std::vector<int> int_array_type;
      typedef std::vector<std::string> string_array_type;

      public:
        typedef enum{
          UNKNOW,
          STRING,
          INTEGER,
          INTEGER_ARRAY
        }arg_type;

      private:

        typedef std::vector<std::pair<std::string,std::string> > cmds_list_type;
        typedef cmds_list_type::iterator cmds_iterator_list_type;

        typedef intifada::Map<std::string,arg_type> id_list_type;
        typedef id_list_type::iterator id_list_iterator_type;
        typedef id_list_type::const_iterator id_list_const_iterator_type;
        typedef id_list_type::pair id_list_pair_type;
      public:
        Configuration_Internal_Parser();
        virtual ~Configuration_Internal_Parser();

      public:
        int add_key(const std::string& key,const arg_type& type);
        arg_type get_arg_type(const std::string& key)const;
        std::istream& parse(std::istream& is);

        const std::string& get_key()const;

        void get_arg(std::string& av)const;
        void get_arg(int& av)const;

        void get_arg(int_array_type& array)const;

private:
        void remove_begin_spaces(std::string& num)const;
        void remove_end_spaces(std::string& num)const;
        base_func_type get_base(const std::string& num)const;
public:

        void find_arg(const arg_type& at,std::string& av)const;
        void find_arg(const arg_type& at,int& av)const;
        void find_arg(const arg_type& at,uint8_t& av)const;

        int add_arg(const arg_type& at,const std::string& av);

      private:
        std::istream& parse_string(std::istream& is,std::string& val)const;
        std::istream& parse_array(std::istream& is,string_array_type& val)const;
      private:
        cmds_list_type cmds_;
        id_list_type keys_;

        std::string key_;
        std::string arg_;
        string_array_type arg_array_;

      };
  }

#endif // CONFIGURATION_INTERNAL_PARSER_HXX_
