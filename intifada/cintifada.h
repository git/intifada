/*
   Copyright (C) 2009,2010 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CINTIFADA_H_
# define CINTIFADA_H_

/**
* C interface to intifada
*
* It is actually just oriented for wireshark plugin
*/
# include <intifada/inttypes.h>
#ifdef __cplusplus
extern "C" {
#endif

  typedef void* i_ptr_Configuration_XML_Reader;
  typedef void* i_ptr_Record_Repository;
  typedef void* i_ptr_Message;
  typedef void* i_ptr_Record_Iterator_List;
  typedef void* i_ptr_Record;
  typedef void* i_ptr_Record_Iterator;
  typedef uint16_t stream_size_type; /* mapping of intifada::Stream::size_type */
  typedef uint16_t block_size_type;
  typedef uint16_t record_size_type;
  typedef char* i_path_name_type;
  typedef const char* i_path_name_const_type;
  typedef void* i_type;
  typedef char i_char_type;
  typedef const char i_const_char_type;
  typedef i_const_char_type* i_const_string_type;
  typedef i_char_type* i_string_type;
  typedef void* user_data_type;
  typedef void* i_family_list_type;

  typedef int (*record_iterator_category)(uint8_t,user_data_type);
  typedef int (*record_iterator)(i_ptr_Record,block_size_type,record_size_type,user_data_type);
  typedef int (*data_field_iterator)(stream_size_type,stream_size_type,const i_path_name_const_type,i_type,user_data_type);

  /**
  * Return a pointer to a instance of xml reader file
  * prepare to read a XML file
  * \param dtd a XML DTD
  * \param xml XML file description
  * \return an opaque i_ptr_Configuration_XML_Reader
  * \attention i_ptr_Configuration_XML_Reader must be released after use
  */
  i_ptr_Configuration_XML_Reader i_create_xml_configuration_reader(
      const char*dtd
      ,const char*xml);

  /**
  *
  * @param c object to release
  */
  void i_release_xml_configuration_reader(i_ptr_Configuration_XML_Reader c);

  /**
  *
  * @param c object used to read file
  * @return -1 if a problem occurs
  * @return 0 if xml file is successfully readed
  */
  int i_open_xml_configuration(i_ptr_Configuration_XML_Reader c);

  /**
  *
  * @return a pointer on record repository
  */
  i_ptr_Record_Repository i_get_record_repository_instance();

  /**
  *
  * @return a pointer on the asterix family list
  * @attention it must be released after use
  */
  i_family_list_type i_create_family_list_type();

  /**
   *
   * @param f family list to release
   */
  void i_release_family_list_type(i_family_list_type f);

  /**
   * get families
   * @param r record repository
   * @param f family list
   */
  void i_get_record_families(i_ptr_Record_Repository r,i_family_list_type f);

  /**
   * Getting family list size
   * @param f family list to get size
   * @return family list size
   */
  int i_get_families_size(i_family_list_type f);

  /**
   * getting a family name
   * @param f family list
   * @param idx family list array index
   * @return a family name
   */
  const char* i_get_family(i_family_list_type f,int idx);

  /**
   * load a record repository with asterix records described in xml reader
   * @param l xml reader object
   * @param r asterix record repository
   * @return -1 if a problem occurs
   * @return 0 if record repository is successfully loaded
   */
  int i_xml_parse(i_ptr_Configuration_XML_Reader l,i_ptr_Record_Repository r);

  /**
   * Creating an empty asterix message
   * @param r record repository (used to create the structure)
   * @param family (family with the message is associated)
   * @return a pointer on the message
   * @attention the created message must be released after use
   */
  i_ptr_Message i_create_message(i_ptr_Record_Repository r,const char*family);

  /**
   * release an asterix message
   * @param msg message to release
   */
  void i_release_message(i_ptr_Message msg);

  /**
   * fill an asterix message with a byte stream
   * @param msg asterix message to fill
   * @param s byte stream to copy in msg
   * @param o asterix begining offset in s
   * @param m byte stream size
   * @return -1 if something goes wrong
   * @return 0 if copy is a success
   */
  int i_message_set_stream(i_ptr_Message msg,const uint8_t *s, stream_size_type o, stream_size_type m);

  /**
   * fill a byte stream with an asterix message
   * @param msg asterix message to copy in s
   * @param s byte stream to fill
   * @param o write offset in byte stream
   * @param m byte stream size
   * @return -1 if something goes wrong
   * @return 0 if copy is a success
   */
  int i_message_get_stream(i_ptr_Message msg,uint8_t *s, stream_size_type o, stream_size_type m);

  /**
   * Create an object to handle list of callbacks to access asterix message
   * @return the object
   * @attention the object must be released after use
   */
  i_ptr_Record_Iterator_List i_create_record_iterator_list();

  /**
   * Releasing callbacks list handler
   * @param rl callbacks list handler
   */
  void i_release_record_iterator_list(i_ptr_Record_Iterator_List rl);

  /**
   * Registering callbacks against list handler. Could be called several times
   * @param rl list handler
   * @param fc callback which be called for each new asterix block
   * @param r callback which be called for each new asterix record
   * @param ud user data
   * @return a pointer on the object handling callbacks
   * @attention the object must be released after use
   */
  i_ptr_Record_Iterator i_register_record_iterator(i_ptr_Record_Iterator_List rl,record_iterator_category fc,record_iterator r,user_data_type ud);

  /**
   * Releasing callbacks handler object
   * @param ri callbacks handler to release
   */
  void i_release_record_iterator(i_ptr_Record_Iterator ri);

  /**
   * walk through a asterix message
   * @param msg asterix message to walk through
   * @param rl callbacks list to apply on
   */
  void i_message_foreach_record(i_ptr_Message msg,i_ptr_Record_Iterator_List rl);

  /**
   * walk through on all asterix record data fields
   * @param rec asterix record
   * @param dfi callback which called on each data field
   * @param ud user data
   */
  void i_record_foreach_data_field(i_ptr_Record rec,data_field_iterator dfi,user_data_type ud);

  /**
   * Getting value (string form) of t
   *
   * @param t type to obtain value from
   * @return the value
   * @attention value must be released after use
   */
  i_const_string_type i_get_string_value(i_type t);

  void i_release_string_value(i_const_string_type t);

  #ifdef __cplusplus
}
#endif

#endif /* CINTIFADA_H_ */
