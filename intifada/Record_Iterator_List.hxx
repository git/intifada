/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_RECORD_ITERATOR_LIST_HXX
# define INTIFADA_RECORD_ITERATOR_LIST_HXX

# include <intifada/Record_Iterator.hxx>

# include <intifada/Map.hxx>

namespace intifada
{
  class Record_Iterator_List
  {
  private:

    template <typename _Elem>
    class multimap
    {
    public:
      typedef int16_t key;
    private:
      typedef Multi_Map<key,_Elem> list_type;
    public:
      typedef typename list_type::iterator iterator;
      typedef typename list_type::const_iterator const_iterator;

    private:
      typedef typename list_type::pair pair;
    public:
      multimap();

      const_iterator end()const;

      iterator lower_bound(key k);

      const_iterator lower_bound(key k)const;

      iterator upper_bound(key k);

      const_iterator upper_bound(key k)const;

      iterator insert(_Elem e,key c);

      bool exist(key k)const;

    private:
      list_type list_;
    };
  private:
    typedef multimap<Record_Iterator*> list_type;
    typedef multimap<Record_Const_Iterator*> const_list_type;
  public:
    Record_Iterator_List();

    int register_iterator(Record_Iterator& it,uint8_t c);

    int register_iterator(Record_Const_Iterator& it,uint8_t c);

    int register_iterator(Record_Iterator& it);

    int register_iterator(Record_Const_Iterator& it);

    int apply(
	     uint8_t cat,Record& rec,
	     const Message::block_list_size_t& b,
	     const Block::record_list_size_t& r,
	     bool first_call
	     )const;

    int apply_on_const(
	     uint8_t cat,const Record& rec,
	     const Message::block_list_size_t& b,
	     const Block::record_list_size_t& r,
	     bool first_call
	     )const;

    int all_categories_apply(
	     uint8_t cat,Record& rec,
	     const Message::block_list_size_t& b,
	     const Block::record_list_size_t& r,
	     bool first_call
	     )const;

    int all_categories_apply_on_const(
	     uint8_t cat,const Record& rec,
	     const Message::block_list_size_t& b,
	     const Block::record_list_size_t& r,
	     bool first_call
	     )const;

    bool category_presence(uint8_t cat)const;

  private:
    list_type l_;
    const_list_type cl_;
  };
}

# include <intifada/Record_Iterator_List.txx>

#endif
