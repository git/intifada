/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Stream.hxx>

#ifndef INTIFADA_USE_INLINE
# include<intifada/Stream.ixx>
#endif

intifada::Stream::Stream()
:t_()
,stream_pos_(-1)
,stream_size_(-1)
{}

intifada::Stream::Stream(size_type n)
:t_(n)
,stream_pos_(-1)
,stream_size_(-1)
{}

intifada::Stream::~Stream()
{
	t_.clear();
}

void intifada::Stream::resize(size_type new_size)
{
	t_.resize(new_size);
}

void intifada::Stream::extend(size_type new_size)
{
	if(t_.size() < new_size)
	{
		this->resize(new_size);
	}
}

/// bit modificator by byte and offset
void intifada::Stream::set_bit(uint8_t b,uint8_t o,bool s)
{
	uint8_t byte = b - 1;
	uint8_t byte_offset = o-1;

	if(s==true)
		t_[byte] |= 1 << byte_offset;
	else
		t_[byte] &= ~(1 << byte_offset);
}

/// Byte modification by total offset
void intifada::Stream::set_bit(uint8_t b,bool s)
{
	uint8_t byte=t_.size() - 1 - b/8;

	uint8_t byte_offset=b%8;
	if(s==true)
		t_[byte] |= 1 << byte_offset;
	else
		t_[byte] &= ~(1 << byte_offset);
}

/// Byte access by total offset
bool intifada::Stream::get_bit(uint8_t b)const
{
	intifada::Stream::size_type byte=t_.size() - 1 - b/8;

	bool ret=false;
	// Check if requested bit oversize vector size
	if(byte<t_.size()){
		// Ok, requested byte is in vector
		uint8_t byte_offset = b%8;
		ret = t_[byte] & (1 << byte_offset);
	}

	return ret;
}

/// bit access by byte and offset
bool intifada::Stream::get_bit(uint8_t b,uint8_t o)const
{
	uint8_t byte = b - 1;
	uint8_t byte_offset = o-1;

	bool ret=false;
	if((byte<t_.size())){
		// Ok, requested byte is in vector
		ret = t_[byte] & (1 << byte_offset);
	}


	return ret;
}

intifada::Stream::const_reverse_iterator intifada::Stream::get_stream_position(const Data_Field_Characteristics&)const
{
	return t_.rbegin();
}

intifada::Stream::reverse_iterator intifada::Stream::get_stream_position(const Data_Field_Characteristics&)
{
	return t_.rbegin();
}

intifada::Stream::size_type intifada::Stream::set_stream(const uint8_t *s, intifada::Stream::size_type m)
{
	intifada::Stream::size_type read_size;

	// Throw exception if trying to read more bytes than buffer could handle
	if(t_.size()>m){
		throw Parsing_Input_Length_Exception(t_.size(),m);
	}

	for(read_size=0;read_size<t_.size();++read_size){
		t_[read_size]=s[read_size];
	}
	return read_size;
}

/// Ecriture du flux binaire
intifada::Stream::size_type intifada::Stream::get_stream(uint8_t *s, intifada::Stream::size_type m)const
{
	intifada::Stream::size_type write_size;

	// Throw exception if writing buffer is too small
	if(m<t_.size()){
		throw Parsing_Output_Length_Exception(t_.size(),m);
	}

	for(write_size=0;write_size<t_.size();++write_size){
		s[write_size]=t_[write_size];
	}
	return write_size;
}

void intifada::Stream::set_stream_position(const size_type& pos,const size_type& size)const
{
	stream_pos_=pos;
	stream_size_=size;
}

void intifada::Stream::get_stream_position(size_type& pos,size_type& size)const
{
	pos=stream_pos_;
	size=stream_size_;
}
