/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_FL_CONVERTER_HXX
# define INTIFADA_FL_CONVERTER_HXX

# include <intifada/Unit_Converter.hxx>
namespace intifada
{
/// Class handling types and transferts to/from streams
template <typename T>
class FL_Converter_T : public Unit_Converter_Base_Type<T>
{

public:
	typedef Unit_Converter_Base_Type<T> inherited;
public:

	/// Default ctor
	FL_Converter_T()
	:inherited(){}

	/// Copy constructor
	FL_Converter_T(const FL_Converter_T&rhs)
	:inherited(rhs)
	{}

	/// return the type identification
	virtual std::string get(const T& v)const
	{
		std::ostringstream os;

		os << v*25 << "ft, FL"<<v/4 << " ,(" << v << ")";
		return os.str();
	}

	/// Copy operator
	virtual FL_Converter_T& operator=(const FL_Converter_T&rhs)
	{
		if(&rhs!=this)
		{
			inherited::operator=(rhs);
		}
		return *this;
	}
	/// Clone unit converter
	/**
	 * @return a dynamicly allocated Unit_Converter based on *this structure
	 */
	virtual Unit_Converter* clone()const
				{
		return new FL_Converter_T<T>(*this);
				}
};
}

#endif // INTIFADA_FL_CONVERTER_HXX
