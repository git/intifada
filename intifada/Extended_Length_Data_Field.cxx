/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Extended_Length_Data_Field.hxx>
#include <intifada/Logger.hxx>
#include <sstream>

// Pour ne pas exécuter du code supprimé par la MOE Outils-DPS
#define DISABLE_REMOVED_CODE_MOE_OUTILS_DPS

intifada::Extended_Length_Data_Field::Update_Stream_Functor::Update_Stream_Functor(
		Stream& byte_stream
		,const Property_Uint8& primary
		,const Property_Uint8& secondaries):
		byte_stream_(byte_stream)
,stream_pos_(0)
,stream_size_(0)
,primary_(primary)
,secondaries_(secondaries)
{
	byte_stream.get_stream_position(stream_pos_,stream_size_);
}

int intifada::Extended_Length_Data_Field::Update_Stream_Functor::operator()(
		const Path_Name &
		,const Data_Field_Characteristics &c
		,const Data_Field_Characteristics_List::part_size_type&part)
{
	if(c.is_cached()==true)
	{
		Type *t=c.get_type();
		if(t->is_changed_state()==true)
		{
			unsigned int minsize=primary_+ part * secondaries_;

			// if(size()<minsize)
			if(byte_stream_.size()<minsize)
			{
				byte_stream_.resize(minsize);
			}

			Stream::size_type lsb_position=primary_-1;
			if(part>0)
			{
				lsb_position += secondaries_*part;
			}

			Stream::reverse_iterator rit = byte_stream_.rend()-lsb_position-1;
			t->to_stream(rit,c.shift(),c.size());
			// now stream reflects type, so we can reset changed state
			t->reset_changed_state();
		}
	}
	return 0;
}

intifada::Extended_Length_Data_Field::Extended_Length_Data_Field()
:inherited(),
 byte_stream_(),
 repetitive_(),
 primary_(),
 secondaries_()
{
	this->set_extended_characteristics();
	this->register_property("repetitive",repetitive_);
	this->register_property("primary_size",primary_);
	this->register_property("secondaries_size",secondaries_);

}

intifada::Extended_Length_Data_Field::Extended_Length_Data_Field(const Extended_Length_Data_Field &rhs)
:inherited(rhs),
 byte_stream_(),
 repetitive_(rhs.repetitive_),
 primary_(rhs.primary_),
 secondaries_(rhs.secondaries_)
{
	this->relink_property("repetitive",repetitive_);
	this->relink_property("primary_size",primary_);
	this->relink_property("secondaries_size",secondaries_);
}

void intifada::Extended_Length_Data_Field::set_property(const std::string& prop,const std::string& val)
    								  {
	this->Properties::set_property(prop,val);
	if(repetitive_==true)
	{
		secondaries_=primary_;
		this->set_repetitive_characteristics();
	}
    								  }

void intifada::Extended_Length_Data_Field::set_property(const std::string& prop,const Property_Type& val)
    								  {
	this->Properties::set_property(prop,val);
	if(repetitive_==true){
		secondaries_=primary_;
		this->set_repetitive_characteristics();
	}
    								  }

intifada::Stream::size_type
intifada::Extended_Length_Data_Field::size()const
{
	this->update_stream();
	// update FX to co
	this->update();

	return byte_stream_.size();
}

uint8_t
intifada::Extended_Length_Data_Field::primary_size()const
{
	return primary_;
}

uint8_t
intifada::Extended_Length_Data_Field::secondaries_size()const
{
	return secondaries_;
}

intifada::Data_Field_Characteristics*
intifada::Extended_Length_Data_Field::insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size,uint8_t part)
{
	Data_Field_Characteristics* f=NULL;
	int status;

	f = this->get_new_structure_description();
	status=f->set(type,start,size);
	if(status==0)
	{
		if(repetitive_!=true)
		{
			insert_to_structure(name,f,part);
		}
		else
		{
			insert_to_structure(name,f,0);
		}
	}
	else
	{
		this->release_structure_description(f);
		f=NULL;
	}
	return f;
}

intifada::Stream::size_type
intifada::Extended_Length_Data_Field::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	Stream::size_type read_size=0;
	Stream::size_type stream_pos=o;

	clog("Extended_Length_Data_Field") << "set_stream(" << (void*) s << ",0x"<< std::hex << o <<",Ox" << std::hex << m << ")" << std::endl;
	clog("Extended_Length_Data_Field") << "\tprimary=" << primary_.string_value() << std::endl;
	clog("Extended_Length_Data_Field") << "\tsecondaries=" << secondaries_.string_value() << std::endl;
	clog("Extended_Length_Data_Field") << "\trepetitive=" << repetitive_.string_value() << std::endl;


	// first part acquisition
	if(m<primary_){
		throw Parsing_Input_Length_Exception(0,m);
	}

	for(read_size=0;read_size<primary_;++read_size)
	{
		byte_stream_.push_back(s[stream_pos+read_size]);
	}
	// Last byte memorisation of first part because it's last lsb byte
	// which indicate next byte presence
	uint8_t last_byte = s[stream_pos+read_size-1];

	while((last_byte & 0x01)==1)
	{
		if(repetitive_==true)
		{
			Data_Field::push_back_characteristics();
		}
		// Size control
		if((m-read_size)<secondaries_)
		{
			throw Parsing_Input_Length_Exception(read_size,m);
		}
		// secondary part acquisition
		Stream::size_type secondary_read_size;
		for(secondary_read_size=0;secondary_read_size<secondaries_;++secondary_read_size)
		{
			byte_stream_.push_back(s[stream_pos+read_size+secondary_read_size]);
		}
		last_byte = byte_stream_[read_size+secondary_read_size-1];
		read_size +=secondaries_;
	}
	byte_stream_.set_stream_position(o,read_size);

	return read_size;
}

intifada::Stream::size_type
intifada::Extended_Length_Data_Field::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{
	this->update_stream();
	if(byte_stream_.empty())
	{
		return 0;
	}
	Stream::size_type write_size=0;
	Stream::size_type offset=o;
	if(m<primary_)
		throw Parsing_Output_Length_Exception(0,m);

	// update FXs information
	update();
	for(;write_size<primary_;++write_size)
	{
		s[offset+write_size]=byte_stream_[write_size];
	}

	// update FX information
	// update();

	uint8_t last_byte = byte_stream_[write_size-1];

	while((last_byte & 0x01)==1)
	{
		// size control
		if((m-offset-write_size)<secondaries_)
			throw Parsing_Output_Length_Exception(write_size,m);

		// secondary part acquisition
		Stream::size_type secondary_write_size;
		for(secondary_write_size=0;secondary_write_size<secondaries_;++secondary_write_size)
		{
			s[offset+write_size+secondary_write_size]=byte_stream_[write_size+secondary_write_size];
		}
		last_byte = byte_stream_[write_size+secondary_write_size-1];
		write_size +=secondaries_;
	}
	byte_stream_.set_stream_position(offset,write_size);
	return write_size;
}

const intifada::Type& intifada::Extended_Length_Data_Field::get_value(const Path_Name& n)const
{
	Data_Field_Characteristics_List::part_size_type part=Data_Field_Characteristics_List::npos;
	const Data_Field_Characteristics *pt_c;
	if(repetitive_==true)
	{
		std::string spart=n.get_back();
		part = get_idx(spart);
		Path_Name name=n;
		name.pop_back();
		pt_c=&get_structure_characteristics(name,part);
	}
	else
	{
		pt_c=&get_structure_characteristics(n);
		part=get_structure_part(n);

	}

	intifada::Type* type=pt_c->get_type();


	if(pt_c->is_cached()==false)
	{
		unsigned int minsize=primary_+ part * secondaries_;
		if(byte_stream_.size()<minsize)
		{
			//byte_stream_.resize(get_lsb_position(minsize));
			byte_stream_.resize(minsize);
		}

		Stream::const_reverse_iterator rit = byte_stream_.rend() - get_lsb_position(part) - 1;
		type->set_from_stream(rit,pt_c->shift(),pt_c->size());

		pt_c->set_cached();
		type->reset_changed_state();
	}


	return *type;
}

intifada::Type& intifada::Extended_Length_Data_Field::get_value(const Path_Name& n)
{
	Data_Field_Characteristics_List::part_size_type part=Data_Field_Characteristics_List::npos;
	const Data_Field_Characteristics *pt_c;
	if(repetitive_==true)
	{
		std::string spart=n.get_back();
		part = get_idx(spart);
		Path_Name name=n;
		name.pop_back();
		pt_c=&get_structure_characteristics(name,part);
	}
	else
	{
		pt_c=&get_structure_characteristics(n);
		part=get_structure_part(n);

	}

	intifada::Type* type=pt_c->get_type();


	if(pt_c->is_cached()==false)
	{
		unsigned int minsize=primary_+ part * secondaries_;
		if(byte_stream_.size()<minsize)
		{
			//byte_stream_.resize(get_lsb_position(minsize));
			byte_stream_.resize(minsize);
		}

		Stream::const_reverse_iterator rit = byte_stream_.rend() - get_lsb_position(part) - 1;
		type->set_from_stream(rit,pt_c->shift(),pt_c->size());

		pt_c->set_cached();
		type->reset_changed_state();
	}


	return *type;
}

intifada::Data_Field * intifada::Extended_Length_Data_Field::clone()const
{
	return new Extended_Length_Data_Field(*this);
}

uint8_t
intifada::Extended_Length_Data_Field::get_part(uint8_t o) const
{
	uint8_t part=0;
	if(o>=primary_){
		o -=primary_;
		part=o/secondaries_;
		++part;
	}

	return part;
}

int intifada::Extended_Length_Data_Field::foreach(Functor &it,const Path_Name& path)
{
	this->update_stream();

	Data_Functor f(path,it,byte_stream_,primary_,secondaries_);
	Data_Field::foreach(f);
	return 0;
}

int intifada::Extended_Length_Data_Field::foreach(Const_Functor &it,const Path_Name& path)const
{
	this->update_stream();

	Data_Const_Functor f(path,it,byte_stream_,primary_,secondaries_);
	Data_Field::foreach(f);
	return 0;
}

const intifada::Type& intifada::Extended_Length_Data_Field::get_type(const Path_Name& n)const
{
	Data_Field_Characteristics_List::part_size_type part=Data_Field_Characteristics_List::npos;
	const Data_Field_Characteristics *pt_c;
	if(repetitive_==true)
	{
		std::string spart=n.get_back();
		part = get_idx(spart);
		Path_Name name=n;
		name.pop_back();
		pt_c=&get_structure_characteristics(name,part);
	}
	else
	{
		pt_c=&get_structure_characteristics(n);
		part=get_structure_part(n);

	}

	intifada::Type* type=pt_c->get_type();

	return *type;
}

void intifada::Extended_Length_Data_Field::update_stream()const
{
	Update_Stream_Functor f(byte_stream_,primary_,secondaries_);
	Data_Field::foreach(f);
}

intifada::Stream::size_type
intifada::Extended_Length_Data_Field::get_lsb_position(Stream::size_type p)const
{
	Stream::size_type ret=primary_-1;

	if(p>0){
		ret += secondaries_*p;
	}

	return ret;

}

intifada::Stream::size_type
intifada::Extended_Length_Data_Field::get_msb_position(Stream::size_type p)const
{
	Stream::size_type ret=0;

	if(p>0){
		ret = primary_ + secondaries_*(p-1);
	}

	return ret;
}

void
intifada::Extended_Length_Data_Field::update()const
{
	// Start with the last byte and back to the first secondary part
	// not yet the last byte
	uint8_t last_byte = byte_stream_.size();
	if(last_byte > 0){
		--last_byte;
		// now, last byte is really the last byte
	}else{
		return;
	}
	uint8_t last_part = this->get_part(last_byte);
	
	// Début partie supprimée dans le code MOE DPS-Outils : conservé au cas où
#ifdef DISABLE_REMOVED_CODE_MOE_OUTILS_DPS
	int16_t idx=last_part;
	do{
		// Removing all null parts
		if(this->is_part_null(last_part)==true){
			// part is null. Remove it
			if(last_part==0)
				byte_stream_.pop_back(primary_);
			else
				byte_stream_.pop_back(secondaries_);
		}else{
			break;
		}
		if(last_part==0){
			break;
		}else{
			--last_part;
		}
		--idx;
	}while(idx>=0);
#endif // DISABLE_REMOVED_CODE_MOE_OUTILS_DPS
	// Fin partie supprimée dans le code MOE DPS-Outils : conservé au cas où

	// last_part is pointing on the last non null part.
	// lsb must be set to 0
	// set_bit interface: byte(begin at one) bit(in byte) value
	byte_stream_.set_bit(get_lsb_position(last_part)+1,1,false);

	while(last_part>0){
		--last_part;
		byte_stream_.set_bit(get_lsb_position(last_part)+1,1,true);
	}

	if( (byte_stream_.empty()==true) && (force_presence_==true)){
		Stream::value_type v=0;
		for(Stream::size_type p=0;p<primary_;++p){
			byte_stream_.push_back(v);
		}
	}
}

bool
intifada::Extended_Length_Data_Field::is_part_null(uint8_t part)const
{
	Stream::size_type msb=get_msb_position(part);
	Stream::size_type lsb=get_lsb_position(part);

	// Scan every byte except lsb because a special
	// treatment must be applied
	bool is_null=true;
	for(Stream::size_type b = msb;b<lsb;++b){
		if(byte_stream_[b]!=0){
			is_null=false;
			break;
		}
	}

	// Handle lsb of the concerned part only if isn't already
	// not null
	if(is_null==true){
		if((byte_stream_[lsb] & 0xfe)!=0){
			is_null=false;
		}
	}
	return is_null;
}
