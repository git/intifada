/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_STRING_TYPE_HXX
# define INTIFADA_STRING_TYPE_HXX

# include <intifada/Type.hxx>

namespace intifada
{
/// String type
/**
 * This type could handle user fixed length character and ascii or ICAO character
 * encoding.
 * Supported properties:
 * stream_coding:string:ascii|icao
 * stream_character_size:int:character stream size
 * keep_code:bool:if stream character is unknow, keep the original code if true
 */
class String_Type : public intifada::Type
{
private:

	static const uint8_t ascii2icao[];
	static const uint8_t icao2ascii[];


public:
	typedef enum{
		ASCII,
		ICAO
	}encoding;
private:
	typedef intifada::Type inherited;
private:
	char internal_character_representation(const uint8_t&)const;
	uint8_t external_character_representation(const uint8_t&)const;
public:
	String_Type();
	String_Type(const std::string& value);

public:
	/// Copy constructor
	String_Type(const String_Type& rhs);

	String_Type(const Type& rhs);

	virtual String_Type& operator=(const Type&rhs);

	/// Copy operator
	String_Type& operator=(const String_Type&rhs);

	String_Type& operator=(const std::string&rhs);

	/// Set this to null value
	/**
	 * set a value's instance to null (eg 0 for a numeric type, "" for a string type ...)
	 */
	virtual void set_null();

	/// handle changed state
	/**
	 * changed states could be detected. This method should be called to
	 * indicate which value to remember
	 */
	virtual void reset_changed_state();

	/// handle changed state
	/**
	 * \return true if current value differs from value when reset_changed_state were called
	 * \return false if current value doesn't differ from value when reset_changed_state were called
	 */
	virtual bool is_changed_state()const;


	/// Get size
	/**
	 * \return size object (unit is the bit)
	 */
	virtual ssize_t get_size()const;

	/// Compare with a string
	virtual bool operator==(const std::string& rhs)const;

private:

	/// adapt stream to internal data
	/**
	 * before setting internal data from stream, this method
	 * is called with raw stream extracted. This could be used by sub classes
	 * to do anything particular
	 *
	 * \param v data coming from stream
	 * \param size size in bits extracted from stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	//virtual std::string stream_to_internal(const std::string&v,int size)const;

	/// adapt internal data to stream
	/**
	 * above symetric
	 * \param v data coming from internal
	 * \param size size in bits that will be inserted in stream
	 * \return v by default
	 *
	 * inherited new method must adapt the returned value
	 */
	//virtual std::string internal_to_stream(const std::string&v,int size)const;

public:

	/// Get internal value to string
	/**
	 * \return internal value
	 */
	virtual std::string get_to_string()const;

	/// Set internal value from string
	/**
	 * \param v value
	 * \return 0 if no error
	 */
	virtual int set_from_string(const std::string& v);

	/// Transfert a stream into a real type
	/**
	 * \param b stream to transfert
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 */
	virtual void set_from_stream(Stream::const_reverse_iterator b,int start,int size);

	/// Transfert a real type into a stream
	/**
	 * \param b target stream
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 */
	virtual void to_stream(Stream::reverse_iterator b,int start,int size)const;

	/// Clone type
	/**
	 * allocate a new type from this. Must be freed by user using delete
	 */
	virtual String_Type* clone()const;

	virtual void set_property(const std::string& prop,const std::string& val);

	// String_Type& operator=(const std::string& rhs);

	operator std::string();
private:
	std::string trim_trailing_spaces(const std::string& v)const;

private:
	encoding code_;

	std::string value_;
	std::string base_value_;

	Property_String stream_coding_;
	Property_Int stream_character_size_;
	Property_Bool keep_code_;
};

inline char String_Type::internal_character_representation(const uint8_t&c)const
{
	char ret=c;
	if(code_==ICAO){
		ret=icao2ascii[c];
		if((keep_code_==true)&&(ret==0)){
			ret=c;
		}
	}
	return ret;
}
inline uint8_t String_Type::external_character_representation(const uint8_t&c)const
{
	uint8_t ret=c;
	if(code_==ICAO){
		ret=ascii2icao[c];
		if((keep_code_==true)&&(ret==0)){
			ret=c;
		}
	}
	return ret;
}
}

std::ostream& operator<<(std::ostream& os,const intifada::String_Type& str);

#endif // INTIFADA_STRING_TYPE_HXX
