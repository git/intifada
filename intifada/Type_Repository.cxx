/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Type_Repository.hxx>
#include <intifada/Integer.hxx>
#include <intifada/String_Type.hxx>

#include <iostream>
intifada::Type_Repository* intifada::Type_Repository::instance_=NULL;

intifada::Type_Repository::Type_Repository_Iterator::Type_Repository_Iterator() {}

intifada::Type_Repository::Type_Repository_Iterator::~Type_Repository_Iterator() {}

int
intifada::Type_Repository::Type_Repository_Iterator::operator()
(const std::string& t,const Type&)
  {
    std::cout << t << std::endl;
    return 0;
  }

intifada::Type_Repository::Type_Repository():
  types_()
  {}

intifada::Type_Repository::~Type_Repository()
  {
    for(type_map_const_iterator_t i=types_.begin();i!=types_.end();++i){
      const Type* t=(*i).second;
      delete t;
    }
    types_.clear();
  }

int
intifada::Type_Repository::release()
  {
    if(instance_!=NULL){
      delete instance_;
      instance_=NULL;
    }
    return 0;
  }

const intifada::Type*
intifada::Type_Repository::get(const std::string& n)const
{
  type_map_const_iterator_t it=types_.find(n);
  Type*ret=NULL;
  if(it!=types_.end()){
    ret=(*it).second;
  }
  return ret;
}

intifada::Type_Repository* intifada::Type_Repository::instance()
  {
    if(Type_Repository::instance_==NULL){
      instance_=new Type_Repository;
      register_builtintypes();
      instance_->register_type<intifada::String_Type>();
    }
    return instance_;
  }

int
intifada::Type_Repository::foreach(Type_Repository_Iterator& it)const
{
  for(type_map_const_iterator_t i=types_.begin();i!=types_.end();++i){
    const std::string& k=(*i).first;
    const Type* t=(*i).second;
    it(k,*t);
  }
  return 0;
}

int
intifada::Type_Repository::foreach()const
{
  Type_Repository_Iterator it;
  return foreach(it);

}
