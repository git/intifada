/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

template <typename T>
int
intifada::Type_Repository::register_type()
{
  Type *type=new T;
  std::string name=type->get_name();
  type_map_pair_t p=types_.insert(std::make_pair(name,type));

  int ret=0;
  if(p.second!=true){
    delete type;
    ret=-1;
  }

  return ret;
}
