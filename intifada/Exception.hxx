/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTION_HXX
# define EXCEPTION_HXX

# include <intifada/Path_Name.hxx>
# include <intifada/inttypes.h>

/// intifada exceptions
/**
* -    Exception
* -            Description_Exception
* -                    Description_Field_Duplication_Exception
* -                    Description_Field_Unknow_Exception
* -            Parsing_Exception
* -                    Parsing_Stream_Exception
* -                            Parsing_Input_Length_Exception
* -                            Parsing_Output_Length_Exception
* -                    Parsing_XML_Exception
*/

namespace intifada
{

  /// Base class exception
  class Exception
  {
  public:
    Exception();
    virtual ~Exception();

    /// get a string containing human readable reason
    /**
    *
    * @return a string with error description
    */
    virtual std::string what()const;
  };

  class Description_Exception : public Exception
  {
  public:
    Description_Exception();
    virtual ~Description_Exception();
  };

  class Description_Field_Duplication_Exception : public Description_Exception
  {
  public:
    Description_Field_Duplication_Exception();
    virtual ~Description_Field_Duplication_Exception();
  };

  class Description_Field_Unknow_Exception : public Description_Exception
  {
  public:
    Description_Field_Unknow_Exception(const Path_Name& n);
    virtual ~Description_Field_Unknow_Exception();

    const Path_Name& get_name()const;

  private:
    Path_Name name_;
  };

  /// Exception related with parsing error
  /**
  * This class hierarchy handle exceptions related with parsing errors.
  * Parsing is decoding and encoding asterix message. XML errors and input analyse
  * format is handled by another way
  */
  class Parsing_Exception : public Exception
  {
  public:
    Parsing_Exception();
    virtual ~Parsing_Exception();
  };

  /// Exception related to unknow category
  class Parsing_Unknow_Category : public Parsing_Exception
  {
  public:
    Parsing_Unknow_Category(int error_pos, const std::string& family,const uint8_t& c);

    const std::string& get_family()const;

    const uint8_t& get_category()const;

    /// accessor on error position
    /**
    * @return error position
    * @return -1 if non sense
    *
    */
    int get_error_position()const {return error_pos_;}

  private:
    int error_pos_;
    const std::string family_;
    const uint8_t category_;
  };

  /// Exception relating to length problems
  class Parsing_Stream_Exception : public virtual Parsing_Exception
  {
  public:
    /// Ctor
    /**
    *
    * @param error_pos error position in the stream
    * @param expected expected stream length
    * @param received calculated stream length
    */
    Parsing_Stream_Exception(int error_pos,int expected,int received);

    /// Ctor
    /**
    *
    * @param expected expected stream length
    * @param received calculated stream length
    */
    Parsing_Stream_Exception(int expected,int received);

    /// accessor on expected size
    /**
    * @return expected size
    * @return -1 if non sense
    *
    */
    int get_expected()const {return w_;}

    /// accessor on expected size
    /**
    * @return expected size
    * @return -1 if non sense
    *
    */
    int get_received()const {return r_;}

    /// accessor on error position
    /**
    * @return error position
    * @return -1 if non sense
    *
    */
    int get_error_position()const {return error_pos_;}
  private:
    const int error_pos_;
    const int w_;
    const int r_;

  };


  /// Exception relating to length problems
  class Parsing_Input_Length_Exception : public Parsing_Stream_Exception
  {
  public:
    typedef Parsing_Stream_Exception inherited;
    /// Ctor
    /**
    *
    * @param expected expected stream length
    * @param received calculated stream length
    */
    Parsing_Input_Length_Exception(int expected,int received);

    /// Ctor
    /**
    * @param error_pos where (near) the error occurs
    * @param expected expected stream length
    * @param received calculated stream length
    */
    Parsing_Input_Length_Exception(int error_pos,int expected,int received);

  };

  /// Exception relating to length problems
  class Parsing_Output_Length_Exception : public Parsing_Stream_Exception
  {
  public:
    typedef Parsing_Stream_Exception inherited;
    /// Ctor
    /**
    * \param waited length
    * \param received length
    */
    Parsing_Output_Length_Exception(int waited,int received);

  };

  /// Record length problem
  /**
  * throw when a length problem occurs when stream is acquired
  * \remaks waited length isn't set
  */
  class Parsing_Input_Record_Length_Exception : public Parsing_Stream_Exception
  {
  public:
    typedef Parsing_Stream_Exception inherited;
    /// Ctor
    /**
    * \param n concerned record name
    * \param received length
    */
    Parsing_Input_Record_Length_Exception(const Path_Name& n,int received);

    const Path_Name& get_name()const;
  private:
    Path_Name name_;
  };

  /// FSPEC Length problem
  /**
  * throw when Field specification problem is detected.
  *
  */
  class Parsing_Input_Field_Specification_Exception : public Parsing_Stream_Exception
  {
  public:
    typedef Parsing_Stream_Exception inherited;
    /// Ctor
    /**
    * \param waited expected max length if specified (-1 othewise)
    * \param received length
    */
    Parsing_Input_Field_Specification_Exception(int waited,int received);

  };

  /// Exception relating to XML problems
  class Parsing_XML_Exception : public virtual Parsing_Exception
  {
  public:
    /// Ctor
    Parsing_XML_Exception();

  };
  /// Exception relating to XML problems
  class Parsing_XML_Family_Duplicate_Exception : public virtual Parsing_XML_Exception
  {
  public:
    /// Ctor
    Parsing_XML_Family_Duplicate_Exception(const std::string& family);

    virtual std::string what()const;

    std::string get_family()const;

  private:
    std::string family_;
  };

  class Parsing_XML_Dynamic_Type_Exception : public virtual Parsing_XML_Exception
  {
  public:
    Parsing_XML_Dynamic_Type_Exception(const std::string& type_error);

    virtual std::string what()const;

  private:
    std::string type_error_;
  };

}
#endif // EXCEPTION_HXX
