/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_PATH_NAME_HXX
# define INTIFADA_PATH_NAME_HXX

# include <list>
# include <string>
# include <intifada/Map.hxx> // Required to define hash function
class Logger;

namespace intifada
{
/// Data Item Name
/**
 * This class embed all name access functionalities
 * Access to a record is make using the following notation:"it.name"
 * With compounds, it can extend to two or three points.
 *
 */
class Path_Name
{
private:
	typedef std::list<std::string> path_type;
	typedef path_type::size_type path_size_type;
	typedef path_type::const_iterator path_const_iterator_type;
public:

	Path_Name(const char* name,const char& sep='.');

	Path_Name(const std::string& name,const char& sep='.');

	Path_Name(const char& sep='.');

	Path_Name(const Path_Name& rhs);

	Path_Name& operator=(const Path_Name& rhs);

	Path_Name& operator=(const std::string& rhs);

	Path_Name& operator=(const char* rhs);

	const std::string& get_full_name()const;

	const std::string& get_path()const;

	const std::string& get_front()const;

	const std::string& get_back()const;

	void pop_front();

	void pop_back();

	int get_size()const;

	bool empty()const;

	void set_name_size(unsigned int s);

	unsigned int get_name_size()const{return name_size_-1;}

	void push_back(const std::string& p);

	bool operator==(const Path_Name& rhs)const;

	bool operator!=(const Path_Name& rhs)const;

	bool operator==(const std::string& rhs)const;

	bool operator!=(const std::string& rhs)const;

	bool operator<(const Path_Name& rhs)const;

	Path_Name& operator+=(const Path_Name& rhs);

private:
	/// Split string path
	/**
	 * Split the string path and u string_path_ with computed path
	 */
	void split_path(const std::string& name);

	/// Update string path with vector computed path
	void update()const;
private:
	/// Do not use
	operator const std::string&()const;

private:
	char separator_;
	mutable std::string string_full_name_;
	mutable std::string string_partial_path_;

	mutable bool updated_;
	path_type path_;
	unsigned int name_size_;
};
}

inline intifada::Path_Name operator+(const intifada::Path_Name& lhs, const intifada::Path_Name& rhs)
{
	intifada::Path_Name tmp(lhs);
	tmp += rhs;
	return tmp;
}


std::ostream& operator<<(std::ostream& lhs, const intifada::Path_Name& rhs);

Logger& operator<<(Logger& lhs, const intifada::Path_Name& rhs);

# if INTIFADA_HAVE_UNORDERED_MAP==1
namespace std
{
//	template<>
//	class hash<intifada::Path_Name>
//	{
//	public:
//		size_t operator()(intifada::Path_Name pn) const;
//	};

template<>
class hash<const intifada::Path_Name>
{
public:
	size_t operator()(const intifada::Path_Name& pn) const
	{
	  std::string key=pn.get_full_name();
	  //return _Fnv_hash<>::hash(key.data(), key.length());
	  return _Fnv_hash_impl::hash(key.data(), key.length());
	}
};
#if 0
/// Explicit specialization of member operator for Path_Name type.
template<>
size_t std::hash<intifada::Path_Name>::operator()(intifada::Path_Name pn) const;

/// Explicit specialization of member operator for Path_Name type.
template<>
size_t std::hash<const intifada::Path_Name&>::operator()(const intifada::Path_Name& pn) const;
#endif

}
# endif // INTIFADA_HAVE_UNORDERED_MAP==1
#endif // INTIFADA_PATH_NAME_HXX
