/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_MESSAGE_HXX
# define INTIFADA_MESSAGE_HXX

# include <intifada/Data.hxx>

# include <list>
# include <string>

namespace intifada
{
class Block;
class Record;
class Record_Repository;
class Record_Iterator_List;

class Message : public virtual Data
{
private:
	typedef std::list<Block*> block_list_t;
public:
	typedef block_list_t::size_type block_list_size_t;
private:
	typedef block_list_t::iterator block_list_iterator_t;
	typedef block_list_t::const_iterator block_list_const_iterator_t;
	typedef std::pair<block_list_iterator_t,bool> block_list_pair_t;

public:
	Message(const intifada::Record_Repository* rep, const std::string& family);

	Message(const intifada::Record_Repository* rep);

	/// constructor
	/**
	 * setup message with the standard Record_Repository singleton
	 */
	Message(const std::string& family);

	/// constructor
	/**
	 * setup message with the standard Record_Repository singleton
	 */
	Message();

	/// set the policy release
	/**
	 * @param active true on message clear (upon destruction or explicit call)
	 * clear also all subsequent blocks. Default policy
	 * @param active false on message clear (upon destruction or explicit call)
	 * don't clear subsequent blocks
	 */
	void release_on_clear(bool active);

	virtual ~Message();

	/// clear the message content
	void clear();

	/// get field size
	/**
	 * \return size of field
	 */
	virtual Stream::size_type size()const;

	/// Set message's family
	/**
	 * \param f family
	 */
	void set_family(const std::string& f);

	/// Get message's family
	/**
	 * \param f family
	 */
	const std::string& get_family()const;


	/// Byte stream acquisition
	/**
	 * Handle acquisition of asterix binary stream
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return readed total bytes
	 * \throw Parsing_Input_Length_Exception if bytes to read exceed m
	 * \throw Description_Field_Unknow_Exception if an unknow field is encountered
	 */
	virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

	/// Byte stream restitution
	/**
	 * Handle byte stream restitution.
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return writed total bytes
	 * \throw Parsing_Output_Length_Exception if bytes to read exceed m
	 */
	virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

	int push_back(Block* r);

	/// Getting a new block
	Block* get_new_block();

	/// Getting a new block
	Block* get_new_block(uint8_t cat);

	/// Getting block numbers
	/**
	 * \return blocks number in the message
	 */
	block_list_size_t number()const;

	/// Getting a block
	/**
	 * \param i block index (0 is the first block)
	 * \return concerned block
	 * \return NULL if the last block is passed out
	 */
	Block* get(block_list_size_t i);

	/// Remove a block
	/**
	 * \param i block index  (0 is the first block)
	 * \return concerned block
	 * \return NULL if the last block is passed out
	 * \attention memory isn't released, it's user responsibility to freeing
	 * the memory
	 */
	Block* erase(block_list_size_t i);

	/// Insert a block
	/**
	 * \param b block to insert
	 * \param i insertion position. The block will be inserted before the position
	 * \return 0 if success
	 * \return -1
	 */
	int insert_before(Block* b, block_list_size_t i);

	/// Walk the message
	int foreach(const Record_Iterator_List& l);

private:
	const intifada::Record_Repository* record_repository_;

	std::string family_;

	block_list_t blocks_;

	bool delete_blocks_;

};
}

#endif // INTIFADA_BLOCK_HXX
