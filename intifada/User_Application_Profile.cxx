/*
   Copyright (C) 2009, 2011, 2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/User_Application_Profile.hxx>
#include <intifada/Field_Specification_Functor.hxx>
#include <intifada/Condition.hxx>
#include <intifada/Condition_List.hxx>

#include <intifada/Structure_Functor.hxx>

#include <intifada/Field_Specification_Manipulation.hxx>
#include <stdexcept>

intifada::User_Application_Profile::User_Application_Profile()
:inherited(),
 su_(),
 iu_(),
 conds_(),
 max_size_(0),
 reversefrnorder_(false)
{
	clog("Memory_Allocation") << "0x" << this << ":ctor User_Application_Profile" << std::endl;
	this->register_property("reversefrnorder",reversefrnorder_);
}

intifada::User_Application_Profile::User_Application_Profile(uint8_t max_size)
:inherited(),
 su_(),
 iu_(),
 conds_(),
 max_size_(max_size),
 reversefrnorder_(false)
{
	this->register_property("reversefrnorder",reversefrnorder_);
}

intifada::User_Application_Profile::~User_Application_Profile()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor User_Application_Profile" << std::endl;
	for(string_uap_iterator_t it = su_.begin();it != su_.end();++it){
		delete (*it).second;
	}
	for(int_uap_iterator_t it = iu_.begin();it != iu_.end();++it){
		delete (*it).second;
	}
}

int intifada::User_Application_Profile::insert(int frn,const Path_Name& item,Condition_List::size_type cond_idx)
{
	frn=this->get_uap_representation(frn);
	clog("User_Application_Profile") << "insert(" << frn << "," << item << "," << cond_idx << ")" << std::endl;
	// if(conds_==NULL) return -1;
	// insert insto string map
	// locate item
	string_uap_iterator_t sit=su_.find(item);
	int_conditionals_t *iv=NULL;

	if(sit==su_.end()){
		iv=new int_conditionals_t;
		string_uap_pair_t sp=su_.insert(std::make_pair(item,iv));
		if(sp.second!=true){
			return -1;
		}
	}else{
		iv=(*sit).second;
	}

	int_conditional_t ic(cond_idx,frn);
	iv->push_back(ic);

	// insert insto string map
	// locate frn
	int_uap_iterator_t iit=iu_.find(frn);
	string_conditionals_t *sv=NULL;

	if(iit==iu_.end()){
		sv=new string_conditionals_t;
		int_uap_pair_t ip=iu_.insert(std::make_pair(frn,sv));
		if(ip.second!=true){
			return -1;
		}
	}else{
		sv=(*iit).second;
	}

	string_conditional_t sc(cond_idx,item);
	sv->push_back(sc);
	return 0;
}

int intifada::User_Application_Profile::insert(int frn,const Path_Name& item)
{
	frn=this->get_uap_representation(frn);
	clog("User_Application_Profile") << "this=0x"<<this << " insert(" << frn << "," << item << ")" << std::endl;

	string_uap_iterator_t sit=su_.find(item);
	if(sit!=su_.end()){
		return -1;
	}

	// insert into string map
	int_conditionals_t *iv=new int_conditionals_t;
	int_conditional_t ic(frn);
	iv->push_back(ic);
	string_uap_pair_t sp=su_.insert(std::make_pair(item,iv));
	if(sp.second!=true){
		return -1;
	}
	int_uap_iterator_t iit=iu_.find(frn);
	if(iit!=iu_.end()){
		return -1;
	}

	// insert insto string map
	string_conditionals_t *sv=new string_conditionals_t;
	string_conditional_t sc(item);
	sv->push_back(sc);
	int_uap_pair_t ip=iu_.insert(std::make_pair(frn,sv));
	if(ip.second!=true){
		return -1;
	}
	return 0;
}

intifada::Condition_List::size_type intifada::User_Application_Profile::insert_cond(const Condition* c)
{
	Condition_List::size_type idx=0;
	Condition_List::size_type size=conds_.size();
	for(;idx<size;++idx){
		if(conds_[idx]==c)break;
	}
	if(idx==size)
		conds_.push_back(c);

	return idx;
}

int
intifada::User_Application_Profile::get(const Path_Name& item,const intifada::Data_Field* msg)const
{
	int ret=-1;
	string_uap_const_iterator_t sit=su_.find(item);
	if(sit==su_.end()){
		throw Description_Field_Unknow_Exception(item.get_full_name());
	}
	int_conditionals_t *iv=(*sit).second;
	for(int_conditionals_const_iterator_t it = iv->begin();it != iv->end();++it){
		const int_conditional_t &pc=(*it);

		const Condition* scond=NULL;

		if(pc.is_conditional()==true){
			scond=conds_[pc.get_condition()];
		}

		if(scond==NULL){
			ret=pc.get_id();
			break;
		}else{
			if((*scond)(msg)==true){
				ret=pc.get_id();
				break;
			}
		}
	}
	return ret;
}

intifada::Path_Name intifada::User_Application_Profile::get(int frn,const intifada::Data_Field* msg)const
{
	Path_Name ret;
	int_uap_const_iterator_t iit=iu_.find(frn);
	if(iit==iu_.end()){
		return ret;
	}
	string_conditionals_t *sv=(*iit).second;
	for(string_conditionals_const_iterator_t it = sv->begin();it != sv->end();++it){
		const string_conditional_t &pc=(*it);

		const Condition* scond=NULL;

		if(pc.is_conditional()==true){
			scond=conds_[pc.get_condition()];
		}

		if(scond==NULL){
			ret=pc.get_id();
			break;
		}else{
			if((*scond)(msg)==true){
				ret=pc.get_id();
				break;
			}
		}
	}
	return ret;
}

int intifada::User_Application_Profile::foreach(Structure_Functor *f)const
{
	// Knowing condition numbers, tracking all case, begining by the no condition case
	f->begin_uap(NULL);
	for(int_uap_const_iterator_t iit=iu_.begin();iit != iu_.end();++iit){
		const int &bit=(*iit).first;
		string_conditionals_t *sv=(*iit).second;

		for(
				string_conditionals_const_iterator_t it = sv->begin();
				it != sv->end();
				++it){
			const string_conditional_t &pc=(*it);
			if(pc.is_conditional()==false){
				int frn=Field_Specification_Manipulation::bit_to_frn(bit);
				if(reversefrnorder_==true){
					if(max_size_==0) throw std::logic_error("Error:User_Application_Profile: max_size not set");
					frn=Field_Specification_Manipulation::bit_to_frn(bit,max_size_);
				}
				f->frn(frn,pc.get_id());
			}
		}
	}
	f->end_uap();
	// Now, pass through all known conditions
	//if(conds_!=NULL){
	for(Condition_List::size_type idx=0;idx < conds_.size();++idx){
		f->begin_uap(conds_[idx]);
		for(int_uap_const_iterator_t iit=iu_.begin();iit != iu_.end();++iit){
			const int &bit=(*iit).first;
			string_conditionals_t *sv=(*iit).second;
			for(
					string_conditionals_const_iterator_t it = sv->begin();
					it != sv->end();
					++it){
				const string_conditional_t &pc=(*it);
				if(pc.is_conditional()!=false){
					if(pc.get_condition()==idx){
						int frn=Field_Specification_Manipulation::bit_to_frn(bit);
						if(reversefrnorder_==true){
							if(max_size_==0) throw std::logic_error("Error:User_Application_Profile: max_size not set");
							frn=Field_Specification_Manipulation::bit_to_frn(bit,max_size_);
						}
						f->frn(frn,pc.get_id());
					}
				}
			}
		}
		f->end_uap();
	}
	//}
	return 0;
}

int intifada::User_Application_Profile::foreach(Field_Specification_Functor& f)const
{
	// Knowing condition numbers, tracking all case, begining by the no condition case
	for(int_uap_const_iterator_t iit=iu_.begin();iit != iu_.end();++iit){
		//const int &bit=(*iit).first;
		string_conditionals_t *sv=(*iit).second;

		for(
				string_conditionals_const_iterator_t it = sv->begin();
				it != sv->end();
				++it){
			const string_conditional_t &pc=(*it);
			if(pc.is_conditional()==false){
				f(pc.get_id());
			}
		}
	}
	// Now, pass through all known conditions
	//if(conds_!=NULL){
	for(Condition_List::size_type idx=0;idx < conds_.size();++idx){
		for(int_uap_const_iterator_t iit=iu_.begin();iit != iu_.end();++iit){
			//const int &bit=(*iit).first;
			string_conditionals_t *sv=(*iit).second;
			for(
					string_conditionals_const_iterator_t it = sv->begin();
					it != sv->end();
					++it){
				const string_conditional_t &pc=(*it);
				if(pc.is_conditional()!=false){
					if(pc.get_condition()==idx){
						f(pc.get_id());
					}
				}
			}
		}
	}
	//}
	return 0;
}

intifada::User_Application_Profile::User_Application_Profile(const User_Application_Profile& rhs):
		  inherited(rhs),
		  su_(),
		  iu_(),
		  conds_(),
		  max_size_(rhs.max_size_),
		  reversefrnorder_(rhs.reversefrnorder_)
{
	this->relink_property("reversefrnorder",reversefrnorder_);
	copy(rhs);
}

intifada::User_Application_Profile&
intifada::User_Application_Profile::operator=(const User_Application_Profile& rhs)
{
	if(this != &rhs){
		inherited::operator=(rhs);
		max_size_=rhs.max_size_;
		this->relink_property("reversefrnorder",reversefrnorder_);
		copy(rhs);
	}
	return *this;
}

void
intifada::User_Application_Profile::copy(const User_Application_Profile& rhs)
{
	for(Condition_List::size_type idx=0;idx<rhs.conds_.size();++idx){
		const Condition* t=rhs.conds_[idx]->clone();
		conds_.push_back(t);
	}

	// copying su_, manual copy must be done because of pointers (int_conditionals_t)
	for(string_uap_const_iterator_t it=rhs.su_.begin();it!=rhs.su_.end();++it){
		const Path_Name& rhs_first=(*it).first;
		const int_conditionals_t *rhs_second=(*it).second;
		int_conditionals_t *second= new int_conditionals_t;
		(*second)=(*rhs_second);
		su_.insert(std::make_pair(rhs_first,second));
	}
	for(int_uap_const_iterator_t it=rhs.iu_.begin();it!=rhs.iu_.end();++it){
		const int rhs_first=(*it).first;
		const string_conditionals_t *rhs_second=(*it).second;
		string_conditionals_t *second= new string_conditionals_t;
		(*second)=(*rhs_second);
		iu_.insert(std::make_pair(rhs_first,second));
	}
	return;
}

uint8_t
intifada::User_Application_Profile::get_uap_representation(uint8_t er)const
{
	uint8_t byte;
	uint8_t offset;
	if(reversefrnorder_==true){
		//if(compound_==true){
		byte = Field_Specification_Manipulation::sf_to_byte(er,max_size_);
		offset = Field_Specification_Manipulation::sf_to_offset(er);
	}else{
		byte = Field_Specification_Manipulation::frn_to_byte(er);
		offset = Field_Specification_Manipulation::frn_to_offset(er);
	}
	return Field_Specification_Manipulation::offset_bit_to_bit(byte,offset);
}

