/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Data_Field.hxx>
#include <intifada/Structure_Functor.hxx>
#include <sstream>

intifada::Structure_Functor_Updator::Structure_Functor_Updator(Structure_Functor& f)
:f_(f)
{}

int intifada::Structure_Functor_Updator::operator()(const Path_Name &n,const Data_Field_Characteristics &c,const Data_Field_Characteristics_List::part_size_type& part)
{
	const Type* t = c.get_type();
	f_.part(*t,n,c.shift(),c.size(),part);
	return 0;
}

intifada::Data_Field::Functor_Selector::Functor_Selector()
:items_()
{}

intifada::Data_Field::Functor_Selector::~Functor_Selector(){}

void intifada::Data_Field::Functor_Selector::apply_on(const Path_Name& item)
{
	items_.insert(item);
}

bool intifada::Data_Field::Functor_Selector::is_selected(const Path_Name& item)const
{
	bool found=true;
	if(items_.empty()!=true)
	{
		path_name_set_const_iterator_type it=items_.find(item);
		if(it==items_.end())
		{
			found=false;
		}
	}
	return found;
}

intifada::Data_Field::Functor::Functor():inherited(){}
intifada::Data_Field::Functor::~Functor(){}

intifada::Data_Field::Const_Functor::Const_Functor():inherited(){}
intifada::Data_Field::Const_Functor::~Const_Functor(){}

int intifada::Data_Field::Dump::operator()(const Stream::size_type&,const Stream::size_type&,const Path_Name&,const Type&)
{
	return 0;
}


intifada::Data_Field::Data_Field():
										  inherited()
,internal_structure_()
,force_presence_(false)
{
	this->register_property("force_presence",force_presence_);
}

intifada::Data_Field::Data_Field(const Data_Field& rhs):
										  inherited(rhs)
,internal_structure_(rhs.internal_structure_)
,force_presence_(rhs.force_presence_)
{
	this->relink_property("force_presence",force_presence_);
}

bool intifada::Data_Field::get_presence(const Path_Name& name)const
{
	std::cerr << "10-" << name.get_full_name() << std::endl;
	throw Description_Field_Unknow_Exception(name);
}

void intifada::Data_Field::set_presence(const Path_Name& name,bool)
{
	std::cerr << "11-" << name.get_full_name() << std::endl;
	throw Description_Field_Unknow_Exception(name);
}

const intifada::Type& intifada::Data_Field::operator()(const Path_Name& name)const
{
	return this->get_value(name);
}

intifada::Type& intifada::Data_Field::operator()(const Path_Name& name)
{
	return this->get_value(name);
}

intifada::Data_Field::~Data_Field(){}

int
intifada::Data_Field::foreach_structure(Structure_Functor *f)const
{
	Structure_Functor_Updator link(*f);
	internal_structure_.foreach_structure(link);
	return 0;
}

uint8_t
intifada::Data_Field::get_idx(std::string& idx)const
{
	std::istringstream i(idx);
	int to_int;
	i >> to_int;
	return to_int;
}

intifada::Data_Field_Characteristics*
intifada::Data_Field::get_new_structure_description()
{
	return internal_structure_.get_new_description();
}

void intifada::Data_Field::release_structure_description(Data_Field_Characteristics *c)
{
	internal_structure_.release_description(c);
}

void
intifada::Data_Field::insert_to_structure(const Path_Name& name,intifada::Data_Field_Characteristics* item_desc,const intifada::Data_Field_Characteristics_List::part_size_type& part)
{
	internal_structure_.insert(name,item_desc,part);
	return;
}

intifada::Data_Field_Characteristics&
intifada::Data_Field::get_structure_characteristics(const Path_Name& name)const
{
	return internal_structure_.get_characteristics(name);
}

intifada::Data_Field_Characteristics&
intifada::Data_Field::get_structure_characteristics(const Path_Name& name,const Data_Field_Characteristics_List::part_size_type& part)const
{
	return internal_structure_.get_characteristics(name,part);
}


const intifada::Data_Field_Characteristics_List::part_size_type& intifada::Data_Field::get_structure_part(const Path_Name& name)const
{
	return internal_structure_.get_part(name);
}

void intifada::Data_Field::set_repetitive_characteristics()
{
	internal_structure_.set_repetitive();
}

void intifada::Data_Field::set_extended_characteristics()
{
	internal_structure_.set_extended();
}

int intifada::Data_Field::push_back_characteristics()
{
	return internal_structure_.push_back();
}

int intifada::Data_Field::foreach_structure(Data_Field_Characteristics_List::Functor&dflit)const
{
	return internal_structure_.foreach_structure(dflit);
}

int intifada::Data_Field::foreach(Data_Field_Characteristics_List::Functor&dflit)const
{
	return internal_structure_.foreach(dflit);
}
