/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_TYPE_REPOSITORY_HXX
# define INTIFADA_TYPE_REPOSITORY_HXX

# include <intifada/Map.hxx>
# include <string>
# include <intifada/Type.hxx>
namespace intifada
{
  class Type_Repository
  {
  public:

    /// Iterator on each type repository node
    class Type_Repository_Iterator
    {
    public:
      Type_Repository_Iterator();

      virtual ~Type_Repository_Iterator();
    public:

      /// Default iterator
      /**
       * \param t type name (filled internaly)
       * \param pt type reference
       *
       * Default behaviour: print type name on cout
       */
      virtual int operator()(const std::string& t,const Type&);
    };
  private:
    typedef Unordered_Map<std::string,Type*> type_map_t;
    typedef type_map_t::iterator type_map_iterator_t;
    typedef type_map_t::const_iterator type_map_const_iterator_t;
    typedef type_map_t::pair type_map_pair_t;
  public:
    Type_Repository();

    virtual ~Type_Repository();

    int release();

    template <typename T>
    int register_type();

    const Type* get(const std::string& n)const;

    static Type_Repository* instance();

    /// Apply iterator on each node
    /**
     * \param it called iterator.
     * \return 0;
     *
     * it operator will be called for each node with associated name
     */
    int foreach(Type_Repository_Iterator& it)const;

    /// Apply defaul iterator on each node
    /**
     * \param it called iterator.
     * \return 0;
     *
     * it operator will be called for each node with associated name
     */
    int foreach()const;

  private:
    static Type_Repository *instance_;

    type_map_t types_;
  };
}

# include <intifada/Type_Repository.txx>

#endif // INTIFADA_TYPE_REPOSITORY_HXX
