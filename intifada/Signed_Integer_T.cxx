/*
   Copyright (C) 2009,2011,2014  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>

template <typename T_>
intifada::Signed_Integer_T<T_>::Signed_Integer_T(const std::string& id)
:inherited(id){}

template <typename T_>
intifada::Signed_Integer_T<T_>::Signed_Integer_T(const std::string& id,T_ i)
:inherited(id,i){}

template <typename T_>
T_
intifada::Signed_Integer_T<T_>::stream_to_internal(const T_&v,int size)const
{

  // get data from v adapt it to match size and return result
  T_ ret=v;
  int internal_size=sizeof(T_)*8;
  if(internal_size!=size){
    move_sign(size,v,internal_size,ret);
  }
  return ret;
}

template <typename T_>
T_
intifada::Signed_Integer_T<T_>::internal_to_stream(const T_&v,int size)const
{
  T_ ret=v;
  int internal_size=sizeof(T_)*8;
  if(internal_size!=size){
    move_sign(internal_size,v,size,ret);
  }
  return ret;
}

template <typename T_>
template <typename T>
T
intifada::Signed_Integer_T<T_>::mask(int size)const
{
  //T ret=std::pow(2,size+1);
  //std::cout << "size=" << size << " " << "ret 1 " << (unsigned int) ret << std::endl;
  T ret=0;
  for(int i=0;i<size;++i){ret|=(1<<i);}
  //std::cout << "ret 2 " << (unsigned int)ret << std::endl;
  return ret;
}

template <typename T_>
template<typename S,typename T>
void
intifada::Signed_Integer_T<T_>::move_sign(int initial_size, const S& a,int target_size,T& target)const
{
  S na=a;

  bool neg=false;
  if( (a & (1 << (initial_size-1)))!=0){
    // neg
    int initial_mask=mask<S>(initial_size);
    neg=true;
    na-=1;
    na ^= initial_mask;
  }
  target=0;
  int target_mask=mask<T>(target_size);
  target=na&target_mask;
  if(neg!=false){
    target^=target_mask;
    target+=1;
  }
}
