/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Logger.hxx>
#include <cassert>

intifada::Configuration_XML_Reader::Configuration_XML_Reader(const std::string& xmlfile,const std::string& dtdfile):
xml_file_(xmlfile)
,dtd_file_(dtdfile)
,reference_()
,parser_(NULL)
{
}

intifada::Configuration_XML_Reader::~Configuration_XML_Reader()
{
	this->release();
}

int intifada::Configuration_XML_Reader::open()
{

	int options=0;

	//      XML_PARSE_DTDATTR |     /* default DTD attributes */
	//    XML_PARSE_NOENT |        /* substitute entities */
	//  XML_PARSE_DTDVALID;

	parser_ = xmlReadFile(xml_file_.c_str(), NULL,options);
	int valid=-1;
	if (parser_ != NULL) {
		xmlDtdPtr dtd = xmlParseDTD(NULL,BAD_CAST dtd_file_.c_str());
		if(dtd!=NULL)
		{
			xmlValidCtxtPtr cvp = xmlNewValidCtxt();
			valid=xmlValidateDtd(cvp, parser_, dtd);
			xmlFreeValidCtxt(cvp);
			if(valid==1)
			{
				valid=0;
			}
		}
	}
	return valid;
}

void intifada::Configuration_XML_Reader::release()
{
	if(parser_!=NULL)
	{
		xmlFreeDoc(parser_);
		xmlCleanupParser();
		parser_=NULL;
	}
	return;
}



int intifada::Configuration_XML_Reader::parse(Record_Repository*rep)
{
	this->set_repository(rep);
	intifada::Record*rec;
	this->begin_configuration();
	//int parsed= this->parse(rec);

	int parsed=-1;
	if(parser_!=NULL)
	{
		xmlNodePtr root_node=xmlDocGetRootElement(parser_);
		parsed=process_node(rec,root_node);

	}
	this->end_configuration();
	return parsed;
}

int intifada::Configuration_XML_Reader::process_node(intifada::Record*&r,xmlNodePtr a_node)
{
	xmlNode *cur_node = NULL;
	for (cur_node = a_node; cur_node; cur_node = cur_node->next)
	{


		std::string name="--";
		const xmlChar *xmlname;
		xmlname = cur_node->name;//xmlTextReaderConstName(parser_);
		if (xmlname != NULL){
			name=std::string(reinterpret_cast<const char *>(xmlname));
		}
		int type = cur_node->type;//xmlTextReaderNodeType(parser_);
		clog("Configuration_XML_Reader") << "process node <"<<name<< "> (type="<<type<<") line="<<cur_node->line<<std::endl;
		if(name=="intifada"){
		}else if(name=="types"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string from=get_attribute(cur_node,"from");
				int status=this->register_types(from);
				if(status!=0)
				{
					throw intifada::Parsing_XML_Dynamic_Type_Exception("Disabled functionality");
				}
			}
		}else if(name=="family"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string name=get_attribute(cur_node,"name");
				this->create_family(name);
			}
		}else if(name=="id"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string name=get_attribute(cur_node,"name");
				int status=this->create_family_id(name);
				if(status!=0){
					throw intifada::Parsing_XML_Family_Duplicate_Exception(name);
				}
			}
		}else if(name=="record"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string name=get_attribute(cur_node,"name");
				std::string cat=get_attribute(cur_node,"cat");
				std::string id=get_attribute(cur_node,"id");
				uint8_t category=to_integer<uint8_t>(cat);
				this->create_record(r,name,id,category);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				end_record();
			}
		}else if(name=="uap"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string func=get_attribute(cur_node,"if");
				std::string result=get_attribute(cur_node,"value");
				this->create_uap(func,result);
			}
		}else if(name=="frn"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string sfrn=get_attribute(cur_node,"num");
				std::string item=get_attribute(cur_node,"reference");

				reference_=item;

				uint8_t frn=to_integer<uint8_t>(sfrn);
				this->create_frn(r,frn,reference_);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				// reference_.pop_back();
			}
		}else if(name=="item"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string name=get_attribute(cur_node,"name");
				// reference = item name (eg:010)
				std::string reference=get_attribute(cur_node,"reference");

				// SPI: test if no path storing is good
				// reference_.push_back(reference);
				reference_=reference;

				std::string forcepresence=get_attribute(cur_node,"forcepresence");
				clog("Configuration_XML_Reader") << "item:"<<reference_<<std::endl;
				this->create_item(name,reference_,forcepresence);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				//reference_.pop_back();
			}
		}else if(name=="fixed"){
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("datatype");
				this->create_fixed(r);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				this->pop_element();
			}
		}else if(name=="extended"){
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("datatype");
				this->create_extended(r);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				this->pop_element();
			}
		}
#if 0
		else if(name=="explicit"){
			// D:MISE AU POINT
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("datatype");
				Record *c=this->create_explicit(r);
				this->process_node(c,cur_node->children);//this->parse(c);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				this->pop_element();
			}
			// F:MISE AU POINT
		}
#endif
		else if(name=="repetitive"){
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("datatype");

				this->create_repetitive(r);
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				this->pop_element();
			}
		}else if(name=="compound"){
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("datatype");
				std::string l=get_attribute(cur_node,"length");
				uint8_t length=to_integer<uint8_t>(l);
				Record *c=this->create_compound(r,length);
				this->process_node(c,cur_node->children);
			}else if(type==XML_READER_TYPE_END_ELEMENT) {
				this->pop_element();
				end_record();
				return /*type*/0;
			}
		}else if(name=="part"){
			if(type==XML_READER_TYPE_ELEMENT){
				this->push_element("part");
				// name = element reference
				std::string name=get_attribute(cur_node,"name");

				//reference_.push_back(name);
				reference_=name;

				std::string type=get_attribute(cur_node,"type");

				// used only for extended non repetitive fields (byte=-1 if not)
				std::string b=get_attribute(cur_node,"byte");
				std::string st=get_attribute(cur_node,"start");
				std::string si=get_attribute(cur_node,"size");
				int byte=to_integer<int8_t>(b);
				int start=to_integer<uint8_t>(st);
				int size=to_integer<uint8_t>(si);
				int status=this->handle_parts(name,type,byte,start,size);
				if(status==-1){
					std::cerr << "cannot handle part name=" << reference_ << " type="<< type << std::endl;
					exit(EXIT_FAILURE);
				}
			}else if(type==XML_READER_TYPE_END_ELEMENT){
				this->pop_element();
				//reference_.pop_back();
			}
		}else if(name=="converter"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string type=get_attribute(cur_node,"type");

				this->set_item_converter(type);

			}else if(type==XML_READER_TYPE_END_ELEMENT){}
		}else if(name=="property"){
			if(type==XML_READER_TYPE_ELEMENT){
				std::string p=get_text(cur_node);
				std::string::size_type eqpos=p.find_first_of('=');
				std::string prop=p;
				std::string val;
				if(eqpos!=std::string::npos){
					prop=p.substr(0,eqpos);
					val=p.substr(eqpos+1);
				}
				if(this->empty_element()!=true){
					if(this->top_element()=="part"){
						this->set_type_property(prop,val);
					}else if(this->top_element()=="datatype"){
						this->set_item_property(prop,val);
					}
				}
			}
		}
		this->process_node(r,cur_node->children);
	}
	return 0;
}

/// attribute acquisition
std::string intifada::Configuration_XML_Reader::get_attribute(xmlNodePtr node,const std::string& an)
{
	std::string ret="";
	xmlChar *attribute_value;
	attribute_value=xmlGetProp(node,reinterpret_cast<const xmlChar*>(an.c_str()));//xmlTextReaderGetAttribute(parser_,BAD_CAST an.c_str());

	if(attribute_value!=NULL){
		ret=std::string((const char *)attribute_value);
		xmlFree(attribute_value);
	}
	clog("Configuration_XML_Reader") << "get_attribute("<< an << "):<"<< ret << ">" << std::endl;
	return ret;
}

std::string intifada::Configuration_XML_Reader::get_text(xmlNodePtr node)
{
	assert(node!=NULL);
	std::string ret="";
	xmlChar *value=NULL;
	xmlNodePtr child_node=node;
	assert(child_node!=NULL);
	value = xmlNodeGetContent(child_node);
	if (value != NULL){
		ret=reinterpret_cast<const char*>(value);
		xmlFree(value);
	}
	clog("Configuration_XML_Reader") << "get_text("<< (char *)node->name << "):<"<< ret << ">" << std::endl;
	return ret;
}

void intifada::Configuration_XML_Reader::next_node()
{
	//current_node_=current_node_->next;
}

