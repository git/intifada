/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_REPETITIVE_LENGTH_DATA_HXX
# define INTIFADA_REPETITIVE_LENGTH_DATA_HXX

# include <intifada/Data_Field.hxx>
# include <intifada/Data_Field_Characteristics.hxx>

namespace intifada
{
/// Repetitive data field
/**
 * variable length field begining with a first byte
 * indicating the presence of consecutive sub fields
 * each of the same predetermined size
 *
 * Properties:
 * - length:one representation of field length
 * - indicatorlength:write field size instead repetitive factor
 */
class Repetitive_Length_Data_Field:public Data_Field
{
public:
	template <typename F>
	class Data_Functor_T : public Data_Field_Characteristics_List::Functor
	{
	public:
		Data_Functor_T(const Path_Name& n,F &it,Stream& byte_stream,const Property_Uint8& part_size);
		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		const Path_Name& n_;
		F &it_;
		intifada::Stream::size_type stream_pos_;
		intifada::Stream::size_type stream_size_;

		Stream& byte_stream_;
		const Property_Uint8& part_size_;

	};

	class Update_Stream_Functor : public Data_Field_Characteristics_List::Functor
	{
	public:
		Update_Stream_Functor(Stream& byte_stream,const Property_Uint8& part_size);

		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		Stream& byte_stream_;
		const Property_Uint8& part_size_;
	};

	public:
	typedef Data_Field inherited;
	typedef Data_Functor_T<Data_Field::Functor> Data_Functor;
	typedef Data_Functor_T<Data_Field::Const_Functor> Data_Const_Functor;
	public:

	/// Ctor
	/**
	 * \param size sub field size
	 */
	 Repetitive_Length_Data_Field();

	private:
	Repetitive_Length_Data_Field(const Repetitive_Length_Data_Field &rhs);

	public:

	virtual ~Repetitive_Length_Data_Field();

	/// Byte stream acquisition
	/**
	 * Handle acquisition of asterix binary stream
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return readed total bytes
	 * \throw Parsing_Input_Length_Exception if bytes to read exceed m
	 * \throw Description_Field_Unknow_Exception if an unknow field is encountered
	 */
	virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

	/// Byte stream restitution
	/**
	 * Handle byte stream restitution.
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return writed total bytes
	 * \throw Parsing_Output_Length_Exception if bytes to read exceed m
	 */
	virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

	/// get field size
	/**
	 * \return size of field
	 */
	virtual Stream::size_type size()const;

	/// get part size
	/**
	 * \return part size
	 */
	Stream::size_type get_part_size()const;

	/// association between a name, a repetitive field type and a position in a stream
	/**
	 * \param name data name
	 * \param type data type
	 * \param start in the record, the begining of the data (begin at one at the lsb)
	 * \param size data size
	 */
	Data_Field_Characteristics* insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size);

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual const Type& get_value(const Path_Name& name)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual Type& get_value(const Path_Name& name);

#if 0
	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \param ret concrete target variable
	 * \remarks use it when you know the target type
	 */
	virtual void get_value(const Path_Name& name,intifada::Type& ret)const;


	/// set value to asterix message
	/**
	 * \param name name of data item to get
	 * \param ret source variable
	 */
	virtual void set_value(const Path_Name& name,const intifada::Type& ret);
#endif
	/// Clone the data field
	/**
	 * Cloning is meaning only structure copy
	 * \return a freshly allocated Data_Field
	 */
	virtual Data_Field *clone()const;

	/// walk throught the record
	/**
	 * Call Data_Field_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Functor &it,const Path_Name& path);

	/// walk throught the record
	/**
	 * Call Data_Field_Const_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a su item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Const_Functor &,const Path_Name& path)const;

	/// get type from asterix message
	/**
	 * \param name name of data item to get type
	 * \return a reference on type
	 * See Type for details
	 *
	 */
	virtual const Type& get_type(const Path_Name& name)const;

	private:
	/// Return the lsb position of the part
	/**
	 * Convert part representation in vector position (byte offset in the vector)
	 *
	 * \param p part to convert (0 is the primary part)
	 * \return the begining of p (return the low significant byte of the associated part)
	 */
	Stream::size_type get_lsb_position(Stream::size_type p)const;

	/// Return the msb position of the part
	/**
	 * Convert part representation in vector position (byte offset in the vector)
	 *
	 * \param p part to convert (0 is the primary part)
	 * \return the begining of p (return the most significant byte of the associated part)
	 */
	Stream::size_type get_msb_position(Stream::size_type p)const;

	/// adjust stream size to content
	void update()const;

	/// nullity indicator
	/**
	 * \param p pointed part
	 * \return true if part isn't null
	 *
	 * \remarks first part is indexed by 1
	 */
	bool is_part_null(uint8_t part)const;

	/// translate offset vector in a part
	/**
	 * \param o vector position (begining by 0)
	 * \return associated part (begining by 0)
	 */
	uint8_t get_part(uint8_t o) const;

	private:
	/// Update stream with cached values
	/**
	 * Report all cached values to the stream
	 */
	void update_stream()const;

	private:

	// part size
	Property_Uint8 size_;
	mutable uint8_t rep_;

	mutable Stream byte_stream_;

	Property_Bool indicator_length_;
};

template<typename F>
Repetitive_Length_Data_Field::Data_Functor_T<F>::Data_Functor_T(
		const Path_Name& n
		,F &it
		,Stream& byte_stream,const Property_Uint8& part_size):
		n_(n)
		,it_(it)
		,byte_stream_(byte_stream)
		,part_size_(part_size)
		{
	byte_stream.get_stream_position(stream_pos_,stream_size_);
		}
template <typename F>
int Repetitive_Length_Data_Field::Data_Functor_T<F>::operator()(
		const Path_Name &name
		,const Data_Field_Characteristics &t
		,const Data_Field_Characteristics_List::part_size_type&part)
{
	Type *v=t.get_type();
	if(t.is_cached()!=true)
	{
		Stream::size_type lsb_position = (part+1)*part_size_-1;
		Stream::const_reverse_iterator it = byte_stream_.rend() - lsb_position - 1;
		v->set_from_stream(it,t.shift(),t.size());
		t.set_cached();
		v->reset_changed_state();
	}

	Path_Name full_name=n_+name;
	it_(stream_pos_,stream_size_,full_name,*v);
	return 0;
}
}
#endif // INTIFADA_REPETITIVE_LENGTH_DATA_HXX
