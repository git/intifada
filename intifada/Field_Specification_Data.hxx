/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASTERIX_FIELD_SPECIFICATION_DATA_HXX
# define ASTERIX_FIELD_SPECIFICATION_DATA_HXX

# include <intifada/Data.hxx>

# include <intifada/User_Application_Profile.hxx>
# include <intifada/Field_Specification_Manipulation.hxx>

# include <intifada/Condition_List.hxx>

# include <intifada/Exception.hxx>

# include <intifada/Path_Name.hxx>

namespace intifada
  {
    class Condition;
    class Data_Field;
    class Structure_Iterator;
    class Field_Specification_Functor;
    /// Fspec class
    /**
     * This class handle all field specifications properties. Some words must be say about
     * meaning of counting bytes.
     *
     * frn in an asterix message is counting starting by 1. Formula to transform to byte is:
     * Getting concernet byte (begining by 1):
     * (frn-1)/7 + 1
     * Getting concerned bit offset in byte (lsb is 1):
     * offset=frn-1
     * offset %= 7
     * offset = 7 - offset
     *
     * If frn is in a compound message is counting from last fspec byte to the begining.
     * to get byte:
     * size - (sf-1)/8
     * Getting concerned bit offset in byte (lsb is 1):
     * (sf-1) % 8 + 1
     *
     * So what. The user association profile must use only one representation in a compound
     * representation or an asterix message representation
     *
     * Getting frn description from byte/bit representation
     *
     * Field specification data could accept following flags via setf method:
     * -MAXSIZE:set_stream always create a buffer of the maximum size
     * -HIDEFX:set_stream always set to 0 FX bits
     * -REVERSEFRNORDER:reverse frn order
     */
    class Field_Specification_Data : public Data
      {
    public:
      typedef Data inherited;
      /// Ctor
      Field_Specification_Data();

      /// Ctor
      /**
       * \param name record name
       */
      Field_Specification_Data(const Path_Name& name);

      /// Ctor
      /**
       * \param max_size max size of fspec
       * \param compound true:bits aren't frn
       * \param compound false:bits ARE frn
       */
      Field_Specification_Data(uint8_t max_size,const Path_Name& compound_name);


      /// Dtor
      virtual ~Field_Specification_Data();

      /// add flags
      /**
       * Virtual to handling automatic set flags
       */
      virtual void set_property(const std::string& prop,const std::string& val);

      /// get field size
      /**
       * \return size of field
       */
      virtual Stream::size_type size()const;

      /// get maximum fspec size
      /**
       * \return maximum fspec size
       */
      uint8_t get_max_size()const;

      /// create an UAP with a condition
      /**
       * \param pos position in field specification (msb of last field specification byte is 1)
       * \param name subfield name
       * \param cond existance condition
       *
       * \return 0 if insertion is successfull
       * \return -1 if insertion fail
       */
      int insert(
          uint8_t pos
          ,const Path_Name& name
          ,const Condition* cond);

      /// create an UAP without any condition
      /**
       * \param pos position in field specification (msb of last field specification byte is 1)
       * \param name subfield name
       *
       * \return 0 if insertion is successfull
       * \return -1 if insertion fail
       */
      int insert(uint8_t pos, const Path_Name& name);

      /// Byte stream acquisition
      /**
       * Handle acquisition of asterix binary stream
       * \param s stream pointer
       * \param o stream offset
       * \param m maximum stream size
       * \return readed total bytes
       * \throw Parsing_Input_Length_Exception if bytes to read exceed m
       * \throw Description_Field_Unknow_Exception if an unknow field is encountered
       */
      virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

      /// Byte stream restitution
      /**
       * Handle byte stream restitution.
       * \param s stream pointer
       * \param o stream offset
       * \param m maximum stream size
       * \return writed total bytes
       * \throw Parsing_Output_Length_Exception if bytes to read exceed m
       */
      virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

      /// Field reference number modificator
      /**
       * set (or unset) a field reference number
       * \param f field reference number
       * \param s true if the field reference number must be set, false elsewhere
       */
      void set_frn(uint8_t f,bool s);

      /// Field reference number accessor
      /**
       * get a un field reference number
       * \param f field reference number
       */
      bool get_frn(uint8_t f) const;

      /// Field reference number accessor
      /**
       * get a un field reference number
       * \param f field reference
       */
      int get_frn(const std::string& item,const intifada::Data_Field* msg)const;

      /// get item presence
      /**
       * \param item item name
       * \param msg concerned message (needed if conditional frn)
       * \return true if the associated bit is present
       * \return false if the associated bit isn't present
       */
      bool get_presence(const std::string& item,const intifada::Data_Field* msg)const;

      /// set (or reset) item presence
      /**
       * \param item item name
       * \param msg concerned message (needed if conditional frn)
       * \return -1 if item doesn't exist
       * \return 0 if nothing bad
       */
      int set_presence(const std::string& item,bool p,const intifada::Data_Field* msg);

      /// Copy field specification data structure
      /**
       * \param rhs object to clone in this
       * \return this as a clone
       */
      Field_Specification_Data& clone(const Field_Specification_Data& rhs);

      /// Register condition
      /**
       * \param name condition name
       * \param cond associated name cond
       */
      // void register_condition(const std::string& name,Data_Field_Condition* cond);

    private:

      /// translate offset vector in a part
      /**
       * \param o vector position (begining by 0)
       * \return associated part (begining by 0)
       */
      uint8_t get_part(uint8_t o) const;

      /// adjust stream size to content
      /**
       *
       */
      void update()const;

      /// nullity indicator
      /**
       * \param p pointed part
       * \return true if part isn't null
       *
       * \remarks first part is indexed by 1
       */
      bool is_part_null(uint8_t part)const;

      /// Translate external frn to internal uap representation
      /**
       * As a compound mode or a message mode reference presence bits as
       * differents ways, this method return same numerotation either the mode is.
       * \param er external representation (frn or sf)
       * \return uap representation
       */
      uint8_t get_uap_representation(uint8_t er)const;

    public:
      int foreach(Field_Specification_Functor& it,const intifada::Data_Field* msg,const Path_Name&sel=Path_Name())const;

      int foreach_structure(Structure_Functor *f)const;

      int foreach_structure(Field_Specification_Functor& it)const;

    private:
      Field_Specification_Data(const Field_Specification_Data& rhs);
      Field_Specification_Data& operator=(const Field_Specification_Data& rhs);
    private:
      mutable Stream byte_stream_;

      // size between two FX
      uint8_t size_;

      uint8_t max_size_;

      Path_Name compound_name_;

      User_Application_Profile* uap_;
      bool release_uap_;

      Property_Bool reversefrnorder_;
      Property_Bool forceitempresence_;
      Property_Bool hidefx_;
      Property_Bool maxsize_;
      };
  }

#endif // ASTERIX_FIELD_SPECIFICATION_DATA_HXX
