/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Block.hxx>

#include <intifada/Stream.hxx>
#include <intifada/Record.hxx>
#include <intifada/Record_Repository.hxx>

#include <intifada/Record_Iterator_List.hxx>

#include <cassert>

intifada::Block::Block(const intifada::Record_Repository* rep, const std::string& family)
:record_repository_(rep),
 family_(family),
 category_(0),
 record_reference_(NULL),
 records_(),
 delete_records_(true)
{
	clog("Memory_Allocation") << "allocate Block for " << family << std::endl;
	// Deferred record reference as we don't know yet the block category.

}

intifada::Block::Block(const intifada::Record_Repository* rep, const std::string& family,uint8_t c)
:record_repository_(rep),
 family_(family),
 category_(c),
 record_reference_(NULL),
 records_(),
 delete_records_(true)
{
	clog("Memory_Allocation") << "allocate Block for " << family << ":" << (int)c << std::endl;
	set_record_reference(-1,category_);
}

void intifada::Block::release_on_clear(bool active)
{
	delete_records_=active;
}

intifada::Block::~Block()
{
	clog("Memory_Allocation") << "release Block" << std::endl;
	this->clear();
}

uint8_t
intifada::Block::category()const
{
	return category_;
}

void
intifada::Block::clear()
{
	if(delete_records_==true)
	{
		for(record_list_const_iterator_t it = records_.begin(); it != records_.end();++it)
		{
			delete (*it);
		}
	}
	records_.clear();

	// Don't delete it as it only be a pointer reference on record repository
	// record_reference_=NULL;
}

intifada::Stream::size_type
intifada::Block::size()const
{
	Stream::size_type s=3;
	Stream::size_type partial_size=0;
	for(record_list_const_iterator_t it = records_.begin(); it != records_.end();++it){
		Record*r=(*it);
		partial_size=r->size();
		s+=partial_size;
		clog("size debug") << "\t\trecord size=" << partial_size << " (" << s << ")" << std::endl;
	}
	return s;
}

intifada::Record* intifada::Block::get_new_record()const
{
	intifada::Record* cloned_ret=NULL;
	if(record_reference_!=NULL){

		// Return a freshly new allocated record
		cloned_ret=new Record;
		cloned_ret->clone(record_reference_);
	}
	return cloned_ret;
}

intifada::Stream::size_type intifada::Block::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	clog("size debug") << "Block::set_stream(0x"<<s<<","<<o<<","<< m << ")" << std::endl;
	// o is the offset from the start of the stream s. m is the total stream size

	// structure block is following: CAT (1byte) LEN  (2bytes)
	category_=s[o];
	set_record_reference(o,category_);

	// total block size (including category and len
	uint16_t block_size=(s[1+o] << 8) | (s[2+o]);

	Stream::size_type stream_pos=3+o;
	unsigned int block_end_size=block_size+o;
	uint16_t block_writed_size=3;
	// Check if expected block size exceed message size
	if(block_end_size>m)
	{
		throw Parsing_Input_Length_Exception(stream_pos,block_end_size,m);
	}
	while(stream_pos<block_end_size)
	{
		clog("size debug") << "Block::set_stream:block end="<<block_end_size << std::endl;
		Record *r= get_new_record();
		assert(r!=NULL);
		int size=r->set_stream(s,stream_pos,block_end_size);
		block_writed_size+=size;
		stream_pos+=size;
		clog("size debug") << "Block::set_stream:new pos="<<stream_pos << std::endl;
		this->push_back(r);
	}
	return block_writed_size;
}

intifada::Stream::size_type intifada::Block::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{

	if(m==0){
		throw Parsing_Output_Length_Exception(1,0);
	}
	Stream::size_type block_size=0;
	s[o]=category_;

	if(m<=3){
		throw Parsing_Output_Length_Exception(3,m);
	}

	Stream::size_type size;
	block_size+=3;
	for(record_list_const_iterator_t it = records_.begin(); it != records_.end();++it){
		Record*r=(*it);
		size=r->get_stream(s,o+block_size,m);
		assert(size!=0);
		block_size+=size;

	}

	s[o+1]=(block_size & 0xff00) >> 8;
	s[o+2]=block_size & 0x00ff;
	return block_size;
}

int
intifada::Block::push_back(Record* r)
{
	records_.push_back(r);
	return 0;
}

intifada::Block::record_list_size_t
intifada::Block::number()const
{
	return records_.size();
}

intifada::Record*
intifada::Block::get(record_list_size_t i)
{
	Record*ret=NULL;
	record_list_size_t idx=0;
	for(record_list_iterator_t it=records_.begin();it!=records_.end();++it){
		if(i==idx){
			ret=*it;
			break;
		}
		++idx;
	}
	return ret;
}

intifada::Record*
intifada::Block::erase(record_list_size_t i)
{
	Record*ret=NULL;
	record_list_size_t idx=0;
	for(record_list_iterator_t it=records_.begin();it!=records_.end();++it){
		if(i==idx){
			ret=*it;
			records_.erase(it);
			break;
		}
		++idx;
	}
	return ret;
}

int
intifada::Block::insert_before(Record* b, record_list_size_t i)
{
	int ret=-1;
	record_list_size_t idx=0;
	for(record_list_iterator_t it=records_.begin();it!=records_.end();++it){
		if(i==idx){
			record_list_iterator_t iit = records_.insert(it,b);
			if(iit!=records_.end()){
				ret=0;
			}
			break;
		}
		++idx;
	}
	return ret;
}

int intifada::Block::foreach(const Record_Iterator_List& l,const Message::block_list_size_t& b)
{
	Block::record_list_size_t r=0;
	bool first_block=true;
	for(record_list_const_iterator_t it = records_.begin(); it != records_.end();++it)
	{
		Record& rec=*(*it);
		l.apply(category_,rec,b,r,first_block);
		first_block=false;
		++r;
	}
	return 0;
}

void intifada::Block::set_record_reference(const int& stream_pos,const uint8_t& cat)
{
	if(record_reference_==NULL)
	{
		if((family_!="") && (record_repository_!=NULL))
		{
			record_reference_=record_repository_->get_pointer(family_,cat);
		}
	}
	if(record_reference_==NULL)
	{
		throw Parsing_Unknow_Category(stream_pos,family_,cat);
	}
	return;
}
