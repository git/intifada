/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_DECIMAL_CONVERTER_HXX
# define INTIFADA_DECIMAL_CONVERTER_HXX

# include <intifada/Unit_Converter.hxx>
namespace intifada
{
  /// Class handling types and transferts to/from streams
  class Decimal_Converter : public Unit_Converter
  {

  public:
    typedef Unit_Converter inherited;
  public:

    /// Default ctor
    Decimal_Converter();

    /// Copy constructor
    Decimal_Converter(const Decimal_Converter&rhs);

    /// return the type identification
    virtual std::string get(const Type& v)const;

    /// Copy operator
    virtual Decimal_Converter& operator=(const Decimal_Converter&rhs);

  };
}

#endif // INTIFADA_DECIMAL_CONVERTER_HXX
