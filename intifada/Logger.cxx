/*
   Copyright (C) 2009-2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Logger.hxx>

Logger *Logger::instance_=NULL;

Logger::Logger(std::ostream &os):
  display_(true)
  ,pos_(&os)
  ,mask_()
{

}

Logger& Logger::instance(std::ostream &os)
  {
    if(instance_==NULL){
      Logger::instance_=new Logger(os);
    }
    return *Logger::instance_;
  }

int Logger::insert_mask(const std::string& m)
  {
  int ret=0;
#ifndef NDEBUG
    strings_mask_type filter;
    split(filter,m);
    for(strings_mask_const_iterator_type it=filter.begin();it!=filter.end();++it){
      const std::string &f=(*it);
      if(f.empty()!=true){
        mask_.insert(f);
        ++ret;
      }
    }
#else
    UNUSED_ARG(m);
#endif
    return ret;
  }

int Logger::remove_mask(const std::string& m)
{
  int ret=0;
#ifndef NDEBUG
    strings_mask_type filter;
    split(filter,m);
    for(strings_mask_const_iterator_type it=filter.begin();it!=filter.end();++it){
      const std::string &f=(*it);
      if(f.empty()!=true){
        mask_.erase(f);
        ++ret;
      }
    }
#else
    UNUSED_ARG(m);
#endif
    return ret;
}

const Logger::strings_mask_type& Logger::get_masks()const
{
  return mask_;
}

void Logger::clear_mask()
{
#ifndef NDEBUG
  mask_.clear();
#endif
}

bool Logger::is_present(const std::string& m)const
{
  bool ret=false;
#ifndef NDEBUG
  strings_mask_const_iterator_type it=mask_.find(m);
  if(it!=mask_.end())
    {
      ret=true;
    }
#else
  UNUSED_ARG(m);
#endif
  return ret;
}

Logger& Logger::operator()(const std::string& n)
  {
#ifndef NDEBUG
    lock(n);
#else
    UNUSED_ARG(n);
#endif
    return *this;
  }

Logger & Logger::operator<< (const std::string& n)
  {
#ifndef NDEBUG
    if(pos_!=NULL && display_!=false) (*pos_)<<n;
#else
    UNUSED_ARG(n);
#endif
    return *this;
  }

Logger & Logger::operator<< (const char* n)
  {
#ifndef NDEBUG
    if(pos_!=NULL && display_!=false) (*pos_)<<n;
#else
    UNUSED_ARG(n);
#endif
    return *this;
  }

Logger & Logger::operator<< (int n)
  {
#ifndef NDEBUG
    if(pos_!=NULL && display_!=false) (*pos_)<<n;
#else
    UNUSED_ARG(n);
#endif
    return *this;
  }

Logger & Logger::operator<< (std::ostream& (*pf)(std::ostream&))
  {
#ifndef NDEBUG
  std::ostream& (*stdendl)(std::ostream&) =std::endl;
  if(pos_!=NULL && display_!=false) (*pos_)<<pf;
  if(pf==stdendl)
    {
    this->unlock();
    }
#else
    UNUSED_ARG(pf);
#endif
    return *this;
  }

Logger& Logger::operator<<(std::ios_base& (*pf) (std::ios_base&))
  {
#ifndef NDEBUG
    if(pos_!=NULL && display_!=false) (*pos_)<<pf;
#else
    UNUSED_ARG(pf);
#endif
    return *this;
  }

Logger & Logger::operator<<(Logger& (*pf)(Logger&))
  {
#ifndef NDEBUG
    pf(*this);
#else
    UNUSED_ARG(pf);
#endif
    return *this;
  }

void Logger::lock(const std::string& cond)
  {
#ifndef NDEBUG
    strings_mask_const_iterator_type it=mask_.find(cond);
    if(it==mask_.end()){
      display_=false;
    }else{
      display_=true;
      if(pos_!=NULL) (*pos_)<<cond<<":";
    }
#else
    UNUSED_ARG(cond);
#endif
  }

void Logger::unlock()
  {
#ifndef NDEBUG
    display_=true;
#endif
  }

void Logger::split(strings_mask_type& s,const std::string& list_filter)
  {
#ifndef NDEBUG
    s.clear();
    std::string::size_type pos=0;
    std::string::size_type bpart=0;
    std::string::size_type epart=0;
    while(pos!=std::string::npos){
      bpart=pos;
      pos = list_filter.find_first_of('|',pos);
      if(pos!=std::string::npos){
        epart=pos;
      }else{
        epart=list_filter.size();
      }
      std::string part=list_filter.substr(bpart,epart-bpart);
      s.insert(part);

      if(pos!=std::string::npos){
        pos+=1;
      }else{
        pos=std::string::npos;
      }
    }
#else
    UNUSED_ARG(s);
    UNUSED_ARG(list_filter);
#endif
  }

//int nifty_counter::count=0;
//
//nifty_counter::nifty_counter()
//  {
//    std::cout << ">nifty_counter" << std::endl;
//    if (count++ == 0) {
//      std::cout << ">nifty_counter new" << std::endl;
//      new (&clog) Logger(std::cout);
//    }
//    std::cout << "<nifty_counter" << std::endl;
//  }
//nifty_counter::~nifty_counter()
//  {
//    if (--count == 0) {
//      // clean up obj1 ... objn
//    }
//  }
//
//Logger clog=Logger::instance(std::cout);

