/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_UNIT_CONVERTER_HXX
# define INTIFADA_UNIT_CONVERTER_HXX

# include <intifada/build_configuration.h>
# include <intifada/Properties.hxx>
namespace intifada
{
class Type;

/// Class handling types and transferts to/from streams
/**
 * purpose is only to display smart values (upon call to Type::get_to_string)
 */
class Unit_Converter : public Properties
{

public:
	typedef Properties inherited;
public:

	/// Default ctor
	Unit_Converter();

	/// Copy constructor
	Unit_Converter(const Unit_Converter&rhs);

	/// return the type identification
	//virtual std::string get(const Type& v)const=0;

	/// Copy operator
	virtual Unit_Converter& operator=(const Unit_Converter&rhs);

	virtual ~Unit_Converter();

	/// Clone unit converter
	/**
	 * @return a dynamicly allocated Unit_Converter based on *this structure
	 */
	virtual Unit_Converter* clone()const=0;
};

template <typename T>
class Unit_Converter_Base_Type : public Unit_Converter
{
public:
	typedef Unit_Converter inherited;
protected:
	typedef T value_type;
public:
	/// Default ctor
	Unit_Converter_Base_Type():inherited(){}

	/// Copy constructor
	Unit_Converter_Base_Type(const Unit_Converter&rhs):inherited(rhs){}

	virtual std::string get(const value_type& v)const=0;
};

}

#endif // INTIFADA_UNIT_CONVERTER_HXX
