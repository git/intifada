/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_RECORD_REPOSITORY_HXX
# define INTIFADA_RECORD_REPOSITORY_HXX

# include <string>
# include <intifada/Map.hxx>
# include <set>
# include <intifada/inttypes.h>
namespace intifada
{
  class Record;
  class Structure_Functor;
  /// This is to handle record descriptions
/**
 * this could be a singleton, or not
 *
 */
  class Record_Repository
  {
  private:
    class Record_Node
    {
    public:
      Record_Node();

      Record_Node(Record* r,const std::string&name,const std::string& id,uint8_t cat);

      Record_Node(const Record_Node&rhs);

      void release();

      Record_Node& operator=(const Record_Node&rhs);

      void set(Record* r,const std::string&name,const std::string& id,uint8_t cat);

      /// Getting a Record pointer
      /**
       * \return a record pointer
       */
      const Record* get()const;

      /// Get record descritption
      const std::string& get_description()const;

      /// get category
      uint8_t get_category()const;

      /// get id (link between family and record)
      const std::string& get_id()const;

    private:
      Record_Node& copy(const Record_Node&rhs);


    private:
      Record* r_;
      std::string name_;
      std::string id_;
      uint8_t cat_;
    };
public:
  typedef std::set<std::string> family_list_t;
  typedef family_list_t::iterator family_list_iterator_t;
  typedef family_list_t::const_iterator family_list_const_iterator_t;
  typedef std::pair<family_list_iterator_t,bool> family_list_pair_t;

  private:
    typedef Map<uint8_t,std::string> cat_id_repository_t;
    typedef cat_id_repository_t::iterator cat_id_repository_iterator_t;
    typedef cat_id_repository_t::const_iterator cat_id_repository_const_iterator_t;
    typedef cat_id_repository_t::pair cat_id_repository_pair_t;

    typedef Map<std::string,family_list_t> id_family_repository_t;
    typedef id_family_repository_t::iterator id_family_repository_iterator_t;
    typedef id_family_repository_t::pair id_family_repository_pair_t;

    /// Families repository
    typedef Unordered_Map<std::string,cat_id_repository_t> family_id_repository_t;
    typedef family_id_repository_t::iterator family_id_repository_iterator_t;
    typedef family_id_repository_t::const_iterator family_id_repository_const_iterator_t;
    typedef family_id_repository_t::pair family_id_repository_pair_t;

    typedef Unordered_Map<std::string,Record_Node> record_repository_t;
    typedef record_repository_t::iterator record_repository_iterator_t;
    typedef record_repository_t::const_iterator record_repository_const_iterator_t;
    typedef record_repository_t::pair record_repository_pair_t;
  public:
    Record_Repository();

    virtual ~Record_Repository();

    static Record_Repository* instance();

    int insert(Record* r,const std::string&name,const std::string& id,uint8_t cat);

    int insert(const std::string&family,const std::string& id);

    /// get a clone of record
    /**
     * \param family select asterix family
     * \param cat select category in selected family
     * \return NULL if no family or category
     * \return a record clone
     */
    Record* get_clone(const std::string&family,uint8_t cat);

    /// get a pointer of record
    const Record* get_pointer(const std::string&family,uint8_t cat)const;

    // clear all families
    void release();

    // Walk through all known categories
    int foreach(Structure_Functor *f)const;

    // Get families
    void get_families(family_list_t& f)const;

  private:

    const Record* get_node_pointer(const std::string&family,uint8_t cat)const;

    /// move family to definitive place
    /**
     * If record isn't yet created, families are
     */
    void move_family(const std::string&id,uint8_t cat);

    int create_family(const std::string&family,const std::string&id,uint8_t cat);

  private:
    static Record_Repository*instance_;

    record_repository_t rep_;

    id_family_repository_t temp_families_;

    family_id_repository_t families_;
  };
}

#endif // INTIFADA_RECORD_REPOSITORY_HXX
