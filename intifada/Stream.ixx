/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

INTIFADA_INLINE
intifada::Stream::const_reverse_iterator
intifada::Stream::rbegin()const{return t_.rbegin();}

INTIFADA_INLINE
intifada::Stream::const_reverse_iterator
intifada::Stream::rend()const{return t_.rend();}

INTIFADA_INLINE
intifada::Stream::reverse_iterator
intifada::Stream::rbegin(){return t_.rbegin();}

INTIFADA_INLINE
intifada::Stream::reverse_iterator
intifada::Stream::rend(){return t_.rend();}

INTIFADA_INLINE
intifada::Stream::reference
intifada::Stream::operator[] (size_type n){return t_[n];}

INTIFADA_INLINE
intifada::Stream::const_reference
intifada::Stream::operator[] (size_type n) const {return t_[n];}

INTIFADA_INLINE
void
intifada::Stream::push_back (const value_type &x){t_.push_back(x);}

INTIFADA_INLINE
void
intifada::Stream::pop_back(intifada::Stream::size_type s)
{
  for(size_type i = 0; i<s;++i){
    t_.pop_back();
  }
}

INTIFADA_INLINE
intifada::Stream::reference
intifada::Stream::back() {return t_.back();}

INTIFADA_INLINE
bool
intifada::Stream::empty()const {return t_.empty();}

INTIFADA_INLINE
intifada::Stream::size_type
intifada::Stream::size()const {return t_.size();}

INTIFADA_INLINE
void
intifada::Stream::clear() {t_.clear();}

// INTIFADA_INLINE
// const Data_Field_Characteristics&
// intifada::Stream::get_structure(const std::string& name)const
// {
//   return internal_structure_.get_characteristics(name);
// }
