/*
   Copyright (C) 2009, 2011, 2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_TYPE_HXX
# define INTIFADA_TYPE_HXX

# include <intifada/Stream.hxx>
# include <stdexcept>

# include <intifada/build_configuration.h>
# include <intifada/Properties.hxx>
# include <intifada/Logger.hxx>
# include <cstdlib>

namespace intifada
{
class Unit_Converter;

/// Class handling types and transferts to/from streams
class Type : public Properties
{

public:
	typedef Properties inherited;
public:

	/// Constructor
	/**
	 * @param type type identifier
	 */
	Type(const std::string& id);

	/// Copy constructor
	Type(const Type&rhs);

public:
	/// set the unit converter object
	template <typename T>
	const Unit_Converter* set_unit_converter();

	/// set the unit converter object
	/**
	 * @param converter dynamicly allocated converter object
	 *
	 * @attention, Type instance have de free converter responsability
	 */

	void set_unit_converter(const Unit_Converter*converter);

	/// Copy operator
	virtual Type& operator=(const Type&rhs);

	/// Clone type
	/**
	 * allocate a new type from this. Must be freed by user using delete
	 */
	virtual Type* clone()const=0;

	/// Set this to null value
	/**
	 * set a value's instance to null (eg 0 for a numeric type, "" for a string type ...)
	 */
	virtual void set_null()=0;

	virtual ~Type();

	/// handle changed state
	/**
	 * changed states could be detected. This method should be called to
	 * indicate which value to remember
	 */
	virtual void reset_changed_state()=0;

	/// handle changed state
	/**
	 * \return true if current value differs from value when reset_changed_state were called
	 * or if reset_changed_state was never called
	 * \return false if current value doesn't differ from value when
	 * reset_changed_state were called
	 */
	virtual bool is_changed_state()const=0;

	/// Get size
	/**
	 * \return size object (unit is the bit) (-1) if unspecifiable)
	 */
	virtual ssize_t get_size()const=0;

	/// Compare with a string
	virtual bool operator==(const std::string& rhs)const=0;

	bool operator!=(const std::string& rhs)const;

	/// Getting associated string type
	/**
	 * \return associated string type
	 * \attention this method is only for internal purpose
	 */
	const std::string& get_name()const;

	/// Get internal value to string
	/**
	 * \return internal value
	 */
	virtual std::string get_to_string()const=0;

	/// Set internal value from string
	/**
	 * \param v value
	 * \return 0 if no error
	 */
	virtual int set_from_string(const std::string& v)=0;

	/// Transfert a stream into a real type
	/**
	 * \param b stream to transfert
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 *
	 * data source is taken from b and transfered to internal structure
	 */
	virtual void set_from_stream(Stream::const_reverse_iterator b,int start,int size)=0;

	/// Transfert a real type into a stream
	/**
	 * \param b target stream
	 * \param start begining of transfert (lsb start with 1)
	 * \param size size of the transfert
	 * \exception std::out_of_range
	 *
	 * data source is taken from internal structure to b
	 */
	virtual void to_stream(Stream::reverse_iterator b,int start,int size)const=0;

protected:
	/// Put a stream into another stream
	/**
	 * \param t output stream
	 * \param b input asterix stream
	 * \param start lsb bit to read
	 * \param size size to read
	 *
	 * \remarks lsb asterix bit is stored in first byte stream
	 */
	void streamcpy(Stream::reverse_iterator t,Stream::const_reverse_iterator b,int start,unsigned int size)const;

	/// Put a stream into a numeric type
	/**
	 * \param t output numeric_type
	 * \param b input asterix stream
	 * \param start lsb bit to read
	 * \param size size to read
	 *
	 * \remarks lsb asterix bit is stored in first byte stream
	 * \attention this method implementation depend of memory representation
	 */
	template <typename T_>
	void streamcpy(T_ &t,Stream::const_reverse_iterator b,int start,unsigned int size) const;

	template <typename T_>
	void streamcpy(Stream::reverse_iterator b,const T_ &t,int start,unsigned int size) const;

	const Unit_Converter* get_unit_converter()const;

private:
	// Handle unit converter release
	void release_unit_converter();
private:

	std::string type_name_;
	const Unit_Converter* _unit_converter;
};

}

std::ostream& operator<<(std::ostream& os,const intifada::Type& rhs);

# ifdef INTIFADA_USE_INLINE
#  include<intifada/Type.ixx>
# endif

# include <intifada/Type.txx>

#endif // INTIFADA_TYPE_HXX
