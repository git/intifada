/*
   Copyright (C) 2009, 2010, 2011 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DUMP_ITEM_ITERATOR_HXX_
# define DUMP_ITEM_ITERATOR_HXX_

# include <intifada/Data_Field.hxx>

namespace intifada
{
class Dump_Item_Iterator : public intifada::Data_Field::Functor
  {
  private:
  	  typedef std::set<intifada::Path_Name> display_list_type;
  	  typedef display_list_type::const_iterator display_list_const_iterator_type;
  public:
    Dump_Item_Iterator(std::ostream& os);
    void dump_all();

    void display(const intifada::Path_Name& d);

    virtual int operator()(
        const intifada::Stream::size_type&stream_pos
        ,const intifada::Stream::size_type& stream_size
        ,const intifada::Path_Name& full_name
        ,intifada::Type& t);
  private:
    std::ostream& os_;
    bool strip_null_;
    display_list_type _display;

  };
}
#endif // DUMP_ITEM_ITERATOR_HXX_
