/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Unit_Converter_Repository.hxx>
#include <intifada/Octal_Converter.hxx>
#include <intifada/FL_Converter.hxx>

#include <iostream>
intifada::Unit_Converter_Repository* intifada::Unit_Converter_Repository::instance_=NULL;

intifada::Unit_Converter_Repository::Unit_Converter_Repository_Iterator::Unit_Converter_Repository_Iterator() {}

intifada::Unit_Converter_Repository::Unit_Converter_Repository_Iterator::~Unit_Converter_Repository_Iterator() {}

int
intifada::Unit_Converter_Repository::Unit_Converter_Repository_Iterator::operator()
(const std::string& t,const Unit_Converter&)
  {
    std::cout << t << std::endl;
    return 0;
  }

intifada::Unit_Converter_Repository::Unit_Converter_Repository():
		converters_()
  {}

intifada::Unit_Converter_Repository::~Unit_Converter_Repository()
  {
    for(converter_map_const_iterator_type i=converters_.begin();i!=converters_.end();++i)
    {
      const Unit_Converter* t=(*i).second;
      delete t;
    }
    converters_.clear();
  }

int
intifada::Unit_Converter_Repository::release()
  {
    if(instance_!=NULL){
      delete instance_;
      instance_=NULL;
    }
    return 0;
  }

const intifada::Unit_Converter*
intifada::Unit_Converter_Repository::clone(const std::string& n)const
{
  converter_map_const_iterator_type it=converters_.find(n);
  const Unit_Converter*ret=NULL;
  if(it!=converters_.end()){
    ret=(*it).second;
  }
  return ret->clone();
}

intifada::Unit_Converter_Repository* intifada::Unit_Converter_Repository::instance()
  {
    if(Unit_Converter_Repository::instance_==NULL){
      instance_=new Unit_Converter_Repository;
      instance_->insert<intifada::Octal_Converter_T<uint16_t> >("Octal_Converter");
      instance_->insert<intifada::FL_Converter_T<int16_t> >("FL_Converter");
      instance_->insert<intifada::FL_Converter_T<uint16_t> >("FL_Unsigned_Converter");
    }
    return instance_;
  }

int
intifada::Unit_Converter_Repository::foreach(Unit_Converter_Repository_Iterator& it)const
{
  for(converter_map_const_iterator_type i=converters_.begin();i!=converters_.end();++i){
    const std::string& k=(*i).first;
    const Unit_Converter* t=(*i).second;
    it(k,*t);
  }
  return 0;
}

int
intifada::Unit_Converter_Repository::foreach()const
{
	Unit_Converter_Repository_Iterator it;
  return foreach(it);

}
