/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record_Iterator_List.hxx>

intifada::Record_Iterator_List::Record_Iterator_List():
  l_(),
  cl_()
{}


int intifada::Record_Iterator_List::register_iterator(Record_Iterator&it,uint8_t c)
{
  int ret=-1;
  list_type::iterator iter = l_.insert(&it,c);
  if(iter!=l_.end())
    {
    ret=0;
  }
  return ret;
}

int intifada::Record_Iterator_List::register_iterator(Record_Const_Iterator&it,uint8_t c)
{
  int ret=-1;
  const_list_type::iterator iter = cl_.insert(&it,c);
  if(iter!=cl_.end())
    {
    ret=0;
  }
  return ret;
}

int intifada::Record_Iterator_List::register_iterator(Record_Iterator&it)
{
  int ret=-1;
  list_type::iterator iter = l_.insert(&it,-1);
  if(iter!=l_.end())
    {
    ret=0;
  }
  return ret;
}

int
intifada::Record_Iterator_List::register_iterator(Record_Const_Iterator&it)
{
  int ret=-1;
  const_list_type::iterator iter = cl_.insert(&it,-1);
  if(iter!=cl_.end())
    {
    ret=0;
  }
  return ret;
}

int intifada::Record_Iterator_List::apply(
				     uint8_t cat,
				     Record& rec,
				     const Message::block_list_size_t& b,
				     const Block::record_list_size_t& r,
				     bool first_call)const
{
  list_type::const_iterator it=l_.lower_bound(cat);
  if(it!=l_.end())
    {
    for(;it!=l_.upper_bound(cat);++it)
      {
      Record_Iterator* i =(*it).second;
      if(first_call==true)
        {
	(*i)(cat);
      }
      (*i)(rec,b,r);
    }
  }
  all_categories_apply(cat,rec,b,r,first_call);
  return 0;
}

int intifada::Record_Iterator_List::apply_on_const(
					       uint8_t cat,
					       const Record& rec,
					       const Message::block_list_size_t& b,
					       const Block::record_list_size_t& r,
					       bool first_call)const
{
  const_list_type::const_iterator cit=cl_.lower_bound(cat);
  if(cit!=cl_.end())
    {
    for(;cit!=cl_.upper_bound(cat);++cit)
      {
      Record_Const_Iterator* i =(*cit).second;
      if(first_call==true)
        {
	(*i)(cat);
      }
      (*i)(rec,b,r);
    }
  }
  all_categories_apply_on_const(cat,rec,b,r,first_call);
  return 0;
}

int intifada::Record_Iterator_List::all_categories_apply(
				      uint8_t cat,Record& rec,
				      const Message::block_list_size_t& b,
				      const Block::record_list_size_t& r,
				      bool first_call)const
{
  list_type::const_iterator it=l_.lower_bound(-1);
  if(it!=l_.end())
    {
    for(;it!=l_.upper_bound(-1);++it)
      {
      Record_Iterator* i =(*it).second;
      if(first_call==true)
        {
	(*i)(cat);
      }
      (*i)(rec,b,r);
    }
  }
  return 0;
}

int intifada::Record_Iterator_List::all_categories_apply_on_const(
				     uint8_t cat,const Record& rec,
				     const Message::block_list_size_t& b,
				     const Block::record_list_size_t& r,
				     bool first_call)const
{
  const_list_type::const_iterator cit=cl_.lower_bound(-1);
  if(cit!=cl_.end())
    {
    for(;cit!=cl_.upper_bound(-1);++cit)
      {
      Record_Const_Iterator* i =(*cit).second;
      if(first_call==true)
        {
	(*i)(cat);
      }
      (*i)(rec,b,r);
    }
  }
  return 0;
}

bool intifada::Record_Iterator_List::category_presence(uint8_t cat)const
{
  bool ret=cl_.exist(-1)||cl_.exist(cat);
  if(ret==false)
    {
    ret=l_.exist(-1)||l_.exist(cat);
  }

  return ret;
}
