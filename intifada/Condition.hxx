/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_CONDITION_HXX
# define INTIFADA_CONDITION_HXX

# include <string>
# include <intifada/inttypes.h>

namespace intifada
  {
    class Data_Field;

    class Condition
      {
    public:
      Condition(
          const std::string& data_item,
          const std::string& value);

      Condition(const Condition&rhs);

      virtual ~Condition();

      Condition& operator=(const Condition&rhs);

      // bool operator()()const;
      bool operator()(const intifada::Data_Field* msg)const;
    public:
      intifada::Condition* clone()const;

      void close();

      std::string get()const;

      const std::string& get_lvalue()const;

      const std::string& get_rvalue()const;

    private:
      Condition& copy(const Condition& rhs);

    private:
      std::string data_item_;
      std::string svalue_;
      };
  }

#endif // INTIFADA_DATA_ITEM_CONDITION_HXX
