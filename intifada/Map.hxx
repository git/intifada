/*
   Copyright (C) 2009 Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAP_HXX_
# define MAP_HXX_

#include <map>

namespace intifada
  {

    template <typename K_,typename V_,typename S_>
    class inherited_template_3 : public S_
      {
      public:
        typedef K_ t1;
        typedef V_ t2;
        typedef S_ t3;
      public:
        inherited_template_3():t3(){}

      };

    template <typename K_,typename V_>
    class Map : public inherited_template_3<K_,V_,std::map<K_,V_> >
    {
    public:
      typedef inherited_template_3<K_,V_,std::map<K_,V_> > inherited;
      typedef typename inherited::t3 map_type;
      typedef std::pair<typename map_type::iterator,bool> pair;
    public:
      Map():inherited(){}
    };

#if INTIFADA_HAVE_UNORDERED_MAP==1
    template <typename K_,typename V_>
    class Unordered_Map : public inherited_template_3<K_,V_,std::unordered_map<K_,V_> >
    {
    public:
      typedef inherited_template_3<K_,V_,std::unordered_map<K_,V_> > inherited;
      typedef typename inherited::t3 map_type;
      typedef std::pair<typename map_type::iterator,bool> pair;
    public:
      Unordered_Map():inherited(){}

      void rehash(){rehash();}
    };
#else
    template <typename K_,typename V_>
    class Unordered_Map : public inherited_template_3<K_,V_,std::map<K_,V_> >
        {
        public:
          typedef inherited_template_3<K_,V_,std::map<K_,V_> > inherited;
          typedef typename inherited::t3 map_type;
          typedef std::pair<typename map_type::iterator,bool> pair;
        public:
          Unordered_Map():inherited(){}
        };
#endif

    template <typename K_,typename V_>
    class Multi_Map : public inherited_template_3<K_,V_,std::multimap<K_,V_> >
    {
    public:
      typedef inherited_template_3<K_,V_,std::multimap<K_,V_> > inherited;
      typedef typename inherited::t3 map_type;
      typedef std::pair<typename map_type::iterator,bool> pair;
    public:
      Multi_Map():inherited(){}
    };
  }

#endif // MAP_HXX_
