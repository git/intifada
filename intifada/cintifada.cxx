/*
   Copyright (C) 2009, 2010  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/cintifada.h>

#include <intifada/Configuration_XML_Reader.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Message.hxx>
#include <intifada/Record.hxx>
#include <intifada/Record_Iterator_List.hxx>
#include <intifada/Data_Field.hxx>

class _C_Record_Iterator : public intifada::Record_Iterator
{
public:
  _C_Record_Iterator(record_iterator_category c,record_iterator r,user_data_type ud):cat_function_(c),record_function_(r),user_data_(ud){}

  virtual int operator()(uint8_t c)
  {
    return (*cat_function_)(c,user_data_);
  }

  virtual int operator()(
      intifada::Record_Iterator::record_type& i,
      const intifada::Message::block_list_size_t& b,
      const intifada::Block::record_list_size_t& r)
  {
    return (*record_function_)(
        static_cast<void*>(&i)
        ,b,r,user_data_);
  }

private:
  record_iterator_category cat_function_;
  record_iterator record_function_;
  user_data_type user_data_;
};

class _C_Data_Item_Iterator : public intifada::Data_Field::Functor
{
public:
  _C_Data_Item_Iterator(data_field_iterator cb,user_data_type ud):data_item_function_(cb),user_data_(ud){}

  virtual int operator()(const intifada::Stream::size_type&bs,const intifada::Stream::size_type&rs,const intifada::Path_Name& full_name,intifada::Type& t)
  {
    return (*data_item_function_)(bs,rs,full_name.get_full_name().c_str(),static_cast<void*>(&t),user_data_);
  }

private:
  data_field_iterator data_item_function_;
  user_data_type user_data_;
};

i_ptr_Configuration_XML_Reader i_create_xml_configuration_reader(const char*dtd,const char*xml)
{
  return reinterpret_cast<i_ptr_Configuration_XML_Reader>(new intifada::Configuration_XML_Reader(dtd,xml));
}

void i_release_xml_configuration_reader(i_ptr_Configuration_XML_Reader c)
{
  delete reinterpret_cast<intifada::Configuration_XML_Reader*>(c);
}

int i_open_xml_configuration(i_ptr_Configuration_XML_Reader c)
{
  return reinterpret_cast<intifada::Configuration_XML_Reader*>(c)->open();
}

i_ptr_Record_Repository i_get_record_repository_instance()
{
  return reinterpret_cast<i_ptr_Configuration_XML_Reader>(intifada::Record_Repository::instance());
}

i_family_list_type i_create_family_list_type()
{
  return reinterpret_cast<i_family_list_type>(new intifada::Record_Repository::family_list_t);
}

void i_release_family_list_type(i_family_list_type ft)
{
  delete reinterpret_cast<intifada::Record_Repository::family_list_t *>(ft);
}


void i_get_record_families(i_ptr_Record_Repository r,i_family_list_type f)
{
  const intifada::Record_Repository *rep=reinterpret_cast<const intifada::Record_Repository *>(r);

  intifada::Record_Repository::family_list_t *pt_f=reinterpret_cast<intifada::Record_Repository::family_list_t *>(f);
  rep->get_families(*pt_f);
}

int i_get_families_size(i_family_list_type f)
{
  intifada::Record_Repository::family_list_t *pt_f=reinterpret_cast<intifada::Record_Repository::family_list_t *>(f);
  return pt_f->size();
}

const char* i_get_family(i_family_list_type /*f*/,int /*idx*/)
{
  //intifada::Record_Repository::family_list_t *pt_f=reinterpret_cast<intifada::Record_Repository::family_list_t *>(f);
  return NULL;
}

int i_xml_parse(i_ptr_Configuration_XML_Reader l,i_ptr_Record_Repository r)
{
  intifada::Configuration_XML_Reader *reader=reinterpret_cast<intifada::Configuration_XML_Reader*>(l);
  intifada::Record_Repository *rep=reinterpret_cast<intifada::Record_Repository *>(r);
  return reader->parse(rep);
}

i_ptr_Message i_create_message(i_ptr_Record_Repository r,const char*family)
{
  return new intifada::Message(reinterpret_cast<intifada::Record_Repository *>(r),family);
}
void i_release_message(i_ptr_Message msg)
{
  delete reinterpret_cast<intifada::Message *>(msg);
}

int i_message_set_stream(i_ptr_Message msg,const uint8_t *s, stream_size_type o, stream_size_type m)
{
  intifada::Message *ast_msg=reinterpret_cast<intifada::Message *>(msg);
  int ret=0;
  try
  {
    ast_msg->set_stream(s,o,m);
  }
  catch(...)
  {
    ret=-1;
  }
  return ret;
}

int i_message_get_stream(i_ptr_Message msg,uint8_t *s, stream_size_type o, stream_size_type m)
{
  const intifada::Message *ast_msg=reinterpret_cast<intifada::Message *>(msg);
  int ret=0;
  try
  {
    ast_msg->get_stream(s,o,m);
  }
  catch(...)
  {
    ret=-1;
  }
  return ret;

}

i_ptr_Record_Iterator_List i_create_record_iterator_list()
{
  return new intifada::Record_Iterator_List;
}

void i_release_record_iterator_list(i_ptr_Record_Iterator_List rl)
{
  const intifada::Record_Iterator_List*ptr=reinterpret_cast<intifada::Record_Iterator_List*>(rl);
  delete ptr;
}

i_ptr_Record_Iterator i_register_record_iterator(i_ptr_Record_Iterator_List rl,record_iterator_category fc,record_iterator r,user_data_type ud)
{
  intifada::Record_Iterator_List*ptr=reinterpret_cast<intifada::Record_Iterator_List*>(rl);
  _C_Record_Iterator *ri=new _C_Record_Iterator(fc,r,ud);
  ptr->register_iterator(*ri);
  return reinterpret_cast<i_ptr_Record_Iterator>(ri);
}

void i_release_record_iterator(i_ptr_Record_Iterator ri)
{
  const intifada::Record_Iterator *ptr=reinterpret_cast<intifada::Record_Iterator*>(ri);
  delete ptr;
}

void i_message_foreach_record(i_ptr_Message msg,i_ptr_Record_Iterator_List rl)
{
  intifada::Message *ast_msg=reinterpret_cast<intifada::Message *>(msg);
  intifada::Record_Iterator_List*ast_it_list=reinterpret_cast<intifada::Record_Iterator_List*>(rl);
  ast_msg->foreach(*ast_it_list);
}

void i_record_foreach_data_field(i_ptr_Record rec,data_field_iterator cb,user_data_type ud)
{
  intifada::Record *ast_record=reinterpret_cast<intifada::Record *>(rec);
  _C_Data_Item_Iterator fct(cb,ud);
  ast_record->foreach(fct,intifada::Path_Name());
}

i_const_string_type i_get_string_value(i_type t)
{
  const intifada::Type* type=reinterpret_cast<const intifada::Type*>(t);
  std::string svalue = type->get_to_string();
  i_string_type ret=new i_char_type[svalue.size()+1];
  ret[svalue.size()]='\0';
  svalue.copy(ret,svalue.size());
  return ret;
}

void i_release_string_value(i_const_string_type t)
{
  delete[]t;
}
