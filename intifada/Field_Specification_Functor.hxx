/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASTERIX_FIELD_SPECIFICATION_FUNCTOR_HXX
# define ASTERIX_FIELD_SPECIFICATION_FUNCTOR_HXX

namespace intifada
{
  class Path_Name;

  class Field_Specification_Functor
    {
    public:
      virtual ~Field_Specification_Functor();

      /// user action
      /**
       * \param name item name
       * \return size handled
       * \return -1 break walk
       */
      virtual int operator()(const Path_Name& name)=0;
    };
}

#endif // ASTERIX_FIELD_SPECIFICATION_FUNCTOR_HXX
