/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_CONFIGURATION_READER_HXX
# define INTIFADA_CONFIGURATION_READER_HXX

# include <intifada/Stream.hxx>
# include <intifada/Path_Name.hxx>
# include <string>
# include <sstream>
# include <stdexcept>
# include <stack>

namespace intifada
{
class Record;
class Fixed_Length_Data_Field;
class Extended_Length_Data_Field;
class Repetitive_Length_Data_Field;

class Record_Repository;
class Condition;


class Current
{
public:
	class Current_Iterator
	{

	public:
		Current_Iterator();

		virtual ~Current_Iterator();

		void set_property(const std::string& prop,const std::string& val);
		void set_converter(const std::string& converter);
		void set(
				const std::string& name,
				const std::string& type,
				int start,
				int size,
				int byte);
		virtual int operator()(Fixed_Length_Data_Field* t);
		virtual int operator()(Extended_Length_Data_Field* t);
		virtual int operator()(Repetitive_Length_Data_Field* t);
		virtual int operator()(Record* t);

	protected:
		Data_Field_Characteristics *current_field_;
		std::string name_;
		std::string type_;
		int start_;
		int size_;
		int byte_;
	};
public:
	enum{
		NONE,
		FIXED,
		EXTENDED,
		REPETITIVE,
		COMPOUND
	};

public:
	Current();

	void set_fixed(Fixed_Length_Data_Field* fixed);

	void set_extended(Extended_Length_Data_Field* extended);

	void set_repetitive(Repetitive_Length_Data_Field* repetitive);

	void set_compound(Record* compound);

	void register_handler(Current_Iterator*h);

	int apply(
			const std::string& name,
			const std::string& type,
			int start,
			int size,
			int byte);
	void set_property(const std::string& n, const std::string& v);
	void set_type_property(const std::string& n, const std::string& v);

	void set_converter(const std::string& converter);
private:
	Fixed_Length_Data_Field* fixed_;
	Extended_Length_Data_Field* extended_;
	Repetitive_Length_Data_Field* repetitive_;
	Record* compound_;

	Current_Iterator* handler_;

	int current_;
};

class Part_Configuration : public Current::Current_Iterator
{
public:
	typedef Current::Current_Iterator inherited;
public:
	Part_Configuration();
	virtual int operator()(intifada::Fixed_Length_Data_Field* t);
	virtual int operator()(intifada::Extended_Length_Data_Field* t);
	virtual int operator()(intifada::Repetitive_Length_Data_Field* t);
};

class Configuration_Reader
{
private:
	typedef std::stack<std::string> string_stack_type;

protected:
	Configuration_Reader();
public:
	virtual ~Configuration_Reader();

	/// Configuration parser intitialisation
	/**
	 * \return 0 on success
	 *
	 * handle intifada intialisation
	 */
	 virtual int open()=0;

	/// effective configuration load
	/**
	 * \param r asterix repository structure
	 * \return 0 on success
	 *
	 * after this call, intifada is ready
	 */
	 virtual int parse(Record_Repository*r)=0;

	/// convenient method for configuration load
	/**
	 * \return 0 on success
	 *
	 * after this call, intifada is ready
	 * setup the Record_Repository standard singleton
	 */
	 int parse();

	/// free ressources
	virtual void release()=0;

protected:
	void set_repository(Record_Repository*r);

	int register_types(const std::string& name);

	void create_record(
			intifada::Record*&r,
			const std::string& name,
			const std::string& id,
			uint8_t category
	);

	intifada::Record* create_compound(intifada::Record*&r,uint8_t length);

	void begin_configuration();

	void end_configuration();

	void end_record();

	intifada::Record* create_explicit(intifada::Record*&r);


	void create_uap(const std::string& func,const std::string& result);

	void create_frn(intifada::Record*r,uint8_t frn,const Path_Name& item);

	void create_item(const std::string& name,const Path_Name& reference,const std::string& forcepresence);

	//void pop_item();

	void create_fixed(intifada::Record*r);

	void create_extended(intifada::Record*r);

	void create_repetitive(intifada::Record*r);

	void create_family(const std::string& name);

	int create_family_id(const std::string& name);

	int handle_parts(
			const std::string& name,
			const std::string& type,
			int byte,
			int start,
			int size
	);

	void set_type_property(const std::string& prop,const std::string& val);
	void set_item_property(const std::string& name,const std::string& value);
	void set_item_converter(const std::string& converter);

protected:
	void push_element(const std::string& e);
	void pop_element();
	bool empty_element()const;
	const std::string& top_element()const;

private:

	Condition* current_condition_;
	std::string current_item_longname_;
	Path_Name current_item_;
	// Path_Name current_part_;
	std::string current_forcepresence_;
	std::string current_family_;

	Record_Repository*rep_;

	Current current_;

	Current::Current_Iterator *handler_;

	string_stack_type node_stack_;
};
}
#endif  // INTIFADA_CONFIGURATION_READER_HXX
