/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_UNIT_CONVERTER_REPOSITORY_HXX
# define INTIFADA_UNIT_CONVERTER_REPOSITORY_HXX

# include <intifada/Map.hxx>
# include <string>
# include <intifada/Unit_Converter.hxx>
namespace intifada
{
  class Unit_Converter_Repository
  {
  public:

    /// Iterator on each converter repository node
    class Unit_Converter_Repository_Iterator
    {
    public:
    	Unit_Converter_Repository_Iterator();

      virtual ~Unit_Converter_Repository_Iterator();
    public:

      /// Default iterator
      /**
       * \param t converter name (filled internaly)
       * \param pt type reference
       *
       * Default behaviour: print type name on cout
       */
      virtual int operator()(const std::string& t,const Unit_Converter&);
    };
  private:
    typedef Unordered_Map<std::string,Unit_Converter*> converter_map_type;
    typedef converter_map_type::iterator converter_map_iterator_type;
    typedef converter_map_type::const_iterator converter_map_const_iterator_type;
    typedef converter_map_type::pair converter_map_pair_type;
  public:
    Unit_Converter_Repository();

    virtual ~Unit_Converter_Repository();

    int release();

    template <typename T>
    int insert(const std::string& name);

    const Unit_Converter* clone(const std::string& n)const;

    static Unit_Converter_Repository* instance();

    /// Apply iterator on each node
    /**
     * \param it called iterator.
     * \return 0;
     *
     * it operator will be called for each node with associated name
     */
    int foreach(Unit_Converter_Repository_Iterator& it)const;

    /// Apply defaul iterator on each node
    /**
     * \param it called iterator.
     * \return 0;
     *
     * it operator will be called for each node with associated name
     */
    int foreach()const;

  private:
    static Unit_Converter_Repository *instance_;

    converter_map_type converters_;
  };
}

# include <intifada/Unit_Converter_Repository.txx>

#endif // INTIFADA_CONVERTER_REPOSITORY_HXX
