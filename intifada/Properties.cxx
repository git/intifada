/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Properties.hxx>
#include <intifada/Logger.hxx>

#include <algorithm>

Property_Type::Property_Type()
{
	clog("Memory_Allocation") << "0x" << this << ":ctor Property_Type" << std::endl;
}

Property_Type::~Property_Type()
{
	clog("Memory_Allocation") << "0x" << this << ":dtor Property_Type" << std::endl;
}

bool Property_Type::operator!=(const std::string& rhs)const
{
  return !operator==(rhs);
}

bool Property_Type::operator>=(const std::string& rhs)const
{
  return !operator<(rhs);
}

bool Property_Type::operator<=(const std::string& rhs)const
{
  return !operator>(rhs);
}

Property_String::Property_String():str_(),default_value_(){}

Property_String::Property_String(const std::string& s,const std::string& d):
  str_(s),default_value_(d){}

Property_String::Property_String(const Property_String& rhs):
  str_(rhs.str_),default_value_(rhs.default_value_){}

Property_String& Property_String::operator=(const Property_String& rhs)
  {
    if(this!=&rhs){
      str_=rhs.str_;
      default_value_=rhs.default_value_;
    }
    return *this;
  }

Property_String& Property_String::operator=(const std::string& rhs)
  {
    str_=rhs;
    return *this;
  }

std::string Property_String::string_value()const
{
  return str_;
}

Property_String* Property_String::duplicate()const
{
  return new Property_String(str_);
}

bool Property_String::operator==(const std::string& rhs)const
{
  return str_==rhs;
}

bool Property_String::operator<(const std::string& rhs)const
{
  return str_<rhs;
}
bool Property_String::operator>(const std::string& rhs)const
{
  return str_>rhs;
}

bool Property_String::default_value()const
{
  return str_==default_value_;
}

Property_Bool::Property_Bool():v_(false),default_value_(false){}

Property_Bool::Property_Bool(const bool& v,const bool& d):v_(v),default_value_(d){}

Property_Bool::Property_Bool(const Property_Bool& rhs):v_(rhs.v_),default_value_(rhs.default_value_){}

Property_Bool& Property_Bool::operator=(const Property_Bool& rhs)
  {
    if(this!=&rhs){
      v_=rhs.v_;
      default_value_=rhs.default_value_;
    }
    return *this;
  }

Property_Bool& Property_Bool::operator=(const std::string& rhs)
  {
    v_ = to_bool(rhs);
    return *this;
  }

std::string Property_Bool::string_value()const
{
  std::ostringstream os;
  os << std::boolalpha << v_;
  return os.str();
}

Property_Bool* Property_Bool::duplicate()const
{
  return new Property_Bool(v_);
}

bool Property_Bool::operator==(const std::string& rhs)const
{
  bool nval=to_bool(rhs);
  return nval==v_;
}

bool Property_Bool::operator<(const std::string& rhs)const
{
  bool nval=to_bool(rhs);
  return nval<v_;
}



bool Property_Bool::operator>(const std::string& rhs)const
{
  bool nval=to_bool(rhs);
  return nval>v_;
}

bool Property_Bool::operator==(const bool& rhs)const
{
  return rhs==v_;
}

bool Property_Bool::operator!=(const bool& rhs)const
{
  return !this->operator==(rhs);
}

bool Property_Bool::operator==(const Property_Bool& rhs)const
{
  return v_==rhs.v_;
}

bool Property_Bool::operator!=(const Property_Bool& rhs)const
{
  return !this->operator==(rhs);
}

bool Property_Bool::default_value()const
{
  return v_==default_value_;
}
#include <iostream>
bool Property_Bool::to_bool(const std::string& sval)const
{
  bool ret=false;

  if(sval=="true"){
    ret=true;
  }else if(sval=="false"){
    ret=false;
  }else{
    int ival;
    std::istringstream is(sval);
    is >> ival;
    if(ival != 0){
      ret=true;
    }
  }

  return ret;
}

Properties::Properties():props_(){}

Properties::Properties(const Properties& rhs):props_()
    {
      std::for_each(rhs.props_.begin(),rhs.props_.end (),Duplicate(*this));
    }

Properties& Properties::operator=(const Properties& rhs)
  {
    if(this!=&rhs){
      std::for_each(props_.begin(),props_.end (),Destroy());
      props_.clear();
      std::for_each(rhs.props_.begin(),rhs.props_.end (),Duplicate(*this));
    }
    return *this;
  }

Properties::~Properties()
  {
    std::for_each(props_.begin(),props_.end (),Destroy());
    props_.clear();
  }

int Properties::register_property(const std::string& n, Property_Type*up,bool del)
  {
    prop_list_pair_type status = props_.insert(std::make_pair(n,std::make_pair(up,del)));
    int ret=0;
    if(status.second!=true){
      ret=-1;
    }
    return ret;
  }

int Properties::erase_property(const std::string& n)
  {
    prop_list_iterator_type it=props_.find(n);
    int ret=-1;
    if(it!=props_.end()){
      ret=0;
      if((*it).second.second==true){
        delete (*it).second.first;
      }

      props_.erase(it);
    }
    return ret;
  }

Property_Type& Properties::find_property(const std::string& n)
      {
        prop_list_iterator_type it=props_.find(n);
        Property_Type *ret=NULL;
        if(it!=props_.end()){
          ret=(*it).second.first;
        }else{
          std::string error="Properties::find("+n+")";
          throw std::domain_error(error);
        }
        return *ret;
      }

const Property_Type& Properties::find_property(const std::string& n)const
    {
      prop_list_const_iterator_type it=props_.find(n);
      const Property_Type *ret=NULL;
      if(it!=props_.end()){
        ret=(*it).second.first;
      }else{
        std::string error="Properties::find("+n+")";
        throw std::domain_error(error);
      }
      return *ret;
    }

void Properties::set_property(const std::string& prop,const std::string& val)
      {
        Property_Type& p=this->find_property(prop);
        // Pass here only if prop is found
        p=val;
      }

void Properties::set_property(const std::string& prop,const Property_Type& val)
  {
    Property_Type& p=this->find_property(prop);
    p=val.string_value();
  }


std::string Properties::get_property(const std::string& prop)const
      {
        const Property_Type& p=this->find_property(prop);
        // Pass here only if prop is found
        return p.string_value();
      }
void Properties::for_each_property(Property_Functor& f)const
{
  f.begin();
  std::for_each(props_.begin(),props_.end (),Property_Functor_Connector(f));
  f.end();
}

void Properties::dump(std::ostream& os)const
{
  std::for_each(props_.begin(),props_.end (),Dump(os));
}
