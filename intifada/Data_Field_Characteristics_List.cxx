/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Data_Field_Characteristics_List.hxx>
#include <intifada/Logger.hxx>
#include <cassert>
#include <sstream>

intifada::Data_Field_Characteristics_List::part_size_type intifada::Data_Field_Characteristics_List::npos=255;

intifada::Data_Field_Characteristics_List::Functor::~Functor(){}

intifada::Data_Field_Characteristics_List::Data_Field_Characteristics_List():
  repetitive_(false)
  ,extended_(false)
  ,parts_()
  ,fast_access_()
  ,insert_order_()
{}

intifada::Data_Field_Characteristics_List::Data_Field_Characteristics_List(const Data_Field_Characteristics_List& rhs):
  repetitive_(rhs.repetitive_)
  ,extended_(rhs.extended_)
  ,parts_()
  ,fast_access_(rhs.fast_access_)
  ,insert_order_(rhs.insert_order_)
  {
    uint8_t part=0;
    for(vector_map_const_iterator_type it=rhs.parts_.begin();it!=rhs.parts_.end();++it,++part)
      {
        const map &rhs_map=(*it);
        for(map_const_iterator mit=rhs_map.begin();mit != rhs_map.end();++mit)
          {
            Data_Field_Characteristics*rhs_d=(*mit).second;
            Path_Name rhs_n=(*mit).first;
            Data_Field_Characteristics*d=get_new_description();
            *d=*rhs_d;
            insert_parts(rhs_n,d,part);
          }
      }
  }

intifada::Data_Field_Characteristics_List&
intifada::Data_Field_Characteristics_List::operator=(const Data_Field_Characteristics_List& rhs)
  {
    if(this!=&rhs)
      {
        uint8_t part=0;
        for(vector_map_const_iterator_type it=rhs.parts_.begin();it!=rhs.parts_.end();++it,++part)
          {
            const map &rhs_map=(*it);
            for(map_const_iterator mit=rhs_map.begin();mit != rhs_map.end();++mit)
              {
                Data_Field_Characteristics*rhs_d=(*mit).second;
                Path_Name rhs_n=(*mit).first;
                Data_Field_Characteristics*d=get_new_description();
                *d=*rhs_d;
                insert_parts(rhs_n,d,part);
              }
          }
        repetitive_=rhs.repetitive_;
        extended_=rhs.extended_;
        fast_access_=rhs.fast_access_;
        insert_order_=rhs.insert_order_;
      }
    return *this;
  }

intifada::Data_Field_Characteristics_List::~Data_Field_Characteristics_List()
  {
    for(vector_map_const_iterator_type it=parts_.begin();it!=parts_.end();++it)
      {
        const map &todelete_map=(*it);
        for(map_const_iterator mit=todelete_map.begin();mit != todelete_map.end();++mit)
          {
            release_description((*mit).second);
          }
      }
  }


intifada::Data_Field_Characteristics*
intifada::Data_Field_Characteristics_List::get_new_description(/*const Data_Field_Characteristics::type &t*/)const
  {
	intifada::Data_Field_Characteristics* ret=new Data_Field_Characteristics;
	clog("Memory_Allocation") << "0x" << ret << ":dynamic allocation Data_Field_Characteristics" << std::endl;
	return ret;
  }

void
intifada::Data_Field_Characteristics_List::release_description(Data_Field_Characteristics* d)const
  {
	clog("Memory_Allocation") << "0x" << this << ":dynamic release Data_Field_Characteristics" << std::endl;
	delete d;
  }

void intifada::Data_Field_Characteristics_List::insert(const Path_Name& name,Data_Field_Characteristics* item_desc, const part_size_type& part)
{
  this->insert_parts(name,item_desc,part);
  fast_access_.insert(std::make_pair(name,part));
  insert_order_.push_back(std::make_pair(name,part));
}


intifada::Data_Field_Characteristics &
intifada::Data_Field_Characteristics_List::get_characteristics(const Path_Name& name,const part_size_type& part)const
{
  clog("Data_Field_Characteristics_List") << "Data_Field_Characteristics_List::get_characteristics("<<name<<","<< (int)part<< ")" << std::endl;

  // Get part map
  if(repetitive_!=true)
    {
    if(parts_.size()<=part)
      {
    	std::cerr << "1-" << name.get_full_name() << std::endl;
        throw Description_Field_Unknow_Exception(name.get_full_name());
      }
    }
  else
    {

      while(parts_.size()<=part)
        {
          parts_.push_back(map());
          map& m=parts_.back();
          // update m with part 0 value
          this->copy(m,parts_[0]);
        }
    }
    const map& m=parts_[part];

  map_const_iterator it = m.find(name);
  if(it==m.end())
    {
	  std::cerr << "2-" << name.get_full_name() << std::endl;
      throw Description_Field_Unknow_Exception(name.get_full_name());
    }
  return *(*it).second;
}

intifada::Data_Field_Characteristics &
intifada::Data_Field_Characteristics_List::get_characteristics(const Path_Name& name)const
{
  clog("Data_Field_Characteristics_List") << "Data_Field_Characteristics_List::get_characteristics("<<name<<")" << std::endl;

  const part_size_type& part=this->get_part(name);
  const map& m=parts_[part];
  map_const_iterator mit = m.find(name);
  assert(mit!=m.end());
  return *(*mit).second;
}

const intifada::Data_Field_Characteristics_List::part_size_type& intifada::Data_Field_Characteristics_List::get_part(const Path_Name& name)const
  {
    clog("Data_Field_Characteristics_List") << "Data_Field_Characteristics_List::get_part("<<name<<")" << std::endl;

      name_part_map_const_iterator_type it = this->fast_access_.find(name);

      if(it==this->fast_access_.end())
        {
    	  std::cerr << "3-" << name.get_full_name() << std::endl;
          throw Description_Field_Unknow_Exception(name.get_full_name());
        }
      // Get part
      return (*it).second;

  }

int intifada::Data_Field_Characteristics_List::foreach_structure(Data_Field_Characteristics_List::Functor&dflit)const
{
  bool transmit_part=false;
  part_size_type transmitted_part=npos;
  if(extended_==true && repetitive_==false)
    {
      transmit_part=true;
    }

  for(vector_name_part_const_iterator_type oit=insert_order_.begin();oit!=insert_order_.end();++oit)
    {
      const Path_Name& n=(*oit).first;
      const part_size_type& part=(*oit).second;
      const Data_Field_Characteristics& c=this->get_characteristics(n,part);
      if(transmit_part!=false)
        {
          transmitted_part=part;
        }
      dflit(n,c,transmitted_part);
    }
  return 0;
}

int intifada::Data_Field_Characteristics_List::foreach(Data_Field_Characteristics_List::Functor&dflit)const
{
  part_size_type part=0;

  for(vector_map_const_iterator_type it=parts_.begin();it!=parts_.end();++it,++part)
    {
      const map &todelete_map=(*it);
      for(map_const_iterator mit=todelete_map.begin();mit != todelete_map.end();++mit)
        {
          Path_Name name=(*mit).first;
          if(repetitive_==true)
            {
              std::ostringstream os;
              os << (int)part;
              name.push_back(os.str());
              name.set_name_size(2);
            }
          dflit(name,*(*mit).second,part);
        }
    }

  return 0;
}

int intifada::Data_Field_Characteristics_List::push_back()
  {
    int ret=-1;
    if(repetitive_==true)
      {
        parts_.push_back(map());
        map& m=parts_.back();
        // update m with part 0 value
        this->copy(m,parts_[0]);
        ret=0;
      }
    return ret;
  }

void intifada::Data_Field_Characteristics_List::set_repetitive()
  {
    repetitive_=true;
  }
void intifada::Data_Field_Characteristics_List::set_extended()
  {
    extended_=true;
  }

uint8_t intifada::Data_Field_Characteristics_List::copy(map&target,const map&source)const
{
  uint8_t ret=0;
  for(map_const_iterator mit=source.begin();mit != source.end();++mit,++ret)
    {
      const Data_Field_Characteristics*rhs_d=(*mit).second;
      Data_Field_Characteristics*d=get_new_description();
      Path_Name name=(*mit).first;
      *d=*rhs_d;
      d->set_null();
      map_pair p = target.insert(std::make_pair(name.get_full_name(),d));
      if(p.second!=true)
        {
          release_description(d);
          throw Description_Field_Duplication_Exception();
        }
    }
  return ret;
}

intifada::Data_Field_Characteristics_List::map_iterator intifada::Data_Field_Characteristics_List::insert_parts(const Path_Name& name,Data_Field_Characteristics* item_desc, const part_size_type& part)
{

  // Get part map
  while(parts_.size()<=part)
    {
      parts_.push_back(map());
    }
  map& m=parts_[part];
  map_pair p = m.insert(std::make_pair(name.get_full_name(),item_desc));
  if(p.second!=true)
    {
      release_description(item_desc);
      throw Description_Field_Duplication_Exception();
    }
  return p.first;
}
