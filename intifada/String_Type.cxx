/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/String_Type.hxx>

#include <sstream>
#include <limits>
#include <intifada/Logger.hxx>

/// ASCII -> ICAO
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
// 0 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20
// 1 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20
// 2 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20
// 3 0x30 0x31 0x32 0x33 0x34 0x35 0x36 0x37 0x38 0x39 0x20 0x20 0x20 0x20 0x20 0x20
// 4 0x20 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D 0x0E 0x0F
// 5 0x10 0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18 0x19 0x1A 0x20 0x20 0x20 0x20 0x20
// 6 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20
// 7 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20

const uint8_t intifada::String_Type::ascii2icao[]=
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x00,0x00,0x00,0x00,0x00,0x00
		,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F
		,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x00,0x00,0x00,0x00,0x00
		,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
/// ICAO -> ASCII
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
// 0 0x00 0x41 0x42 0x43 0x44 0x45 0x46 0x47 0x48 0x49 0x4A 0x4B 0x4C 0x4D 0x4E 0x4F
// 1 0x50 0x51 0x52 0x53 0x54 0x55 0x56 0x57 0x58 0x59 0x5A 0x20 0x20 0x20 0x20 0x20
// 2 0x20 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
// 3 0x30 0x31 0x32 0x33 0x34 0x35 0x36 0x37 0x38 0x39 0x00 0x00 0x00 0x00 0x00 0x00
const uint8_t intifada::String_Type::icao2ascii[]=
{
		0x00,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F
		,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x20,0x00,0x00,0x00,0x00
		,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x00,0x00,0x00,0x00,0x00,0x00
};

intifada::String_Type::String_Type()
:inherited("String_Type")
,code_()
,value_()
,base_value_()
,stream_coding_("ascii")
,stream_character_size_(8)
,keep_code_(false)
{
	this->register_property("stream_coding",stream_coding_);
	this->register_property("stream_character_size",stream_character_size_);
	this->register_property("keep_code",keep_code_);

}

intifada::String_Type::String_Type(const std::string& rhs)
:inherited("String_Type")
,code_()
,value_()
,base_value_()
,stream_coding_("ascii")
,stream_character_size_(8)
,keep_code_(false)
{
	this->register_property("stream_coding",stream_coding_);
	this->register_property("stream_character_size",stream_character_size_);
	this->register_property("keep_code",keep_code_);
	this->set_from_string(rhs);
}

intifada::String_Type::String_Type(const String_Type& rhs)
:inherited(rhs)
,code_(rhs.code_)
,value_(rhs.value_)
,base_value_(rhs.base_value_)
,stream_coding_(rhs.stream_coding_)
,stream_character_size_(rhs.stream_character_size_)
,keep_code_(rhs.keep_code_)
{
	this->relink_property("stream_coding",stream_coding_);
	this->relink_property("stream_character_size",stream_character_size_);
	this->relink_property("keep_code",keep_code_);

}
intifada::String_Type::String_Type(const Type&rhs)
:inherited(rhs)
{
	const String_Type& rhs_string=dynamic_cast<const String_Type&>(rhs);
	code_=rhs_string.code_;
	value_=rhs_string.value_;
	base_value_=rhs_string.base_value_;
	stream_coding_=rhs_string.stream_coding_;
	stream_character_size_=rhs_string.stream_character_size_;
	keep_code_=rhs_string.keep_code_;
	this->relink_property("stream_coding",stream_coding_);
	this->relink_property("stream_character_size",stream_character_size_);
	this->relink_property("keep_code",keep_code_);
}

intifada::String_Type& intifada::String_Type::operator=(const Type&rhs)
{

	return this->operator=(dynamic_cast<const String_Type&>(rhs));
}

intifada::String_Type& intifada::String_Type::operator=(const String_Type& rhs)
{
	if(this != &rhs){
		/*
		 * CORRECTION des suites de la FFT 13/MOE-O-IVS/11 : Mauvais encodage du ModeS ID
		 * Il ne faut pas appeler l'opérateur d'affectation de la classe mere
		 * En effet, String_Type -> Type -> Properties
		 * et toutes les Properties de l'objet affecté sont perdues
		 * Or on souhaite les conserver car ce sont celles définies par asterix.xml notamment
		 */
		//inherited::operator=(rhs);

		code_=rhs.code_;
		value_=rhs.value_;
		this->relink_property("stream_coding",stream_coding_);
		this->relink_property("stream_character_size",stream_character_size_);
		this->relink_property("keep_code",keep_code_);

	}
	return *this;
}

intifada::String_Type& intifada::String_Type::operator=(const std::string&rhs)
{
	this->set_from_string(rhs);
	return *this;
}

void intifada::String_Type::set_null()
{
	value_.clear();
}

void intifada::String_Type::reset_changed_state()
{
	base_value_=value_;
}

bool intifada::String_Type::is_changed_state()const
{
	return base_value_!=value_;
}


ssize_t intifada::String_Type::get_size()const
{
	return -1;
}

bool
intifada::String_Type::operator==(const std::string& rhs)const
{
	return trim_trailing_spaces(value_)==rhs;
}

std::string intifada::String_Type::get_to_string()const
{
	return trim_trailing_spaces(value_);
}

int intifada::String_Type::set_from_string(const std::string& v)
{
	value_=v;
	return 0;
}

void intifada::String_Type::set_from_stream(Stream::const_reverse_iterator b,int start,int size)
{
	clog("String_Type") \
			<< "set_from_stream("<< this << ":"
			<< stream_coding_.string_value() <<"):" << std::dec << start << "," << size << std::endl;

	int char_start=start-1;
	int char_total=size/static_cast<int>(stream_character_size_);
	int current_size=0;
	std::string value;
	while(current_size < char_total){

		++current_size;
		uint8_t c=0;
		inherited::streamcpy(c,b,char_start,stream_character_size_);
		clog("String_Type")
		<< "set_from_stream:loop:" << std::dec << char_start << ","
		<< static_cast<int>(stream_character_size_) <<":" << std::hex << (int)c
		<< "->" << this->internal_character_representation(c)
		<< std::endl;
		value+=this->internal_character_representation(c);
		char_start+=static_cast<int>(stream_character_size_);
	}
	value_.clear();
	for(std::string::reverse_iterator it=value.rbegin();it!=value.rend();++it){
		value_+=(*it);
	}
	clog("String_Type") << "set_from_stream:<" << value_ << ">" << std::endl;
	return;
}

void intifada::String_Type::to_stream(Stream::reverse_iterator b,int start,int size)const
{

	int char_start=start-1;
	unsigned int char_total=size/static_cast<int>(stream_character_size_);
	unsigned int current_size=0;
	std::string value;
	for(std::string::const_reverse_iterator it=value_.rbegin();it!=value_.rend();++it){
		value+=(*it);
	}
	while(current_size < char_total){
		uint8_t c=this->external_character_representation(' ');
		if(current_size < value.size()){
			c=this->external_character_representation(value[current_size]);
		}
		++current_size;

		inherited::streamcpy(b,c,char_start,stream_character_size_);

		char_start+=static_cast<int>(stream_character_size_);
	}

	return;
}

intifada::String_Type::operator std::string()
{
	return value_;
}

intifada::String_Type* intifada::String_Type::clone()const
{
	return new String_Type(*this);
}

void
intifada::String_Type::set_property(const std::string& prop,const std::string& val)
{
	this->Properties::set_property(prop,val);
	if(prop=="stream_coding"){
		if(val=="icao"){
			code_=ICAO;
		}else if(val=="ascii"){
			code_=ASCII;
		}else{
			code_=ASCII;
		}
	}
	else if(prop=="stream_character_size")
	{
		std::istringstream is(val);
		Stream::size_type new_size;
		is >> new_size;
		stream_character_size_ = new_size;
	}
}
std::string intifada::String_Type::trim_trailing_spaces(const std::string& v)const
{
	// to strip trailling spaces
	const std::string::size_type size = v.find_last_not_of(" ") + 1;
	return v.substr(0, size);
}

std::ostream& operator<<(std::ostream& os,const intifada::String_Type& str)
{
	os << str.get_to_string();
	return os;
}
