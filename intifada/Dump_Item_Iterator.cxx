/*
   Copyright (C) 2009, 2010,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Dump_Item_Iterator.hxx>
#include <intifada/Logger.hxx>
#include <cstdlib>

intifada::Dump_Item_Iterator::Dump_Item_Iterator(std::ostream& os)
:
os_(os)
,strip_null_(true)
,_display()
{

}

void intifada::Dump_Item_Iterator::dump_all()
{
	strip_null_=false;
}

void intifada::Dump_Item_Iterator::display(const intifada::Path_Name& d)
{
	_display.insert(d);
}

int intifada::Dump_Item_Iterator::operator()(
		const intifada::Stream::size_type&stream_pos
		,const intifada::Stream::size_type& stream_size
		,const intifada::Path_Name& full_name
		,intifada::Type& t)
{
	clog("Check_Item_Iterator") << "-->" << std::endl;
	intifada::Path_Name path_name=full_name.get_path();

	bool display=true;
	if(_display.empty()!=true)
	{
		display_list_const_iterator_type it=_display.find(path_name);
		if(it==_display.end())
		{
			display=false;
		}
	}

	try{
		if(display==true)
		{
			std::string value=t.get_to_string();
			// don't print if strip_null is true and value=0 s . (v-)
			// print if s- + v
			if(strip_null_!=true || (value != "0" && value !="")){
				os_ << "\t\t("<< stream_pos << ","<< stream_size << "):"<<full_name << "=" << value << std::endl;
			}
		}
	}catch(intifada::Description_Field_Unknow_Exception& ex){
		std::cerr << "FATAL:EDASTERIX: unknow field:full name:<"<<full_name<<">" << std::endl;
		std::cerr << "EDASTERIX: unknow field:path name:<"<<path_name<<">(path size="<< full_name.get_name_size() << ")" << std::endl;
		exit(EXIT_FAILURE);
	}
	return 0;
}
