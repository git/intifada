/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Exception.hxx>

intifada::Exception::Exception(){}

intifada::Exception::~Exception(){}

std::string
intifada::Exception::what()const
{
  return "you talk to me?";
}

intifada::Description_Exception::Description_Exception(){}

intifada::Description_Exception::~Description_Exception(){}

intifada::Description_Field_Duplication_Exception::Description_Field_Duplication_Exception(){}

intifada::Description_Field_Duplication_Exception::~Description_Field_Duplication_Exception(){}

intifada::Description_Field_Unknow_Exception::Description_Field_Unknow_Exception(const Path_Name& n):name_(n){}

intifada::Description_Field_Unknow_Exception::~Description_Field_Unknow_Exception(){}

const intifada::Path_Name&
intifada::Description_Field_Unknow_Exception::get_name()const
{
  return name_;
}

intifada::Parsing_Exception::Parsing_Exception(){}

intifada::Parsing_Exception::~Parsing_Exception(){}

intifada::Parsing_Unknow_Category::Parsing_Unknow_Category(int error_pos,const std::string& family,const uint8_t& c):
      error_pos_(error_pos),family_(family),category_(c){}

const std::string& intifada::Parsing_Unknow_Category::get_family()const
    {
  return family_;
    }

const uint8_t& intifada::Parsing_Unknow_Category::get_category()const
    {
  return category_;
    }

intifada::Parsing_Input_Length_Exception::Parsing_Input_Length_Exception(int waited,int received):
      inherited(waited,received)
      {}

intifada::Parsing_Input_Length_Exception::Parsing_Input_Length_Exception(int error_pos,int waited,int received):
      inherited(error_pos,waited,received)
      {}

intifada::Parsing_Input_Record_Length_Exception::Parsing_Input_Record_Length_Exception(const Path_Name& n,int received):
      inherited(-1,received),
      name_(n)
      {}
const intifada::Path_Name& intifada::Parsing_Input_Record_Length_Exception::get_name()const
    {
  return name_;
    }

intifada::Parsing_Input_Field_Specification_Exception::Parsing_Input_Field_Specification_Exception(int waited,int received):
      inherited(waited,received)
      {}

intifada::Parsing_Stream_Exception::Parsing_Stream_Exception(int error_pos,int expected,int received):
        error_pos_(error_pos)
        ,w_(expected)
        ,r_(received)
        {}
intifada::Parsing_Stream_Exception::Parsing_Stream_Exception(int expected,int received):
        error_pos_(-1)
        ,w_(expected)
        ,r_(received)
        {}

intifada::Parsing_Output_Length_Exception::Parsing_Output_Length_Exception(int waited,int received):
      inherited(waited,received)
      {}

intifada::Parsing_XML_Exception::Parsing_XML_Exception(){}

intifada::Parsing_XML_Family_Duplicate_Exception::Parsing_XML_Family_Duplicate_Exception(const std::string& family):family_(family){}

std::string intifada::Parsing_XML_Family_Duplicate_Exception::what()const
    {
  return "Parsing_XML_Family_Duplicate_Exception:"+family_+" duplicate";
    }

std::string intifada::Parsing_XML_Family_Duplicate_Exception::get_family()const
    {
  return family_;
    }

intifada::Parsing_XML_Dynamic_Type_Exception::Parsing_XML_Dynamic_Type_Exception(const std::string& type_error):type_error_(type_error){}

std::string intifada::Parsing_XML_Dynamic_Type_Exception::what()const
    {
  return "Parsing_XML_Dynamic_Type_Exception:"+type_error_;
    }
