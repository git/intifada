/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_RECORD_ITERATOR_HXX
# define INTIFADA_RECORD_ITERATOR_HXX

# include <intifada/Block.hxx>
# include <intifada/Message.hxx>
namespace intifada
{
// Base class used to walk throught an asterix message
  template <typename R_>
  class Record_Iterator_T
  {
  public:
    typedef R_ record_type;
  public:
    Record_Iterator_T(){}

    virtual ~Record_Iterator_T(){}

    // const uint8_t& cat()const {return cat_;}

  public:
    /// virtual call operator
    /**
     * Should be only called by Record_Iterator_List
     */
    virtual int operator()(uint8_t c);

  public:
    /// virtual Call operator
    /**
     * Called when a record is encountered
     *
     * \param i record pointer
     * \param b block id in message (first block in message id is 0)
     * \param r record id in block (first record in block is 0)
     */
    virtual int operator()(
			   intifada::Record_Iterator_T<R_>::record_type& i,
			   const intifada::Message::block_list_size_t& b,
			   const intifada::Block::record_list_size_t& r)=0;
  };

  typedef Record_Iterator_T<Record> Record_Iterator;
  typedef Record_Iterator_T<const Record> Record_Const_Iterator;
}

# include <intifada/Record_Iterator_T.txx>

#endif // INTIFADA_RECORD_ITERATOR_HXX
