/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Fixed_Length_Data_Field.hxx>

intifada::Fixed_Length_Data_Field::Update_Stream_Functor::Update_Stream_Functor(Stream& byte_stream)
:it_()
{
	it_=byte_stream.rbegin();
}

int intifada::Fixed_Length_Data_Field::Update_Stream_Functor::operator()(
		const Path_Name &
		,const Data_Field_Characteristics &c
		,const Data_Field_Characteristics_List::part_size_type&)
{
	if(c.is_cached()==true)
	{
		Type *t=c.get_type();
		if(t->is_changed_state()==true)
		{
			t->to_stream(it_,c.shift(),c.size());
			// now stream reflects type, so we can reset changed state
			t->reset_changed_state();
		}
	}
	return 0;
}

intifada::Fixed_Length_Data_Field::Fixed_Length_Data_Field()
:inherited()
,initial_size_()
,byte_stream_(initial_size_)
{
	this->register_property("size",initial_size_);
}



intifada::Fixed_Length_Data_Field::Fixed_Length_Data_Field(const Fixed_Length_Data_Field&rhs)
:inherited(rhs)
,initial_size_(rhs.initial_size_)
,byte_stream_(rhs.initial_size_)
{
	this->relink_property("size",initial_size_);
}

void
intifada::Fixed_Length_Data_Field::set_property(const std::string& prop,const std::string& val)
{
	this->Properties::set_property(prop,val);
	if(prop=="size")
	{
		std::istringstream is(val);
		Stream::size_type new_size;
		is >> new_size;
		byte_stream_.resize(new_size);
	}
	return;
}

intifada::Stream::size_type
intifada::Fixed_Length_Data_Field::size()const
{
	this->update_stream();
	return byte_stream_.size();
}

intifada::Data_Field_Characteristics*
intifada::Fixed_Length_Data_Field::insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size)
{
	// Get a new Data_Field_Characteristics
	Data_Field_Characteristics* f = this->get_new_structure_description(/*Data_Field_Characteristics::SINGLE*/);
	int status=f->set(type,start,size);
	if(status==0)
	{
		// Fixed length:insert to first part (0)
		this->insert_to_structure(name,f,0);
	}
	else
	{
		this->release_structure_description(f);
		f=NULL;
	}
	return f;
}

intifada::Stream::size_type intifada::Fixed_Length_Data_Field::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	// Throw an exception if reading size exceed buffer
	if(byte_stream_.size()>m)
	{
		throw Parsing_Input_Length_Exception(byte_stream_.size(),m);
	}
	intifada::Stream::size_type read_size;
	for(read_size=0;read_size<byte_stream_.size();++read_size)
	{
		byte_stream_[read_size]=s[o+read_size];
	}
	byte_stream_.set_stream_position(o,read_size);

	return read_size;
}

intifada::Stream::size_type intifada::Fixed_Length_Data_Field::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{
	// Update stream
	this->update_stream();

	intifada::Stream::size_type write_size;

	// Throw an exception if writing exceed buffer
	if( (m-o)<byte_stream_.size())
	{
		throw Parsing_Output_Length_Exception(byte_stream_.size(),m);
	}

	for(write_size=0;write_size<byte_stream_.size();++write_size)
	{
		s[o+write_size]=byte_stream_[write_size];
	}
	byte_stream_.set_stream_position(o,write_size);
	return write_size;
}

const intifada::Type& intifada::Fixed_Length_Data_Field::get_value(const Path_Name& name)const
{
	Data_Field_Characteristics &c=this->get_structure_characteristics(name);
	Type* t=c.get_type();
	if(c.is_cached()==false)
	{
		intifada::Stream::const_reverse_iterator it=byte_stream_.rbegin();
		t->set_from_stream(it,c.shift(),c.size());
		c.set_cached();
		t->reset_changed_state();
	}
	return *t;
}

intifada::Type& intifada::Fixed_Length_Data_Field::get_value(const Path_Name& name)
{
	Data_Field_Characteristics &c=this->get_structure_characteristics(name);
	Type* t=c.get_type();
	if(c.is_cached()==false)
	{
		intifada::Stream::const_reverse_iterator it=byte_stream_.rbegin();
		t->set_from_stream(it,c.shift(),c.size());
		c.set_cached();
		t->reset_changed_state();
	}
	return *t;
}

#if 0
void intifada::Fixed_Length_Data_Field::get_value(const Path_Name& name,intifada::Type& ret)const
{
	const Type *t=this->get_value(name);
	ret=*t;
	return;
}

void intifada::Fixed_Length_Data_Field::set_value(const Path_Name& name,const intifada::Type& ret)
{
	Type *t=this->get_value(name);
	*t=ret;
	return;
}
#endif

intifada::Data_Field *
intifada::Fixed_Length_Data_Field::clone()const
{
	return new Fixed_Length_Data_Field(*this);
}

int intifada::Fixed_Length_Data_Field::foreach(Data_Field::Functor &it,const Path_Name& path)
{
	this->update_stream();
	Data_Functor f(path,it,byte_stream_);
	Data_Field::foreach(f);
	return 0;
}

int intifada::Fixed_Length_Data_Field::foreach(Data_Field::Const_Functor &it,const Path_Name& path)const
{
	this->update_stream();
	Data_Const_Functor f(path,it,byte_stream_);
	Data_Field::foreach(f);
	return 0;
}

const intifada::Type& intifada::Fixed_Length_Data_Field::get_type(const Path_Name& name)const
{
	const Data_Field_Characteristics &c=this->get_structure_characteristics(name);
	const Type* t=c.get_type();
	return *t;
}

void intifada::Fixed_Length_Data_Field::update_stream()const
{
	Update_Stream_Functor f(byte_stream_);
	Data_Field::foreach(f);
	return;
}
