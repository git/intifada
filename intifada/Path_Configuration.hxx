/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_PATH_CONFIGURATION_HXX
# define INTIFADA_PATH_CONFIGURATION_HXX

# include <string>
namespace intifada
  {
    /// Handling compile stage path
    class Path_Configuration
      {
      public:
        Path_Configuration();

        virtual ~Path_Configuration();

        static Path_Configuration* instance();

        void release();

        /// datadir path
        /**
         * \return datadir path (fixed during compile stage)
         */
        const std::string& get_factory_datadir_path()const;

        /// sysconfdir path
        /**
         * \return sysconfdir path (fixed during compile stage)
         */
        const std::string& get_factory_sysconfdir_path()const;

        /// Get asterix description set at compile stage
        const std::string& get_factory_description()const;

        /// Get asterix validation description set at compile stage
        const std::string& get_factory_validation()const;

      private:
        void make_file_access(std::string& file,const std::string& p, const std::string& f)const;
      private:
        static Path_Configuration*instance_;

        const std::string datadir_;
        const std::string sysconfdir_;

        std::string description_;
        std::string validation_;
      };
  }

#endif // INTIFADA_PATH_CONFIGURATION_HXX
