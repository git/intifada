/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_DATA_FIELD_HXX
# define INTIFADA_DATA_FIELD_HXX

# include <intifada/Data.hxx>

# include <intifada/Type.hxx>
# include <intifada/Data_Field_Characteristics_List.hxx>
# include <set>

namespace intifada
{
class Structure_Functor;

class Path_Name;

/// Object functor used to link internal Data_Field_Characteristics to external view
class Structure_Functor_Updator : public Data_Field_Characteristics_List::Functor
{
public:
	Structure_Functor_Updator(Structure_Functor& f);

	virtual int operator()(const Path_Name &n,const Data_Field_Characteristics &t,const Data_Field_Characteristics_List::part_size_type& part);

private:
	Structure_Functor& f_;
};

/// Base class of all data fields format
/**
 * This class handle common fields characteristics.
 *
 * It bring an object functor class (Functor). A Functor derivated object
 * could be applied to each item composing the data field by calling
 * Data_Field::foreach() or Data_Field::foreach() const methods
 */
class Data_Field : public Data
{
public:

	/// Base class for functor
	/**
	 * This permit to select on which item inherited functor will be applied.
	 */
	class Functor_Selector
	{
	private:
		typedef std::set<Path_Name> path_name_set_type;
		typedef path_name_set_type::const_iterator path_name_set_const_iterator_type;
	public:
		Functor_Selector();
		virtual ~Functor_Selector();

		void apply_on(const Path_Name& item);

		bool is_selected(const Path_Name& item)const;

	private:
		path_name_set_type items_;
	};
	class Functor : public Functor_Selector
	{
	public:
		typedef Functor_Selector inherited;
	public:
		Functor();
		virtual ~Functor();
		virtual int operator()(const Stream::size_type& stream_pos,const Stream::size_type& stream_size,const Path_Name& name,Type& t)=0;
	};
	class Const_Functor : public Functor_Selector
	{
	public:
		typedef Functor_Selector inherited;

	public:
		Const_Functor();
		virtual ~Const_Functor();
		virtual int operator()(const Stream::size_type& stream_pos,const Stream::size_type& stream_size,const Path_Name& name,const Type& t)=0;
	};

	class Dump : public virtual Const_Functor
	{
	public:
		virtual int operator()(const Stream::size_type& stream_pos,const Stream::size_type& stream_size,const Path_Name& name,const Type& t);
	};

	public:
	typedef Data inherited;

	public:

	Data_Field();

	protected:
	Data_Field(const Data_Field& rhs);

	public:
	virtual ~Data_Field();



	/// get presence of a field
	/**
	 * \param name target item
	 * \return true if item is present in the record
	 * \return false if item isn't present in the record
	 * \throw Description_Field_Unknow_Exception if name isn't an item
	 */
	virtual bool get_presence(const Path_Name& name)const;

	/// set presence of a field
	/**
	 * \param name target item
	 * \param set presence/absence (true/false) of name
	 * \throw Description_Field_Unknow_Exception if name isn't an item
	 */
	virtual void set_presence(const Path_Name& name,bool set);

	/// get value
	/**
	 * \param name of data item to get
	 * \return a reference on type
	 */
	const Type& operator()(const Path_Name& name)const;

	/// get/set value
	/**
	 * \param name of data item to get or set
	 * \return a reference on type
	 */
	Type& operator()(const Path_Name& name);

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual const Type& get_value(const Path_Name& name)const=0;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual Type& get_value(const Path_Name& name)=0;

	/// Clone the data field
	/**
	 * Cloning is meaning only structure copy
	 * \return a freshly allocated Data_Field
	 */
	virtual Data_Field *clone()const=0;

	/// walk throught the record
	/**
	 * Call Data_Field_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Functor &it,const Path_Name& path)=0;

	/// walk throught the record
	/**
	 * Call Data_Field_Const_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a su item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Const_Functor &,const Path_Name& path)const=0;

	/// walk throught the record
	/**
	 * used to get internal types (type, size and offset)
	 * \param f functor to apply
	 * \return 0 if no errors
	 */
	int foreach_structure(Structure_Functor *f)const;


	/// get type from asterix message
	/**
	 * \param name name of data item to get type
	 * \return a reference on type
	 * See Type for details
	 *
	 */
	virtual const Type& get_type(const Path_Name& name)const=0;

	protected:
	/// getting name and part
	/**
	 * \param name obtained name from partname
	 * \param part obtained part from partname
	 * \param partname name and part 'name.part'
	 * \todo not a lot error checking level. No comment...
	 */
	uint8_t get_idx(std::string& idx)const;

	/// split tree name
	/**
	 * \param name obtained name from first partname
	 * \param part obtained part from second partname
	 * \param partname name and part 'name.name'
	 * \todo not a lot error checking level. No comment...
	 */
	// void get_name(std::string& name,std::string& part,const std::string& partname)const;

	/// Obtain new item description object
	/**
	 * if return value isn't passed to insert, it must be passed to the
	 * release_description method to release memory
	 */
	Data_Field_Characteristics* get_new_structure_description(/*const Data_Field_Characteristics::type &t*/);

	/// Release item description object
	/**
	 * \param c object to delete
	 */
	void release_structure_description(Data_Field_Characteristics *c);

	/// Item description insertion
	/**
	 * \param name path name's Data_Field_Characteristics
	 * \param item_desc item description must be obtained by get_new_description method
	 * \throw Description_Field_Duplication_Exception if name is already registered
	 * \deprecated
	 */
	void insert_to_structure(const Path_Name& name,Data_Field_Characteristics* item_desc,const intifada::Data_Field_Characteristics_List::part_size_type& part);

	/// Get a node characteristics
	/**
	 * \param type node name
	 * \return node characteristics
	 * \throw Description_Field_Unknow_Exception if node doesn't exist
	 */
	Data_Field_Characteristics& get_structure_characteristics(const Path_Name& name)const;

	/// Get a node characteristics
	/**
	 * \param type node name
	 * \param part node part
	 * \return node characteristics
	 * \throw Description_Field_Unknow_Exception if node doesn't exist
	 */
	Data_Field_Characteristics& get_structure_characteristics(const Path_Name& name,const Data_Field_Characteristics_List::part_size_type& part)const;

	/// Get a node part
	/**
	 * \param name node name
	 * \return node part
	 * \throw Description_Field_Unknow_Exception if node doesn't exist
	 */
	const Data_Field_Characteristics_List::part_size_type& get_structure_part(const Path_Name& name)const;


	void set_repetitive_characteristics();

	void set_extended_characteristics();

	int push_back_characteristics();

	int foreach_structure(Data_Field_Characteristics_List::Functor&dflit)const;

	int foreach(Data_Field_Characteristics_List::Functor&dflit)const;

	private:

	Data_Field_Characteristics_List internal_structure_;

	protected:
	Property_Bool force_presence_;
};
}
# include <intifada/Data_Field.txx>

#endif // INTIFADA_DATA_FIELD_HXX
