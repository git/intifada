/*
   Copyright (C) 2009, 2010, 2012  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USER_APPLICATION_PROFILE_HXX
# define USER_APPLICATION_PROFILE_HXX

# include <string>
# include <intifada/Map.hxx>
# include <vector>
# include <intifada/Condition_List.hxx>
# include <intifada/Path_Name.hxx>
# include <intifada/Logger.hxx>
# include <intifada/Properties.hxx>
# include <intifada/Exception.hxx>

namespace intifada
  {
    class Condition;

    class Structure_Functor;
    class Field_Specification_Functor;
    /// User application profile
    /**
     *
     */
    class User_Application_Profile : public Properties
      {
    private:

      template<typename T>
      class Condition_Pair
        {
        public:
          Condition_Pair(const Condition_List::size_type&cit,const T&i);
          Condition_Pair(const T&i);

          Condition_Pair(const Condition_Pair& rhs);
          Condition_Pair& operator=(const Condition_Pair& rhs);

          bool is_conditional()const;
          Condition_List::size_type get_condition()const;//first
          const T& get_id()const;//second
        private:
          bool conditional_;
          Condition_List::size_type cit_;
          T id_;
        };
    public:
      typedef Properties inherited;

    private:

      typedef Condition_Pair<int> int_conditional_t;
      typedef Condition_Pair<Path_Name> string_conditional_t;

      typedef std::vector<int_conditional_t > int_conditionals_t;
      typedef int_conditionals_t::iterator int_conditionals_iterator_t;
      typedef int_conditionals_t::const_iterator int_conditionals_const_iterator_t;

      typedef std::vector<string_conditional_t > string_conditionals_t;
      typedef string_conditionals_t::const_iterator string_conditionals_const_iterator_t;

      typedef Unordered_Map<Path_Name,int_conditionals_t *> string_uap_t;
      typedef string_uap_t::iterator string_uap_iterator_t;
      typedef string_uap_t::const_iterator string_uap_const_iterator_t;
      typedef string_uap_t::pair string_uap_pair_t;

      typedef Unordered_Map<int,string_conditionals_t *> int_uap_t;
      typedef int_uap_t::iterator int_uap_iterator_t;
      typedef int_uap_t::const_iterator int_uap_const_iterator_t;
      typedef int_uap_t::pair int_uap_pair_t;

    public:
      User_Application_Profile();

      User_Application_Profile(uint8_t max_size);

      virtual ~User_Application_Profile();

    public:

      int insert(int frn,const Path_Name& item,Condition_List::size_type idx);

      int insert(int frn,const Path_Name& item);

      Condition_List::size_type insert_cond(const Condition* c);

      // void set_condition_list(const Condition_List* c);

      /// Get associated numeric identifiant
      /**
       * @param item item name
       * @param msg concerned message
       * @return item numeric identifiant
       * @throw  if item name couldn't be associated
       */
      int get(const Path_Name& item,const intifada::Data_Field* msg)const;

      /// Item name by field reference number
      /**
       * \param frn concerned bit
       * \param msg concerned message
       */
      Path_Name get(int frn,const intifada::Data_Field* msg)const;

      int foreach(Structure_Functor *f)const;

      int foreach(Field_Specification_Functor& it)const;

    private:
      User_Application_Profile(const User_Application_Profile& rhs);
      User_Application_Profile& operator=(const User_Application_Profile& rhs);

      void copy(const User_Application_Profile& rhs);

      /// Translate external frn to internal uap representation
      /**
       * As a compound mode or a message mode reference presence bits as
       * differents ways, this method return same numerotation either the mode is.
       * \param er external representation (frn or sf)
       * \return uap representation
       */
      uint8_t get_uap_representation(uint8_t er)const;

    private:
      string_uap_t su_;
      int_uap_t iu_;

      Condition_List conds_;

      uint8_t max_size_;

      Property_Bool reversefrnorder_;
      };
  }

template <typename T>
intifada::User_Application_Profile::Condition_Pair<T>::Condition_Pair(const Condition_List::size_type&cit,const T&i)
:conditional_(true),
 cit_(cit),
 id_(i)
    {}

// template <typename T>
// intifada::User_Application_Profile::Condition_Pair<T>::Condition_Pair(const Condition* c,const T&i):
//   c_(c),cit_(),id_(i)
// {}

template <typename T>
intifada::User_Application_Profile::Condition_Pair<T>::Condition_Pair(const T&i)
:conditional_(false),
 cit_(0),
 id_(i)
    {
    }

template <typename T>
intifada::User_Application_Profile::Condition_Pair<T>::Condition_Pair(const Condition_Pair& rhs)
:conditional_(rhs.conditional_),
 cit_(rhs.cit_),
 id_(rhs.id_)
    {}

template <typename T>
intifada::User_Application_Profile::Condition_Pair<T>&
intifada::User_Application_Profile::Condition_Pair<T>::operator=(const Condition_Pair& rhs)
  {
    if(this!=&rhs){
      this->conditional_=rhs.conditional_;
      this->cit_=rhs.cit_;
      this->id_=rhs.id_;
    }
    return *this;
  }

template <typename T>
bool
intifada::User_Application_Profile::Condition_Pair<T>::is_conditional()const
{
  return conditional_;
}

template <typename T>
intifada::Condition_List::size_type
intifada::User_Application_Profile::Condition_Pair<T>::get_condition()const
{
  return cit_;
}

template <typename T>
const T&
intifada::User_Application_Profile::Condition_Pair<T>::get_id()const
{
  return id_;
}

#endif // USER_APPLICATION_PROFILE_HXX
