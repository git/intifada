/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <sstream>
#include <limits>
#include <intifada/Unit_Converter.hxx>

template <typename T_>
intifada::Integer_T<T_>::Integer_T(const std::string& id)
:inherited(id)
 ,value_(0)
 ,base_value_(0)
 {}

template <typename T_>
intifada::Integer_T<T_>::Integer_T(const std::string& id,const T_& i)
:inherited(id)
 ,value_(i)
 ,base_value_(i)
 {}

template <typename T_>
intifada::Integer_T<T_>::Integer_T(const Integer_T&rhs)
:inherited(rhs),
 value_(rhs.value_)
 ,base_value_(rhs.base_value_)
 {}

template <typename T_>
intifada::Integer_T<T_>&
intifada::Integer_T<T_>::operator=(const Integer_T&rhs)
{
	if(this != &rhs){
		inherited::operator=(rhs);
		value_=rhs.value_;
	}
	return *this;
}

template <typename T_>
void intifada::Integer_T<T_>::set_null()
{
	value_=0;
}

template <typename T_>
void intifada::Integer_T<T_>::reset_changed_state()
{
	base_value_=value_;
}

template <typename T_>
bool intifada::Integer_T<T_>::is_changed_state()const
{
	return value_!=base_value_;
}

template <typename T_>
ssize_t intifada::Integer_T<T_>::get_size()const
{
	return sizeof(value_)*8;
}

template <typename T_>
bool intifada::Integer_T<T_>::operator==(const std::string& rhs)const
{
	T_ cv;
	std::istringstream is(rhs);
	is >> cv;
	return cv==value_;
}

template <typename T_>
T_
intifada::Integer_T<T_>::stream_to_internal(const T_&v,int)const
{
	return v;
}

template <typename T_>
T_
intifada::Integer_T<T_>::internal_to_stream(const T_&v,int)const
{
	return v;
}

#if 0
template <typename T_>
intifada::Integer_T<T_>::Integer_T&
intifada::Integer_T<T_>::operator=(const T_& rhs)
{
	value_=rhs;
	return *this;
}
#endif

template <typename T_>
std::string
intifada::Integer_T<T_>::get_to_string()const
{
	const Unit_Converter_Base_Type<T_> *converter=dynamic_cast<const Unit_Converter_Base_Type<T_> *>(this->get_unit_converter());

	std::string ret;
	if(converter!=NULL)
	{
		ret=converter->get(value_);
	}
	else
	{
		std::ostringstream os;

		if(sizeof(value_)==1){
			os << static_cast<int>(value_);
		}else{
			os << value_;
		}
		ret=os.str();
	}
	return ret;
}

template <typename T_>
int
intifada::Integer_T<T_>::set_from_string(const std::string& v)
{
	int64_t ival;// = strtoll(v.c_str(),NULL,0);
	std::istringstream os(v);
	os >> ival;
	int ret=-1;
	if(
			(ival!=std::numeric_limits<int64_t>::max()) &&
			(ival!=std::numeric_limits<int64_t>::min())){
		ret=0;
		value_=ival;
	}
	return ret;
}

template <typename T_>
void
intifada::Integer_T<T_>::set_from_stream(Stream::const_reverse_iterator b,int start,int size)
{
	T_ value=0;
	inherited::streamcpy(value,b,start-1,size);
	value_=stream_to_internal(value,size);

	return;
}

template <typename T_>
void intifada::Integer_T<T_>::to_stream(Stream::reverse_iterator b,int start,int size)const
{
	T_ value;
	value=internal_to_stream(value_,size);

	inherited::streamcpy(b,value,start-1,size);

	return;
}

template <typename T_>
bool
intifada::Integer_T<T_>::operator==(const Integer_T& rhs)const{return value_ == rhs.value_;}
template <typename T_>
bool
intifada::Integer_T<T_>::operator==(const T_& rhs)const{return value_ == rhs;}


template <typename T_>
bool
intifada::Integer_T<T_>::operator!=(const Integer_T& rhs)const{return !operator==(rhs);}
template <typename T_>
bool
intifada::Integer_T<T_>::operator!=(const T_& rhs)const{return !operator==(rhs);}

template <typename T_>
intifada::Integer_T<T_>::operator const T_&()const {return value_;}

template <typename T_>
intifada::Integer_T<T_>& intifada::Integer_T<T_>::operator+=(const T_& rhs)
{
	value_+=rhs;
	return *this;
}

template <typename T_>
intifada::Integer_T<T_>& intifada::Integer_T<T_>::operator-=(const T_& rhs)
{
	value_-=rhs;
	return *this;
}

template <typename T_>
intifada::Integer_T<T_>& intifada::Integer_T<T_>::operator*=(const T_& rhs)
{
	value_*=rhs;
	return *this;
}

template <typename T_>
intifada::Integer_T<T_>& intifada::Integer_T<T_>::operator/=(const T_& rhs)
{
	value_/=rhs;
	return *this;
}

