/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <cassert>
#include <sstream>
#include <intifada/Logger.hxx>

intifada::Repetitive_Length_Data_Field::Update_Stream_Functor::Update_Stream_Functor(
		Stream& byte_stream
		,const Property_Uint8& part_size):
		byte_stream_(byte_stream)
,part_size_(part_size)
{}

int intifada::Repetitive_Length_Data_Field::Update_Stream_Functor::operator()(
		const Path_Name &
		,const Data_Field_Characteristics &c
		,const Data_Field_Characteristics_List::part_size_type&part)
{
	if(c.is_cached()==true)
	{
		Type *t=c.get_type();
		if(t->is_changed_state()==true)
		{
			unsigned int minsize=part_size_*(1 + part_size_);
			if(byte_stream_.size()<minsize)
			{
				byte_stream_.resize(minsize);
			}
			Stream::reverse_iterator it = byte_stream_.rend() - ((part+1)*part_size_-1) - 1;

			t->to_stream(it,c.shift(),c.size());

			// now stream reflects type, so we can reset changed state
			t->reset_changed_state();
		}
	}
	return 0;
}


intifada::Repetitive_Length_Data_Field::Repetitive_Length_Data_Field()
:inherited()
,size_(0)
,rep_(0)
,byte_stream_()
,indicator_length_(false)
{
	this->register_property("length",size_);
	this->register_property("indicatorlength",indicator_length_);
	this->set_repetitive_characteristics();
}

intifada::Repetitive_Length_Data_Field::Repetitive_Length_Data_Field(const Repetitive_Length_Data_Field &rhs)
:inherited(rhs)
//,size_(rhs.size_)
,size_(0)
,rep_(0)
,byte_stream_()
,indicator_length_(false)
{
	this->relink_property("length",size_);
	this->relink_property("indicatorlength",indicator_length_);
	this->set_repetitive_characteristics();
}

intifada::Repetitive_Length_Data_Field::~Repetitive_Length_Data_Field()
{
	//   for(data_size_type_t i=0;i<field_.size();++i){
	//     delete field_[i];
	//   }
	//   field_.clear();
}

intifada::Stream::size_type intifada::Repetitive_Length_Data_Field::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	clog("Repetitive_Length_Data_Field") << "set_stream(" << (void*) s << ",0x"<< std::hex << o <<",Ox" << std::hex << m << ")" << std::endl;
	byte_stream_.clear();

	Stream::size_type read_size=0;

	//Getting byte size to read
	if(m==0)
	{
		throw Parsing_Input_Length_Exception(0,0);
	}

	uint8_t total_size;
	if(indicator_length_==true)
	{
		// s[0] is total length
		total_size=s[o+0]-1;
		rep_=total_size/size_;
	}
	else
	{
		rep_=s[o+0];
		total_size =rep_*size_;
	}

	if(total_size>m)
	{
		throw Parsing_Input_Length_Exception(total_size,m);
	}

	// update characteristics list
	for(uint8_t i=0;i<rep_-1;++i)
	{
		Data_Field::push_back_characteristics();
	}
	++read_size;



	// fill internal stream with s
	for(uint8_t i=1;i<=total_size;++i)
	{
		++read_size;
		clog("Repetitive_Length_Data_Field") << "set_stream:0x" << std::hex << s[o+i] << std::endl;
		byte_stream_.push_back(s[o+i]);
	}
	byte_stream_.set_stream_position(o,read_size);
	return read_size;
}

intifada::Stream::size_type intifada::Repetitive_Length_Data_Field::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
{

	Stream::size_type write_size=0;

	if(m==0){
		throw Parsing_Output_Length_Exception(0,0);
	}
	this->update_stream();
	update();
	// Writing repetitive factor or item size
	if(indicator_length_==true){
		s[o+0]=byte_stream_.size()+1;
	}else{
		s[o+0]=byte_stream_.size()/size_;
		assert(rep_==s[o+0]);
	}
	++write_size;

	// Looping on sub parts
	for(uint8_t idx=0;idx < byte_stream_.size();++idx){
		++write_size;
		clog("Repetitive_Length_Data_Field") << "get_stream:0x" << std::hex << byte_stream_[idx] << std::endl;
		s[o+idx+1]=byte_stream_[idx];
	}
	byte_stream_.set_stream_position(o,write_size);
	return write_size;
}

/// Accesseur sur la taille de l'enregistrement
intifada::Stream::size_type
intifada::Repetitive_Length_Data_Field::size()const
{
	this->update_stream();
	update();
	return byte_stream_.size() + 1;
}

intifada::Stream::size_type
intifada::Repetitive_Length_Data_Field::get_part_size()const
{
	return size_;
}

intifada::Data_Field_Characteristics*
intifada::Repetitive_Length_Data_Field::insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size)
{
	Data_Field_Characteristics* f = get_new_structure_description(/*Data_Field_Characteristics::MULTIPART*/);
	int status=f->set(type,start,size/*,0*/);
	if(status==0){
		this->insert_to_structure(name,f,0);
	}else{
		this->release_structure_description(f);
		f=NULL;
	}
	return f;
}

const intifada::Type& intifada::Repetitive_Length_Data_Field::get_value(const Path_Name& n)const
{
	uint8_t part=0;
	std::string spart=n.get_back();
	part = get_idx(spart);
	Path_Name name=n;
	name.pop_back();

	const Data_Field_Characteristics &c=get_structure_characteristics(name,part);
	intifada::Type* t=c.get_type();
	if(c.is_cached()==false)
	{
		unsigned int minsize=size_*(1 + part);
		if(byte_stream_.size()<minsize)
		{
			byte_stream_.resize(minsize);
		}

		Stream::const_reverse_iterator it = byte_stream_.rend() - get_lsb_position(part) - 1;
		t->set_from_stream(it,c.shift(),c.size());

		c.set_cached();
		t->reset_changed_state();
	}
	return *t;
}

intifada::Type& intifada::Repetitive_Length_Data_Field::get_value(const Path_Name& n)
{
	uint8_t part=0;
	std::string spart=n.get_back();
	part = get_idx(spart);
	Path_Name name=n;
	name.pop_back();

	const Data_Field_Characteristics &c=get_structure_characteristics(name,part);
	intifada::Type* t=c.get_type();
	if(c.is_cached()==false)
	{
		unsigned int minsize=size_*(1 + part);
		if(byte_stream_.size()<minsize)
		{
			byte_stream_.resize(minsize);
		}

		Stream::const_reverse_iterator it = byte_stream_.rend() - get_lsb_position(part) - 1;
		t->set_from_stream(it,c.shift(),c.size());

		c.set_cached();
		t->reset_changed_state();
	}
	return *t;
}

#if 0
/// get value from asterix message
void intifada::Repetitive_Length_Data_Field::get_value(const Path_Name& n,intifada::Type& ret)const
{
	const Type *t=this->get_value(n);
	ret=*t;
	return;
}

/// set value to asterix message
void intifada::Repetitive_Length_Data_Field::set_value(const Path_Name& n,const intifada::Type& ret)
{
	Type*v=this->get_value(n);
	*v=ret;
	return;
}
#endif

intifada::Data_Field *
intifada::Repetitive_Length_Data_Field::clone()const
{
	return new Repetitive_Length_Data_Field(*this);
}

int intifada::Repetitive_Length_Data_Field::foreach(Functor &it,const Path_Name& path)
{
	this->update_stream();
	Data_Functor f(path,it,byte_stream_,size_);
	Data_Field::foreach(f);
	return 0;
}

int intifada::Repetitive_Length_Data_Field::foreach(Const_Functor &it,const Path_Name& path)const
{
	this->update_stream();
	Data_Const_Functor f(path,it,byte_stream_,size_);
	Data_Field::foreach(f);
	return 0;
}

const intifada::Type& intifada::Repetitive_Length_Data_Field::get_type(const Path_Name& n)const
{
	uint8_t part=0;
	std::string spart=n.get_back();
	part = get_idx(spart);
	Path_Name name=n;
	name.pop_back();

	const Data_Field_Characteristics &c=get_structure_characteristics(name,part);
	intifada::Type* t=c.get_type();

	return *t;
}

intifada::Stream::size_type
intifada::Repetitive_Length_Data_Field::get_lsb_position(Stream::size_type p)const
{
	return (p+1)*size_-1;
}
intifada::Stream::size_type
intifada::Repetitive_Length_Data_Field::get_msb_position(Stream::size_type p)const
{
	return size_*p;
}

void
intifada::Repetitive_Length_Data_Field::update()const
{
	if(indicator_length_!=true)
	{
		// Start with the last byte and back to the first secondary part
		// not yet the last byte
		uint8_t last_byte = byte_stream_.size();
		if(last_byte > 0){
			--last_byte;
			// now, last_byte is the index
		}else{
			return;
		}
		uint8_t last_part = get_part(last_byte);

		do{
			// Removing all null parts
			if(is_part_null(last_part)==true){
				// part is null. Remove it
				byte_stream_.pop_back(size_);
			}else{
				break;
			}
			--last_part;
		}while(last_part !=0);
		rep_=byte_stream_.size()/size_;
	}
}

bool
intifada::Repetitive_Length_Data_Field::is_part_null(uint8_t part)const
{
	Stream::size_type msb=get_msb_position(part);
	Stream::size_type lsb=get_lsb_position(part);

	// Scan every byte
	bool is_null=true;

	// loop only if byte_stream.size() >= lsb+1
	if(byte_stream_.size()>=(lsb+1)){
		for(Stream::size_type b = msb;b<=lsb;++b){
			if(byte_stream_[b]!=0){
				is_null=false;
				break;
			}
		}
	}
	return is_null;
}

uint8_t
intifada::Repetitive_Length_Data_Field::get_part(uint8_t o) const
{
	uint8_t part=0;
	if(o>=size_){
		o -=size_;
		part=o/size_;
		++part;
	}

	return part;
}

void intifada::Repetitive_Length_Data_Field::update_stream()const
{
	Update_Stream_Functor f(byte_stream_,size_);
	Data_Field::foreach(f);
	return;
}
