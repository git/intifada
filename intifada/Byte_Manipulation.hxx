/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_BYTE_MANIPULATION_HXX
# define INTIFADA_BYTE_MANIPULATION_HXX

# include <intifada/inttypes.h>
# include <intifada/build_configuration.h>

# include <stdexcept>

/// Perform basic manipulation on bytes
/**
 * This class perform basic manipulation on bytes:
 * - Read part of a byte
 * - Write part of a byte
 * This class is a singleton, as it doesn't have any private part
 *
 *
 */
namespace intifada
  {
    class Byte_Manipulation
      {

      private:
        static uint8_t mask_[8];

      protected:
        /// Constructor
        /**
         * protected constructor, as this class musn't be created by the user.
         * The only way is using static method instance
         */
        Byte_Manipulation();

        virtual ~Byte_Manipulation();
      public:

        /// Get part of a byte
        /**
         * \param b source byte
         * \param size size to read
         * \param offset offset of part to get
         * \return the byte part (shifted to right)
         * \remarks pay attention that offset + size must be less or equal to 8
         */
        static uint8_t get_byte_part(uint8_t b, uint8_t size, uint8_t offset);

        /// Set part of a byte
        /**
         * \param target target byte
         * \param source source byte
         * \param size size to write
         * \param offset offset of part to write
         */
        static void set_byte_part(uint8_t &target,uint8_t source, uint8_t size, uint8_t offset);

        /// Get size to the msb
        /**
         *
         *
         * this method return the remain size to access modulo the next entire part
         * \param size get the size to access. set the size to entire part
         * \param offset initial offset of access
         * \param max max size to read
         * \return the remain of access in case size + offset > 8 (pair::first)
         */
        static uint8_t get_access(uint8_t &size,uint8_t offset,uint8_t max);

        /// Build mask to extract byte part
        /**
         * \param size of mask (in bit)
         * \param offset offset from lower bit (most left bit)
         * \throw std::out_of_range if size+offset are out of range
         *
         */
        static uint8_t mask(uint8_t size,uint8_t offset);

      };
  }

#ifdef INTIFADA_USE_INLINE
# include<intifada/Byte_Manipulation.ixx>
#endif

#endif // INTIFADA_BYTE_MANIPULATION_HXX
