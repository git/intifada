/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_DYNAMIC_LOADER_HXX
# define INTIFADA_DYNAMIC_LOADER_HXX

// Disabled because no used today
# if 0
# include <ltdl.h>
# include <string>

namespace intifada
{
  class Dynamic_Loader
  {
  private:
    typedef void (*record_func_t)();
  public:
    Dynamic_Loader();

    virtual ~Dynamic_Loader();

    int open(const std::string& name);

  private:
    std::string dl_;

    lt_dlhandle handle_;

    lt_ptr symbols_;
  };
}
# endif

#endif // INTIFADA_DYNAMIC_LOADER_HXX
