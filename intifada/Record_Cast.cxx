/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Record_Cast.hxx>
#include <intifada/Record.hxx>

intifada::Record_Cast::Copy_Data_Field_Functor::Copy_Data_Field_Functor(intifada::Record* r):record_(r)
{

}


int intifada::Record_Cast::Copy_Data_Field_Functor::operator()(const intifada::Stream::size_type&,const intifada::Stream::size_type&,const intifada::Path_Name& full_name,intifada::Type& t)
    {
  clog("Copy_Data_Field_Functor") << "-->" << std::endl;
  intifada::Path_Name path_name=full_name.get_path();

  (*record_)(full_name)=t;
  record_->set_presence(path_name,true);
  clog("Copy_Data_Field_Functor") << full_name << "=" << t.get_to_string() << std::endl;
  clog("Copy_Data_Field_Functor") << path_name << " presence=true" << std::endl;

  clog("Copy_Data_Field_Functor") << "<--" << std::endl;
  return 0;
    }

void intifada::Record_Cast::Copy_Data_Field_Functor::set_record(intifada::Record* r){record_=r;}



intifada::Record_Cast::Record_Cast():data_items_shifts_(),data_fields_shifts_()
        {}

intifada::Record_Cast::~Record_Cast()
{}

int intifada::Record_Cast::insert_data_item_shift(const intifada::Path_Name& from,const intifada::Path_Name& to)
{
  path_name_map_iterator_type it = data_items_shifts_.insert(std::make_pair(from,to));
  int ret=0;
  if(it==data_items_shifts_.end())
    {
    ret=-1;
    }
  return ret;
}


int intifada::Record_Cast::insert_data_field_shift(const intifada::Path_Name& from,const intifada::Path_Name& to)
{
  path_name_map_iterator_type it = data_fields_shifts_.insert(std::make_pair(from,to));
  int ret=0;
  if(it==data_fields_shifts_.end())
    {
    ret=-1;
    }
  return ret;
}

void intifada::Record_Cast::cast(const Record& source,Record& target)
{

  // Handle data item shifts
  for(path_name_map_iterator_type it=data_items_shifts_.begin();it!=data_items_shifts_.end();++it)
    {
    // Assignment object
    }

  // Handle data fields shifts
  for(path_name_map_iterator_type it=data_fields_shifts_.begin();it!=data_fields_shifts_.end();++it)
    {
    const intifada::Path_Name& source_name=(*it).first;
    const intifada::Path_Name& target_name=(*it).second;
    intifada::Path_Name target_path=target_name.get_path();
    const intifada::Type& t=source(source_name);
    target(target_name)=t;
    target.set_presence(target_path,true);
    }
}
