/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_CONFIGURATION_INTERNAL_READER_HXX
# define INTIFADA_CONFIGURATION_INTERNAL_READER_HXX

# include <intifada/Configuration_Reader.hxx>
# include <intifada/Configuration_Internal_Parser.hxx>

# include <stack>
# include <vector>
# include <intifada/Map.hxx>
namespace intifada{
class Configuration_Internal_Reader:public Configuration_Reader
    {
    private:
      typedef enum{
        LONGNAME,
        TYPE,
        BYTE,
        START,
        SIZE,
        VALUE,
        FORCE,
        FRN,
        DESC,
        ID,
        CAT
      }arg_type;
    public:
        typedef std::stack<Record*> records_stack;
    private:
        typedef std::vector<std::pair<std::string,std::string> > cmds_list_type;
        typedef cmds_list_type::iterator cmds_iterator_list_type;
        typedef intifada::Map<arg_type,std::string> arg_list_type;
        typedef arg_list_type::const_iterator arg_list_const_iterator_type;
    public:
      Configuration_Internal_Reader(const std::string& ifile);
        virtual ~Configuration_Internal_Reader();
        virtual int open();
        virtual int parse(Record_Repository*rep);
        virtual void release(){return;}

    private:
      void find_arg(const arg_type& at,std::string& av)const;
      void find_arg(const arg_type& at,int& av)const;
      void find_arg(const arg_type& at,uint8_t& av)const;

      int add_arg(const arg_type& at,const std::string& av);

       int create_record_call(intifada::Record*&r,const arg_type& at,const std::string& av);
       int create_uap_call(const arg_type& at,const std::string& av);
       int create_frn_call(intifada::Record*&r,const arg_type& at,const std::string& av);
       int create_item_call(const arg_type& at,const std::string& av);
       int create_property_call(const arg_type& at,const std::string& av);
       int create_handle_parts_call(const arg_type& at,const std::string& av);


private:
    std::string ifile_;
    records_stack s_;
    cmds_list_type cmds_;
    arg_list_type args_;
    bool last_call_status_;

    intifada::Configuration_Internal_Parser parser_;

};
}
#endif  // INTIFADA_CONFIGURATION_INTERNAL_READER_HXX

