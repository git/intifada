
#ifndef EVALUATION_HXX
# define EVALUATION_HXX

# include <map>
# include <vector>
# include <ostream>
# include <iostream>
# include <stdexcept>
# include <cmath>
# include <intifada/boolean_interpreter/B_Tree.hxx>

/** Calculator context used to save the parsed expressions. This context is
 * passed along to the boolean_interpreter::Driver class and fill during parsing via bison
 * actions. */

class Evaluation
{
public:

	typedef B_Tree_Node btree_type;
	/// type of the variable storage
	typedef std::map<std::string, double> variablemap_type;

	typedef std::vector<btree_type*> expressions_type;

public:

	/// free the saved expression trees
	~Evaluation()
	{
		clear();
	}

	/// free all saved expression trees
	void	clear()
	{
		for(unsigned int i = 0; i < expressions.size(); ++i)
		{
			delete expressions[i];
		}
		expressions.clear();
	}

	/// check if the given variable name exists in the storage
	bool	exists(const std::string &varname) const
	{
		return variables.find(varname) != variables.end();
	}

	/// return the given variable from the storage. throws an exception if it
	/// does not exist.
	double	get(const std::string &varname) const
	{
		variablemap_type::const_iterator vi = variables.find(varname);
		if (vi == variables.end())
			throw(std::runtime_error("Unknown variable."));
		else
			return vi->second;
	}

	void set(const std::string &varname,const double& v)
	{
		variables[varname]=v;
	}

	void add(btree_type* tree)
	{
		expressions.push_back(tree);
	}

	int size()const
	{
		return expressions.size();
	}

	bool evaluate(int idx)
	{
		return expressions[idx]->evaluate();
	}

	void dump(int idx,std::ostream& os)
	{
		expressions[idx]->dump(os,0);
	}

private:
	/// variable storage. maps variable string to doubles
	variablemap_type variables;

	/// array of unassigned expressions found by the parser. these are then
	/// outputted to the user.
	expressions_type expressions;
};

#endif // EXPRESSION_H
