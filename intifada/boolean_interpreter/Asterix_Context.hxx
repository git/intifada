/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASTERIX_CONTEXT_HXX
# define ASTERIX_CONTEXT_HXX

# include <intifada/boolean_interpreter/Context.hxx>

namespace intifada
{
class Record_Repository;
class Type_Repository;
class Path_Name;
}

class Asterix_Context : public Context
{
public:
	Asterix_Context(const std::string& family,intifada::Record_Repository*rep,intifada::Type_Repository*types);

	virtual bool exist(const std::string& id)const;

	/// get the asterix type associed with id
	/**
	 * @param id item identification
	 * @return type identification
	 *
	 * @remark, id have to be prefixed by the category of the message: ex:1.010.SIC
	 */
	virtual B_Tree_Node* get_leaf(const std::string& id)const;

	virtual void evaluate(const std::string& id)const;

	void set_record(const intifada::Record* record);

private:
	uint8_t get_cat(const intifada::Path_Name& id)const;
	intifada::Path_Name get_item(const intifada::Path_Name& id)const;
private:
	std::string _family;
	intifada::Record_Repository *_rep;
	intifada::Type_Repository* _types;
	intifada::Record* _record;
};
#endif // ASTERIX_CONTEXT_HXX
