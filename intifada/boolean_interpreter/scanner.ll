
%{ /*** C/C++ Declarations ***/

#include <string>

#include <intifada/boolean_interpreter/scanner.h>

/* import the parser's token type into a local typedef */
typedef boolean_interpreter::Parser::token token;
typedef boolean_interpreter::Parser::token_type token_type;

/* By default yylex returns int, we use token_type. Unfortunately yyterminate
 * by default returns 0, which is not of token_type. */
#define yyterminate() return token::END

/* This disables inclusion of unistd.h, which is not available under Visual C++
 * on Win32. The C++ scanner uses STL streams instead. */
#define YY_NO_UNISTD_H

%}

/*** Flex Declarations and Options ***/

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "BooleanFlexLexer" */
%option prefix="Boolean"

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

/* no support for include files is planned */
%option yywrap nounput 

/* enables the use of start condition stacks */
%option stack

/* The following paragraph suffices to track locations accurately. Each time
 * yylex is invoked, the begin position is moved onto the end position. */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng);
%}

spaces 				[ \t\r]+
identifier 			[[:alpha:]_][[:alnum:]_. ]*
integer 			[[:digit:]]+
asterix_item 		[[:digit:]]+.[[:alnum:]_. ]+[[:alnum:]_]+
open_bracket		\(
close_bracket		\)
double_quote		\"
simple_quote		'
logical_equality	==
logical_inequality  !=
logical_and			&&
logical_or			\|\|
logical_xor			\^
logical_neg			!
lt					<
gt					>
le					<=
ge					>=
affectation			=

%% /*** Regular Expressions Part ***/

 /* code to place at the beginning of yylex() */
%{
    // reset location
    yylloc->step();
%}

 /*** BEGIN EXAMPLE - Change the example lexer rules below ***/

{integer} {
    yylval->integerVal = atoi(yytext);
    return token::INTEGER;
}

{identifier} {
    yylval->stringVal = new std::string(yytext, yyleng);
    return token::STRING;
}
{asterix_item} {
	yylval->stringVal = new std::string(yytext, yyleng);
	return token::ASTERIX_ITEM;
}

{logical_equality} {
	return token::LOGICAL_EQ;
}

{logical_inequality} {
	return token::LOGICAL_NEQ;
}

{logical_and} {
	return token::LOGICAL_AND;
}

{logical_or} {
	return token::LOGICAL_OR;
}

{logical_xor} {
	return token::LOGICAL_XOR;
}
{logical_neg} {
	return token::LOGICAL_NEG;
}
{lt} {
	return token::LT;
}
{gt} {
	return token::GT;
}
{le} {
	return token::LE;
}
{ge} {
	return token::GE;
}

{open_bracket} {
	return token::O_BRACKET;
}
{close_bracket} {
	return token::C_BRACKET;
}
{double_quote} {
	return token::DOUBLE_QUOTE;
}
{simple_quote} {
	return token::SIMPLE_QUOTE;
}
{affectation} {
	return token::AFFECTATION;
}

 /* gobble up white-spaces */
{spaces} {
    yylloc->step();
}

 /* gobble up end-of-lines */
\n {
    yylloc->lines(yyleng); yylloc->step();
    return token::EOL;
}

 /* pass all other characters up to bison */
. {
	std::cout << "flex:" << *yytext << std::endl;
    return static_cast<token_type>(*yytext);
}

 /*** END EXAMPLE - Change the example lexer rules above ***/

%% /*** Additional Code ***/

namespace boolean_interpreter {

Scanner::Scanner(std::istream* in,
		 std::ostream* out)
    : BooleanFlexLexer(in, out)
{
}

Scanner::~Scanner()
{
}

void Scanner::set_debug(bool b)
{
    yy_flex_debug = b;
}

}

/* This implementation of BooleanFlexLexer::yylex() is required to fill the
 * vtable of the class BooleanFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the Scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int BooleanFlexLexer::yylex()
{
    std::cerr << "in BooleanFlexLexer::yylex() !" << std::endl;
    return 0;
}

/* When the scanner receives an end-of-file indication from YY_INPUT, it then
 * checks the yywrap() function. If yywrap() returns false (zero), then it is
 * assumed that the function has gone ahead and set up `yyin' to point to
 * another input file, and scanning continues. If it returns true (non-zero),
 * then the scanner terminates, returning 0 to its caller. */

int BooleanFlexLexer::yywrap()
{
    return 1;
}
