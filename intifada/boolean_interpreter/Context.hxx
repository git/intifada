/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CONTEXT_HXX
# define _CONTEXT_HXX

# include <string>

class B_Tree_Node;

class Context
{
public:
	virtual ~Context();

	virtual bool exist(const std::string& id)const=0;

	virtual B_Tree_Node* get_leaf(const std::string& id)const=0;

	virtual void evaluate(const std::string& id)const=0;

};

template<typename TYPE>
class Context_T : public Context
{
public:
	virtual ~Context_T();

	virtual TYPE evaluate(const std::string& id)=0;
};

template<typename TYPE>
Context_T<TYPE>::~Context_T(){}

#endif // _CONTEXT_HXX
