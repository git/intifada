/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVALUATION_DRIVER_HXX
# define EVALUATION_DRIVER_HXX

# include <string>
# include <vector>
# include <intifada/boolean_interpreter/Evaluation.hxx>
# include <intifada/boolean_interpreter/location.hh>

namespace intifada
{
class Record_Repository;
class Type_Repository;
class Path_Name;
}

namespace boolean_interpreter
{
class Scanner;
}
class Context;

/** The Driver class brings together all components. It creates an instance of
 * the Parser and Scanner classes and connects them. Then the input stream is
 * fed into the scanner object and the parser gets it's token
 * sequence. Furthermore the driver object is available in the grammar rules as
 * a parameter. Therefore the driver class contains a reference to the
 * structure into which the parsed data is saved. */
class Evaluation_Driver
{
public:
	/// construct a new parser driver context
	Evaluation_Driver(Evaluation& calc);

	/** Invoke the scanner and parser for a stream.
	 * @param in	input stream
	 * @param sname	stream name for error messages
	 * @return		true if successfully parsed
	 */
	bool parse_stream(Context& ctx,std::istream& in,
			const std::string& sname = "stream input");

	/** Invoke the scanner and parser on an input string.
	 * @param input	input string
	 * @param sname	stream name for error messages
	 * @return		true if successfully parsed
	 */
	bool parse_string(Context& ctx,const std::string& input,
			const std::string& sname = "string stream");

	/** Invoke the scanner and parser on a file. Use parse_stream with a
	 * std::ifstream if detection of file reading errors is required.
	 * @param filename	input file name
	 * @return		true if successfully parsed
	 */
	bool parse_file(Context& ctx,const std::string& filename);

	// To demonstrate pure handling of parse errors, instead of
	// simply dumping them on the standard error output, we will pass
	// them to the driver using the following two member functions.

	/** Error handling with associated line number. This can be modified to
	 * output the error e.g. to a dialog box. */
	void error(const boolean_interpreter::location& l, const std::string& m);

	/** General error handling. This can be modified to output the error
	 * e.g. to a dialog box. */
	void error(const std::string& m);

	Evaluation& calc(){return _calc;}

	// due to bison interface, mandatory to pass an unconst pointer
	std::string* get_stream_name(){return &_stream_name;}

	boolean_interpreter::Scanner* get_lexer(){return _lexer;}

	Evaluation& get_evaluator(){return _calc;}

	void trace_scanning(bool v){_trace_scanning=v;}

	void trace_parsing(bool v){_trace_parsing=v;}


private:
	/// enable debug output in the flex scanner
	bool _trace_scanning;

	/// enable debug output in the bison parser
	bool _trace_parsing;

	/// stream name (file or input stream) used for error messages.
	std::string _stream_name;

	/** Pointer to the current lexer instance, this is used to connect the
	 * parser to the scanner. It is used in the yylex macro. */
	boolean_interpreter::Scanner* _lexer;

	/** Reference to the calculator context filled during parsing of the
	 * expressions. */
	Evaluation& _calc;
};

#endif // EVALUATION_DRIVER_HXX
