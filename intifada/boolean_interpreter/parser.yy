/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

%{ /*** C/C++ Declarations ***/
#define YYDEBUG 1
#include <string>
#include <vector>

#include <intifada/boolean_interpreter/Evaluation_Driver.hxx>
#include <intifada/boolean_interpreter/Evaluation.hxx>
#include <intifada/boolean_interpreter/Context.hxx>
#include <intifada/Path_Name.hxx>
%}

/*** yacc/bison Declarations ***/

/* Require bison 2.4 or later */
%require "2.4"

/* add debug output code to generated parser. disable this for release
 * versions. */
%debug

/* start symbol is named "start" */
%start start

/* write out a header file containing the token defines */
%defines

/* use newer C++ skeleton file */
%skeleton "lalr1.cc"

/* namespace to enclose parser in */
%name-prefix="boolean_interpreter"

/* set the parser's class identifier */
%define "parser_class_name" "Parser"

/* keep track of the current position within the input */
%locations
%initial-action
{
    // initialize the initial location object
    @$.initialize(driver.get_stream_name());
    //@$.begin.filename = @$.end.filename = &driver.get_stream_name();
};

/* The driver is passed by reference to the parser and to the scanner. This
 * provides a simple but effective pure interface, not relying on global
 * variables. */
%parse-param { class Evaluation_Driver& driver }
%parse-param { class Context& ctx }
/* verbose error messages */
%error-verbose

 /*** BEGIN EXAMPLE - Change the example grammar's tokens below ***/

%union {
    int				integerVal;
    std::string*	stringVal;
    B_Tree_Node*		tree;
}

%token					END	     0		"end of file"
%token					EOL				"end of line"
%token <integerVal> 	INTEGER			"integer"
%token <doubleVal>		DOUBLE			"double"
%token <stringVal>		STRING			"string"
%token <stringVal>		ASTERIX_ITEM	"asterix item"
%token 					O_BRACKET		"("
%token 					C_BRACKET		")"
%token 					DOUBLE_QUOTE	"\""
%token 					SIMPLE_QUOTE	"'"
%token 					LOGICAL_EQ		"=="
%token 					LOGICAL_NEQ		"!="
%token					LOGICAL_AND		"&&"
%token					LOGICAL_OR		"||"
%token					LOGICAL_XOR		"^"
%token					LOGICAL_NEG		"!"
%token					LT				"<"
%token					GT				">"
%token					LE				"<="
%token					GE				">="

%token					AFFECTATION		":="

%type <tree>	constant variable
%type <tree>	atomexpr unaryexpr relexpr eqexpr andexpr orexpr expr

%destructor { delete $$; } STRING
%destructor { delete $$; } constant variable
%destructor { delete $$; } atomexpr unaryexpr relexpr eqexpr andexpr orexpr expr

 /*** END EXAMPLE - Change the example grammar's tokens above ***/

%{

#include "scanner.h"

/* this "connects" the bison parser in the driver to the flex scanner class
 * object. it defines the yylex() function call to pull the next token from the
 * current lexer object of the driver context. */
#undef yylex
#define yylex driver.get_lexer()->lex

%}

%% /*** Grammar Rules ***/

 /*** BEGIN EXAMPLE - Change the example grammar rules below ***/

constant : INTEGER
           {
           std::cout << "pouet" << std::endl;
	       $$ = new Terminal_Node<int>($1);
	   }
         | DOUBLE_QUOTE STRING DOUBLE_QUOTE
	       {
	       $$ = new Terminal_Node<std::string>(*$2);
	       delete $2;
	       }

variable : ASTERIX_ITEM
           {
           // locate asterix type
           bool exist=ctx.exist(*$1);
	       
	       if (exist!=true) {
		   error(yyloc, std::string("Unknown asterix variable \"") + *$1 + "\"");
		   delete $1;
		   YYERROR;
	       }
	       else
	       {
	       $$ = ctx.get_leaf(*$1);
		   delete $1;
	       }
	   }

atomexpr : constant
           {
	       $$ = $1;
	   }
         | variable
           {
	       $$ = $1;
	   }
         | O_BRACKET expr C_BRACKET
           {
	       $$ = $2;
	   }

unaryexpr : atomexpr
            {
		$$ = $1;
	    }
          | LOGICAL_NEG atomexpr
            {
		$$ = new Boolean_Negation_Node($2);
	    }

relexpr : unaryexpr
				{
				$$ = $1;
				}
			| 	relexpr LT unaryexpr
				{
				$$ = new Boolean_Lesser_Than_Node($1, $3);
				}
			|	relexpr GT unaryexpr
				{
				$$ = new Boolean_Greater_Than_Node($1, $3);
				}
			| 	relexpr LE unaryexpr
				{
				$$ = new Boolean_Lesser_Equal_Node($1, $3);
				}
			|	relexpr GE unaryexpr
				{
				$$ = new Boolean_Greater_Equal_Node($1, $3);
				}
			

eqexpr : relexpr
          {
	      $$ = $1;
	  }
        | eqexpr LOGICAL_EQ relexpr
          {
          $$ = new Boolean_Equality_Node($1, $3);
	  }
	  	| eqexpr LOGICAL_NEQ relexpr
          {
	      $$ = new Boolean_No_Equality_Node($1, $3);
	  }

andexpr : eqexpr
			{
					$$ = $1;
			}
			| andexpr LOGICAL_AND eqexpr
			{
				$$ = new Boolean_And_Node($1, $3);
			}  

orexpr : andexpr
          {
	      $$ = $1;
	  }
        | orexpr LOGICAL_OR andexpr
          {
	      $$ = new Boolean_Or_Node($1, $3);
	  }
        | orexpr LOGICAL_XOR andexpr
          {
	      $$ = new Boolean_Xor_Node($1, $3);
	  }

expr	: orexpr
          {
	      $$ = $1;
	  }

assignment : STRING AFFECTATION expr
             {
		 driver.get_evaluator().set(*$1,$3->evaluate());
		 std::cout << "Setting variable " << *$1
			   << " = " << driver.get_evaluator().get(*$1) << "\n";
		 delete $1;
		 delete $3;
	     }

start	: /* empty */
        | start ';'
        | start EOL
	| start assignment ';'
	| start assignment EOL
	| start assignment END
        | start expr ';'
          {
	      driver.get_evaluator().add($2);
	  }
        | start expr EOL
          {
	      driver.get_evaluator().add($2);
	  }
        | start expr END
          {
	      driver.get_evaluator().add($2);
	  }

 /*** END EXAMPLE - Change the example grammar rules above ***/

%% /*** Additional Code ***/

void boolean_interpreter::Parser::error(const Parser::location_type& l,
			    const std::string& m)
{
    driver.error(l, m);
}
