#include <intifada/boolean_interpreter/Evaluation_Driver.hxx>

#include <fstream>
#include <sstream>

#include <intifada/boolean_interpreter/scanner.h>
#include <intifada/Record_Repository.hxx>
#include <intifada/Path_Name.hxx>
#include <intifada/Exception.hxx>
#include <intifada/Record.hxx>
#include <intifada/Type.hxx>

Evaluation_Driver::Evaluation_Driver(Evaluation& c)
:_trace_scanning(false)
,_trace_parsing(false)
,_stream_name()
,_lexer(NULL)
,_calc(c)
{}

bool Evaluation_Driver::parse_stream(Context& ctx,std::istream& in, const std::string& sname)
{
	_stream_name = sname;

	boolean_interpreter::Scanner scanner(&in);
	scanner.set_debug(_trace_scanning);
	_lexer = &scanner;

	boolean_interpreter::Parser parser(*this,ctx);
	parser.set_debug_level(_trace_parsing);
	return (parser.parse() == 0);
}

bool Evaluation_Driver::parse_file(Context& ctx,const std::string &filename)
{
	std::ifstream in(filename.c_str());
	if (!in.good()) return false;
	return parse_stream(ctx,in, filename);
}

bool Evaluation_Driver::parse_string(Context& ctx,const std::string &input, const std::string& sname)
{
	std::istringstream iss(input);
	return parse_stream(ctx,iss, sname);
}

void Evaluation_Driver::error(const boolean_interpreter::location& l,
		const std::string& m)
{
	std::cerr << l << ": " << m << std::endl;
}

void Evaluation_Driver::error(const std::string& m)
{
	std::cerr << m << std::endl;
}
