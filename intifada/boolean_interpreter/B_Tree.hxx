/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef B_TREE_HXX
# define B_TREE_HXX

# include <string>

class Context;

class B_Tree_Node
{
public:
	B_Tree_Node(const bool& terminal=false);
    virtual ~B_Tree_Node();

    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth=0)const=0;

    bool is_terminal()const;

    virtual bool operator&&(const B_Tree_Node& rhs)const{return false;}

protected:
    static std::string tabulation(unsigned int d);

private:
    bool _terminal;
};

template<typename DATA_TYPE>
class Terminal_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef const Context context_type;
public:
	/// Create a litteral Node
    explicit Terminal_Node(const DATA_TYPE& v);

    /// Create a symbolic Node
    /**
     * the litteral value of a symbolic node is grabbed when it needed
     */
    explicit Terminal_Node(const std::string& id,context_type* ctx);

    const DATA_TYPE& get_value()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;
private:
    const std::string _id;
    context_type* _ctx;
    DATA_TYPE _value;
};

class Boolean_And_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_And_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_And_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};


class Boolean_Or_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Or_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Or_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_Xor_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Xor_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Xor_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};


class Boolean_Negation_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Negation_Node(const hand_type*node);
    virtual ~Boolean_Negation_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _node;
};

class Boolean_Equality_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Equality_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Equality_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_No_Equality_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_No_Equality_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_No_Equality_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_Lesser_Than_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Lesser_Than_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Lesser_Than_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_Greater_Than_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Greater_Than_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Greater_Than_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_Lesser_Equal_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Lesser_Equal_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Lesser_Equal_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};

class Boolean_Greater_Equal_Node : public B_Tree_Node
{
public:
	typedef B_Tree_Node inherited;
	typedef B_Tree_Node hand_type;
public:
    explicit Boolean_Greater_Equal_Node(const hand_type*lhs,const hand_type*rhs);
    virtual ~Boolean_Greater_Equal_Node();


    virtual bool evaluate()const;

    virtual void dump(std::ostream &os, unsigned int depth)const;

private:
    const hand_type* _lhs;
    const hand_type* _rhs;
};


# include <intifada/boolean_interpreter/B_Tree.txx>

#endif // B_TREE_HXX
