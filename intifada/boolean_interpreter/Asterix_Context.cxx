/*
   Copyright (C) 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/boolean_interpreter/Asterix_Context.hxx>

#include <intifada/Record_Repository.hxx>
#include <intifada/Record.hxx>
#include <intifada/Integer.hxx>
#include <intifada/String_Type.hxx>
#include <intifada/boolean_interpreter/B_Tree.hxx>

Asterix_Context::Asterix_Context(const std::string& family,intifada::Record_Repository*rep,intifada::Type_Repository*types)
:_family(family)
,_rep(rep)
,_types(types)
,_record(NULL)
{}

bool Asterix_Context::exist(const std::string& id)const
{
	uint8_t cat=this->get_cat(id);
	if(cat==0)
		return false;
	const intifada::Record *record=_rep->get_pointer(_family,cat);
	if(record==NULL)
		return false;
	intifada::Path_Name item=this->get_item(id);
	return record->exist_item(item);
}

B_Tree_Node* Asterix_Context::get_leaf(const std::string& id)const
{
	uint8_t cat=this->get_cat(id);

	if(cat==0)
	{
		throw intifada::Parsing_Unknow_Category(0,_family,cat);
	}

	const intifada::Record *record=_rep->get_pointer(_family,cat);
	intifada::Path_Name item=this->get_item(id);
	const intifada::Type& t=record->get_type(item);

	std::string type_id=t.get_name();

	if(type_id=="uint8_T")
		return new Terminal_Node<intifada::uint8_T>(item.get_full_name(),this);
	if(type_id=="int8_T")
		return new Terminal_Node<intifada::int8_T>(item.get_full_name(),this);
	if(type_id=="uint16_T")
		return new Terminal_Node<intifada::uint16_T>(item.get_full_name(),this);
	if(type_id=="int16_T")
		return new Terminal_Node<intifada::int16_T>(item.get_full_name(),this);
	if(type_id=="uint32_T")
		return new Terminal_Node<intifada::uint32_T>(item.get_full_name(),this);
	if(type_id=="int32_T")
		return new Terminal_Node<intifada::int32_T>(item.get_full_name(),this);
	if(type_id=="uint64_T")
		return new Terminal_Node<intifada::uint64_T>(item.get_full_name(),this);
	if(type_id=="int64_T")
		return new Terminal_Node<intifada::int64_T>(item.get_full_name(),this);
	if(type_id=="bool_T")
		return new Terminal_Node<intifada::bool_T>(item.get_full_name(),this);
	if(type_id=="String_Type")
		return new Terminal_Node<intifada::String_Type>(item.get_full_name(),this);

	return NULL;
}

void Asterix_Context::evaluate(const std::string& id)const
{

}

void Asterix_Context::set_record(const intifada::Record* record)
{
	_record=record;
}

uint8_t Asterix_Context::get_cat(const intifada::Path_Name& id)const
{
	int cat=0;
	std::istringstream is(id.get_front());
	is >> cat;
	return cat;
}

intifada::Path_Name Asterix_Context::get_item(const intifada::Path_Name& id)const
{
	intifada::Path_Name item=id;
	std::string scat=item.get_front();
	item.pop_front();
	return item;
}
