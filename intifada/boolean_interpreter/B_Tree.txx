#include <iostream>

template<typename DATA_TYPE>
Terminal_Node<DATA_TYPE>::Terminal_Node(const DATA_TYPE& v)
:inherited(true)
 ,_ctx(NULL)
 ,_value(v)
 {}

template<typename DATA_TYPE>
Terminal_Node<DATA_TYPE>::Terminal_Node(const std::string& id,context_type* ctx)
:inherited(true)
 ,_id(id)
 ,_ctx(ctx)
 ,_value()
 {}

template<typename DATA_TYPE>
const DATA_TYPE& Terminal_Node<DATA_TYPE>::get_value()const
{
	if(_ctx!=NULL)
	{
		_value=_ctx.evaluate(_id);
	}
	return _value;
}

template<typename DATA_TYPE>
void
Terminal_Node<DATA_TYPE>::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth) << _value << std::endl;
	return;
}

