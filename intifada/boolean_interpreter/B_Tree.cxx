
#include <intifada/boolean_interpreter/B_Tree.hxx>
#include <typeinfo>

B_Tree_Node::B_Tree_Node(const bool& terminal)
:_terminal(terminal)
{}


B_Tree_Node::~B_Tree_Node(){}


bool B_Tree_Node::evaluate()const
{
	return false;
}


std::string
B_Tree_Node::tabulation(unsigned int d)
{
	return std::string(d, '\t');
}


bool B_Tree_Node::is_terminal()const
{
	return _terminal;
}



Boolean_And_Node::Boolean_And_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_And_Node::~Boolean_And_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_And_Node::evaluate()const
{
//	if(_lhs->is_terminal()!=true && _rhs->is_terminal()!=true)
//	{
//		return _lhs->evaluate() && _rhs->evaluate();
//	}
//
//	if(_lhs->is_terminal()==true && _rhs->is_terminal()!=true)
//	{
//		return (*_lhs)&&_rhs->evaluate();
//	}
//	if(_lhs->is_terminal()!=true && _rhs->is_terminal()==true)
//	{
//
//		return (*_rhs)&&_lhs->evaluate();
//	}
//	if(_lhs->is_terminal()==true && _rhs->is_terminal()==true)
//		{
//
//			return (*_lhs)&&(*_rhs);
//		}


	//	if(_lhs->is_terminal()==true && _rhs->is_terminal()==true)
	//	{
	//		return _lhs->get_value() && _rhs->get_value();
	//	}
	return _lhs->evaluate() && _rhs->evaluate();
}


void Boolean_And_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"&& (boolean and)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}



Boolean_Or_Node::Boolean_Or_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Or_Node::~Boolean_Or_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Or_Node::evaluate()const
{
	return _lhs->evaluate() || _rhs->evaluate();
}


void Boolean_Or_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"|| (boolean or)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

Boolean_Xor_Node::Boolean_Xor_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Xor_Node::~Boolean_Xor_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Xor_Node::evaluate()const
{
	return _lhs->evaluate()^_rhs->evaluate();
}


void Boolean_Xor_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"^ (boolean xor)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}


Boolean_Negation_Node::Boolean_Negation_Node(const hand_type*node)
:inherited()
,_node(node)
{}

Boolean_Negation_Node::~Boolean_Negation_Node()
{
	delete _node;
}

bool Boolean_Negation_Node::evaluate()const
{
	return !_node->evaluate();
}


void Boolean_Negation_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"! (boolean negation)"<<std::endl;
	_node->dump(os,depth+1);
}


Boolean_Equality_Node::Boolean_Equality_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Equality_Node::~Boolean_Equality_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Equality_Node::evaluate()const
{
	return _lhs->evaluate()==_rhs->evaluate();
}


void Boolean_Equality_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"== (boolean equality)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}


Boolean_No_Equality_Node::Boolean_No_Equality_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_No_Equality_Node::~Boolean_No_Equality_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_No_Equality_Node::evaluate()const
{
	return _lhs->evaluate()!=_rhs->evaluate();
}


void Boolean_No_Equality_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"!= (boolean no equality)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

Boolean_Lesser_Than_Node::Boolean_Lesser_Than_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Lesser_Than_Node::~Boolean_Lesser_Than_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Lesser_Than_Node::evaluate()const
{
	return _lhs->evaluate()<_rhs->evaluate();
}


void Boolean_Lesser_Than_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"< (lesser than)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

Boolean_Greater_Than_Node::Boolean_Greater_Than_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Greater_Than_Node::~Boolean_Greater_Than_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Greater_Than_Node::evaluate()const
{
	return _lhs->evaluate()>_rhs->evaluate();
}


void Boolean_Greater_Than_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"> (greater than)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

Boolean_Lesser_Equal_Node::Boolean_Lesser_Equal_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Lesser_Equal_Node::~Boolean_Lesser_Equal_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Lesser_Equal_Node::evaluate()const
{
	return _lhs->evaluate()<_rhs->evaluate();
}


void Boolean_Lesser_Equal_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<"<= (lesser than or equal)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

Boolean_Greater_Equal_Node::Boolean_Greater_Equal_Node(const hand_type*lhs,const hand_type*rhs)
:inherited()
,_lhs(lhs)
,_rhs(rhs)
{}

Boolean_Greater_Equal_Node::~Boolean_Greater_Equal_Node()
{
	delete _lhs;
	delete _rhs;
}

bool Boolean_Greater_Equal_Node::evaluate()const
{
	return _lhs->evaluate()>_rhs->evaluate();
}


void Boolean_Greater_Equal_Node::dump(std::ostream &os, unsigned int depth)const
{
	os << B_Tree_Node::tabulation(depth)<<">= (greater than or equal)"<<std::endl;
	_lhs->dump(os,depth+1);
	_rhs->dump(os,depth+1);
}

