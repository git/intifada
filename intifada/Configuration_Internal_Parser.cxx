/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_Internal_Parser.hxx>
#include <sstream>
#include <stdlib.h>

intifada::Configuration_Internal_Parser::Configuration_Internal_Parser():keys_(),key_(),arg_(),arg_array_(){}

intifada::Configuration_Internal_Parser::~Configuration_Internal_Parser(){}

int intifada::Configuration_Internal_Parser::add_key(const std::string& key,const arg_type& type)
{
  id_list_pair_type p=keys_.insert(std::make_pair(key,type));
  int ret=-1;
  if(p.second!=false)
    {
    ret=0;
    }
  return ret;
}
intifada::Configuration_Internal_Parser::arg_type intifada::Configuration_Internal_Parser::get_arg_type(const std::string& key)const
    {
  id_list_const_iterator_type it=keys_.find(key);
  intifada::Configuration_Internal_Parser::arg_type ret=UNKNOW;
  if(it!=keys_.end())
    {
    ret=(*it).second;
    }
  return ret;
    }

std::istream& intifada::Configuration_Internal_Parser::parse(std::istream& is)
{
  uint8_t c;
  key_.clear();
  std::string arg;
  while(is)
    {
    // get key
    c=is.get();
    if(c=='/')
      {
      uint8_t next=is.peek();
      if(next=='/')
        {
        return is;
        }
      }
    if(c=='\n')
      {
      return is;
      }

    if(c!='=')
      {
      key_+=c;
      }
    else
      {
      arg_.clear();
      arg_array_.clear();
      this->remove_begin_spaces(key_);
      this->remove_end_spaces(key_);
      switch(this->get_arg_type(key_))
      {
      case UNKNOW:
      case STRING:
      case INTEGER:
        this->parse_string(is,arg_);
        break;
      case INTEGER_ARRAY:
        this->parse_array(is,arg_array_);
        break;
      default:
        this->parse_string(is,arg_);
      }
      return is;
      }
    }
  return is;
}

const std::string& intifada::Configuration_Internal_Parser::get_key()const
    {
  return key_;
    }

void intifada::Configuration_Internal_Parser::get_arg(std::string& av)const
    {
  av=arg_;
    }
void intifada::Configuration_Internal_Parser::get_arg(int& av)const
    {
  std::istringstream is(arg_);
  is >> this->get_base(arg_);
  is >> av;
    }
void intifada::Configuration_Internal_Parser::get_arg(int_array_type& array)const
    {
  for(int_array_type::size_type idx=0;idx<arg_array_.size();++idx)
    {

    std::istringstream is(arg_array_[idx]);
    int value;
    is >> this->get_base(arg_array_[idx]);
    is >> value;
    array.push_back(value);
    }
    }
void intifada::Configuration_Internal_Parser::remove_begin_spaces(std::string& n)const
    {
  std::string::size_type space_pos=0;
  do
    {
    // remove first spaces
    space_pos=n.find_first_of(" \t");
    if(space_pos==0)
      {
      // remove first space
      n.erase(0,1);
      }
    }while(space_pos==0);
    }

void intifada::Configuration_Internal_Parser::remove_end_spaces(std::string& n)const
    {
  std::string::size_type space_pos=0;
    do
      {
      // remove first spaces
      space_pos=n.find_first_of(" \t");
      if(space_pos!=std::string::npos)
        {
          ++space_pos;
        }
      if(space_pos==n.size())
        {
        // remove first space
        n.erase(n.size()-1,1);
        }
      }while(space_pos==n.size());
    }


intifada::Configuration_Internal_Parser::base_func_type intifada::Configuration_Internal_Parser::get_base(const std::string& num)const
    {
  // eat spaces in front of string
  std::string n=num;
  this->remove_begin_spaces(n);

  // Now if next character is 0 assume is oct
  // if next are 0x (or 0X) assume is hex
  // else assume is dec
  base_func_type ret=std::dec;
  if(n.compare(0,2,"0x")==0 || n.compare(0,2,"0X")==0)
    {
    ret=std::hex;
    }
  else if(n.compare(0,1,"0")==0)
    {
    ret=std::oct;
    }
  return ret;
    }
#if 0
void intifada::Configuration_Internal_Parser::find_arg(const arg_type& at,std::string& av)const
    {
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
    av=(*it).second;
    }
  else
    {
    throw std::domain_error("no arg");
    }

  return;
    }

void intifada::Configuration_Internal_Parser::find_arg(const arg_type& at,int& av)const
    {
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
    std::string av_string=(*it).second;
    std::istringstream is((*it).second);
    is >> av;
    }
  else
    {
    throw std::domain_error("no arg");
    }

  return;
    }

void intifada::Configuration_Internal_Parser::find_arg(const arg_type& at,uint8_t& av)const
    {
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
    std::string av_string=(*it).second;
    std::istringstream is((*it).second);
    uint16_t av16;
    is >> av16;
    av=av16;
    }
  else
    {
    throw std::domain_error("no arg");
    }

  return;
    }
#endif

std::istream& intifada::Configuration_Internal_Parser::parse_string(std::istream& is,std::string& val)const
    {
  uint8_t c;
  val.clear();
  while(is)
    {
    // get arg value
    c=is.get();
    if(c=='\n')
      {
      return is;
      }
    val+=c;
    }
  return is;
    }

std::istream& intifada::Configuration_Internal_Parser::parse_array(std::istream& is,string_array_type& val)const
    {
  // an array begin with a { and end with a }
  bool in_array=false;
  bool encountered_array=false;
  bool in_comment=false;
  uint8_t c;
  std::string part;
  while(is)
    {
    c=is.get();
    if(c=='/')
      {
      uint8_t next=is.peek();
      if(next=='/')
        {
        in_comment=true;
        }
      }
    else if(c=='\n')
      {
      if(in_comment==true)
        {
        in_comment=false;
        }
      if(in_array==false && encountered_array==true)
        {
        return is;
        }
      }
    else if(c=='{' && in_comment==false)
      {
      in_array=true;
      encountered_array=true;
      }
    else if(c=='}' && in_comment==false)
      {
      in_array=false;
      if(part.empty()!=true)
        {
        val.push_back(part);
        }
      }
    else if(in_comment==false && in_array==true)
      {
      // We are in the array and outside a comment
      if(c==',')
        {
        val.push_back(part);
        part.clear();
        }
      else
        {
        part+=c;
        }

      }
    }
  return is;
    }
