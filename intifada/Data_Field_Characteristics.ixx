/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Type.hxx>

INTIFADA_INLINE
bool intifada::Data_Field_Characteristics::is_cached()const
{
  return cached_;
}
INTIFADA_INLINE
void intifada::Data_Field_Characteristics::set_cached()const
  {
    cached_=true;
  }

INTIFADA_INLINE
uint8_t
intifada::Data_Field_Characteristics::size()const
{
  return size_;
}

INTIFADA_INLINE
uint8_t
intifada::Data_Field_Characteristics::shift()const
{
  return start_position_;
}

INTIFADA_INLINE
intifada::Type*
intifada::Data_Field_Characteristics::get_type()const
{
  return ti_;
}

INTIFADA_INLINE
void
intifada::Data_Field_Characteristics::set_null()
  {
    ti_->set_null();
  }
