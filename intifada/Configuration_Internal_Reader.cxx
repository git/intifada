/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_Internal_Reader.hxx>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <intifada/Logger.hxx>

intifada::Configuration_Internal_Reader::Configuration_Internal_Reader(const std::string& ifile):
  ifile_(ifile)
  ,s_()
  ,cmds_()
  ,args_()
  ,last_call_status_(true)
  ,parser_()
  {}
intifada::Configuration_Internal_Reader::~Configuration_Internal_Reader()
  {this->release();}

int intifada::Configuration_Internal_Reader::open()
  {
    std::ifstream file(ifile_.c_str());
    int opened=-1;
    while(file)
      {
        opened=0;
        parser_.parse(file);
        const std::string& key=parser_.get_key();
        std::string arg;
        parser_.get_arg(arg);
        if(key.empty()!=true)
        {
          cmds_.push_back(std::make_pair(key,arg));
        }
      }
    return opened;
  }

int intifada::Configuration_Internal_Reader::parse(Record_Repository*rep)
  {
    this->set_repository(rep);
    intifada::Record*r;
    for(cmds_iterator_list_type it=cmds_.begin();it!=cmds_.end();++it)
      {
        int call_status=0;
        if((*it).first=="create_family")
          {
            clog("Configuration_Internal_Reader") << "this->create_family(\""<<(*it).second<<"\")"<<std::endl;
            this->create_family((*it).second);
          }
        else if((*it).first=="create_family_id")
          {
            clog("Configuration_Internal_Reader") << "this->create_family_id(\""<<(*it).second<<"\")"<<std::endl;
            this->create_family_id((*it).second);
          }
        else if((*it).first=="create_record_desc")
          {
            call_status=this->create_record_call(r,DESC,(*it).second);
          }
        else if((*it).first=="create_record_id")
          {
            call_status=this->create_record_call(r,ID,(*it).second);
          }
        else if((*it).first=="create_record_cat")
          {
            call_status=this->create_record_call(r,CAT,(*it).second);
          }
        else if((*it).first=="push_record")
          {
            s_.push(r);
          }
        else if((*it).first=="pop_record")
          {
            if(s_.empty()!=true){
              r=s_.top();
              s_.pop();
            }
          }
        else if((*it).first=="end_compound")
          {
            end_record();
          }
        else if((*it).first=="create_uap_id")
          {
            call_status=this->create_uap_call(ID,(*it).second);
          }
        else if((*it).first=="create_uap_frn")
          {
            call_status=this->create_uap_call(FRN,(*it).second);
          }
        else if((*it).first=="create_frn")
          {
            call_status=this->create_frn_call(r,FRN,(*it).second);
          }
        else if((*it).first=="create_frn_id")
          {
            call_status=this->create_frn_call(r,ID,(*it).second);
          }
        else if((*it).first=="create_item_longname")
          {
            call_status=this->create_item_call(LONGNAME,(*it).second);
          }
        else if((*it).first=="create_item_reference")
          {
            call_status=this->create_item_call(ID,(*it).second);
          }
        else if((*it).first=="create_item_force")
          {
            call_status=this->create_item_call(FORCE,(*it).second);
          }
        else if((*it).first=="create_fixed")
          {
            this->push_element("datatype");
            this->create_fixed(r);
          }
        else if((*it).first=="create_extended")
          {
            this->push_element("datatype");
            this->create_extended(r);
          }
        else if((*it).first=="create_repetitive")
          {
            this->push_element("datatype");
            this->create_repetitive(r);
          }
        else if((*it).first=="create_compound_size")
          {

            this->push_element("datatype");
            std::istringstream is((*it).second);
            int csize;
            is >> csize;
            this->create_compound(r,csize);
          }
        else if((*it).first=="property_name")
          {
            call_status=this->create_property_call(ID,(*it).second);
          }
        else if((*it).first=="property_value")
          {
            call_status=this->create_property_call(VALUE,(*it).second);
          }
        else if((*it).first=="handle_parts_full_name")
          {
            this->push_element("part");
            call_status=this->create_handle_parts_call(LONGNAME,(*it).second);
          }
        else if((*it).first=="handle_parts_type")
          {
            call_status=this->create_handle_parts_call(TYPE,(*it).second);
          }
        else if((*it).first=="handle_parts_byte")
          {
            call_status=this->create_handle_parts_call(BYTE,(*it).second);
          }
        else if((*it).first=="handle_parts_start")
          {
            call_status=this->create_handle_parts_call(START,(*it).second);
          }
        else if((*it).first=="handle_parts_size")
          {
            call_status=this->create_handle_parts_call(SIZE,(*it).second);
          }
        else
          {
            std::cerr << "unknow command:<" <<  (*it).first << ":" << (*it).second << ">" << std::endl;
          }
      }
    return 0;
  }

void intifada::Configuration_Internal_Reader::find_arg(const arg_type& at,std::string& av)const
{
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
      av=(*it).second;
    }
  else
    {
      throw std::domain_error("no arg");
    }

  return;
}

void intifada::Configuration_Internal_Reader::find_arg(const arg_type& at,int& av)const
{
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
      std::string av_string=(*it).second;
      std::istringstream is((*it).second);
      is >> av;
    }
  else
    {
      throw std::domain_error("no arg");
    }

  return;
}

void intifada::Configuration_Internal_Reader::find_arg(const arg_type& at,uint8_t& av)const
{
  arg_list_const_iterator_type it = args_.find(at);
  if(it!=args_.end())
    {
      std::string av_string=(*it).second;
      std::istringstream is((*it).second);
      uint16_t av16;
      is >> av16;
      av=av16;
    }
  else
    {
      throw std::domain_error("no arg");
    }

  return;
}

int intifada::Configuration_Internal_Reader::add_arg(const arg_type& at,const std::string& av)
  {
    int ret=0;
    if(args_.empty()==true)
      {
        if(last_call_status_==false)
          {
            ret=-1;
          }
        else
          {
            last_call_status_=false;
          }
      }
    if(ret!=-1)
      {
        args_.insert(std::make_pair(at,av));
      }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_record_call(intifada::Record*&r,const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==3)
        {
          try
          {
            std::string desc;
            std::string id;
            uint8_t cat;

            this->find_arg(DESC,desc);
            this->find_arg(ID,id);
            this->find_arg(CAT,cat);
            clog("Configuration_Internal_Reader") << "this->create_record(r,\""<<desc<<"\",\""<<id<<"\"," << static_cast<int>(cat)<<")"<<std::endl;
            this->create_record(r,desc,id,cat);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_uap_call(const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==2)
        {
          try
          {
            std::string id;
            std::string frn;
            this->find_arg(ID,id);
            this->find_arg(FRN,frn);
            this->create_uap(id,frn);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_frn_call(intifada::Record*&r,const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==2)
        {
          try
          {
            uint8_t frn;
            std::string id;

            this->find_arg(FRN,frn);
            this->find_arg(ID,id);
            this->create_frn(r,frn,id);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_item_call(const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==3)
        {
          try
          {
            std::string longname;
            std::string id;
            std::string force;

            this->find_arg(LONGNAME,longname);
            this->find_arg(ID,id);
            this->find_arg(FORCE,force);
            this->create_item(longname,id,force);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_property_call(const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==2)
        {
          try
          {
            std::string id;
            std::string value;

            this->find_arg(ID,id);
            this->find_arg(VALUE,value);
            this->set_item_property(id,value);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

int intifada::Configuration_Internal_Reader::create_handle_parts_call(const arg_type& at,const std::string& av)
  {
    int ret=this->add_arg(at,av);
    if(ret!=-1){
      if(args_.size()==5)
        {
          try
          {
            std::string longname;
            std::string type;
            int byte;
            int start;
            int size;

            this->find_arg(LONGNAME,longname);
            this->find_arg(TYPE,type);
            this->find_arg(BYTE,byte);
            this->find_arg(START,start);
            this->find_arg(SIZE,size);

            this->handle_parts(longname,type,byte,start,size);
            last_call_status_=true;
            args_.clear();
          }
          catch(std::domain_error& ex)
            {
              ret=-1;
            }
        }
    }
    return ret;
  }

