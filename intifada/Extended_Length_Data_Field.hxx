/*
   Copyright (C) 2009, 2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_EXTENDED_LENGTH_DATA_FIELD_HXX
# define INTIFADA_EXTENDED_LENGTH_DATA_FIELD_HXX

# include <intifada/Data_Field.hxx>
# include <intifada/Data_Field_Characteristics.hxx>

namespace intifada
{

/// Extended length data field
/**
 * Extended length shall comprise a predetermined length primary part
 * followed by predetermined length secondary parts. Presence of next part
 * is determined by the low significant byte of the previous part (the
 * Field Extension Indicator, FX)
 *
 * field properties:
 * - repetitive:reflecting if extended field is repetitive
 * - primary_size:size in bytes of the first part field
 * - secondaries_size:size in bytes of the secondaries parts field (ignored if repetitive is set)
 */
class Extended_Length_Data_Field : public Data_Field
{
public:
	template <typename F>
	class Data_Functor_T : public Data_Field_Characteristics_List::Functor
	{
	public:
		Data_Functor_T(const Path_Name& n,F &it,Stream& byte_stream,const Property_Uint8& primary,const Property_Uint8& secondaries);
		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		const Path_Name& n_;
		F &it_;
		Stream& byte_stream_;
		intifada::Stream::size_type stream_pos_;
		intifada::Stream::size_type stream_size_;

		const Property_Uint8& primary_;
		const Property_Uint8& secondaries_;
	};

	class Update_Stream_Functor : public Data_Field_Characteristics_List::Functor
	{
	public:
		Update_Stream_Functor(Stream& byte_stream,const Property_Uint8& primary,const Property_Uint8& secondaries);

		virtual int operator()(
				const Path_Name &n
				,const Data_Field_Characteristics &t
				,const Data_Field_Characteristics_List::part_size_type& part);
	private:
		Stream& byte_stream_;
		intifada::Stream::size_type stream_pos_;
		intifada::Stream::size_type stream_size_;

		const Property_Uint8& primary_;
		const Property_Uint8& secondaries_;
	};

	public:
	typedef Data_Field inherited;
	typedef Data_Functor_T<Data_Field::Functor> Data_Functor;
	typedef Data_Functor_T<Data_Field::Const_Functor> Data_Const_Functor;
	public:

	Extended_Length_Data_Field();

	/// Constructor for extended length data field
	/**
	 * \param k primary part size
	 * \param i secondaries part size
	 */
	 // Extended_Length_Data_Field(Stream::size_type k,Stream::size_type i);

	/// Constructor for repetitive extended length data field
	/**
	 * \param k primary part size
	 */
	 // Extended_Length_Data_Field(Stream::size_type k);

	private:
	Extended_Length_Data_Field(const Extended_Length_Data_Field &rhs);

	public:

	/// add flags
	/**
	 * Virtual to handling automatic set flags
	 */
	virtual void set_property(const std::string& prop,const std::string& val);

	virtual void set_property(const std::string& prop,const Property_Type& val);

	/// get field size
	/**
	 * \return size of field
	 */
	virtual Stream::size_type size()const;

	/// get primary size
	/**
	 * \return primary size
	 */
	uint8_t primary_size()const;

	/// get secondaries size
	/**
	 * \return secondaries size
	 */
	uint8_t secondaries_size()const;

	/// get repetitive status
	/**
	 * \return true if repetitive
	 */
	// bool is_repetitive()const;

	/// association between a name, a field type and a position in a stream
	/**
	 * \param name data name
	 * \param type data type
	 * \param start in the record, the begining of the data (begin at one at the lsb)
	 * \param size data size
	 * \param part associated part of data field
	 */
	Data_Field_Characteristics* insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size,uint8_t part);

	/// association between a name, a repetitive field type and a position in a stream
	/**
	 * \param name data name
	 * \param type data type
	 * \param start in the record, the begining of the data (begin at one at the lsb)
	 * \param size data size
	 */
	// void insert(const std::string& name,const std::string& type,uint8_t start, uint8_t size);

	/// Byte stream acquisition
	/**
	 * Handle acquisition of asterix binary stream
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return readed total bytes
	 * \throw Parsing_Input_Length_Exception if bytes to read exceed m
	 * \throw Description_Field_Unknow_Exception if an unknow field is encountered
	 */
	virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

	/// Byte stream restitution
	/**
	 * Handle byte stream restitution.
	 * \param s stream pointer
	 * \param o stream offset
	 * \param m maximum stream size
	 * \return writed total bytes
	 * \throw Parsing_Output_Length_Exception if bytes to read exceed m
	 */
	virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual const Type& get_value(const Path_Name& name)const;

	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \return a reference on filled value type
	 * \remarks use it when you dont't know the type.
	 * See Type for details
	 *
	 */
	virtual Type& get_value(const Path_Name& name);

#if 0
	/// get value from asterix message
	/**
	 * \param name name of data item to get
	 * \param ret concrete target variable
	 * \remarks use it when you know the target type
	 */
	virtual void get_value(const Path_Name& name,intifada::Type& ret)const;

	/// set value to asterix message
	/**
	 * \param name name of data item to get
	 * \param ret source variable
	 */
	virtual void set_value(const Path_Name& name,const intifada::Type& ret);
#endif
	/// Clone the data field
	/**
	 * Cloning is meaning only structure copy
	 * \return a freshly allocated Extended_Length_Data_Field
	 */
	virtual Data_Field *clone()const;

	/// translate offset vector in a part
	/**
	 * @param o vector position (begining by 0)
	 * @return associated part (begining by 0)
	 */
	uint8_t get_part(uint8_t o) const;

	/// walk throught the record
	/**
	 * Call Data_Field_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a sub item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Functor &it,const Path_Name& path);

	/// walk throught the record
	/**
	 * Call Data_Field_Const_Iterator for each sub item met in the record. They are
	 * called in their declaration order
	 * \param it iterator which will be called each time a su item will be met
	 * \param path path accès to item
	 * \return 0
	 */
	virtual int foreach(Const_Functor &,const Path_Name& path)const;

	/// get type from asterix message
	/**
	 * \param name name of data item to get type
	 * \return a reference on type
	 * See Type for details
	 *
	 */
	virtual const Type& get_type(const Path_Name& name)const;

	private:

	//void update_internal_values();

	void update_stream()const;

	/// Return the lsb position of the part
	/**
	 * Convert part representation in vector position (byte offset in the vector)
	 *
	 * \param p part to convert (0 is the primary part)
	 * \return the begining of p (return the low significant byte of the associated part)
	 */
	Stream::size_type get_lsb_position(Stream::size_type p)const;

	/// Return the msb position of the part
	/**
	 * Convert part representation in vector position (byte offset in the vector)
	 *
	 * \param p part to convert (0 is the primary part)
	 * \return the begining of p (return the most significant byte of the associated part)
	 */
	Stream::size_type get_msb_position(Stream::size_type p)const;

	/// adjust stream size to content
	void update()const;

	/// nullity indicator
	/**
	 * \param p pointed part
	 * \return true if part isn't null
	 *
	 * \remarks first part is indexed by 1
	 */
	bool is_part_null(uint8_t part)const;

	private:
	// byte_stream_ is mutable because when it exported by a const accessor, it must
	// be resized.
	mutable Stream byte_stream_;

	Property_Bool repetitive_;

	Property_Uint8 primary_;
	Property_Uint8 secondaries_;
};
template<typename F>
Extended_Length_Data_Field::Data_Functor_T<F>::Data_Functor_T(
		const Path_Name& n
		,F &it
		,Stream& byte_stream
		,const Property_Uint8& primary
		,const Property_Uint8& secondaries):
		n_(n)
		,it_(it)
		,byte_stream_(byte_stream)
		,primary_(primary)
		,secondaries_(secondaries)
		{
	byte_stream.get_stream_position(stream_pos_,stream_size_);
		}
template <typename F>
int Extended_Length_Data_Field::Data_Functor_T<F>::operator()(
		const Path_Name &name
		,const Data_Field_Characteristics &t
		,const Data_Field_Characteristics_List::part_size_type&part)
{
	//std::cout << "Data_Functor:"<<name << "," << (int)part << std::endl;
	Type *v=t.get_type();
	if(t.is_cached()==false)
	{// v doesn't have stream value. Get it !
		Stream::size_type lsb_position=primary_-1;
		if(part>0)
		{
			lsb_position += secondaries_*part;
		}
		if(lsb_position <byte_stream_.size())
		{
			Stream::const_reverse_iterator rit = byte_stream_.rend() - lsb_position - 1;
			v->set_from_stream(rit,t.shift(),t.size());
		}
		t.set_cached();
	}
	Path_Name full_name=n_+name;
	it_(stream_pos_,stream_size_,full_name,*v);
	return 0;
}
}
#endif // INTIFADA_EXTENDED_LENGTH_DATA_FIELD_HXX
