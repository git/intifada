/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Type.hxx>

#ifndef INTIFADA_USE_INLINE
# include<intifada/Type.ixx>
#endif

#include <intifada/Unit_Converter.hxx>
#include <intifada/Byte_Manipulation.hxx>

intifada::Type::Type(const std::string& id)
:inherited()
,type_name_(id)
,_unit_converter(NULL)
{}

intifada::Type::Type(const Type&rhs)
:inherited(rhs)
,type_name_(rhs.type_name_)
{
	if(rhs._unit_converter!=NULL)
		_unit_converter=rhs._unit_converter->clone();
	else
		_unit_converter=NULL;
}

void intifada::Type::set_unit_converter(const Unit_Converter*converter)
{
	this->release_unit_converter();
	_unit_converter=converter;
}

intifada::Type& intifada::Type::operator=(const Type&rhs)
{
	if(this != &rhs)
	{
		inherited::operator=(rhs);
		// type_name_=rhs.type_name_;
		if(rhs._unit_converter!=NULL)
			_unit_converter=rhs._unit_converter->clone();
		else
			_unit_converter=NULL;
	}
	return *this;
}

intifada::Type::~Type()
{
	this->release_unit_converter();
}

/// Put asterix stream in a stream byte
void
intifada::Type::streamcpy(Stream::reverse_iterator t,Stream::const_reverse_iterator b,int start,unsigned int size)const
{
	// Compute byte number and initial offset
	// lsb begin at stream end, so start / 8 give number of byte
	// to shift to get the concerned part. The modulo give shift
	// position in the concerned byte
	Stream::const_reverse_iterator input_it = b + (start / 8);
	uint8_t input_shift = start % 8;
	uint8_t input_size = size;

	// Acqusition strategy:
	// Reading is drived by output write possibility


	uint8_t next_input_size;

	// output write capability is 8 like we don't write anything in
	uint8_t output_write_capability = 8;
	uint8_t output_shift = 0;

	do
	{ // Reading loop

		next_input_size= Byte_Manipulation::get_access(input_size,input_shift,output_write_capability);
		// next_input_size hold size remain to read
		// input_size hold size to currently read


		uint8_t input_read=Byte_Manipulation::get_byte_part(*input_it,input_size,input_shift);

		Byte_Manipulation::set_byte_part(*t,input_read,input_size,output_shift);

		output_write_capability-=input_size;
		output_shift+=input_size;
		input_shift+=input_size;
		if(next_input_size!=0)
		{
			// we must do another loop
			input_size=next_input_size;

			if(input_shift==8)
			{
				++input_it;
				input_shift=0;
			}
			if(output_write_capability==0)
			{
				output_write_capability=8;
				output_shift=0;
				++t;
			}
		}

	}
	while(next_input_size!=0); // reading loop
}

const intifada::Unit_Converter* intifada::Type::get_unit_converter()const
{
	return _unit_converter;
}

void intifada::Type::release_unit_converter()
{
	if(_unit_converter!=NULL)
	{
		delete _unit_converter;
		_unit_converter=NULL;
	}
}

std::ostream& operator<<(std::ostream& os,const intifada::Type& rhs)
{
	os << rhs.get_to_string();
	return os;
}
