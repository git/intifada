/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Message.hxx>

#include <intifada/Stream.hxx>
#include <intifada/Block.hxx>
#include <intifada/Record_Repository.hxx>
#include <intifada/Record_Iterator_List.hxx>
#include <intifada/Logger.hxx>
#include <cassert>
intifada::Message::Message(const intifada::Record_Repository* rep, const std::string& family)
:record_repository_(rep),
 family_(family),
 blocks_(),
 delete_blocks_(true)
{}

intifada::Message::Message(const intifada::Record_Repository* rep)
:record_repository_(rep),
 family_(),
 blocks_(),
 delete_blocks_(true)
{}

intifada::Message::Message(const std::string& family)
:record_repository_(intifada::Record_Repository::instance()),
 family_(family),
 blocks_(),
 delete_blocks_(true)
{}

intifada::Message::Message()
:record_repository_(intifada::Record_Repository::instance()),
 family_(),
 blocks_(),
 delete_blocks_(true)
{}

void intifada::Message::release_on_clear(bool active)
{
	delete_blocks_=active;
}

intifada::Message::~Message()
{
	this->clear();
}

void
intifada::Message::clear()
{
	if(delete_blocks_==true)
	{
		for(block_list_const_iterator_t it = blocks_.begin(); it != blocks_.end();++it)
		{
			delete (*it);
		}
	}
	blocks_.clear();
}

intifada::Stream::size_type intifada::Message::size()const
{
	Stream::size_type s=0;
	Stream::size_type partial_size=0;
	for(block_list_const_iterator_t it = blocks_.begin(); it != blocks_.end();++it){
		Block*r=(*it);
		partial_size=r->size();
		s+=partial_size;
		clog("size debug") << "\tblock size=" << partial_size << std::endl;
	}
	clog("size debug") << "message size=" << s << std::endl;
	return s;
}

void intifada::Message::set_family(const std::string& f)
{
	family_=f;
}

const std::string& intifada::Message::get_family()const
{
	return family_;
}

intifada::Stream::size_type
intifada::Message::set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)
{
	// o is the offset from the start of the stream s. m is the maximum index
	clog("size debug") << "Message::set_stream(0x"<<s<<","<<o<<","<< m << ")" << std::endl;
	unsigned int stream_pos=o;

	while(stream_pos<m)
	{
		Block *b= new Block(record_repository_,family_);
		int size = b->set_stream(s,stream_pos,m);

		if(size==0)
		{
			delete b;
			// Potentially an error on stream
			throw Parsing_Input_Length_Exception(0,0);
		}
		stream_pos+=size;
		this->push_back(b);
		clog("size debug") << "Message::set_stream:stream:size:"<<size << " pos=" << stream_pos << std::endl;

	}
	clog("size debug") << "Message::set_stream:return:"<<stream_pos << std::endl;
	return stream_pos;
}

intifada::Stream::size_type intifada::Message::get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const
		{
	Stream::size_type size=0;
	Stream::size_type offset=o;
	for(block_list_const_iterator_t it = blocks_.begin(); it != blocks_.end();++it){
		Block*b=(*it);
		size=b->get_stream(s,offset,m);
		assert(size!=0);
		offset+=size;

	}

	return offset;
		}

int
intifada::Message::push_back(Block* r)
{
	blocks_.push_back(r);
	return 0;
}

intifada::Block*
intifada::Message::get_new_block()
{
	return new Block(record_repository_,family_);
}

intifada::Block*
intifada::Message::get_new_block(uint8_t cat)
{
	return new Block(record_repository_,family_,cat);
}

intifada::Message::block_list_size_t
intifada::Message::number()const
{
	return blocks_.size();
}

intifada::Block*
intifada::Message::get(block_list_size_t i)
{
	Block*ret=NULL;
	block_list_size_t idx=0;
	for(block_list_iterator_t it=blocks_.begin();it!=blocks_.end();++it){
		if(i==idx){
			ret=*it;
			break;
		}
		++idx;
	}
	return ret;
}

intifada::Block*
intifada::Message::erase(block_list_size_t i)
{
	Block*ret=NULL;
	block_list_size_t idx=0;
	for(block_list_iterator_t it=blocks_.begin();it!=blocks_.end();++it){
		if(i==idx){
			ret=*it;
			blocks_.erase(it);
			break;
		}
		++idx;
	}
	return ret;
}

int intifada::Message::insert_before(Block* b, block_list_size_t i)
{
	int ret=-1;
	block_list_size_t idx=0;
	for(block_list_iterator_t it=blocks_.begin();it!=blocks_.end();++it)
	{
		if(i==idx)
		{
			block_list_iterator_t iit = blocks_.insert(it,b);
			if(iit!=blocks_.end())
			{
				ret=0;
			}
			break;
		}
		++idx;
	}
	return ret;
}

int intifada::Message::foreach(const Record_Iterator_List& l)
{
	Message::block_list_size_t b=0;
	for(block_list_const_iterator_t it = blocks_.begin(); it != blocks_.end();++it)
	{
		Block*block=(*it);
		uint8_t block_category=block->category();
		if(l.category_presence(block_category)==true)
		{
			block->foreach(l,b);
		}
		++b;
	}
	return 0;
}
