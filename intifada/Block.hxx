/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_BLOCK_HXX
# define INTIFADA_BLOCK_HXX

# include <intifada/Data.hxx>

# include <list>

// bad inclusion, just needed to get Message::block_list_size_t type. This is a cyclic
// potential problem as Block and message are intimate each other
# include <intifada/Message.hxx>

namespace intifada
{
  class Record;
  class Record_Repository;
  class Record_Iterator_List;

  class Block : public virtual Data
  {
  private:
    typedef std::list<Record*> record_list_t;
  public:
    typedef record_list_t::size_type record_list_size_t;
  private:
    typedef record_list_t::iterator record_list_iterator_t;
    typedef record_list_t::const_iterator record_list_const_iterator_t;
    typedef std::pair<record_list_iterator_t,bool> record_list_pair_t;
  public:
    Block(const intifada::Record_Repository* rep, const std::string& family);

    Block(const intifada::Record_Repository* rep, const std::string& family, uint8_t cat);

    /// set the policy release
    /**
     * @param active true on block clear (upon destruction or explicit call)
     * clear also all subsequent records. Default policy
     * @param active fasle on block clear (upon destruction or explicit call)
     * don't clear subsequent records
     */
    void release_on_clear(bool active);

    virtual ~Block();

    /// clear the block content
    void clear();

    /// get block category
    uint8_t category()const;

    /// get field size
    /**
     * \return size of field
     */
    virtual Stream::size_type size()const;

    /// Return a freshly allocated intifada::Record
    /**
     * build record from associated block family and category
     * \return a freshly allocated intifada::Record
     */
    Record* get_new_record()const;

    /// Byte stream acquisition
    /**
     * Handle acquisition of asterix binary stream
     * \param s stream pointer
     * \param o stream offset
     * \param m maximum stream size
     * \return readed total bytes
     * \return 0 if internal stream couldn't be set
     * \throw Parsing_Input_Length_Exception if bytes to read exceed m
     * \throw Description_Field_Unknow_Exception if an unknow field is encountered
     */
    virtual Stream::size_type set_stream(const uint8_t *s, const Stream::size_type& o, const Stream::size_type& m);

    /// Byte stream restitution
        /**
         * Handle byte stream restitution.
         * \param s stream pointer
         * \param o stream offset
         * \param m maximum stream size
         * \return writed total bytes
         * \throw Parsing_Output_Length_Exception if bytes to read exceed m
         */
        virtual Stream::size_type get_stream(uint8_t *s, const Stream::size_type& o, const Stream::size_type& m)const;

    /// insert a record at the end of block
    int push_back(Record* r);

    /// Getting record numbers
    /**
     * \return records number in the message
     */
    record_list_size_t number()const;

    /// Getting a record
    /**
     * \param i record index (0 is the first block)
     * \return concerned record
     * \return NULL if the last record is passed out
     */
    Record* get(record_list_size_t i);

    /// Remove a record
    /**
     * \param i record index  (0 is the first record)
     * \return concerned record
     * \return NULL if the last record is passed out
     * \attention memory isn't released, it's user responsibility to freeing
     * the memory
     */
    Record* erase(record_list_size_t i);

    /// Insert a record
    /**
     * \param b record to insert
     * \param i insertion position. The record will be inserted before the position
     * \return 0 if success
     * \return -1
     */
    int insert_before(Record* b, record_list_size_t i);

    /// Walk the message
    /**
     * \param l record iterator list to apply
     * \param b block id in message
     */
    int foreach(const Record_Iterator_List& l,const Message::block_list_size_t& b);

  private:
    void set_record_reference(const int& stream_pos,const uint8_t& cat);

  private:
    const intifada::Record_Repository* record_repository_;

    std::string family_;
    uint8_t category_;

    const Record* record_reference_;

    record_list_t records_;

    bool delete_records_;

  };
}

#endif // INTIFADA_BLOCK_HXX
