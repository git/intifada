/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Field_Specification_Manipulation.hxx>

uint8_t
Field_Specification_Manipulation::frn_to_offset(uint8_t f)
{
  uint8_t offset=f-1;
  offset %= 7;
  offset = 7 - offset;

  return offset+1;
}

uint8_t
Field_Specification_Manipulation::frn_to_byte(uint8_t f)
{
  return (f-1)/7 + 1;
}

uint8_t
Field_Specification_Manipulation::sf_to_byte(uint8_t f,uint8_t s)
{
  return s - (f-1)/8;
}

uint8_t
Field_Specification_Manipulation::sf_to_offset(uint8_t f)
{
  return (f-1) % 8 + 1;
}

uint8_t
Field_Specification_Manipulation::offset_bit_to_bit(uint8_t byte,uint8_t offset)
{
  //switch offset and byte shift
  // 8-offset+1 + (byte-1)*8;

  return 1-offset + 8*byte;
}

uint8_t
Field_Specification_Manipulation::bit_to_frn(uint8_t bit)
{
  return bit - (bit-1)/8;
}

uint8_t
Field_Specification_Manipulation::bit_to_frn(uint8_t bit,uint8_t size)
{
  return size*8-bit+1;
}
