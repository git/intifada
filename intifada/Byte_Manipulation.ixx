/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

INTIFADA_INLINE
uint8_t intifada::Byte_Manipulation::get_byte_part(uint8_t b, uint8_t size, uint8_t offset)
{
  return (b & Byte_Manipulation::mask(size,offset)) >> offset;
}

INTIFADA_INLINE
void intifada::Byte_Manipulation::set_byte_part(uint8_t &target,uint8_t source, uint8_t size, uint8_t offset)
{
  uint8_t source_mask = Byte_Manipulation::mask(size,0);
  uint8_t target_mask = source_mask << offset;

  target &= ~target_mask;

  target |= (source & source_mask) << offset;
}


INTIFADA_INLINE
uint8_t intifada::Byte_Manipulation::mask(uint8_t size,uint8_t offset)
{
  if ((size<=0)||((size+offset)>8))
    throw std::out_of_range("");

  uint8_t m=mask_[size-1] << offset;

  return m;
}

