/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Path_Name.hxx>
#include <intifada/Logger.hxx>
#include <cassert>
intifada::Path_Name::Path_Name(const char* name, const char& sep) :
  separator_(sep), string_full_name_(), string_partial_path_(), updated_(true),
  path_(),name_size_(2){
    split_path(std::string(name));
}

intifada::Path_Name::Path_Name(const std::string& name, const char& sep) :
  separator_(sep), string_full_name_(), string_partial_path_(), updated_(true),
  path_(),name_size_(2){
    split_path(name);
}
intifada::Path_Name::Path_Name(const char& sep) :
  separator_(sep), string_full_name_(), string_partial_path_(), updated_(true),
  path_(),name_size_(2){
}

intifada::Path_Name::Path_Name(const Path_Name& rhs):
  separator_(rhs.separator_)
  ,string_full_name_(rhs.string_full_name_)
  ,string_partial_path_(rhs.string_partial_path_)
  ,updated_(rhs.updated_)
  ,path_(rhs.path_)
  ,name_size_(rhs.name_size_){

}

intifada::Path_Name& intifada::Path_Name::operator=(const Path_Name& rhs)
  {
    if (&rhs != this){
      separator_=rhs.separator_;
      string_full_name_=rhs.string_full_name_;
      string_partial_path_=rhs.string_partial_path_;
      updated_=rhs.updated_;
      path_=rhs.path_;
      name_size_=rhs.name_size_;

    }
    return *this;
  }

intifada::Path_Name& intifada::Path_Name::operator=(const std::string& rhs)
  {
    split_path(rhs);
    return *this;
  }

intifada::Path_Name& intifada::Path_Name::operator=(const char* rhs)
  {
    split_path(rhs);
    return *this;
  }

const std::string& intifada::Path_Name::get_full_name() const
{
  update();
  return string_full_name_;
}

const std::string& intifada::Path_Name::get_path()const
{
  update();
  return string_partial_path_;
}


const std::string& intifada::Path_Name::get_front() const
{
  return path_.front();
}

const std::string& intifada::Path_Name::get_back() const
{
  return path_.back();
}

void intifada::Path_Name::push_back(const std::string& p)
  {
    updated_ = false;
    path_.push_back(p);
  }

void intifada::Path_Name::pop_front()
  {
    updated_ = false;
    path_.pop_front();
  }

void intifada::Path_Name::pop_back()
  {
    updated_ = false;
    path_.pop_back();
  }

int intifada::Path_Name::get_size() const
{
  return path_.size();
}

bool intifada::Path_Name::empty() const
{
  return path_.empty();
}

void intifada::Path_Name::set_name_size(unsigned int s)
  {
    name_size_=s+1;
  }

bool intifada::Path_Name::operator==(const Path_Name& rhs) const
{
  bool equal = true;
  if (&rhs != this){
    // Update rhs part and compare strings
    rhs.update();
    this->update();
    if (string_full_name_ != rhs.string_full_name_){
      equal = false;
    }
  }
  return equal;
}

bool intifada::Path_Name::operator!=(const Path_Name& rhs) const
{
  return !operator==(rhs);
}

bool intifada::Path_Name::operator==(const std::string& rhs) const
{
  return operator==(Path_Name(rhs, separator_));
}

bool intifada::Path_Name::operator!=(const std::string& rhs) const
{
  return !operator==(rhs);
}

bool intifada::Path_Name::operator<(const Path_Name& rhs) const
{
  bool ret = false;
  if (&rhs != this){
    // Update rhs part and compare strings
    rhs.update();
    this->update();
    ret = string_full_name_ < rhs.string_full_name_;
  }
  return ret;
}

intifada::Path_Name& intifada::Path_Name::operator+=(const Path_Name& rhs)
            {
              this->updated_=false;
              for (path_const_iterator_type it = rhs.path_.begin(); it != rhs.path_.end(); ++it){
                path_.push_back((*it));
              }
              this->name_size_=rhs.name_size_;
              return *this;
            }

void intifada::Path_Name::split_path(const std::string& rhs)
  {
    updated_ = false;
    path_.clear();
    std::string::size_type sep_pos = 0;
    std::string::size_type pos = 0;
    std::string::size_type bpart = 0;
    std::string::size_type epart = 0;
    while (pos != std::string::npos){
      sep_pos = rhs.find_first_of(separator_, pos);
      bpart = pos;
      if (sep_pos != std::string::npos){
        epart = sep_pos;
      }else{
        epart = rhs.size();
      }
      std::string part = rhs.substr(bpart, epart - bpart);
      path_.push_back(part);

      if (sep_pos != std::string::npos){
        pos = sep_pos + 1;
      }else{
        pos = std::string::npos;
      }
    }
  }

void intifada::Path_Name::update() const
{
  if (updated_ == false){
    updated_ = true;

#if 0
    std::cout << "update:";
    for(path_const_iterator_type i=path_.begin();i!=path_.end();++i)
      {
        std::cout << (*i) << " ";
      }
    std::cout << std::endl;
#endif
    string_full_name_.clear();
    string_partial_path_.clear();
    if(path_.empty()!=true){

      path_size_type eit = path_.size();
      path_size_type epit = eit;
      path_const_iterator_type bit=path_.begin();
      if (epit >= name_size_){
        epit-=name_size_;
      }
      path_size_type it = 0;

      while (it < eit){
        if (it != 0){
          string_full_name_ += separator_;
        }
        assert(bit!=path_.end());
        //std::cout << "<" << string_full_name_ << ">:<" << (*bit) << ">" << std::endl;
        string_full_name_ += (*bit);
        if (it == epit){
          string_partial_path_ = string_full_name_;
        }
        ++it;
        ++bit;
      }
    }
  }
  //std::cout << "->"<< this << ":" << string_full_name_ << "<" << std::endl;
}

intifada::Path_Name::operator const std::string&()const
{
  return this->get_full_name();
}


std::ostream& operator<<(std::ostream& lhs, const intifada::Path_Name& rhs)
  {
    lhs << rhs.get_full_name();
    return lhs;
  }

Logger& operator<<(Logger& lhs, const intifada::Path_Name& rhs)
  {
    lhs << rhs.get_full_name();
    return lhs;
  }

#if INTIFADA_HAVE_UNORDERED_MAP==1

//template<>
//std::size_t
//std::hash<intifada::Path_Name>::operator()(intifada::Path_Name pn) const
//{
//  std::string key=pn.get_full_name();
//  //return _Fnv_hash<>::hash(key.data(), key.length());
//  return _Fnv_hash_impl::hash(key.data(), key.length());
//}
#if 0
template<>
std::size_t
std::hash<const intifada::Path_Name&>::operator()(const intifada::Path_Name& pn) const
{
  std::string key=pn.get_full_name();
  //return _Fnv_hash<>::hash(key.data(), key.length());
  return _Fnv_hash_impl::hash(key.data(), key.length());
}
#endif

#endif // INTIFADA_HAVE_UNORDERED_MAP==1
