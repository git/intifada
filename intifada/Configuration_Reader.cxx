/*
   Copyright (C) 2009,2011  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <intifada/Configuration_Reader.hxx>

#include <intifada/Fixed_Length_Data_Field.hxx>
#include <intifada/Extended_Length_Data_Field.hxx>
#include <intifada/Repetitive_Length_Data_Field.hxx>
#include <intifada/Record.hxx>
#include <intifada/Record_Repository.hxx>

#include <intifada/Condition.hxx>

#include <intifada/Dynamic_Loader.hxx>

#include <sstream>
#include <fstream>

#include <intifada/Logger.hxx>
#include <cassert>

intifada::Part_Configuration::Part_Configuration():inherited(){}

int intifada::Part_Configuration::operator()(intifada::Fixed_Length_Data_Field* t){
	int ret=-1;
	current_field_=t->insert(name_,type_,start_,size_);
	if(current_field_!=NULL){
		ret=0;
	}
	return ret;
}

int intifada::Part_Configuration::operator()(intifada::Extended_Length_Data_Field* t){
	int ret=-1;
	current_field_=t->insert(name_,type_,start_,size_,byte_);
	if(current_field_!=NULL){
		ret=0;
	}
	return ret;
}

int intifada::Part_Configuration::operator()(intifada::Repetitive_Length_Data_Field* t){
	int ret=-1;
	current_field_=t->insert(name_,type_,start_,size_);
	if(current_field_!=NULL){
		ret=0;
	}
	return ret;
}

intifada::Current::Current_Iterator::Current_Iterator():
												  current_field_(NULL)
,name_()
,type_()
,start_()
,size_()
,byte_(){

}

intifada::Current::Current_Iterator::~Current_Iterator(){}

void intifada::Current::Current_Iterator::set_property(const std::string& prop,const std::string& val)
{
	if(current_field_!=NULL){
		current_field_->set_property(prop,val);
	}
}

void intifada::Current::Current_Iterator::set_converter(const std::string& converter)
{
	if(current_field_!=NULL){
		current_field_->set_converter(converter);
	}
}

void intifada::Current::Current_Iterator::set(
		const std::string& name,
		const std::string& type,
		int start,
		int size,
		int byte)
{
	name_=name;
	type_=type;
	start_=start;
	size_=size;
	byte_=byte;
}

int
intifada::Current::Current_Iterator::operator()(Fixed_Length_Data_Field*){return 0;}

int
intifada::Current::Current_Iterator::operator()(Extended_Length_Data_Field*){return 0;}

int
intifada::Current::Current_Iterator::operator()(Repetitive_Length_Data_Field*){return 0;}

int
intifada::Current::Current_Iterator::operator()(Record*){return 0;}


intifada::Current::Current():
												  fixed_(NULL),
												  extended_(NULL),
												  repetitive_(NULL),
												  compound_(NULL),
												  handler_(NULL),
												  current_(NONE)
{}

void
intifada::Current::set_fixed(Fixed_Length_Data_Field* fixed)
{
	fixed_=fixed;
	current_=FIXED;
}

void
intifada::Current::set_extended(Extended_Length_Data_Field* extended)
{
	extended_=extended;
	current_=EXTENDED;
}

void
intifada::Current::set_repetitive(Repetitive_Length_Data_Field* repetitive)
{
	repetitive_=repetitive;
	current_=REPETITIVE;
}

void
intifada::Current::set_compound(Record* compound)
{
	compound_=compound;
	current_=COMPOUND;
}

void
intifada::Current::register_handler(Current_Iterator*h)
{
	handler_=h;
}

int intifada::Current::apply(
		const std::string& name,
		const std::string& type,
		int start,
		int size,
		int byte)
{

	int status=-1;
	if(handler_==NULL) return status;
	handler_->set(name,type,start,size,byte);

	if(current_==FIXED){
		status=(*handler_)(fixed_);
	}else if(current_==EXTENDED){
		status=(*handler_)(extended_);
	}else if(current_==REPETITIVE){
		status=(*handler_)(repetitive_);
	}else if(current_==COMPOUND){
		status=(*handler_)(compound_);
	}
	return status;
}

void intifada::Current::set_property(const std::string& n, const std::string& v)
{
	try{
		if(current_==FIXED){
			fixed_->set_property(n,v);
		}else if(current_==EXTENDED){
			extended_->set_property(n,v);
		}else if(current_==REPETITIVE){
			repetitive_->set_property(n,v);
		}else if(current_==COMPOUND){
			compound_->set_property(n,v);
		}
	}catch(std::domain_error&){

	}
}

void intifada::Current::set_type_property(const std::string& n, const std::string& v)
{
	try{
		if(handler_!=0){
			handler_->set_property(n,v);
		}
	}catch(std::domain_error&){

	}
}
void intifada::Current::set_converter(const std::string& converter)
{
	if(handler_!=0){
		handler_->set_converter(converter);
	}
}

intifada::Configuration_Reader::Configuration_Reader()
:current_condition_(NULL),
 current_item_longname_(),
 current_item_(),
 // current_part_(),
 current_forcepresence_("false"),
 current_family_(),
 rep_(NULL),
 current_(),
 handler_(NULL),
 node_stack_()
{
	handler_=new Part_Configuration;
	current_.register_handler(handler_);
}

intifada::Configuration_Reader::~Configuration_Reader()
{
	delete handler_;
}

int intifada::Configuration_Reader::parse()
{
	return this->parse(intifada::Record_Repository::instance());
}

void
intifada::Configuration_Reader::set_repository(Record_Repository*r)
{
	rep_=r;
}

int
intifada::Configuration_Reader::register_types(const std::string& /*name*/)
{
#if 0
	Dynamic_Loader load;
	int ret=load.open(name);

	return ret;
#else
	return -1;
#endif
}

void
intifada::Configuration_Reader::create_record(
		intifada::Record*&r,
		const std::string& name,
		const std::string& id,
		uint8_t category
)
{
	r=new intifada::Record;
	clog("Configuration_Reader") << "create_record (0x << " << std::hex << r << ")" << std::endl;

	r->set_identity(name,id,category);
	rep_->insert(r,name,id,category);
}

intifada::Record* intifada::Configuration_Reader::create_compound(
		intifada::Record*&r,
		uint8_t length
)
{

	clog("Configuration_Reader") << "->create_compound" << std::endl;
	intifada::Record *c=new intifada::Record(length,current_item_);
	// c->setf(intifada::Record::COMPOUND);
	c->set_property("compound","true");
	clog("Configuration_Reader") << "create_compound:new this=" << c << " <" << (uint16_t)length<<"," << current_item_ << ">" << std::endl;
	// c->set_forcepresence(current_forcepresence_);
	c->set_property("force_presence",current_forcepresence_);
	r->insert(current_item_,current_item_longname_,c);
	clog("Configuration_Reader") << "<-create_compound" << std::endl;
	return c;
}

void intifada::Configuration_Reader::begin_configuration()
{
}

void intifada::Configuration_Reader::end_configuration()
{
}


void intifada::Configuration_Reader::end_record()
{
}

intifada::Record* intifada::Configuration_Reader::create_explicit(intifada::Record*&r)
{
	intifada::Record *c=new intifada::Record(current_item_);
	// c->setf(intifada::Record::BASICEXPLICIT);
	c->set_property("basicexplicit","true");
	clog("Configuration_Reader") << "create_explicit:new this=" << c << current_item_ << std::endl;

	r->insert(current_item_,current_item_longname_,c);
	return c;
}

void
intifada::Configuration_Reader::create_uap(
		const std::string& func,
		const std::string& result
)
{
	if(func.empty()!=true){
		clog("Configuration_Reader") << "create_uap:if=" << func << " value=" << result << std::endl;
		current_condition_=new intifada::Condition(func,result);
	}else{
		clog("Configuration_Reader") << "create_uap" << std::endl;
		current_condition_=NULL;
	}
}

void
intifada::Configuration_Reader::create_frn(
		intifada::Record*r,
		uint8_t frn,
		const Path_Name& item
)
{
	Path_Name lastpartitem=Path_Name(item.get_back());
	clog("Configuration_Reader") << "create_frn " << std::dec << int(frn) << " " << lastpartitem << " (for 0x" << std::hex << r << ")"<< std::endl;
	if(current_condition_!=NULL){
		r->insert(frn,lastpartitem,current_condition_);
	}else{
		r->insert(frn,lastpartitem);
	}
}


void
intifada::Configuration_Reader::create_item(
		const std::string& name,
		const Path_Name& reference,
		const std::string& forcepresence
)
{
	current_item_longname_=name;
	current_item_=reference;
	// current_part_.push_back(reference);
	current_forcepresence_="false";
	if(forcepresence=="true"){
		current_forcepresence_=forcepresence;
	}
}

void intifada::Configuration_Reader::create_fixed(intifada::Record*r)
{
	Fixed_Length_Data_Field *f=new Fixed_Length_Data_Field;
	f->set_property("force_presence",current_forcepresence_);
	current_.set_fixed(f);
	r->insert(current_item_,current_item_longname_,f);
}

void intifada::Configuration_Reader::create_extended(intifada::Record*r)
{
	Extended_Length_Data_Field *f=new Extended_Length_Data_Field;

	f->set_property("force_presence",current_forcepresence_);

	current_.set_extended(f);
	r->insert(current_item_,current_item_longname_,f);
}

void
intifada::Configuration_Reader::create_repetitive(intifada::Record*r)
{
	clog("Configuration_Reader") << "create_repetitive " << current_item_ << std::endl;
	Repetitive_Length_Data_Field *item=new Repetitive_Length_Data_Field;
	// if(sf=="length"){
	//item->setf(Repetitive_Length_Data_Field::INDICATORLENGTH);
	//item->set_property("indicatorlength","true");
	// }
	// item->set_forcepresence(current_forcepresence_);
	item->set_property("force_presence",current_forcepresence_);
	current_.set_repetitive(item);
	r->insert(current_item_,current_item_longname_,item);
}

void
intifada::Configuration_Reader::create_family(const std::string& name)
{
	current_family_=name;
}

int
intifada::Configuration_Reader::create_family_id(const std::string& name)
{
	int r = rep_->insert(current_family_,name);
	return r;
}

int intifada::Configuration_Reader::handle_parts(
		const std::string& name,
		const std::string& type,
		int byte,
		int start,
		int size
)
{
	// current_part_.push_back(name);
	return current_.apply(name,type,start,size,byte);
	// current_part_.pop_back();
}

void intifada::Configuration_Reader::set_type_property(const std::string& prop,const std::string& val)
                												{
	current_.set_type_property(prop,val);
                												}

void intifada::Configuration_Reader::set_item_property(
		const std::string& name,
		const std::string& value)
		{
	current_.set_property(name,value);
		}
void intifada::Configuration_Reader::set_item_converter(const std::string& converter)
{
	current_.set_converter(converter);
}
void intifada::Configuration_Reader::push_element(const std::string& e)
{
	node_stack_.push(e);
}
void intifada::Configuration_Reader::pop_element()
{
	node_stack_.pop();
}
bool intifada::Configuration_Reader::empty_element()const
{
	return node_stack_.empty();
}
const std::string& intifada::Configuration_Reader::top_element()const
{
	return node_stack_.top();
}
