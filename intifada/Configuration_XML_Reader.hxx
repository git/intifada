/*
   Copyright (C) 2009  Stephane Pion
   This file is part of Intifada.

    Intifada is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intifada is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Intifada.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTIFADA_CONFIGURATION_XML_READER_HXX
# define INTIFADA_CONFIGURATION_XML_READER_HXX

# include <intifada/Configuration_Reader.hxx>
# include <intifada/Path_Configuration.hxx>
# include <libxml/xmlreader.h>

namespace intifada
  {

    template<typename T>
    T to_integer(const std::string& n);

    class Configuration_XML_Reader:public Configuration_Reader
      {
    public:

      Configuration_XML_Reader(
          const std::string& xmlfile=Path_Configuration::instance()->get_factory_description()
          ,const std::string& dtdfile=Path_Configuration::instance()->get_factory_validation());
      virtual ~Configuration_XML_Reader();

      /// Configuration parser initialization
      /**
       * \return 0 on success
       *
       * handle parser initialization
       */
      virtual int open();

      /// parse the configuration file
      /**
       * \param rep asterix repository structure
       * \return 0 on success
       */
      virtual int parse(Record_Repository*rep);

      /// free ressources
      virtual void release();

    private:
      int process_node(intifada::Record*&r,xmlNodePtr a_node);

      std::string get_attribute(xmlNodePtr node,const std::string& an);

      std::string get_text(xmlNodePtr node);

      void next_node();

    private:
      std::string xml_file_;
      std::string dtd_file_;
      Path_Name reference_;
      xmlDocPtr parser_;
      //xmlNodePtr current_node_;
      };
  }

template<typename T>
T
intifada::to_integer(const std::string& n)
  {
    T ret=0;
    if(n.size()!=0){
      std::istringstream istr(n);
      if(sizeof(T) != 1){
        istr >> ret;
      }else{
        int t;
        istr >> t;
        ret=t;
      }
    }
    return ret;

  }

#endif  // INTIFADA_CONFIGURATION_READER_HXX
